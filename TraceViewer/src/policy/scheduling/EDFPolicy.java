package policy.scheduling;

import java.util.Comparator;


import task.Job;
import task.Job.JobState;
import edu.unibo.ciri.realtime.scheduling.policy.SchedulingPolicy;
import event.EventType;

public class EDFPolicy implements SchedulingPolicy {
	
		
	Comparator<Job> jobComparator = new Comparator<Job>(){

		@Override
		public int compare(Job job1, Job job2) {
			
			long absoluteDeadline1 = job1.getEvent(EventType.JOB_DEADLINE).getEventTime().getTimeValue();
			long absoluteDeadline2 = job2.getEvent(EventType.JOB_DEADLINE).getEventTime().getTimeValue();
					
			if(absoluteDeadline1 > absoluteDeadline2)
				return -1;
			
			if(absoluteDeadline1 < absoluteDeadline2)
				return 1;
			
			
			/* If the two jobs have the same deadline prefer the one that was previusly running.
			 * If both are running prefer job1 */
			if(job1.getState() == JobState.RUNNING)
				return 1;
			if(job2.getState() == JobState.RUNNING)
			 	return -1;
			return 0;
			
		}
		
	};
	

	@Override
	public String getName() {
		
		return "Earliest Deadline Firt";
	}

	

	@Override
	public Comparator<Job> getJobComparator() {
		
		return jobComparator;
	}

}
