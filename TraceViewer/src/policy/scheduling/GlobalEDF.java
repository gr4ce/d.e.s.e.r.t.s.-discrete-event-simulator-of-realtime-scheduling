package policy.scheduling;

public class GlobalEDF implements SchedulingAlgorithm {

	
	@Override
	public String getName() {
		return "EDF";
	}

	@Override
	public MigrationMode getMigrationMode() {
		return MigrationMode.FULL_MIGRATION;
	}

	@Override
	public PriorityAssignmentMode getPriorityAssignmentMode() {
		return PriorityAssignmentMode.FIXED_JOB;
	}

}
