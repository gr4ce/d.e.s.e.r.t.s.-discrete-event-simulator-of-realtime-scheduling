package policy.scheduling;

import task.Job;

public interface PriorityComparator {
	
	public int compare(Job j1, Job j2);

}
