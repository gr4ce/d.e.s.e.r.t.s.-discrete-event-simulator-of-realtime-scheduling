package policy.scheduling.preemptionthreshold;
import java.util.ArrayList;

import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.policies.scheduling.PreemptionThresholdAssignment;

//import org.junit.experimental.theories.internal.Assignments;



public class ThresholdAssignmentGenerator {
	
	private TaskSet ts;
	private int currentAssignmentIndex = 1;
	private PreemptionThresholdAssignment motherAssignment;
	private ArrayList<PreemptionThresholdAssignment> assigments = new ArrayList<PreemptionThresholdAssignment>();
	
	public ThresholdAssignmentGenerator(TaskSet t){
		ts = t;
		generateAssignments();
	}
	
	public PreemptionThresholdAssignment getNextPTAssignment(){
		if(currentAssignmentIndex < assigments.size() - 1){			
			return assigments.get(currentAssignmentIndex++);
		}
		return null;		
	}
	
	public int size(){
		return assigments.size();
	}
	
	public PreemptionThresholdAssignment getPreemptiveAssignment(){
		return assigments.get(assigments.size() - 1);
	}
	
	public PreemptionThresholdAssignment getNonPreemptiveAssignment(){
		return assigments.get(0);
	}
	
	public PreemptionThresholdAssignment getAssignment(int index){
		if(index > assigments.size() - 1)
			return null;
		return assigments.get(index);		
	}
	
	private void generateAssignments(){
				
		int maxPrio = ts.getMaxPriority();
		motherAssignment = new PreemptionThresholdAssignment(ts.size());
		for(int i = 0; i < ts.size(); i++){
			
			motherAssignment.addTaskThresholdPair(ts.getTask(i), maxPrio);
		}
//		System.out.println("Generating Mother = " + motherAssignment.toString());
		
		recursiveAssignThreshold(0, maxPrio, maxPrio);
	}
	
	private void recursiveAssignThreshold(int taskIndex, int threshold, int maxthreshold){
		Task current = ts.getTask(taskIndex);
		motherAssignment.setTaskThresholdPair(current, threshold);
		
		if((taskIndex + 1) < ts.size()){
//			System.out.println("Nominal priority = "+ts.getTask(taskIndex + 1).getTaskParameters().getStaticPriority());
			for(int k = maxthreshold; k >= ts.getTask(taskIndex + 1).getTaskParameters().getStaticPriority(); k--){
				recursiveAssignThreshold(taskIndex + 1, k, maxthreshold);				
			}
		}
		else{
			PreemptionThresholdAssignment newOne = motherAssignment.clone();
//			System.out.println("Generating new assignment = " + newOne.toString());
			assigments.add(newOne);
		}
		
	}
	
	

}
