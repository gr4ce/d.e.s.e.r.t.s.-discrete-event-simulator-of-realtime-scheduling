package policy.scheduling.preemptionthreshold;


import edu.unibo.ciri.realtime.model.Job;
import edu.unibo.ciri.realtime.model.TaskParameters;
import edu.unibo.ciri.realtime.model.Job.JobState;
import edu.unibo.ciri.realtime.policies.scheduling.base.PriorityComparator;

public class PreemptionThresholdPriority implements PriorityComparator{

	private static PreemptionThresholdPriority rule = new PreemptionThresholdPriority();

	private PreemptionThresholdPriority(){}
	
	public static PreemptionThresholdPriority getInstance(){
		return rule;
	}
	
	@Override
	public int compare(Job firstJob, Job secondJob) {		
		
		int firstJobPriority,secondJobPriority;
		TaskParameters firstTaskParam  = firstJob.getTask().getTaskParameters();
		TaskParameters secondTaskParam = secondJob.getTask().getTaskParameters();
		
		if(firstJob.getState() == JobState.RUNNING || firstJob.getState() == JobState.PREEMPTED)
			firstJobPriority = firstTaskParam.getThreshold();
		else
			firstJobPriority = firstTaskParam.getStaticPriority();
		
		if(secondJob.getState() == JobState.RUNNING || secondJob.getState() == JobState.PREEMPTED)
			secondJobPriority = secondTaskParam.getThreshold();
		else
			secondJobPriority = secondTaskParam.getStaticPriority();
		
		if(firstJobPriority != secondJobPriority)
			return firstJobPriority - secondJobPriority;
		
		if(firstJob.getState() == JobState.RUNNING || firstJob.getState() == JobState.PREEMPTED)
			return 1;
		
		if(secondJob.getState() == JobState.RUNNING || secondJob.getState() == JobState.PREEMPTED)
			return -1;
		
		return 0;
			
	}
}