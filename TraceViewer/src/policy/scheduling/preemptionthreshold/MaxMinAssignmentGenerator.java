package policy.scheduling.preemptionthreshold;

import java.util.ArrayList;

import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.policies.scheduling.PreemptionThresholdAssignment;

//import org.junit.experimental.theories.internal.Assignments;

import rta.PreemptionThresholdModelRTA;

public class MaxMinAssignmentGenerator {
	
	private TaskSet ts;
	private TaskSet subset = new TaskSet();
	private PreemptionThresholdModelRTA prova;
	private int currentAssignmentIndex = 1;
	private PreemptionThresholdAssignment motherAssignment;
	private PreemptionThresholdAssignment subsetAssignment;
	private ArrayList<PreemptionThresholdAssignment> assigments = new ArrayList<PreemptionThresholdAssignment>();
	private boolean failure=false;
	
	public MaxMinAssignmentGenerator(TaskSet t){
		ts = t;
		generateMaxAssignment();
		if (failure==false) {
			generateMinAssignment();
		}//end if
	}//end method MaxMinAssignmentGenerator
	
	public PreemptionThresholdAssignment getNextPTAssignment(){
		if(currentAssignmentIndex < assigments.size() - 1){			
			return assigments.get(currentAssignmentIndex++);
		}//end if
		return null;		
	}//end method PreemptionThresholdAssignment getNextPTAssignment
	
	public boolean ExistValidAssignment() {
		if (failure == true) 
			return false;
		else
			return true;
	}//end method ExistValidAssignment
	
	public PreemptionThresholdAssignment getMaxAssignment(){
		return assigments.get(0);
	}//end method PreemptionThresholdAssignment getMaxAssignment
	
	
	public PreemptionThresholdAssignment getMinAssignment(){
		return assigments.get(1);
	}//end method PreemptionThresholdAssignment getMinAssignment	
	
	
	private void generateMaxAssignment(){				
		int maxPrio = ts.getMaxPriority();
		int i;
		//motherAssignment = new PreemptionThresholdAssignment();
		subsetAssignment = new PreemptionThresholdAssignment(ts.size());
		prova = new PreemptionThresholdModelRTA(subset);
		
		for (i = 0; i < ts.size(); i++) {
			subset.addTask(ts.getTask(i));			
			//motherAssignment.addTaskThresholdPair(ts.getTask(i), maxPrio);
			subsetAssignment.setTaskThresholdPair(subset.getTask(i), maxPrio);
			subsetAssignment.assignThreshold(subset);
			prova.setTaskset(subset);	
			
			for (int j = 0; j < i; j++) {
				if (prova.compute_wcrt(j) > ts.getTask(j).getTaskParameters().getDeadline().getTimeValue()) {
					//motherAssignment.setTaskThresholdPair(ts.getTask(i), ts.getTask(j).getTaskParameters().getStaticPriority() - 1);
					subsetAssignment.setTaskThresholdPair(subset.getTask(i), subset.getTask(j).getTaskParameters().getStaticPriority() - 1);
				}//end if
			}//end secondary for
			if (i>0) {
				if (prova.compute_wcrt(i) > ts.getTask(i).getTaskParameters().getDeadline().getTimeValue()) {
					failure=true;
					break;
				}//end secondary if
			}//end primary if
		}//end primary for
		if (failure!=true) {		
			PreemptionThresholdAssignment newOne = subsetAssignment.clone();
			assigments.add(newOne);
		}//end if		
	}//end method generateMaxAssignment
	
	
	private void generateMinAssignment(){
		int i;
		motherAssignment = new PreemptionThresholdAssignment(ts.size());		
		motherAssignment = assigments.get(0).clone();
		motherAssignment.assignThreshold(ts);
		prova.setTaskset(ts);
		
		for (i = ts.size() - 1; i >= 0; i--) {
			while (motherAssignment.getThreshold(ts.getTask(i)) > ts.getTask(i).getTaskParameters().getStaticPriority()) {
				motherAssignment.setTaskThresholdPair(ts.getTask(i), motherAssignment.getThreshold(ts.getTask(i)) - 1);
				motherAssignment.assignThreshold(ts);
				if (prova.compute_wcrt(i) > ts.getTask(i).getTaskParameters().getDeadline().getTimeValue()) {
					motherAssignment.setTaskThresholdPair(ts.getTask(i), motherAssignment.getThreshold(ts.getTask(i)) + 1);
					motherAssignment.assignThreshold(ts);
					break;
				}//end if
			}//end while
		}//end for
		PreemptionThresholdAssignment newOne = motherAssignment.clone();
		assigments.add(newOne);
	}//end method generateMinAssignment
	
}//end class
