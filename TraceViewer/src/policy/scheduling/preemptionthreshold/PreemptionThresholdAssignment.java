package policy.scheduling.preemptionthreshold;

import java.util.Enumeration;
import java.util.Hashtable;

import task.Task;
import task.TaskSet;

public class PreemptionThresholdAssignment {
	
	private Hashtable<Task,Integer> assignment = new Hashtable<Task,Integer>();
	private boolean isFeasible = false;
	
	public PreemptionThresholdAssignment(){}
	
	public void addTaskThresholdPair(Task task, int threshold){
		assignment.put(task, new Integer(threshold));
	}
	
	public void setTaskThresholdPair(Task task, int threshold){
		assignment.remove(task);
		assignment.put(task, new Integer(threshold));
	}
	
	public void setFeasible(){
		isFeasible = true;
	}
	
	public boolean isFeasible(){
		return isFeasible;
	}
	
	public int getThresholdByTaskName(String name){
		return assignment.get(name).intValue();
	}
	
	public int getThreshold(Task task){
		return assignment.get(task).intValue();
	}
	
	public String toString(){
		String s = "{";
		Enumeration<Task> keys = assignment.keys();
		while(keys.hasMoreElements()){
			Task task = keys.nextElement();
			s += "(" + task.getName() + ",";
			s += assignment.get(task) + "),";
		}
		
		s = s.substring(0, s.length()-1);
		s += "}";
		
		return s;
	}
	
	public void assignThreshold(TaskSet ts){
		Enumeration<Task> keys = assignment.keys();
		while(keys.hasMoreElements()){
			Task task = keys.nextElement();
			ts.getTask(task.getName()).getTaskParameters().setThreshold(assignment.get(task));			
		}
	}
	
	public PreemptionThresholdAssignment clone(){
		PreemptionThresholdAssignment copy = new PreemptionThresholdAssignment();
		Enumeration<Task> keys = assignment.keys();
		while(keys.hasMoreElements()){
			Task t = keys.nextElement();
			copy.addTaskThresholdPair( t, assignment.get(t));
		}
		return copy;
	}

}
	
	