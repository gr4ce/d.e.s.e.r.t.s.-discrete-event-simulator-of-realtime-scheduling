package policy.scheduling;

//import edu.unibo.ciri.desert.config.Configuration;
import edu.unibo.ciri.collection.PriorityLinkedList;
import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskParameters;
import edu.unibo.ciri.realtime.model.TaskSet;

public class RateMonotonicPrioirtyOrdering {
	
	public static class RMTask implements Comparable<RMTask>{

		private Task task;
		
		public RMTask(Task t) {
			task = t;
		}
		
		public TaskParameters getTaskParameters(){
			return task.getTaskParameters();
		}
		
		public Task getTask(){
			return task;
		}
		
		public String toString(){
			return this.task.getName();
		}
				
		public int compareTo(RMTask anotherTask){
			long thisPeriod = task.getTaskParameters().getPeriod().getTimeValue();
			long anotherPeriod = anotherTask.getTaskParameters().getPeriod().getTimeValue();
			
			if(thisPeriod < anotherPeriod)
				return 1;
			if(thisPeriod > anotherPeriod)
				return -1;
			else return 0;
		}
	}
	
	public static void assignPriority(TaskSet taskset){
		
		PriorityLinkedList<RMTask> orderedTaskset = new PriorityLinkedList<RMTask>();
		
		for(Task t: taskset.getArrayList()){
			orderedTaskset.orderedInsert(new RMTask(t));
		}
		
		RMTask t;
		int priority = orderedTaskset.size();
//		System.out.println(orderedTaskset);
		
		
		while((t = orderedTaskset.popHead()) != null){
			t.getTaskParameters().setStaticPriority(priority);
			t.getTaskParameters().setThreshold(priority--);
//			System.out.println(t.getTask().getName() + " " + t.getTaskParameters().getStaticPriority());
			
		}
		
		
		
	}
	
	public static Task[] getOrderedArray(TaskSet taskset){
		
		Task[] orderedTasks = new Task[taskset.size()];
		
		PriorityLinkedList<RMTask> orderedTaskset = new PriorityLinkedList<RMTask>();
		
		for(Task t: taskset.getArrayList()){
			orderedTaskset.orderedInsert(new RMTask(t));
		}
		
		RMTask t;
		
		
		int i = 0;
		while((t = orderedTaskset.popHead()) != null){
			//t.getTaskParameters().setStaticPriority(priority);
			//t.getTaskParameters().setThreshold(priority--);
			orderedTasks[i++] = t.getTask();
//			System.out.println(orderedTasks[i-1].getName() + " prio = "+ orderedTasks[i-1].getTaskParameters().getStaticPriority());
		}	
		
		return orderedTasks;
	}

}
