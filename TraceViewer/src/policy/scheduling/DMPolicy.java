package policy.scheduling;

import java.util.Comparator;

import edu.unibo.ciri.realtime.scheduling.policy.SchedulingPolicy;
import edu.unibo.ciri.realtime.scheduling.policy.StaticSortingPolicy;


import task.Job;
import task.Task;
import task.TaskSet;
import util.SortingAlgorithm;
import util.SortingAlgorithm.SortingOrder;


public class DMPolicy extends StaticSortingAlgorithm implements SchedulingPolicy {
	
	
	
	Comparator<Job> jobComparator = new Comparator<Job>(){

		@Override
		public int compare(Job job1, Job job2) {
			
			long deadLine1 = job1.getTask().getTaskParameters().getDeadline().getTimeValue();
			long deadLine2 = job2.getTask().getTaskParameters().getDeadline().getTimeValue() ;		

			
			/* The shorter the relative deadline the higher the priority of the job*/
			
			if(deadLine1 < deadLine2)
				return 1;
			if(deadLine1 > deadLine2)
				return -1;
			return 0;
		}
		
	};
	
	private class DMTaskComparator implements Comparator<Task>{
		
		
		public int compare(Task t1, Task t2) {
			
			long deadLine1 = t1.getTaskParameters().getDeadline().getTimeValue();
			long deadLine2 = t2.getTaskParameters().getDeadline().getTimeValue();		
					
			if(deadLine1 < deadLine2)
				return 1;
			if(deadLine1 > deadLine2)
				return -1;
			return 0;
			
		}
		
	}
	
		
	public DMPolicy(SortingAlgorithm<Task> sa) {
		
		super( new TaskSorter() {
			
			private SortingAlgorithm<Task> sortingAlgorithm;
			
			@Override
			public void setSortingAlgorithm(
					SortingAlgorithm<Task> sortingAlgorithm) {
				
				this.sortingAlgorithm = sortingAlgorithm;  
			}

			@Override
			public SortingAlgorithm<Task> getSortingAlgorithm() {
				
				return this.sortingAlgorithm;
			}
			
			@Override
			public void sort(Task[] array) {
				
				sortingAlgorithm.sort(array, SortingOrder.NON_INCREASING_ORDER);		
			}

			@Override
			public Task[] sort(TaskSet taskset) {
				
				Task[] orderedArray = new Task[taskset.size()];
				taskset.getArrayList().toArray(orderedArray);
				sortingAlgorithm.sort(orderedArray, SortingOrder.NON_INCREASING_ORDER);
				
				return orderedArray;
			}		

		});// End of TaskSorter definition and instantiation
		
		sa.setComparator( new DMTaskComparator() );		
		getStaticTaskSorter().setSortingAlgorithm(sa);
		
	}

	
	


	@Override
	public String getName() {
		
		return "Deadline Monotonic";
	}


	

	@Override
	public Comparator<Job> getJobComparator() {
		
		return jobComparator;
	}

}
