package policy.scheduling;

import java.util.Comparator;

import edu.unibo.ciri.realtime.scheduling.policy.SchedulingPolicy;
import edu.unibo.ciri.realtime.scheduling.policy.StaticSortingPolicy;


import task.Job;
import task.Task;
import task.TaskSet;
import task.Job.JobState;
import util.SortingAlgorithm;
import util.SortingAlgorithm.SortingOrder;

public class TkCPolicy extends StaticSortingAlgorithm implements SchedulingPolicy {
	
	
		
	Comparator<Job> jobComparator = new Comparator<Job>(){

		@Override
		public int compare(Job job1, Job job2) {
			
			long tkC1 = job1.getTask().getTaskParameters().getPeriod().getTimeValue() - job1.getTask().getTaskParameters().getComputation().getTimeValue();
			long tkC2 = job2.getTask().getTaskParameters().getPeriod().getTimeValue() - job2.getTask().getTaskParameters().getComputation().getTimeValue();		

			long result = -(tkC1 - tkC2);
			
			if(result > 0)
				return 1;
			if(result < 0)
				return -1;
			
			if(job1.getState() == JobState.RUNNING)
				return 1;
			if(job2.getState() == JobState.RUNNING)
			 	return -1;
			return 0;
			
		}
		
	};
	
	private class TkCTaskComparator implements Comparator<Task>{

		private int k;
		
		public TkCTaskComparator(int kConstant){
			setkConstant(kConstant);
		}
		
		public void setkConstant(int kConstant){
			this.k = kConstant;
		}
		
		
		public int compare(Task t1, Task t2) {
			
			long tkC1 = t1.getTaskParameters().getPeriod().getTimeValue() - k * t1.getTaskParameters().getComputation().getTimeValue();
			long tkC2 = t2.getTaskParameters().getPeriod().getTimeValue() - k * t2.getTaskParameters().getComputation().getTimeValue();		

			long result = -(tkC1 - tkC2);
			
			if(result > 0)
				return 1;
			else if(result < 0)
				return -1;
			return 0;
			
		}
		
	}
	
		
	public TkCPolicy(SortingAlgorithm<Task> sa, int kConstant) {
		
		super(new TaskSorter() {
			
			private SortingAlgorithm<Task> sortingAlgorithm;
			
			@Override
			public void setSortingAlgorithm(
					SortingAlgorithm<Task> sortingAlgorithm) {
				
				this.sortingAlgorithm = sortingAlgorithm;  
			}

			@Override
			public SortingAlgorithm<Task> getSortingAlgorithm() {
				
				return this.sortingAlgorithm;
			}
			
			@Override
			public void sort(Task[] array) {
				
				sortingAlgorithm.sort(array, SortingOrder.NON_INCREASING_ORDER);		
			}

			@Override
			public Task[] sort(TaskSet taskset) {
				
				Task[] orderedArray = new Task[taskset.size()];
				taskset.getArrayList().toArray(orderedArray);
				sortingAlgorithm.sort(orderedArray, SortingOrder.NON_INCREASING_ORDER);
				
				return orderedArray;
			}		

		});// End of TaskSorter definition and instantiation
		
		sa.setComparator( new TkCTaskComparator(kConstant) );
		getStaticTaskSorter().setSortingAlgorithm(sa);
		
	}


	

	@Override
	public String getName() {
		
		return "TkC";
	}

	
	
	@Override
	public Comparator<Job> getJobComparator() {
		
		return jobComparator;
	}


	

	

}
