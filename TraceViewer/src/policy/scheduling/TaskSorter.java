package policy.scheduling;



import task.Task;
import task.TaskSet;

import util.SortingAlgorithm;

public interface TaskSorter {
	
	void setSortingAlgorithm(SortingAlgorithm<Task> sortingAlgorthm);
	
	SortingAlgorithm<Task> getSortingAlgorithm();
		
	void sort(Task[] array);
		
	Task[] sort(TaskSet taskset);	
	
}
