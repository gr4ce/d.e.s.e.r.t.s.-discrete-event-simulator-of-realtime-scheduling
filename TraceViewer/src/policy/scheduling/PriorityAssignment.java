package policy.scheduling;

import task.Job;
import task.Task;

public class PriorityAssignment {
	
	public Task[] tasks;
	public int[] priorities;
	
	public PriorityAssignment(int number_of_task){
		tasks = new Task[number_of_task];
		priorities = new int[number_of_task];
	}
	
	public void setTaskPriorityPair(Task task, int priority){
		tasks[priority] = task;
		priorities[task.getTaskId()] = priority;
	}
	
	public int getPriority(int taskId){
		return priorities[taskId];
	}
	
	public int getPriority(Task task){
		return getPriority(task.getTaskId());
	}
	
	public int getTaskIdAtPriority(int priority){
		return tasks[priority].getTaskId();
	}
	
	public Task getTaskAtPriority(int priority){
		return tasks[priority];
	}
	
	public void clear(){
		for(int i = 0; i < tasks.length; i++){
			priorities[i] = 0;
			tasks[i] = null;
		}
	}

}
