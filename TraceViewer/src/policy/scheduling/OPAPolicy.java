package policy.scheduling;

import java.util.ArrayList;
import java.util.Iterator;

import edu.unibo.ciri.desert.analysis.schedulability.test.SchedulabilityTest;
import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.policies.scheduling.FixedPriorityAssignment;
import edu.unibo.ciri.realtime.policies.staticsorting.RMSortingPolicy;
import edu.unibo.ciri.realtime.policies.staticsorting.TaskSorter;
import edu.unibo.ciri.util.sort.algorithm.QuickSort;

public class OPAPolicy implements Iterator<FixedPriorityAssignment>{
	
	private SchedulabilityTest test;
	private TaskSet t;
	
	//state of iteration
	private boolean hasNext;
	private FixedPriorityAssignment currentAssignment;
	private int currentPriority = -1;
	
	private class FringeElement{
		public int priority;
		public Task task;
	};
	
	private ArrayList<FringeElement> fringe = new ArrayList<OPAPolicy.FringeElement>();
	private ArrayList<Task> unassigned = new ArrayList<Task>();
	private ArrayList<Task> assigned = new ArrayList<Task>();
	
	private void printFringe(){
		System.out.print("{");
		for(int i = 0; i < fringe.size(); i++){
			FringeElement e = fringe.get(i);
			System.out.print("("+e.task.getName()+","+e.priority+")");
		}
		System.out.println("}");
	}
	
	private void printUnassigned(){
		System.out.print("{");
		for(int i = 0; i < unassigned.size(); i++){
			Task t = unassigned.get(i);
			System.out.print("("+t.getName()+")");
		}
		System.out.println("}");
	}
	
	private void printAssigned(){
		System.out.print("{");
		for(int i = 0; i < assigned.size(); i++){
			Task t = assigned.get(i);
			System.out.print("("+t.getName()+","+currentAssignment.getPriority(t)+")");
		}
		System.out.println("}");
	}
	
	public OPAPolicy(SchedulabilityTest st){
		test = st;
	}
	
	public void setTaskset(TaskSet t){
		currentAssignment = new FixedPriorityAssignment(t.size());
		int maxpriority = t.size();
		currentPriority = maxpriority - 1;
		for(int i = 0; i < t.size(); i++){
			FringeElement f = new FringeElement();
			f.priority = currentPriority;
			f.task = t.getTask(i);			
			fringe.add(f);
			unassigned.add(f.task);
		}
	}
	
	public FixedPriorityAssignment getPriorityAssignment(TaskSet ts){
		
		int n = ts.size();
		FixedPriorityAssignment optimalPriorityAssignment = new FixedPriorityAssignment(n);
		RMSortingPolicy rm = new RMSortingPolicy(new QuickSort<Task>());
		TaskSorter sorter = rm.getStaticTaskSorter();
		Task[] orderedTasks = sorter.sort(ts);
		
		
		return optimalPriorityAssignment;		
	}
	
	private boolean recursiveAssign(int priorityLevel, ArrayList<FringeElement> f, ArrayList<Task> u){
		
		System.out.println("recursion step prio="+priorityLevel);
		System.out.print("fringe="); printFringe();
		System.out.print("unassigned=");printUnassigned();
		System.out.print("assigned=");printAssigned();
		
		FringeElement head = fringe.remove(0);
		u.remove(head.task);
		assigned.add(head.task);
		currentAssignment.setTaskPriorityPair(head.task, head.priority);
		for(int k = u.size() - 1; k >= 0; k--){
			FringeElement fe = new FringeElement();
			fe.priority = priorityLevel - 1;
			fe.task = u.get(k);
			fringe.add(0, fe);
		}
		
		if(priorityLevel != 0)			
			return recursiveAssign(priorityLevel - 1, f, u);
		
		u.add(0,assigned.remove(assigned.size() - 1));
		u.add(0,assigned.remove(assigned.size() - 1));
		currentPriority = priorityLevel + 1;
				
		return true;
		
	}

	@Override
	public boolean hasNext() {
		currentAssignment.clear();
		return recursiveAssign(currentPriority, fringe, unassigned);
	}

	@Override
	public FixedPriorityAssignment next() {		
		return currentAssignment;
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	}

	

}
