package policy.scheduling;

import java.util.Comparator;

import task.Job;
import task.TaskParameters;
import task.Job.JobState;
import edu.unibo.ciri.realtime.scheduling.policy.SchedulingPolicy;

public class PreemptionThresholdPolicy implements SchedulingPolicy {

	private SchedulingPolicy schedulingPolicy;
	
	private Comparator<Job> jobComparator = new Comparator<Job>(){

		@Override
		public int compare(Job firstJob, Job secondJob) {
			
			int firstJobPriority,secondJobPriority;
			TaskParameters firstTaskParam  = firstJob.getTask().getTaskParameters();
			TaskParameters secondTaskParam = secondJob.getTask().getTaskParameters();
			
			if(firstJob.getState() == JobState.RUNNING || firstJob.getState() == JobState.PREEMPTED)
				firstJobPriority = firstTaskParam.getThreshold();
			else
				firstJobPriority = firstTaskParam.getStaticPriority();
			
			if(secondJob.getState() == JobState.RUNNING || secondJob.getState() == JobState.PREEMPTED)
				secondJobPriority = secondTaskParam.getThreshold();
			else
				secondJobPriority = secondTaskParam.getStaticPriority();
			
			// The less the numeric value, the higher the priority
			if(firstJobPriority != secondJobPriority)
				return -(firstJobPriority - secondJobPriority);
			
			if(firstJob.getState() == JobState.RUNNING || firstJob.getState() == JobState.PREEMPTED)
				return 1;
			
			if(secondJob.getState() == JobState.RUNNING || secondJob.getState() == JobState.PREEMPTED)
				return -1;
			
			return 0;
		}
		
	};
	
	public PreemptionThresholdPolicy(SchedulingPolicy sa){
		this.schedulingPolicy = sa;
	}
	
	@Override
	public String getName() {
		
		return "Preemption Threshold with " + schedulingPolicy.getName();
	}

	@Override
	public Comparator<Job> getJobComparator() {
		
		return jobComparator;
	}

}
