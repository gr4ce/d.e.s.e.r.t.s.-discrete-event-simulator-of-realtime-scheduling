package policy.scheduling;

import java.util.Comparator;

import edu.unibo.ciri.realtime.scheduling.policy.SchedulingPolicy;
import edu.unibo.ciri.realtime.scheduling.policy.StaticSortingPolicy;


import task.Job;
import task.Task;
import task.TaskSet;
import util.SortingAlgorithm;
import util.SortingAlgorithm.SortingOrder;


public class RMPolicy extends StaticSortingAlgorithm implements SchedulingPolicy {
	
	private Comparator<Job> jobComparator = new Comparator<Job>(){

		@Override
		public int compare(Job job1, Job job2) {
			
			long period1 = job1.getTask().getTaskParameters().getPeriod().getTimeValue();
			long period2 = job2.getTask().getTaskParameters().getPeriod().getTimeValue();
			
			/* The shorter the relative deadline the higher the priority of the job*/			
			if(period1 < period2)
				return 1;
			if(period1 > period2)
				return -1;
			return 0;
		}
		
	};
	
	private class RMTaskComparator implements Comparator<Task>{
		
		
		public int compare(Task t1, Task t2) {
			
			long period1 = t1.getTaskParameters().getPeriod().getTimeValue();
			long period2 = t2.getTaskParameters().getPeriod().getTimeValue();		
					
			if(period1 < period2)
				return 1;
			if(period1 > period2)
				return -1;
			return 0;
			
		}
		
	}
	
		
	public RMPolicy(SortingAlgorithm<Task> sa) {
		
		super( new TaskSorter() {
			
			private SortingAlgorithm<Task> sortingAlgorithm;
			
			@Override
			public void setSortingAlgorithm(
					SortingAlgorithm<Task> sortingAlgorithm) {
				
				this.sortingAlgorithm = sortingAlgorithm;  
			}

			@Override
			public SortingAlgorithm<Task> getSortingAlgorithm() {
				
				return this.sortingAlgorithm;
			}
			
			@Override
			public void sort(Task[] array) {
				
				sortingAlgorithm.sort(array, SortingOrder.NON_INCREASING_ORDER);		
			}

			@Override
			public Task[] sort(TaskSet taskset) {
				
				Task[] orderedArray = new Task[taskset.size()];
				taskset.getArrayList().toArray(orderedArray);
				sortingAlgorithm.sort(orderedArray, SortingOrder.NON_INCREASING_ORDER);
				
				return orderedArray;
			}		

		});//End of TaskSorter definition and instantiation
		
		sa.setComparator( new RMTaskComparator() );		
		getStaticTaskSorter().setSortingAlgorithm(sa);
		
	}
	


	@Override
	public String getName() {
		
		return "Rate Monotonic";
	}

	
	@Override
	public Comparator<Job> getJobComparator() {
		
		return jobComparator;
	}

	



}
