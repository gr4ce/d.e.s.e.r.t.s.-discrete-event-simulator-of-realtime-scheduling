package policy.scheduling;

import edu.unibo.ciri.realtime.model.Job;
import edu.unibo.ciri.realtime.model.Job.JobState;
import edu.unibo.ciri.realtime.policies.scheduling.base.PriorityComparator;

public class RateMonotonicPriority 	implements PriorityComparator{

		private static RateMonotonicPriority rule = new RateMonotonicPriority();
		
		private RateMonotonicPriority(){}
		
		public static RateMonotonicPriority getInstance(){
			return rule;
		}
		
		@Override
		public int compare(Job job1, Job job2) {		
			long relativePeriod1 = job1.getTask().getTaskParameters().getPeriod().getTimeValue();
			long relativePeriod2 = job2.getTask().getTaskParameters().getPeriod().getTimeValue();
			
//			System.out.println("Control says: \njob1 DL = "+absoluteDeadline1+"\njob1 DL = "+absoluteDeadline2+"\nthan I return "+job1.getJobDeadline().compareTo(job2.getJobDeadline()));
//			System.out.println("job1: "+job1.getState());
//			System.out.println("job2: "+job2.getState());
			long result = -(relativePeriod1 - relativePeriod2);
			
			if(result > 0)
				return 1;
			else if(result < 0)
				return -1;
			else{
				if(job1.getState() == JobState.RUNNING)
					return 1;
				if(job2.getState() == JobState.RUNNING)
				 	return -1;
				return 0;
			}
		

	}

}
