package policy.scheduling;

import edu.unibo.ciri.desert.event.EventType;
import edu.unibo.ciri.realtime.model.Job;
import edu.unibo.ciri.realtime.model.Job.JobState;
import edu.unibo.ciri.realtime.policies.scheduling.base.PriorityComparator;

public class EarliestDeadlineFirstPriority implements PriorityComparator{

	private static EarliestDeadlineFirstPriority rule = new EarliestDeadlineFirstPriority();
	
	private EarliestDeadlineFirstPriority(){}
	
	public static EarliestDeadlineFirstPriority getInstance(){
		return rule;
	}
	
	@Override
	public int compare(Job job1, Job job2) {		
		
		long absoluteDeadline1 = job1.getEvent(EventType.JOB_DEADLINE).getEventTime().getTimeValue();
		long absoluteDeadline2 = job2.getEvent(EventType.JOB_DEADLINE).getEventTime().getTimeValue();
				
		int result = -(int)(absoluteDeadline1 - absoluteDeadline2);
		
		if(result != 0)
			return result;
		else{
			if(job1.getState() == JobState.RUNNING)
				return 1;
			if(job2.getState() == JobState.RUNNING)
			 	return -1;
			return 0;
		}
	}

}
