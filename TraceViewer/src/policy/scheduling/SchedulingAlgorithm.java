package policy.scheduling;

public interface SchedulingAlgorithm {
	
	String getName();
	
	MigrationMode getMigrationMode();
	
	PriorityAssignmentMode 	getPriorityAssignmentMode();

}
