package rta;

import task.Task;
import task.TaskSet;

public class AnotherNPModel extends SchedulingModel{
	
	
	private long blockTime[];	
	private long busyPeriod[];
	
	public AnotherNPModel(){
		
	}
	
	public AnotherNPModel(TaskSet ts){
		
		this.setTaskSet(ts);
		
	}
	
	public void setTaskSet(TaskSet ts){
			
		super.setTaskSet(ts);
		blockTime = new long[nTask];
		busyPeriod = new long[nTask];		
		
	}
	
	public long compute_wcrt(int i){
		
		
		long responseTime;
		long primianoContrib;
		long computation = ts.getTask(i).getTaskParameters().getComputation().getTimeValue();
		
		
		blockTime[i] = compute_blocking(i);		
		
		primianoContrib = compute_start_time_primiano(i);
		responseTime = primianoContrib + computation;
		
				
		return responseTime;
		
	}
	
	private long max(long[] e){
		long max = 0;
		for(int i = 0; i < e.length; i++){
			if(e[i] > max)
				max = e[i];
		}
		return max;
	}
	
	private long busy_period_sum(Task[] hp, long L_prev){
		int sum = 0;
		Task tmp;
		double C,T;
		for(int i = 0; i < hp.length; i++){
			tmp = hp[i];
			C = tmp.getTaskParameters().getComputation().getTimeValue();
			T = tmp.getTaskParameters().getPeriod().getTimeValue();
			
			sum += ( Math.ceil(L_prev/T) )*C;
		}
		return sum;
	}
	
	
	
	private Task[] getHighEqualPriority(int i){
		
		Task[] higher_prio = getHighPriority(i);
		Task[] higher_equal_prio = new Task[higher_prio.length+1];
		
		int k;
		for(k = 0; k < higher_prio.length; k++)
			higher_equal_prio[k] = higher_prio[k];		
		higher_equal_prio[k] = ts.getTask(i);
		
		
		return higher_equal_prio;
	}
	
	private long start_time_sum_primiano(Task[] hp,long S_prev){
		
		long sum = 0;
		Task tmp;
		double Cj,Tj;
		for(int i = 0; i < hp.length; i++){
			tmp = hp[i];
			Cj = tmp.getTaskParameters().getComputation().getTimeValue();
			Tj = tmp.getTaskParameters().getPeriod().getTimeValue();
			
//			sum += ( 1 + Math.floor((S_prev/Tj) )*Cj;
			sum += (  Math.ceil(S_prev/Tj) )*Cj;
		}
		return sum;
	}
	
	private long compute_start_time_primiano(int i){
		
		long S_prev,S_curr;
		Task[] hp = getHighPriority(i);
		
		S_prev = -1;
		S_curr = 1;
		
		while(S_prev != S_curr){
			S_prev = S_curr;						
			S_curr = blockTime[i] + start_time_sum_primiano(hp,S_prev);
		}
		
		return S_curr;
	}
	
	private long compute_blocking(int task_index){
		long B = 0;
		Task[] lp;
		
		lp = getLowPriority(task_index);
		if(lp.length == 0)
			return 0;
		
		for(int i = 0; i < lp.length; i++){
			if((lp[i].getTaskParameters().getComputation().getTimeValue() - 1) > B)
				B = lp[i].getTaskParameters().getComputation().getTimeValue() - 1;
		}
		
		return B;
			
	}

}
