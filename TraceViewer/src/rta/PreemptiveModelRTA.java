package rta;

import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskSet;

public class PreemptiveModelRTA extends SchedulingModel{
	
	
	
	//private int busyPeriod[];
	
	public PreemptiveModelRTA(){
		
	}
	
	public PreemptiveModelRTA(TaskSet ts){
		
		this.setTaskSet(ts);
		
	}
	
	
	
	public long compute_wcrt(int i){
		
		long R_prev, R_curr;
		long computation = ts.getTask(i).getTaskParameters().getComputation().getTimeValue();
		Task hp[] = getHighPriority(i);
		
		R_prev = 0;
		R_curr = computation;
		
		while(R_prev != R_curr){
			R_prev = R_curr;
			
			R_curr = computation + interference_sum(hp, R_prev);
		}		
		
		worstCaseResponseTime[i] = R_curr;
				
		return worstCaseResponseTime[i];
		
	}
	
	private long interference_sum(Task hp[],long R_prev){
		
		long sum = 0;
		Task tmp;
		double C,T;
		for(int i = 0; i < hp.length; i++){
			tmp = hp[i];
			C = tmp.getTaskParameters().getComputation().getTimeValue();
			T = tmp.getTaskParameters().getPeriod().getTimeValue();
			
			sum +=  Math.ceil(R_prev/T) * C;
		}
		return sum;
	}
	

	
	

}
