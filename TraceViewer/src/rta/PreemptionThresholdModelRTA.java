package rta;
import java.util.ArrayList;

import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.policies.scheduling.PreemptionThresholdAssignment;






public class PreemptionThresholdModelRTA extends SchedulingModel {

	
	private long blockTime[];	
	private long busyPeriod[];
	
	public PreemptionThresholdModelRTA(){		
	}
	
	public PreemptionThresholdModelRTA(TaskSet ts){
		
		super.setTaskSet(ts);
		blockTime = new long[nTask];
		busyPeriod = new long[nTask];
		
	}
	
	public void setTaskset(TaskSet taskSet){
		super.setTaskSet(taskSet);
		blockTime  = new long[taskSet.size()];
		busyPeriod = new long[taskSet.size()];
	}
	
	public boolean findFeasibleThresholdAssignment(){
		
		//initialize 
		for(int i = 0; i < ts.size(); i++){
			ts.getTask(i).getTaskParameters().setThreshold(ts.getTask(i).getTaskParameters().getStaticPriority());
		}
		
		int currentThreshold;
		int maxPriority = ts.getMaxPriority();
		Task currentTask;
		for(int i = ts.size()-1; i >= 0 ; i--){
			
			currentTask = ts.getTask(i);
			currentThreshold = currentTask.getTaskParameters().getThreshold();
			
			while(compute_wcrt(i) > currentTask.getTaskParameters().getDeadline().getTimeValue()){
				currentThreshold++;
				if(currentThreshold > maxPriority)
					return false;
				currentTask.getTaskParameters().setThreshold(currentThreshold);
			}
			
		}
		return true;
	}
	
	public boolean isThresholdAssignmentFeasible(PreemptionThresholdAssignment assign){
		
		setThresholdAssignment(assign);
				
		for(int i = ts.size()-1; i >= 0; i--){
			
			if(!isTaskFeasible(i))			
				return false;
		}
		return true;
	}
	
	public void setThresholdAssignment(PreemptionThresholdAssignment assign){
		
		Task currentTask;
		for(int i = ts.size()-1; i >= 0; i--){
			
			currentTask = ts.getTask(i);
			currentTask.getTaskParameters().setThreshold(assign.getThresholdByTaskName(currentTask.getName()));			
						
		}
	}
	
	public boolean isTaskFeasible(int i){
		
		return compute_wcrt(i) <= ts.getTask(i).getTaskParameters().getDeadline().getTimeValue();
	}
	
	
	@Override
	public long compute_wcrt(int i) {
		
		long finishingTime[];
		long responseTime[];
		long startTime[];
		long period = ts.getTask(i).getTaskParameters().getPeriod().getTimeValue();
		

		
		blockTime[i]  = compute_blocking(i);
		//System.out.println("blocco= "+ blockTime[i]);
	
		
		busyPeriod[i] = compute_busy_period(i);
		//System.out.println("B[" + i + "] = " + blockTime[i]);
		
		
		//Q is the max number of instances to take in account
		int Q = (int) Math.floor(busyPeriod[i]/period);
		finishingTime = new long[Q+1];
		responseTime  = new long[Q+1];
		startTime 	  = new long[Q+1];
		
		
		for(int q = 0; q <= Q; q++ ){
			startTime[q] = compute_start_time(i, q);
			finishingTime[q] = compute_finish_time(i, q, startTime[q]);
			responseTime[q] = finishingTime[q] - (q * period);
		}
		
		worstCaseResponseTime[i] = max(responseTime);	
		
		return worstCaseResponseTime[i];
	}
	
	
	
	private long compute_blocking(int i){
		
		long B = 0;
		Task[] lpht;
		
				
		lpht = getLowPrioHighEqualThresh(i);
		
		if(lpht.length == 0) {
			return 0;
		}
		
		for(int k = 0; k < lpht.length; k++){
			if((lpht[k].getTaskParameters().getComputation().getTimeValue() - 1) > B)
				B = lpht[k].getTaskParameters().getComputation().getTimeValue() - 1;
		}
		
		return B;
			
	}
	
	
	
	private Task[] getLowPrioHighEqualThresh(int i){
		
		ArrayList<Task> lowPrioHighThresTask = new ArrayList<Task>();
		
		int currentLevel = ts.getTask(i).getTaskParameters().getStaticPriority();
		Task tmp;
		
		for(int k = 0; k < ts.size(); k++){
			tmp = ts.getTask(k);
			//modificato
			if(tmp.getTaskParameters().getStaticPriority() < currentLevel && tmp.getTaskParameters().getThreshold() >= currentLevel)
				lowPrioHighThresTask.add(tmp);
		}
		return lowPrioHighThresTask.toArray(new Task[lowPrioHighThresTask.size()]);
	}
	
	
	
	private long busy_period_sum(Task[] hp, long L_prev){
		long sum = 0;
		Task tmp;
		double C,T;
		for(int i = 0; i < hp.length; i++){
			tmp = hp[i];
			C = tmp.getTaskParameters().getComputation().getTimeValue();
			T = tmp.getTaskParameters().getPeriod().getTimeValue();

			sum += ( Math.ceil(L_prev/T) )*C;			
		}
		return sum;
	}
	
	private long compute_busy_period(int i){
		long L_prev,L_curr;
		
		
		//priority higher or equal to i
		Task[] hep = getHighEqualPriority(i);
		
		
		L_prev = 0;
		L_curr = ts.getTask(i).getTaskParameters().getComputation().getTimeValue();
		
		while(L_prev != L_curr){
			L_prev = L_curr;
						
			L_curr = blockTime[i] + busy_period_sum(hep,L_prev);
		}
		
		return L_curr;
		
	}
	
	//TODO da mettere nel taskset???
	private Task[] getHighEqualPriority(int i){
		
		Task[] higher_prio = getHighPriority(i);
		Task[] higher_equal_prio = new Task[higher_prio.length+1];
		
		int k;
		for(k = 0; k < higher_prio.length; k++)
			higher_equal_prio[k] = higher_prio[k];		
		higher_equal_prio[k] = ts.getTask(i);
		
		
		return higher_equal_prio;
	}
	
	
	private long start_time_sum(Task[] hp,long S_prev){
		
		long sum = 0;
		Task tmp;
		double C,T;
		for(int i = 0; i < hp.length; i++){
			tmp = hp[i];
			C = tmp.getTaskParameters().getComputation().getTimeValue();
			T = tmp.getTaskParameters().getPeriod().getTimeValue();
			
			sum += ( 1 + Math.floor(S_prev/T) )*C;
		}
		return sum;
	}
	
	private long compute_start_time(int i, int q){
		
		long S_prev,S_curr;
		Task[] hp = getHighPriority(i);
		
		S_prev = -1;
		S_curr = 0;
		
		while(S_prev != S_curr){
			S_prev = S_curr;
						
			S_curr = blockTime[i] + q*ts.getTask(i).getTaskParameters().getComputation().getTimeValue() + start_time_sum(hp,S_prev);
		}
		
		return S_curr;
	}
	
	
	private long finish_time_sum(Task[] pgtt,long F_prev,long start_time){
		
		int sum = 0;
		Task tmp;
		double Cj,Tj;
		for(int j = 0; j < pgtt.length; j++){
			tmp = pgtt[j];
			Cj = tmp.getTaskParameters().getComputation().getTimeValue();
			Tj = tmp.getTaskParameters().getPeriod().getTimeValue();
			
			sum += ( Math.ceil(F_prev/Tj) - ( 1 + Math.floor(start_time/Tj) )) * Cj;
		}
		return sum;
	}
	
	private long compute_finish_time(int i, int q, long start_time){
		
		long F_prev,F_curr;
		Task[] pgtt = ts.getTaskWithPriorityGreaterThenThreshold(i);
		F_prev = -1;
		F_curr = 0;
		
		while(F_prev != F_curr){
			F_prev = F_curr;
						
			F_curr = start_time + ts.getTask(i).getTaskParameters().getComputation().getTimeValue() + finish_time_sum(pgtt,F_prev,start_time);
		}
		
		return F_curr;
	}
	
	private long max(long[] e){
		long max = 0;
		for(int i = 0; i < e.length; i++){
			if(e[i] > max)
				max = e[i];
		}
		return max;
	}
	

	public int getSize() {
		   return ts.size();
	   }
	
}// end class