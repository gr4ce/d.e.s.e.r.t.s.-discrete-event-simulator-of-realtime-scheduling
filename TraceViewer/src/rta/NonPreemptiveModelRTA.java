package rta;

import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskSet;





public class NonPreemptiveModelRTA extends SchedulingModel {
	
	
	private long blockTime[];	
	private long busyPeriod[];
	
	public NonPreemptiveModelRTA(){
		
	}
	
	public NonPreemptiveModelRTA(TaskSet ts){
		
		this.setTaskSet(ts);
		
	}
	
	public void setTaskSet(TaskSet ts){
			
		super.setTaskSet(ts);
		blockTime = new long[nTask];
		busyPeriod = new long[nTask];		
		
	}
	
	public long compute_wcrt(int i){
		
		long finishingTime[];
		long responseTime[];
		long startTime[];
		long computation = ts.getTask(i).getTaskParameters().getComputation().getTimeValue();
		long period = ts.getTask(i).getTaskParameters().getPeriod().getTimeValue();
		
		blockTime[i] = compute_blocking(i);		
		busyPeriod[i] = compute_busy_period(i);
		System.out.println("BP["+i+"] = "+busyPeriod[i] + " and blocking is " + blockTime[i]);
		
		//Q is the max number of instances to take in account
		int Q = (int) Math.floor(busyPeriod[i]/period);
		finishingTime = new long[Q+1];
		responseTime  = new long[Q+1];
		startTime 	  = new long[Q+1];
		
		for(int q = 0; q <= Q; q++ ){
			startTime[q] = compute_start_time(i, q);
			finishingTime[q] = startTime[q] + computation;
			responseTime[q] = finishingTime[q] - (q * period);
		}
		
		worstCaseResponseTime[i] = max(responseTime);		
		return worstCaseResponseTime[i];
		
	}
	
	private long max(long[] e){
		long max = 0;
		for(int i = 0; i < e.length; i++){
			if(e[i] > max)
				max = e[i];
		}
		return max;
	}
	
	private long busy_period_sum(Task[] hp, long L_prev){
		
		long sum = 0;
		Task tmp;
		double C,T;
		
		for(int i = 0; i < hp.length; i++){
			
			tmp = hp[i];
			C = tmp.getTaskParameters().getComputation().getTimeValue();
			T = tmp.getTaskParameters().getPeriod().getTimeValue();
			
			sum += ( Math.ceil(L_prev/T) )*C;
			
		}
		
		return sum;
	}
	
	private long compute_busy_period(int i){
		
		long L_prev,L_curr;		
		
		//priority higher or equal to i
		Task[] hep = getHighEqualPriority(i);		
		
		L_prev = 0;
		L_curr = ts.getTask(i).getTaskParameters().getComputation().getTimeValue();
		
		while(L_prev != L_curr){
			
			L_prev = L_curr;						
			L_curr = blockTime[i] + busy_period_sum(hep,L_prev);
			
		}
		
		return L_curr;
		
	}
	
	private Task[] getHighEqualPriority(int i){
		
		Task[] higher_prio = getHighPriority(i);
		Task[] higher_equal_prio = new Task[higher_prio.length+1];
		
		int k;
		for(k = 0; k < higher_prio.length; k++)
			higher_equal_prio[k] = higher_prio[k];		
		higher_equal_prio[k] = ts.getTask(i);
		
		
		return higher_equal_prio;
	}
	
	private long start_time_sum(Task[] hp,long S_prev){
		
		long sum = 0;
		Task tmp;
		double C,T;
		for(int i = 0; i < hp.length; i++){
			tmp = hp[i];
			C = tmp.getTaskParameters().getComputation().getTimeValue();
			T = tmp.getTaskParameters().getPeriod().getTimeValue();
			
			sum += ( 1 + Math.floor(S_prev/T) )*C;
		}
		return sum;
	}
	
	private long compute_start_time(int i, int q){
		
		long S_prev,S_curr;
		Task[] hp = getHighPriority(i);
		
		S_prev = -1;
		S_curr = 0;
		
		while(S_prev != S_curr){
			S_prev = S_curr;
						
			S_curr = blockTime[i] + q*ts.getTask(i).getTaskParameters().getComputation().getTimeValue() + start_time_sum(hp,S_prev);
		}
		
		return S_curr;
	}
	
	private long compute_blocking(int task_index){
		long B = 0;
		Task[] lp;
		
		lp = getLowPriority(task_index);
		if(lp.length == 0)
			return 0;
		
		System.out.print("\nI'm "+ task_index);
		for(int i = 0; i < lp.length; i++){
			System.out.print(" and " + lp[i].getName());
			if((lp[i].getTaskParameters().getComputation().getTimeValue() - 1) > B)
				B = lp[i].getTaskParameters().getComputation().getTimeValue() - 1;
		}
		System.out.println(" has low priority");
		
		return B;
			
	}
	

}
