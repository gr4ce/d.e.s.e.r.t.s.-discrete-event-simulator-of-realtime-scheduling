package rta;
import java.util.ArrayList;

import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskSet;

import policy.scheduling.RateMonotonicPrioirtyOrdering;



public abstract class SchedulingModel {
	
	protected long worstCaseResponseTime[];
	protected TaskSet ts = new TaskSet();
	protected int nTask;
	
	public abstract long compute_wcrt(int i);

	
	public boolean isSchedulable(){
		
		if(ts.getUtilization() > 1){
			System.out.println("U = "+ts.getUtilization());
			return false;
		}
		
		
		int n = ts.size();
		Task tmp;
		for(int i = 0; i < n; i++){
			tmp = ts.getTask(i);
			if(worstCaseResponseTime[i] > tmp.getTaskParameters().getDeadline().getTimeValue())
				return false;
		}
		return true;
	}
	
	public void setTaskSet(TaskSet taskset){
		ts = taskset;
		nTask = ts.size();
		worstCaseResponseTime = new long[nTask];
		
	}
	
	public Task[] getHighPriority(int i){
		
		ArrayList<Task> hpTask = new ArrayList<Task>();
		
		int currentLevel = ts.getTask(i).getTaskParameters().getStaticPriority();
		Task tmp;
		
		Task[] ordered = RateMonotonicPrioirtyOrdering.getOrderedArray(ts);
		
		for(int k = 0; k < ordered.length; k++){
			tmp = ordered[k];
			if(tmp.getTaskParameters().getStaticPriority() > currentLevel){
				hpTask.add(tmp);
			}
		}
		
		return hpTask.toArray(new Task[hpTask.size()]);
	}
	
	public Task[] getLowPriority(int i){
		
		ArrayList<Task> lpTask = new ArrayList<Task>();
		
		int currentLevel = ts.getTask(i).getTaskParameters().getStaticPriority();
		Task tmp;
		
		Task[] ordered = RateMonotonicPrioirtyOrdering.getOrderedArray(ts);
		
		for(int k = 0; k < ordered.length; k++){
			tmp = ordered[k];
			if(tmp.getTaskParameters().getStaticPriority() < currentLevel){
				lpTask.add(tmp);
			}
		}
		
		return lpTask.toArray(new Task[lpTask.size()]);
	}
	


}
