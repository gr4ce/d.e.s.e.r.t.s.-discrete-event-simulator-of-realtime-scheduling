package util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;


public class Queue<E> implements Iterable<E>{

	private int size = 0;
	private SingleLinkElementWrap<E> head = null;
	private SingleLinkElementWrap<E> tail = null;
	private Vector<SingleLinkElementWrap<E>> freeWrappers;
	private int freeWrapperStartCapacity = 100;
	
	public Queue(){
		
		freeWrappers = new Vector<SingleLinkElementWrap<E>>(freeWrapperStartCapacity);
		
		for(int i = 0; i < freeWrapperStartCapacity; i++)
			freeWrappers.add(new SingleLinkElementWrap<E>());
	}
	
			
	public int size(){		
		return size;
	}
	
		
	public E pick(){
				
		if(head == null)
			return null;
		
		SingleLinkElementWrap<E> second = head.getNext();
		E elem = head.getContent();
		assert(elem != null);		 
		
		freeWrappers.add(head);
		
		head = second;
		size--;		
		assert(size >= 0);
		if(size == 0)
			tail = null;
		
		return elem;
	}
	
	public E getFirst(){
		
		if(head == null)
			return null;
		
		return head.getContent();
	}
	
	public E getNext(E elem){
		
		if(head == null)
			return null;
		
		SingleLinkElementWrap<E> curr = head;
		
		do{
			if(elem.equals(curr.getContent())){
				if(curr.getNext() != null)
					return curr.getNext().getContent();			
				return null;
			}				
		}while( (curr = curr.getNext()) != null);
		
		return null;
	}
		
	
	public void enqueue(E elem){    	
		
		SingleLinkElementWrap<E> linkedElem;
		
		int currentFreeWrappersSize = freeWrappers.size(); 
		
		if(currentFreeWrappersSize > 0){
			linkedElem = freeWrappers.remove(currentFreeWrappersSize - 1);
			linkedElem.setContent(elem);
		}
		else{
			linkedElem = new SingleLinkElementWrap<E>(elem);
		}    	    	    	
    	
    	if(head == null){    		
    		assert(tail == null);
    		assert(size == 0);
    		
    		head = tail = linkedElem;    		
    	}	
    	else{
    		tail.setNext(linkedElem);
    		tail = linkedElem;
    	}
    	size++;
    	
	}
    
    
	public boolean contains(E elem){
		
		if(head == null)
			return false;
		
		SingleLinkElementWrap<E> current = head;
		do{
			if(current.getContent().equals(elem))
				return true;
		}while((current = current.getNext()) != null);
		
		return false;		
	}
	
	public void remove(E elem){
		
		if(elem == null || head == null || !contains(elem))
			return;
		
		
	}
	
	
	
	public String toString(){
		
		if(head == null)
			return "empty";
		
		StringBuffer sb = new StringBuffer();
		SingleLinkElementWrap<E> current = head;
		sb.append("HEAD");
		
		do{
			sb.append( " -> " + current.getContent().toString() );			
		}while((current = current.getNext())!= null);
		if(current == tail)
			sb.append(" -> TAIL");
		
		return sb.toString();
	}


	@Override
	public Iterator<E> iterator() {
		return  new QueueIterator<E>(this);
	}
	
	public void clear(){
		
		SingleLinkElementWrap<E> elem = head;
		
		while(elem != null){
			elem.setContent(null);
			freeWrappers.add(elem);
			elem = elem.getNext();
		}
		
	}
	
}
