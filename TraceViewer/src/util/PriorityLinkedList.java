package util;



public class PriorityLinkedList<E extends Comparable<E>> {

	private int size;
	protected DoubleLinkElementWrap<E> head;
	protected DoubleLinkElementWrap<E> tail;
	public  enum PolicyForTieInsetion {INSERT_BEFORE, INSERT_AFTER};
	private PolicyForTieInsetion policy;
	private static PolicyForTieInsetion defaultPolicy = PolicyForTieInsetion.INSERT_AFTER;
	
	public PriorityLinkedList(){
		policy = defaultPolicy;
		initList();
	}
	
	public PriorityLinkedList(PolicyForTieInsetion pol){
		this.policy = pol;
		initList();
	}
	
	protected void initList(){
		
		head = new DoubleLinkElementWrap<E>(null);
		tail = new DoubleLinkElementWrap<E>(null);
		head.setNext(tail);
		head.setPrev(null);
		tail.setPrev(head);
		tail.setNext(null);		
		size = 0;

	}
		
	
	protected DoubleLinkElementWrap<E> getInstance(E elem){
		return new DoubleLinkElementWrap<E>(elem);
	}
		
	public int size(){		
		return size;
	}	
	
	protected void insertBetween(DoubleLinkElementWrap<E> target, DoubleLinkElementWrap<E> prev, DoubleLinkElementWrap<E> next){		
		target.setNext(next);
		target.setPrev(prev);
		prev.setNext(target);
		next.setPrev(target);
		size++;		
	}
	
	public int positionOf(E elem){
		if(elem == null || size == 0)
			return -1;
		
		DoubleLinkElementWrap<E> linkedElem = head;
		
		int count = 0;
		while((linkedElem = linkedElem.getNext()) != null 
				&& !linkedElem.getContent().equals(elem) )
			count++;	
		
		return count;		
	}
	
	protected void removeBetween(DoubleLinkElementWrap<E> target, DoubleLinkElementWrap<E> prev, DoubleLinkElementWrap<E> next){		
		prev.setNext(next);
		next.setPrev(prev);
		target.unlink();
		target.setContent(null);
		size--;
	}
	
	public void clear(){		
		//TODO really need that?????? You can't simply unlink head and tail
		if(head.getNext() == tail){
			initList();
			return;
		}
			
		DoubleLinkElementWrap<E> curr = head.getNext();
		DoubleLinkElementWrap<E> prev;
		while((curr = curr.getNext()) != tail){
			prev = curr.getPrev();
			prev.setPrev(null);
			prev.setNext(null);
		}
		
		initList();
	}
	
	public E getElementAt(int position){
		
		if(position < 0)
			return null;
		
		if(size == 0)
			return null;
		
		if(position >= size)
			return null;
		
		DoubleLinkElementWrap<E> elem = head;
		
		int count = 0;
		while((elem = elem.getNext()) != null && count != position)count++;	
		
		return elem.getContent();
	}
	
	
	public boolean removeElementAt(int position){		
		
		if( position >= size || position < 0)
			return false;
		
		int count = 0;
		DoubleLinkElementWrap<E> curr = head.getNext();
		while( count != position ){
			curr = curr.getNext();
			count++;
		}
		removeBetween(curr, curr.getPrev(), curr.getNext());
		return true;
	
	}
	
	
	public int getInsertPosition(E elem){
		
		if(elem == null)
    		return -1;
    	
    	int position = 0;
    	
    	
    	if(head.getNext() == tail)
    		return position;
    	
    	    	
    	DoubleLinkElementWrap<E> curr = head;
    	DoubleLinkElementWrap<E> linkedElem = getInstance(elem);    	
    	
    	boolean policyInsertAfterActive = false;    	
    	
    	while((curr = curr.getNext())!= tail){
    		if(!policyInsertAfterActive){
	    		if(curr.compareTo(linkedElem) < 0){
	    			return position;
	    		}    			 
	    		else if(curr.compareTo(linkedElem) == 0){
	    			if(policy == PolicyForTieInsetion.INSERT_BEFORE){	    
	        			return position;
	    			}				
	    			else{
		    			policyInsertAfterActive = true;	
	    			}	    				
	    		}
    		}
    		else{
    			if(curr.compareTo(linkedElem) != 0){
    	   			return position;
	    		}
    		}     		
    		position++;
    	}
    	    	
    	return position;
		
	}
	
	public boolean removeElement(E elem){
		
		if(elem == null)
			return false;
		
		if(!this.contains(elem))
			return false;
				
		DoubleLinkElementWrap<E> curr = head;
		while( !(curr = curr.getNext()).getContent().equals(elem) ){
		}
		removeBetween(curr, curr.getPrev(), curr.getNext());
		return true;
				
	}
	
	public E popHead(){
		
		if(head.getNext() == tail)
			return null;
		
		DoubleLinkElementWrap<E> first = head.getNext();
		E elem = first.getContent();
		assert(elem != null);		 
		
		removeBetween(first, head, first.getNext());
		
		assert(size >= 0);		
		return elem;
	}
	
	public E getFirst(){
		
		if(head.getNext() == tail)
			return null;
		
		return head.getNext().getContent();
	}
	
	public E getNext(E elem){
		
		//first identify element
		DoubleLinkElementWrap<E> curr = head;
		while( (curr = curr.getNext()) != tail){
			if(elem.equals(curr.getContent()) && curr.getNext() != tail){
				return curr.getNext().getContent();
			}
		}
		return null;
	}
	
	public E getLast(){
		
		if(tail.getPrev() == head)
			return null;
		
		return tail.getPrev().getContent();
	}
	

	
	
	public int orderedInsertBefore(E elem){
		if(elem == null)
    		return -1;
    	
    	int position = 0;
    	
    	policy = PolicyForTieInsetion.INSERT_BEFORE;
    	
    	position = orderedInsert(elem);
    	
    	policy = defaultPolicy;
    	
    	return position;		
	}
	
	public int orderedInsert(E elem){
    	
    	if(elem == null)
    		return -1;
    	
    	int position = 0;
    	
    	DoubleLinkElementWrap<E> linkedElem = getInstance(elem);
    	if(head.getNext() == tail){
    		insertBetween(linkedElem, head, tail);
    		return position;
    	}
    	    	
    	DoubleLinkElementWrap<E> curr = head; 
    	boolean policyInsertAfterActive = false;
    	
    	while((curr = curr.getNext())!= tail){
//    		System.out.println("Inspecting " + curr.getContent().toString());
    		if(!policyInsertAfterActive){
	    		if(curr.compareTo(linkedElem) < 0){
//	    			System.out.println(curr.getContent().getName() + " has much prio then " + elem.getName());
	    			insertBetween(linkedElem, curr.getPrev(), curr);
	    			return position;
	    		}    			 
	    		else if(curr.compareTo(linkedElem) == 0){
	    			if(policy == PolicyForTieInsetion.INSERT_BEFORE){
	    				insertBetween(linkedElem, curr.getPrev(), curr);
	        			return position;
	    			}				
	    			else{
		    			policyInsertAfterActive = true;	
	    			}	    				
	    		}
    		}
    		else{
    			if(curr.compareTo(linkedElem) != 0){
    				insertBetween(linkedElem, curr.getPrev(), curr);
        			return position;
	    		}
    		}    			
    		
    		position++;
    	}
    	
    	insertBetween(linkedElem, curr.getPrev(), curr);
    	return position;		
	}
    
    
	public boolean contains(E elem){
		
		DoubleLinkElementWrap<E> current = head;
		while((current = current.getNext()) != tail){
			if(current.getContent() == elem)
				return true;
		}
		return false;
		
	}
	
	public void merge(PriorityLinkedList<E> anotherList){
		
		if(anotherList.size() == 0)
			return;
		
		E elem;
		while((elem = anotherList.popHead()) != null)
			this.orderedInsert(elem);
	}
	
	
	
	public String toString(){
		
		if(head.getNext() == tail)
			return "empty";
		
		StringBuffer sb = new StringBuffer();
		DoubleLinkElementWrap<E> current = head;
		
		
		while((current = current.getNext())!= tail){
			sb.append( current.getContent().toString() + "\n" );			
		}
		
		
		return sb.toString();
	}
	
}
