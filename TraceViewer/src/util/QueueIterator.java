package util;

import java.util.Iterator;

import edu.unibo.ciri.collection.Queue;

public class QueueIterator<E> implements Iterator<E>{
	
	Queue<E> queue;
	E current = null;
	
	public QueueIterator(Queue<E> q){
		this.queue = q;
	}

	@Override
	public boolean hasNext() {
		if(current == null){
			if((current = queue.getFirst()) != null)
				return true;
			return false;
		}else{
			if((current = queue.getNext(current)) != null)
				return true;
			return false;
		}
		
	}

	@Override
	public E next() {		
		return current;
	}

	@Override
	public void remove() {
		
		
	}

}
