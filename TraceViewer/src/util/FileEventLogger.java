package util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import event.Event;
import event.EventListener;
import event.MultipleEvent;

@SuppressWarnings("unused")
public class FileEventLogger implements EventListener {
	
	private StringBuffer logged;
	private File logFile;	
	private FileWriter writer;
	
	private String defaultFilePath = "log.txt";
	private static FileEventLogger singleton = null;
	
		
	private FileEventLogger(){
		logged = new StringBuffer();
	}
	
	private FileEventLogger(int capacity){
		logged = new StringBuffer(capacity);
	}
	
	public static FileEventLogger getInstance(){
		if(singleton == null)
			singleton = new FileEventLogger();
		return singleton;
	}
	
	public boolean setFile(String filePath){
		logFile = new File(filePath);
		try {
			writer = new FileWriter(logFile,false);
		} catch (IOException e) {
			return false;
		}
		return true;
	}


	private void handleJobCompletion(Event event) {
		logged.append(event.toString());
		logged.append("\n");
	}


	private void handleJobRelease(Event event) {
		logged.append(event.toString());
		logged.append("\n");
	}


	public void handleJobDeadline(Event event) {
		logged.append(event.toString());
		logged.append("\n");
	}


	private void handleSimulationHalt(Event event) {
		logged.append(event.toString());
		logged.append("\n");
	}


	private void handleSimulationSoftStop(Event event) {
		logged.append(event.toString());
		logged.append("\n");
	}



	private void handleJobStart(Event event) {
		logged.append(event.toString());
		logged.append("\n");
	}


	private void handleJobResume(Event event) {
		logged.append(event.toString());
		logged.append("\n");
	}


	private void handleJobPreemption(Event event) {
		logged.append(event.toString());
		logged.append("\n");
	}


	private void handleZeroLaxity(Event event) {
		logged.append(event.toString());
		logged.append("\n");
	}


	private void handleJobPriorityChange(Event event) {
		logged.append(event.toString());
		logged.append("\n");
	}
	
	public void append(String s){
		logged.append(s);
		logged.append("\n");
	}
	
	public void flush(){
		try {
			writer.write(logged.toString());
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void handle(Event event) {
		
		switch(event.getEventType()){
			case JOB_RELEASE:
				handleJobRelease(event);
				break;
			case JOB_START:
				handleJobStart(event);
				break;
			case JOB_RESUME:
				handleJobResume(event);
				break;
			case JOB_COMPLETION:
				handleJobCompletion(event);
				break;		
			case JOB_PREEMPTION:
				handleJobPreemption(event);
				break;		
			case JOB_DEADLINE:
				handleJobDeadline(event);
				break;
			case JOB_PRIORITY_CHANGE:
				handleJobPriorityChange(event);
				break;		
			case SIMULATION_HALT:
				handleSimulationHalt(event);
				break;
			case SIMULATION_SOFT_STOP:
				handleSimulationSoftStop(event);
				break;
			default:			
		}		
	}




}

