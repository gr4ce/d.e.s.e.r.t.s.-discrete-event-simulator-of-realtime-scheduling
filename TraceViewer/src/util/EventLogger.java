package util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;

import event.Event;
import event.EventListener;
import event.MultipleEvent;

@SuppressWarnings("unused")
public class EventLogger implements EventListener {
	
	public Vector<HandledEventInfo> logged;
	int currentElem = 0;
		
	public EventLogger(int startElementSize){
		logged = new Vector<HandledEventInfo>(startElementSize);
		for(int i = 0; i < startElementSize; i++)
			logged.add(new HandledEventInfo());
	}
	
	public Vector<HandledEventInfo> getLoggedEventInfo(){
		logged.trimToSize();
		return logged;
	}
	
	public int getNumberOfEventsLogged(){
		//XXX Sicuro??? Cosa accade se currElem supera size()??
		return Math.min(currentElem, logged.size());
	}


	private void handleJobCompletion(Event event) {
		if(currentElem < logged.size())
			logged.get(currentElem++).convertToHandledEventInfo(event);
		else 
			logged.add(new HandledEventInfo(event));
	}


	private void handleJobRelease(Event event) {
		if(currentElem < logged.size())
			logged.get(currentElem++).convertToHandledEventInfo(event);
		else 
			logged.add(new HandledEventInfo(event));
	}


	private void handleJobDeadline(Event event) {
		if(currentElem < logged.size())
			logged.get(currentElem++).convertToHandledEventInfo(event);
		else 
			logged.add(new HandledEventInfo(event));
	}


	private void handleSimulationHalt(Event event) {
		if(currentElem < logged.size())
			logged.get(currentElem++).convertToHandledEventInfo(event);
		else 
			logged.add(new HandledEventInfo(event));
	}


	public void handleSimulationSoftStop(Event event) {
		if(currentElem < logged.size())
			logged.get(currentElem++).convertToHandledEventInfo(event);
		else 
			logged.add(new HandledEventInfo(event));
	}

	


	private void handleJobStart(Event event) {
		if(currentElem < logged.size())
			logged.get(currentElem++).convertToHandledEventInfo(event);
		else 
			logged.add(new HandledEventInfo(event));
	}

	private void handleJobResume(Event event) {
		if(currentElem < logged.size())
			logged.get(currentElem++).convertToHandledEventInfo(event);
		else 
			logged.add(new HandledEventInfo(event));
	}

	private void handleJobPreemption(Event event) {
		if(currentElem < logged.size())
			logged.get(currentElem++).convertToHandledEventInfo(event);
		else 
			logged.add(new HandledEventInfo(event));
	}


	private void handleJobPriorityChange(Event event) {	
		
	}
	
	private void handleJobOverrun(Event event){
		
	}
	
	private void handleJobMigration(Event event){
		
	}

	@Override
	public void handle(Event event) {
		
		switch(event.getEventType()){
			case JOB_PREEMPTION:
				handleJobPreemption(event);
				break;
			case JOB_MIGRATION:
				handleJobMigration(event);
				break;
			case JOB_DEADLINE:
				handleJobDeadline(event);
				break;
			case JOB_OVERRUN:
				handleJobOverrun(event);
				break;
			case JOB_START:
				handleJobStart(event);
				break;
			case JOB_RESUME:
				handleJobResume(event);
				break;
			case JOB_RELEASE:
				handleJobRelease(event);
				break;
			default:
				
		}
		
	}



}
