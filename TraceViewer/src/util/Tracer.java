package util;

public class Tracer {
	
	public enum TracingMode{ONLINE_PRINT, ONLINE_LOG, BUFFERED_PRINT, BUFFERED_LOG};
	
	private static TracingMode mode;
	private static StringBuffer buff;
	
	
	public static void setTracingMode(TracingMode m){
		mode = m;
	}
	
	public static void setInitialCapacity(int capacity){
		buff = new StringBuffer(capacity); 
	}
	
	public static void trace(String s){
		switch(mode){
			case ONLINE_PRINT:
				System.out.println(s);
				break;
			case ONLINE_LOG:
				break;
			case BUFFERED_PRINT:
				buff.append(s+"\n");
				break;
			case BUFFERED_LOG:
				break;
			default:
		}
	}
	
	public static StringBuffer getBuffer(){
		return buff;
	}

}
