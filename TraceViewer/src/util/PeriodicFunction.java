package util;

import edu.unibo.ciri.realtime.model.time.TimeDuration;

public class PeriodicFunction {
	
	
	private TimeDuration offset;
	private IntervalFunction function;
	private TimeDuration period;
	
	public PeriodicFunction(TimeDuration offset, IntervalFunction function, TimeDuration period) {
		this.setOffset(offset);
		this.setFunction(function);
		this.setPeriod(period);
	}

	public void setFunction(IntervalFunction function) {
		this.function = function;
	}

	public IntervalFunction getFunction() {
		return function;
	}

	public void setOffset(TimeDuration offset) {
		this.offset = offset;
	}

	public TimeDuration getOffset() {
		return offset;
	}

	public void setPeriod(TimeDuration period) {
		this.period = period;
	}

	public TimeDuration getPeriod() {
		return period;
	}

}
