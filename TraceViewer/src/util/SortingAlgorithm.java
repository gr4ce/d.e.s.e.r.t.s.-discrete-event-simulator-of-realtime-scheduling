package util;

import java.util.Comparator;

public interface SortingAlgorithm <E>{
	
	public enum SortingOrder{ NON_INCREASING_ORDER, NON_DECREASING_ORDER}
	
	public void setArray(E[] array);
	
	public E[] getArray();
	
	public <C extends Comparator<E>> void setComparator(C comp);
	
	public Comparator<E> getComparator();
	
	
	
	public void sort(SortingOrder sa);
	
	public void sort(E[] array, SortingOrder sa);

	

}
