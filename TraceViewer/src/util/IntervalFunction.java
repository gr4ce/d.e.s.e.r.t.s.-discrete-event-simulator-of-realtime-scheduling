package util;

import edu.unibo.ciri.collection.PriorityLinkedList;
import edu.unibo.ciri.realtime.model.time.Time;
import edu.unibo.ciri.realtime.model.time.TimeDuration;
import edu.unibo.ciri.realtime.model.time.TimeInstant;



public class IntervalFunction {
	
	private Long offset;
	private TimeDuration period = new TimeDuration(0);
	private int  multiply = 1;
	private TimeDuration actualExtension = new TimeDuration(0);
	
	private PriorityLinkedList<TimeInstant> timeAxes = new PriorityLinkedList<TimeInstant>();
	
	
	public IntervalFunction(){
		
	}
	
	public void setOffset(long offset){
		this.offset = offset;
	}
	
	public long getOffset(){
		return this.offset;
	}
	
	public TimeDuration getPeriod(){
		return this.period;
	}
	
	public void setPeriod(long period){
		this.period.setTimeValue(period, Time.getResolution());
		setMultiply(1);
		setActualExtension(period);
	}
	
	public TimeDuration getActualExtension(){
		return actualExtension;
	}
	
	public void setMultiply(int mul){
		this.multiply = mul;
	}
	
	
	
	private void setActualExtension(long extension){
		this.actualExtension.setTimeValue(extension, Time.getResolution());
	}
		
	
		
	public void addValue(ValuedTimeInstant vt){
		timeAxes.orderedInsert(vt);
	} 
	
	public void removeValue(ValuedTimeInstant vt){
		timeAxes.removeElement(vt);
	}

	public void subtract(IntervalFunction arg){
		
	}
	
	public String toString(){
		String s = new String();

		ValuedTimeInstant v;
		
		for(int k = 0; k < multiply; k++){
			v = (ValuedTimeInstant)timeAxes.getFirst();
			long scale = k*period.getTimeValue();
			while(v != null){
				s += (scale + v.getTimeValue()) + "\t" + v.getValue() + "\n";
				v = (ValuedTimeInstant)timeAxes.getNext(v);
			}
		}
		
		return s;
	}
}
