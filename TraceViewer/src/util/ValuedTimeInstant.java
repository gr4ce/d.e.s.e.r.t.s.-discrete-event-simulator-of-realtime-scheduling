package util;

import java.util.concurrent.TimeUnit;

import edu.unibo.ciri.realtime.model.time.TimeInstant;

public class ValuedTimeInstant extends TimeInstant  {

	private int value = -1;
	
	public ValuedTimeInstant(long time) {
		super(time);
		// TODO Auto-generated constructor stub
	}
	
	public ValuedTimeInstant(long time, TimeUnit unit, int value){
		super(time, unit);
		setValue(value);
		
	}
	
	public void setValue(int value){
		this.value = value;
	}
	
	public int getValue(){
		return this.value;
	}
	
	
}
