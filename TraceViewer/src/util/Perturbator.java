package util;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import edu.unibo.ciri.realtime.model.time.Time;

public class Perturbator {
	
	private static Random r = new Random(System.currentTimeMillis());
	
	
	public static void perturbTime(Time t){
		long val = t.getTimeValue(TimeUnit.MICROSECONDS);
		long var = (r.nextLong()%100) - 50;
		t.setTimeValue(val + var, TimeUnit.MICROSECONDS);
	}

}
