package util;



public class SingleLinkElementWrap<E> {
	
	
	private SingleLinkElementWrap<E> next = null;
	private E content;
	
	public SingleLinkElementWrap(){
		content = null;
	}
	
	public  SingleLinkElementWrap(E element){
		content = element;
	}
	
		
	public SingleLinkElementWrap<E> getNext() {
		return next;
	}
	
	public void setNext(SingleLinkElementWrap<E> next) {
		this.next = next;
	}
	public E getContent() {
		return content;
	}
	public void setContent(E content) {
		this.content = content;
	} 
	
	public void unlink(){
		this.next = null;
	}

	
	

	

}
