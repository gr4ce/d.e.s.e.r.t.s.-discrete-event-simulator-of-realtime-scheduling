package util;

import java.util.Comparator;
import java.util.concurrent.TimeUnit;


public class TimeInstant extends Time implements Comparable<TimeInstant>,Comparator<TimeInstant> {

	public TimeInstant(){
		super(0);	
	}
	
	public TimeInstant(long value) {
		super(value);		
	}
	
	public TimeInstant(long value, TimeUnit unit){
		super(value, unit);		
	}
	
	@Override
	/* 
	 * Returns 
	 * 1 when "this" is greater then arg
	 * -1 when "this" is less then arg
	 */
	public int compareTo(TimeInstant arg) {
		
		if(this.getTimeValue() > arg.getTimeValue())
			return -1;
		
		if(this.getTimeValue() < arg.getTimeValue())
			return 1;
		
		return 0;
	}

	@Override
	public int compare(TimeInstant t1, TimeInstant t2) {
		
		long v1 = t1.getTimeValue(), v2 = t2.getTimeValue();
		
		if(v1 > v2) 
			return -1;
		
		if(v1 < v2)
			return 1;
		
		return 0;
	}
	
	

}
