package view;

import java.awt.Color;

import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.event.management.EventListener;
import edu.unibo.ciri.desert.graphics.diagram.schedule.ScheduleDiagramContainer;
import edu.unibo.ciri.desert.graphics.diagram.schedule.elements.DrawableEvent;
import edu.unibo.ciri.desert.graphics.diagram.schedule.elements.DrawableRange;
import edu.unibo.ciri.model.task.job.JobInfo;

public class EventGraphicsDisplay implements EventListener {
	
	ScheduleDiagramContainer container;	
		
	public void setScheduleDiagramContainer(ScheduleDiagramContainer container){
		this.container = container;
	}
	
	public ScheduleDiagramContainer getScheduleDiagramContainer(){
		return this.container;
	}

	
	private void handleJobCompletion(Event event) {
		//timeDiagram.addDrawableJob(new DrawableJob(event.getJob()));
		//System.out.println("GRAPHIC DISPLAY "+event.getEventTimeValue()+" "+event.getTask().getName()+ " " +event.getJob().getId());
		
		container.addElement(event.getJob());
		Color[] procMap = container.getScheduleDiagramDrawer().getProcessorMap();
		
		JobInfo jInfo = event.getJob().getInfo();
		long activation = jInfo.getActivationTime();
		long start      = jInfo.getStartTime();
		long finish     = jInfo.getFinishingTime();
		long[] preempt  = jInfo.getPreemptionTime();
		long[] resume   = jInfo.getResumeTime();
		int[] processor = jInfo.getProcessors();
		
		assert(preempt   != null);
		assert(resume    != null);
		assert(processor != null);
		
//		assert(schduleDiagram.procMap[processor[0]] != null);
		
		// Prints ready interval if any
		if( (start - activation) > 0){
			container.addElement(new DrawableRange(activation,(start-activation),event.getJob(),Color.green));			
		}
		
		// Prints all preemption e resumed execution interval
		for(int k = 0; k < preempt.length; k++){
			container.addElement(new DrawableRange(preempt[k],(resume[k]-preempt[k]),event.getJob(),Color.GREEN));	
			
			if( k+1 < resume.length ){
				finish = preempt[k+1];				
			}else{
				finish = jInfo.getFinishingTime();				
			}
				container.addElement(new DrawableRange(resume[k],(finish-resume[k]),event.getJob(), procMap[processor[k+1]]));	
		}
		
		if(preempt.length == 0){
			container.addElement(new DrawableRange(start,(finish-start),event.getJob(),procMap[processor[0]]));	
		}else{
			container.addElement(new DrawableRange(start,(preempt[0]-start),event.getJob(),procMap[processor[0]]));	
		}	
		
		//Print execution
		
	}

	
	private void handleJobRelease(Event event) {

	}

	
	private void handleJobDeadline(Event event){

	}
	
	private void handleDelayedRelease(Event event){

	}
	
	private void handleSimulationHalt(Event event) {

	}
	
	private void handleSimulationSoftStop(Event event) {
	
	}


	private void handleJobStart(Event event) {
		// TODO Auto-generated method stub

	}

	
	private void handleJobResume(Event event) {
		// TODO Auto-generated method stub

	}


	private void handleJobPreemption(Event event) {
		// TODO Auto-generated method stub

	}

	
	private void handleZeroLaxity(Event event) {
		// TODO Auto-generated method stub

	}

	
	private void handleJobPriorityChange(Event event) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void handle(Event event) {
		
		switch(event.getEventType()){
			case JOB_RELEASE:
				handleJobRelease(event);
				break;
			case JOB_START:
				handleJobStart(event);
				break;
			case JOB_RESUME:
				handleJobResume(event);
				break;
			case JOB_COMPLETION:
				handleJobCompletion(event);
				break;		
			case JOB_PREEMPTION:
				handleJobPreemption(event);
				break;		
			case JOB_DEADLINE:
				handleJobDeadline(event);
				break;
			case JOB_PRIORITY_CHANGE:
				handleJobPriorityChange(event);
				break;		
			case SIMULATION_HALT:
				handleSimulationHalt(event);
				break;
			case SIMULATION_SOFT_STOP:
				handleSimulationSoftStop(event);
				break;
			case JOB_ZERO_LAXITY:				//giacomo (ho rinominato l'evento da ZERO_LAXITY...)
				handleZeroLaxity(event);		
				break;							
			case DELAYED_RELEASE:
				handleDelayedRelease(event);
				break;
			default:			
		}		
	}

	@Override
	public String getName() {
		
		return "EventGraphicDisplay";
	}


	

}
