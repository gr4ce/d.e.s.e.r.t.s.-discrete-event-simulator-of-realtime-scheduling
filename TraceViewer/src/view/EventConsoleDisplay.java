package view;

import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.event.management.EventListener;

public class EventConsoleDisplay implements EventListener {

	
	private void handleJobCompletion(Event event) {
		System.out.println("[ConsoleDisplay] - "+event.toString());
	}
	
	private void handleJobRelease(Event event) {
		System.out.println("[ConsoleDisplay] - "+event.toString());
	}
	
	private void handleJobDeadline(Event event) {
		System.out.println("[ConsoleDisplay] - "+event.toString());
	}
	
	private void handleSimulationHalt(Event event) {
		System.out.println("[ConsoleDisplay] - "+event.toString());
	}

	
	private void handleSimulationSoftStop(Event event) {
		System.out.println("[ConsoleDisplay] - "+event.toString());
	}

		
	private void handleJobStart(Event event) {
		System.out.println("[ConsoleDisplay] - "+event.toString() + "cpu_"+event.getCpu());		
	}

	
	private void handleJobResume(Event event) {
		System.out.println("[ConsoleDisplay] - "+event.toString() + "cpu_"+event.getCpu() + " remaining = "+event.getJob().getInfo().getRemainingExecution());		
	}

	
	private void handleJobPreemption(Event event) {
		System.out.println("[ConsoleDisplay] - "+event.toString() + "cpu_"+event.getCpu() + " remaining = "+event.getJob().getInfo().getRemainingExecution());		
	}	
	
	private void handleStartTransaction(Event event) {
		System.out.println("[ConsoleDisplay] - "+event.toString() );		
	}
	
	private void handleStopTransaction(Event event) {
		System.out.println("[ConsoleDisplay] - "+event.toString() );	
	}
	
	private void handleJobPriorityChange(Event event) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void handle(Event event) {
		
		switch(event.getEventType()){
			case JOB_RELEASE:
				handleJobRelease(event);
				break;
			case JOB_START:
				handleJobStart(event);
				break;
			case JOB_RESUME:
				handleJobResume(event);
				break;
			case JOB_COMPLETION:
				handleJobCompletion(event);
				break;		
			case JOB_PREEMPTION:
				handleJobPreemption(event);
				break;		
			case JOB_DEADLINE:
				handleJobDeadline(event);
				break;
			case JOB_PRIORITY_CHANGE:
				handleJobPriorityChange(event);
				break;		
			case SIMULATION_HALT:
				handleSimulationHalt(event);
				break;
			case SIMULATION_SOFT_STOP:
				handleSimulationSoftStop(event);
				break;
			case START_TRANSACTION:
				handleStartTransaction(event);
				break;
			case END_TRANSACTION:
				handleStopTransaction(event);
				break;
			default:			
		}		
	}


}
