/**
 * 
 */
package test;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.Test;

import task.Task;
import task.TaskParameters;
import util.TimeInstant;

import event.Event;
import event.EventList;
import event.EventType;

/**
 * @author Giuseppe
 *
 */
public class TestEventPriority {

	/**
	 * Test method for {@link event.Event#compareTo(event.Event)}.
	 * Vengono creati 2 eventi dello stesso tipo e innescati nello stesso istante
	 * In questo caso gli eventi sono ordinati secondo l'ordine di inserimento
	 */
	@Test
	public void testCompareTo1() {
		
		EventList el = new EventList();
		
		Task t = new Task("Dummy 1");
		TaskParameters tp = new TaskParameters();
		tp.setComputation(2,TimeUnit.MILLISECONDS);
		tp.setDeadline(10,TimeUnit.MILLISECONDS);
		tp.setPeriod(10,TimeUnit.MILLISECONDS);
		tp.setStaticPriority(3);
		tp.setThreshold(3);
		t.setTaskParameters(tp);
		
		Event mostPriorityEvent = new Event(new TimeInstant(0), t, EventType.JOB_RELEASE);
		
		t = new Task("Dummy 2");
		tp = new TaskParameters();
		tp.setComputation(2,TimeUnit.MILLISECONDS);
		tp.setDeadline(10,TimeUnit.MILLISECONDS);
		tp.setPeriod(10,TimeUnit.MILLISECONDS);
		tp.setStaticPriority(2);
		tp.setThreshold(2);
		t.setTaskParameters(tp);
				
		Event lessPriorityEvent = new Event(new TimeInstant(0), t, EventType.JOB_RELEASE);
		
		el.trigger(mostPriorityEvent);
		el.trigger(lessPriorityEvent);		
		assertEquals(mostPriorityEvent, el.pop());
		assertEquals(lessPriorityEvent, el.pop());
		assertEquals(null,el.pop());
		
		//il test deve essere valido anche nel momento in cui gli inserimenti
		//sono fatti in maniera invertita: in questo caso per� a parit� di priorit�
		//prevarr� l'ordine di inserimento.
		
		el.trigger(lessPriorityEvent);
		el.trigger(mostPriorityEvent);
		assertEquals(lessPriorityEvent, el.pop());
		assertEquals(mostPriorityEvent, el.pop());
		assertEquals(null,el.pop());
		
	}

	
	/**
	 * Test method for {@link event.Event#compareTo(event.Event)}.
	 * Vengono creati 2 eventi di tipo diverso e innescati nello stesso istante
	 * In questo caso gli eventi seguono l'ordine dettato dalla priorit� del
	 * tipo di evento.
	 */
	@Test
	public void testCompareTo2() {
		
		EventList el = new EventList();
		
		Task t = new Task("Dummy 1");
		TaskParameters tp = new TaskParameters();
		tp.setComputation(2,TimeUnit.MILLISECONDS);
		tp.setDeadline(10,TimeUnit.MILLISECONDS);
		tp.setPeriod(10,TimeUnit.MILLISECONDS);
		tp.setStaticPriority(3);
		tp.setThreshold(3);
		t.setTaskParameters(tp);
		
		Event mostPriorityEvent = new Event(new TimeInstant(0), t, EventType.JOB_DEADLINE);
		
		t = new Task("Dummy 2");
		tp = new TaskParameters();
		tp.setComputation(2,TimeUnit.MILLISECONDS);
		tp.setDeadline(10,TimeUnit.MILLISECONDS);
		tp.setPeriod(10,TimeUnit.MILLISECONDS);
		tp.setStaticPriority(2);
		tp.setThreshold(2);
		t.setTaskParameters(tp);
				
		Event lessPriorityEvent = new Event(new TimeInstant(0), t, EventType.JOB_RELEASE);
		
		el.trigger(mostPriorityEvent);
		assertEquals(mostPriorityEvent, el.inspectHead());
		el.trigger(lessPriorityEvent);		
		assertEquals(mostPriorityEvent, el.pop());
		assertEquals(lessPriorityEvent, el.pop());
		assertEquals(null,el.pop());		
		
		//il test deve essere valido anche nel momento in cui gli inserimenti
		//sono fatti in maniera invertita: la priorit�, se diversa, deve prevalere
		//rispetto all'ordine di inserimento.
		
		el.trigger(lessPriorityEvent);
		assertEquals(lessPriorityEvent, el.inspectHead());
		el.trigger(mostPriorityEvent);		
		assertEquals(mostPriorityEvent, el.pop());
		assertEquals(lessPriorityEvent, el.pop());
		assertEquals(null,el.pop());	
	}
	
	
	/**
	 * Test method for {@link event.Event#compareTo(event.Event)}.
	 * Vengono creati 2 eventi dello stesso tipo e innescati nello stesso istante
	 * ma i target sono task con priorit� differente.
	 */
	@Test
	public void testCompareTo3() {
		
		EventList el = new EventList();
		
		Task t = new Task("Dummy 1");
		TaskParameters tp = new TaskParameters();
		tp.setComputation(2,TimeUnit.MILLISECONDS);
		tp.setDeadline(10,TimeUnit.MILLISECONDS);
		tp.setPeriod(10,TimeUnit.MILLISECONDS);
		tp.setStaticPriority(3);
		tp.setThreshold(3);
		t.setTaskParameters(tp);
		t.releaseNewJob();
		
		Event mostPriorityEvent = new Event(new TimeInstant(0), t, EventType.JOB_RELEASE);
		
		t = new Task("Dummy 2");
		tp = new TaskParameters();
		tp.setComputation(2,TimeUnit.MILLISECONDS);
		tp.setDeadline(10,TimeUnit.MILLISECONDS);
		tp.setPeriod(10,TimeUnit.MILLISECONDS);
		tp.setStaticPriority(2);
		tp.setThreshold(2);
		t.setTaskParameters(tp);
		t.releaseNewJob();
				
		Event lessPriorityEvent = new Event(new TimeInstant(1), t, EventType.JOB_RELEASE);
		
		
		el.trigger(mostPriorityEvent);
		assertEquals(mostPriorityEvent, el.inspectHead());
		el.trigger(lessPriorityEvent);		
		assertEquals(mostPriorityEvent, el.pop());
		assertEquals(lessPriorityEvent, el.pop());
		assertEquals(null,el.pop());		
		
		//il test deve essere valido anche nel momento in cui gli inserimenti
		//sono fatti in maniera invertita: la priorit�, se diversa, deve prevalere
		//rispetto all'ordine di inserimento.
		
		
		el.trigger(lessPriorityEvent);	
		assertEquals(lessPriorityEvent, el.inspectHead());
		el.trigger(mostPriorityEvent);	
		assertEquals(mostPriorityEvent, el.pop());
		assertEquals(lessPriorityEvent, el.pop());
		assertEquals(null,el.pop());		
		
	}
}
