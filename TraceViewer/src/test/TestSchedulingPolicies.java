package test;

import java.security.InvalidAlgorithmParameterException;
import java.util.concurrent.TimeUnit;

import org.junit.Test;


import edu.unibo.ciri.desert.simulation.management.Simulation;
import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskParameters;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.model.io.TasksetFileParser;
import edu.unibo.ciri.realtime.policies.scheduling.EDZLSchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.GlobalDMSchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.GlobalEDFSchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.GlobalRMSchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.GlobalRMZLSchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.base.SchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.base.SchedulingPolicyFactory;

import junit.framework.TestCase;

public class TestSchedulingPolicies extends TestCase{
	
	@Test
	public void testFactory() {
		
		Simulation sim = new Simulation();
		TaskSet ts = createDummyTaskset();
		sim.setTaskset(ts);
		SchedulingPolicyFactory spf = new SchedulingPolicyFactory(sim);
		
		try {
			SchedulingPolicy sp = spf.newInstanceByName(GlobalDMSchedulingPolicy.policyName);
			System.out.println(sp);
			sp = spf.newInstanceByName(GlobalEDFSchedulingPolicy.policyName);
			System.out.println(sp);
			sp = spf.newInstanceByName(EDZLSchedulingPolicy.policyName);
			System.out.println(sp);
			sp = spf.newInstanceByName(GlobalRMSchedulingPolicy.policyName);
			System.out.println(sp);
			sp = spf.newInstanceByName(GlobalRMZLSchedulingPolicy.policyName);
			System.out.println(sp);
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		}
	}
	
	private TaskSet createDummyTaskset(){
		
		TaskSet ts = new TaskSet();
		
		Task t;
		TaskParameters tp;
		
		t = new Task("Task1");
		tp = new TaskParameters();
		tp.setComputation(2,   TimeUnit.MILLISECONDS);
		tp.setDeadline   (10,  TimeUnit.MILLISECONDS);
		tp.setPeriod     (10,  TimeUnit.MILLISECONDS);
		t.setTaskParameters(tp);
		ts.addTask(t);
		
		t = new Task("Task2"); 
		tp = new TaskParameters();
		tp.setComputation(4,   TimeUnit.MILLISECONDS);
		tp.setDeadline   (15,  TimeUnit.MILLISECONDS);
		tp.setPeriod     (15,  TimeUnit.MILLISECONDS);
		t.setTaskParameters(tp);
		ts.addTask(t);
		
		t = new Task("Task3"); 
		tp = new TaskParameters();
		tp.setComputation(6500,TimeUnit.MICROSECONDS);
		tp.setDeadline   (30,  TimeUnit.MILLISECONDS);
		tp.setPeriod     (30,  TimeUnit.MILLISECONDS);
		t.setTaskParameters(tp);
		ts.addTask(t);
		
		return ts;
	}

}
