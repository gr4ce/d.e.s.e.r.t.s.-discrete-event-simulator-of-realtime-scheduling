package test;

import java.util.concurrent.TimeUnit;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.junit.Before;
import org.junit.Test;


import task.Job;
import task.Task;
import task.TaskParameters;
import task.TaskSet;
import util.PriorityLinkedList;


public class TestPriorityLinkedList extends TestCase{
	
	PriorityLinkedList<Job> list;
	TaskSet taskSet;
	Job j[];
	

	@Before
	public void setUp() throws Exception {
		list = new PriorityLinkedList<Job>();
		taskSet = importTaskset();
		j = new Job[taskSet.size()];
		for(int i = 0; i < j.length; i++){
			j[i] = taskSet.getTask(i).releaseNewJob();
		}
	}

	@Test
	public void testSize() {	
		
		assertTrue(list.size() == 0);
		list.orderedInsert(j[0]);
		assertTrue(list.size() == 1);		
		list.orderedInsert(j[1]);
		assertTrue(list.size() == 2);	
		list.orderedInsert(j[2]);
		assertTrue(list.size() == 3);
		list.orderedInsert(j[3]);
		assertTrue(list.size() == 4);
		list.getFirst();
		assertTrue(list.size() == 4);
		list.getNext(list.getFirst());
		assertTrue(list.size() == 4);
		list.getElementAt(0);
		assertTrue(list.size() == 4);
		list.popHead();
		assertTrue(list.size() == 3);
		list.removeElement(list.getFirst());
		assertTrue(list.size() == 2);
		list.removeElementAt(0);
		assertTrue(list.size() == 1);
		list.clear();
		assertTrue(list.size() == 0);
	}

	@Test
	public void testGetElementAt() {
		
		assertNull(list.getElementAt(0));
		assertNull(list.getElementAt(-1));
		list.orderedInsert(j[0]);
		assertEquals(j[0],list.getElementAt(0));
		list.orderedInsert(j[1]);
		assertEquals(j[0],list.getElementAt(0));
		assertEquals(j[1],list.getElementAt(1));
		list.orderedInsert(j[2]);
		assertEquals(j[0],list.getElementAt(0));
		assertEquals(j[1],list.getElementAt(1));
		assertEquals(j[2],list.getElementAt(2));
		list.orderedInsert(j[3]);
		assertEquals(j[0],list.getElementAt(0));
		assertEquals(j[1],list.getElementAt(1));
		assertEquals(j[2],list.getElementAt(2));
		assertEquals(j[3],list.getElementAt(3));
		list.removeElement(j[1]);
		assertEquals(j[0],list.getElementAt(0));
		assertEquals(j[2],list.getElementAt(1));
		assertEquals(j[3],list.getElementAt(2));
		list.orderedInsert(j[1]);
		assertEquals(j[0],list.getElementAt(0));
		assertEquals(j[1],list.getElementAt(1));
		assertEquals(j[2],list.getElementAt(2));
		assertEquals(j[3],list.getElementAt(3));
		list.removeElementAt(0);
		assertEquals(j[1],list.getElementAt(0));
		assertEquals(j[2],list.getElementAt(1));
		assertEquals(j[3],list.getElementAt(2));
		list.orderedInsert(j[0]);
		list.removeElementAt(list.size() - 1);
		assertEquals(j[0],list.getElementAt(0));
		assertEquals(j[1],list.getElementAt(1));
		assertEquals(j[2],list.getElementAt(2));
		list.clear();
		assertEquals(null,list.getElementAt(0));
		assertEquals(null,list.getElementAt(1));
		assertEquals(null,list.getElementAt(2));
	}

	@Test
	public void testRemoveElementAt() {
		
		assertFalse(list.removeElementAt(-1));
		assertFalse(list.removeElementAt(0));
		assertTrue(list.size() == 0);
		list.orderedInsert(j[0]);
		list.orderedInsert(j[1]);
		list.orderedInsert(j[2]);
		list.orderedInsert(j[3]);
		assertTrue(list.size() == 4);
		assertTrue(list.removeElementAt(3));
		assertTrue(list.size() == 3);
		assertEquals(j[0],list.getElementAt(0));
		assertEquals(j[1],list.getElementAt(1));
		assertEquals(j[2],list.getElementAt(2));
		list.orderedInsert(j[3]);
		assertTrue(list.removeElementAt(0));
		assertTrue(list.size() == 3);
		assertEquals(j[1],list.getElementAt(0));
		assertEquals(j[2],list.getElementAt(1));
		assertEquals(j[3],list.getElementAt(2));
		list.clear();
	}

	@Test
	public void testRemoveElement() {
		
		assertFalse(list.removeElement(null));		
		assertTrue(list.size() == 0);
		list.orderedInsert(j[0]);
		list.orderedInsert(j[1]);
		list.orderedInsert(j[2]);
		list.orderedInsert(j[3]);
		assertTrue(list.removeElementAt(0));
		assertEquals(j[1],list.getElementAt(0));
		assertEquals(j[2],list.getElementAt(1));
		assertEquals(j[3],list.getElementAt(2));
		assertTrue(list.removeElementAt(1));
		assertEquals(j[1],list.getElementAt(0));
		assertEquals(j[3],list.getElementAt(1));
		assertTrue(list.removeElementAt(1));
		assertEquals(j[1],list.getElementAt(0));
		assertTrue(list.removeElementAt(0));
		assertFalse(list.removeElementAt(0));
		assertTrue(list.size() == 0);
	}
	
	@Test
	public void testPositionOf(){
		
		list.orderedInsert(j[1]);
		assertEquals(list.positionOf(j[1]),0);		
		list.orderedInsert(j[0]);
		assertEquals(list.positionOf(j[0]),0);
		assertEquals(list.positionOf(j[1]),1);
		list.orderedInsert(j[2]);
		assertEquals(list.positionOf(j[0]),0);
		assertEquals(list.positionOf(j[1]),1);
		assertEquals(list.positionOf(j[2]),2);
		list.orderedInsert(j[3]);
		assertEquals(list.positionOf(j[0]),0);
		assertEquals(list.positionOf(j[1]),1);
		assertEquals(list.positionOf(j[2]),2);
		assertEquals(list.positionOf(j[3]),3);
		list.orderedInsert(j[4]);
		assertEquals(list.positionOf(j[0]),0);
		assertEquals(list.positionOf(j[1]),1);
		assertEquals(list.positionOf(j[2]),2);
		assertEquals(list.positionOf(j[3]),3);
		assertEquals(list.positionOf(j[4]),4);
		list.orderedInsert(j[5]);
		/* note that j[5] has the same priority than
		   j[2] but the default policy for insertion
		   is INSERT_AFTER so, j[5] is inserted between
		   j[2] and j[3], with j[3] and j[4] position 
		   shifted */
		assertEquals(list.positionOf(j[0]),0);
		assertEquals(list.positionOf(j[1]),1);
		assertEquals(list.positionOf(j[2]),2);
		assertEquals(list.positionOf(j[5]),3);
		assertEquals(list.positionOf(j[3]),4);
		assertEquals(list.positionOf(j[4]),5);
		
	}

	@Test
	public void testPick() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetFirst() {
		fail("Not yet implemented");
	}
	
	@Test
	public void testClear(){
		fail("Not yet implemented");
	}

	@Test
	public void testGetNext() {
		fail("Not yet implemented");
	}

	@Test
	public void testInsertPosition() {
		
		assertEquals(list.getInsertPosition(j[1]),list.orderedInsert(j[1]));		
		assertEquals(list.getInsertPosition(j[0]),list.orderedInsert(j[0]));
		
		assertEquals(list.getInsertPosition(j[3]),2);
		assertEquals(list.orderedInsert(j[3]),2);
		
		assertEquals(list.getInsertPosition(j[4]),3);
		assertEquals(list.getInsertPosition(j[5]),2);
		assertEquals(list.orderedInsert(j[5]),2);
		assertEquals(list.getInsertPosition(j[4]),4);
	}
	
	public static TestSuite suite(){
		return new TestSuite(TestPriorityLinkedList.class);
	}
	
	private TaskSet importTaskset(){
		
		TaskSet ts = new TaskSet(6);
		
		Task t = new Task("T0");
		TaskParameters tp = new TaskParameters();
		tp.setComputation(2,TimeUnit.MILLISECONDS);
		tp.setDeadline(10,TimeUnit.MILLISECONDS);
		tp.setPeriod(10,TimeUnit.MILLISECONDS);
		tp.setStaticPriority(4);
		tp.setThreshold(tp.getStaticPriority());
		t.setTaskParameters(tp);
		ts.addTask(t);
		
		t = new Task("T1");
		tp = new TaskParameters();
		tp.setComputation(6,TimeUnit.MILLISECONDS);
		tp.setDeadline(20,TimeUnit.MILLISECONDS);
		tp.setPeriod(20,TimeUnit.MILLISECONDS);
		tp.setStaticPriority(3);
		tp.setThreshold(tp.getStaticPriority());
		t.setTaskParameters(tp);
		ts.addTask(t);
		
		t = new Task("T2");
		tp = new TaskParameters();
		tp.setComputation(2,TimeUnit.MILLISECONDS);
		tp.setDeadline(30,TimeUnit.MILLISECONDS);
		tp.setPeriod(30,TimeUnit.MILLISECONDS);
		tp.setStaticPriority(2);
		tp.setThreshold(tp.getStaticPriority());
		t.setTaskParameters(tp);
		ts.addTask(t);
		
		t = new Task("T3");
		tp = new TaskParameters();
		tp.setComputation(13,TimeUnit.MILLISECONDS);
		tp.setDeadline(40,TimeUnit.MILLISECONDS);
		tp.setPeriod(40,TimeUnit.MILLISECONDS);
		tp.setStaticPriority(1);
		tp.setThreshold(tp.getStaticPriority());
		t.setTaskParameters(tp);
		ts.addTask(t);
		
		t = new Task("T4");
		tp = new TaskParameters();
		tp.setComputation(17,TimeUnit.MILLISECONDS);
		tp.setDeadline(50,TimeUnit.MILLISECONDS);
		tp.setPeriod(50,TimeUnit.MILLISECONDS);
		tp.setStaticPriority(0);
		tp.setThreshold(tp.getStaticPriority());
		t.setTaskParameters(tp);
		ts.addTask(t);
		
		t = new Task("PrioEqualToT2");
		tp = new TaskParameters();
		tp.setComputation(17,TimeUnit.MILLISECONDS);
		tp.setDeadline(50,TimeUnit.MILLISECONDS);
		tp.setPeriod(50,TimeUnit.MILLISECONDS);
		tp.setStaticPriority(2);
		tp.setThreshold(tp.getStaticPriority());
		t.setTaskParameters(tp);
		ts.addTask(t);
		
		return ts;
	}

}
