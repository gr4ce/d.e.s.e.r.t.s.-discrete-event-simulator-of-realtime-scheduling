/**
 * 
 */
package test;

import static org.junit.Assert.*;

import org.junit.Test;

import simulation.SimulationResults;

import event.Event;
import event.EventManager;
import event.EventType;
import event.TimeLineTransactionalLinkedList;

/**
 * @author Giuseppe
 *
 */
public class TestTransactionalEventList {

	
	@Test
	public void testInsert() {
		
		TimeLineTransactionalLinkedList el = new TimeLineTransactionalLinkedList();
		EventManager em = new EventManager();
		el.setEventFactory(em.getEventFactory());
		
		SimulationResults.setTableHeader(String.format("%-25s%-20s%-12s%-20s","EVENT","STATE","TRIGG_TIME","TARGET"));
		SimulationResults.setTableRowFormatForSingleEvent("%-25s%-20s%-12d%-20s");
		SimulationResults.setTableRowFormatForMultipleEvent("%12s  %-25s%-20s%-12s%-20s");
		
		Event release1    = em.getEventFactory().getEventInstance(1, EventType.JOB_RELEASE,    null);
		Event release2    = em.getEventFactory().getEventInstance(2, EventType.JOB_RELEASE,    null);
		Event release3    = em.getEventFactory().getEventInstance(3, EventType.JOB_RELEASE,    null);
		Event deadline2   = em.getEventFactory().getEventInstance(2, EventType.JOB_DEADLINE,   null);
		Event completion3 = em.getEventFactory().getEventInstance(3, EventType.JOB_COMPLETION, null);
		
		/*
		 * Il test deve essere valido anche nel momento in cui gli inserimenti
		 * sono fatti in maniera invertita: in questo caso per� a parit� di priorit�
		 * prevarr� l'ordine di inserimento.
		 */		
		
		el.trigger(release1);
		el.trigger(release3);
		el.trigger(release2);		
		
		Event e = el.getFirst();
		assertEquals(release1, e);
		e = el.getNext(e); 
		assertEquals(release2, e);
		e = el.getNext(e);
		assertEquals(release3, e);
		e = el.getNext(e);
		assertEquals(null, e);
	
				
		/*
		 * L'ordinamento atteso �
		 * 
		 * tempo   	evento
		 * 1		release1
		 * 2		deadline2
		 * 2		release2 
		 * 3		release3 
		 * 
		 * Pure essendoci due eventi innescati allo stesso istante
		 * non deve essere predisposta una transazione
		 */
		el.trigger(deadline2);
		
		e = el.getFirst();
		assertEquals(release1, e);
		e = el.getNext(e); 
		assertEquals(deadline2, e);
		e = el.getNext(e); 
		assertEquals(release2, e);
		e = el.getNext(e);
		assertEquals(release3, e);
		e = el.getNext(e);
		assertEquals(null, e);
		
		
		/*
		 * L'ordinamento atteso �
		 * 
		 * tempo   	evento
		 * 1		release1
		 * 2		deadline2
		 * 2		release2 
		 * 3		START_TRANSACTION
		 * 3		completion3
		 * 3		release3 
		 * 3		END_TRANSACTION
		 * 
		 * Pure essendoci due eventi innescati allo stesso istante
		 * non deve essere predisposta una transazione
		 */
		el.trigger(completion3);
		
		System.out.println(completion3.getEventType() + " " +completion3.getEventType().getTypePriority());
		System.out.println(release3.getEventType() + " " +release3.getEventType().getTypePriority());
		
		e = el.getFirst();
		assertEquals(release1, e);
		
		e = el.getNext(e); 
		assertEquals(deadline2, e);
		
		e = el.getNext(e); 
		assertEquals(release2, e);
		
		e = el.getNext(e); 
		assertEquals(EventType.START_TRANSACTION, e.getEventType());
						
		e = el.getNext(e);
		assertEquals(completion3, e);
		
		e = el.getNext(e);
		assertEquals(release3, e);		
		
		e = el.getNext(e); 
		assertEquals(EventType.END_TRANSACTION, e.getEventType());
		
		e = el.getNext(e);
		assertEquals(null, e);
		
	}

	
}
