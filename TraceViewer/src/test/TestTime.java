package test;

import java.util.Random;

import org.junit.Test;

import edu.unibo.ciri.desert.datastructure.TimeLineRedBlackTree;
import edu.unibo.ciri.realtime.model.time.LinuxTime;
import edu.unibo.ciri.realtime.model.time.TimeInstant;
import event.Event;
import event.EventType;
import junit.framework.TestCase;

public class TestTime extends TestCase {
	
	@Test
	public void testLinuxTime(){
		
		LinuxTime time = new LinuxTime();
		time.setSecondsValue(10);
		time.setNanoSecondsValue(200000);
		System.out.println(time);
	}
	
	@Test
	public void testTime(){
		
//		TimeInstant time = new TimeInstant();
//		time.setTimeValue(value, unit);
		
	}
	
	@Test
	public void testTimeEventList(){
		
		Event e1 = new Event();
		e1.setEventType(EventType.JOB_RELEASE);
		
//		Event e2a = new Event();
//		Event e2b = new Event();
//		Event e2c = new Event();
//		e2a.setEventType(EventType.JOB_RELEASE);
//		e2b.setEventType(EventType.JOB_COMPLETION);
//		e2c.setEventType(EventType.JOB_DEADLINE);		
//		Event e3 = new Event();
//		e3.setEventType(EventType.JOB_DEADLINE);
		
		TimeLineRedBlackTree list = new TimeLineRedBlackTree();
		
		Event e;
		
		int number_of_steps = 1000;
		int frequency_counter[] = new int[number_of_steps];
		int casual_event = 0;
		long casual_time  = 0;
		
		Random r = new Random();
		EventType et;
		
		Event special = new Event();
		special.setEventTimeValue(new TimeInstant(70));
		special.setEventType(EventType.JOB_PREEMPTION);
		
		for(int i = 0; i < number_of_steps; i++){
			
			e = new Event();
			casual_event = r.nextInt(3);
			casual_time = (long)r.nextInt(200);
			
			switch(casual_event){
				case 0:
					et = EventType.JOB_RELEASE;
					break;
				case 1:
					et = EventType.JOB_DEADLINE;
					break;
				case 2:
					et = EventType.JOB_COMPLETION;
					break;
				case 3:
					et = EventType.JOB_MIGRATION; 
				default:
					et = EventType.JOB_OVERRUN;
			}
			
			e.setEventType(et);
			list.trigger(casual_time, e);
			frequency_counter[(int)casual_time]++;
			
		}
		
		list.trigger(special);
		
//		list.trigger(2000, e2b);
//		list.trigger(3000, e3);
//		list.trigger(2000, e2a);
//		
//		list.trigger(2000, e2c);
//		list.trigger(1000, e1);
		
//		System.out.println("Situation after insertion of all events");
//		System.out.println("Events "+list.getEventNumber() );
//		System.out.println("Time instants "+ list.getTimeInstantNumber());
//		for(int i = 0; i < number_of_steps; i++)
//			System.out.println("For time " + i + " there are " + frequency_counter[i] + " events ");
		System.out.println(list);
		
//		System.out.println("Suppressing "+ e3.getEventTimeValue() + " " + e3.getEventType());
//		list.suppress(e3);
		
		System.out.println("Starting pop events");
		assertTrue(list.contains(special));
		e = new Event();
		e.setEventType(EventType.DELAYED_RELEASE);
		e.setEventTimeValue(new TimeInstant(100));
		assertFalse(list.contains(e));
		
//		while(list.getFirst() != null){
//			e = list.pop();			
//			System.out.println(e.getEventTimeValue() + " " + e.getEventType());
//		}
		
		
		
		
		
	}

}
