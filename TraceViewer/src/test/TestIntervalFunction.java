package test;

import java.util.concurrent.TimeUnit;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.junit.Before;
import org.junit.Test;


import task.Job;
import task.Task;
import task.TaskParameters;
import task.TaskSet;
import util.IntervalFunction;
import util.PriorityLinkedList;
import util.ValuedTimeInstant;


public class TestIntervalFunction extends TestCase{
	
	PriorityLinkedList<Job> list;
	TaskSet taskSet;
	Job j[];
	

	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void testDefinition() {	
		
		IntervalFunction periodicFunction = new IntervalFunction();
		periodicFunction.setPeriod(3000);
		periodicFunction.setOffset(0);
		periodicFunction.setMultiply(1);
		ValuedTimeInstant v1,v2;
		v1 = new ValuedTimeInstant(1, TimeUnit.MILLISECONDS, 1);
		v2 = new ValuedTimeInstant(2, TimeUnit.MILLISECONDS, 0);
		periodicFunction.addValue(v1);
		periodicFunction.addValue(v2);
		
		System.out.println(periodicFunction.toString());
		
		IntervalFunction systemFunction = new IntervalFunction();
//		systemFunction.setOffset(0);
//		systemFunction.setPeriod(1);
		v1 = new ValuedTimeInstant(0, TimeUnit.MILLISECONDS, 2);
		systemFunction.addValue(v1);
		System.out.println(systemFunction);
		
	}

	

}
