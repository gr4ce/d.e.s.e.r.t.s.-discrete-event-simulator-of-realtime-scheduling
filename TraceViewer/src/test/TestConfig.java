/**
 * 
 */
package test;

import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Test;

import edu.unibo.ciri.desert.config.Configuration;
import edu.unibo.ciri.desert.config.InvalidPropertyConfigurationException;


/**
 * @author Giuseppe
 *
 */
public class TestConfig extends TestCase{

	/**
	 * 
	 */
	@Test
	public void testConfigurationFile() {
		
		try {
			Configuration.loadConfigurationFromFile();
		} catch (IOException e) {
			e.printStackTrace();
			assertTrue(false);
		} catch (InvalidPropertyConfigurationException ipce){
			ipce.printStackTrace();
			assertTrue(false);
		}
		
		System.out.println(Configuration.getSchedulingPolicyName());
		System.out.println(Configuration.getSchedulingPolicyType());
		System.out.println(Configuration.getStaticPartitioningPolicyName());
		System.out.println(Configuration.getStaticSortingPolicyName());
		System.out.println(Configuration.getParallelSimulationThreads());
		System.out.println(Configuration.getDatasetFileNames()[4]);
		
		
	}

	
}
