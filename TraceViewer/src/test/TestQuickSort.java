package test;

import java.util.Comparator;
import java.util.Random;

import org.junit.Test;

import edu.unibo.ciri.util.sort.algorithm.QuickSort;

import util.SortingAlgorithm.SortingOrder;

import junit.framework.TestCase;

public class TestQuickSort extends TestCase {
	
	

	
	@Test
	public void testQuickSortAsObject(){
		
		int testArraySize = 10;
		
		Integer array[] = new Integer[testArraySize];
		Random r = new Random();
		
		for(int j = 0; j < testArraySize; j++){
			array[j] = Math.abs(r.nextInt());
		}
		
//		System.out.println("Unsorted array");
//		for(Integer i: array){			
//			System.out.println(i);
//		}
		
		class CompInt implements Comparator<Integer>{

			@Override
			public int compare(Integer arg0, Integer arg1) {
				
				if(arg0 > arg1)
					return 1;
				
				if(arg0 < arg1)
					return -1;
				
				return 0;
			}
			
		}
		
			
		QuickSort<Integer> qs = new QuickSort<Integer>( new CompInt(), array );
		
		qs.sort(SortingOrder.NON_DECREASING_ORDER);		
		
		System.out.println("Sorted array in non decreasing order");
		for(Integer i: array){
			System.out.println(i);
		}
		
		assertTrue(qs.isSorted(array,SortingOrder.NON_DECREASING_ORDER));
		assertFalse(qs.isSorted(array,SortingOrder.NON_INCREASING_ORDER));
		
		qs.sort(SortingOrder.NON_INCREASING_ORDER);
		
		System.out.println("Sorted array in non increasing order");
		for(Integer i: array){
			System.out.println(i);
		}
		
		assertTrue(qs.isSorted(array,SortingOrder.NON_INCREASING_ORDER));
		assertFalse(qs.isSorted(array,SortingOrder.NON_DECREASING_ORDER));
		
	}
	

}
