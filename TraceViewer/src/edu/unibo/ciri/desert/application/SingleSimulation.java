package edu.unibo.ciri.desert.application;

import edu.unibo.ciri.desert.config.Configuration;
import edu.unibo.ciri.desert.resource.VirtualPlatform;
import edu.unibo.ciri.desert.scheduler.TimeStepFullPreemptiveScheduler;
import edu.unibo.ciri.desert.simulation.management.OffsetGenerator;
import edu.unibo.ciri.desert.simulation.management.Simulation;
import edu.unibo.ciri.desert.simulation.management.SimulationException;
import edu.unibo.ciri.realtime.model.DataSet;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.model.io.TextDataSetReader;
import edu.unibo.ciri.realtime.policies.scheduling.GlobalEDFSchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.FixedPrioritySchedulingPolicy;
import edu.unibo.ciri.realtime.policies.staticsorting.RMSortingPolicy;



public class SingleSimulation {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		TextDataSetReader reader = new TextDataSetReader();
		reader.setFileName(Configuration.getDatasetFileNames()[0]);
		reader.readDataSet();
		
		DataSet ds = reader.getDataSet();
				
		System.out.println("Readed " + ds.size() + " taskset");
		
		Simulation sim = new Simulation();
		sim.setPlatform(new VirtualPlatform(2));
		sim.setScheduler(new TimeStepFullPreemptiveScheduler(sim));
		
		
		TaskSet ts = ds.next();		
		GlobalEDFSchedulingPolicy sp = new GlobalEDFSchedulingPolicy();
		
		
		/*
		 * Uncomment to enable fixed priority scheduling 
		 */
//		RMSortingPolicy rm = new RMSortingPolicy();
//		FixedPrioritySchedulingPolicy sp = new FixedPrioritySchedulingPolicy(rm);
//		sp.setPriorityAssignment(rm.getStaticPriorityAssignment(ts));
		
		sim.setSchedulingPolicy(sp);
		
		OffsetGenerator gen = new OffsetGenerator(ts);		
		
		
		while(gen.hasNext()){
			
			gen.getNext().applyOffset(ts);
			sim.setTaskset(ts);	
			
			try {
				sim.init();
			} catch (SimulationException e) {
				e.printStackTrace();
			}
			
			try {
				sim.start();
			} catch (SimulationException e) {
				e.printStackTrace();
			}
		}
			
		
				
		
		
		
		
	}
	
	
	
	
}
