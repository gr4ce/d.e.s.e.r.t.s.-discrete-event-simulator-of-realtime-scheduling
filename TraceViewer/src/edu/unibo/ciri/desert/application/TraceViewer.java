package edu.unibo.ciri.desert.application;



import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.io.File;

import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.concurrent.TimeUnit;
import javax.swing.UIManager;

import edu.unibo.ciri.collection.PriorityLinkedList;
import edu.unibo.ciri.desert.event.EventType;
import edu.unibo.ciri.desert.event.RTAIEvent;
import edu.unibo.ciri.desert.graphics.diagram.schedule.ScheduleDiagramContainer;
import edu.unibo.ciri.desert.graphics.diagram.schedule.ScheduleDiagramParam;
import edu.unibo.ciri.desert.graphics.diagram.schedule.drawers.GridDrawer;
import edu.unibo.ciri.desert.graphics.diagram.schedule.drawers.TaskLegendaDrawer;
import edu.unibo.ciri.desert.graphics.diagram.schedule.drawers.TaskScheduleDiagramDrawer;
import edu.unibo.ciri.desert.resource.SharedResource;
import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.model.io.TasksetFileParser;
import edu.unibo.ciri.realtime.model.io.TraceFileParser;
import edu.unibo.ciri.realtime.model.time.Time;


public class TraceViewer extends JFrame {

	/**
	 * 
	 */
	private static final long 			serialVersionUID = 1L;
	private JPanel 						contentPane;
	private ScheduleDiagramContainer 	container;
	private static String				traceFileName;
	public static JFrame frame;
	
	private static Dimension size;
	private static Dimension preferredSize;
	
	ArrayList<TaskSet> 	tasksets 	= new ArrayList<TaskSet>();
	ArrayList<Thread> 	threads 	= new ArrayList<Thread>();
	
	int 				currentTaskset 	= -1;
	int 				runningThread 	= 0;
	
	TaskSet 			ts;
	TasksetFileParser 	taskFileParser;
	
	private Task runningTask;
	private PriorityLinkedList<Task> readyTasks = new PriorityLinkedList<Task>();
	private PriorityLinkedList<Task> blockedTasks = new PriorityLinkedList<Task>();
	boolean pendingRequest = false;
	private Task pendingTask = null;
	


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			if(args.length > 0)
				traceFileName = args[0];			
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TraceViewer frame = new TraceViewer(traceFileName);
					frame.setSize(950, 190);
					frame.setPreferredSize(new Dimension(900,190));
					frame.setTitle("TraceViewer");
					frame.setResizable(true);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}
	
	public void setDiagramParam(TaskSet ts){
		
		this.ts = ts;
		
		ScheduleDiagramParam param = new ScheduleDiagramParam(ts, -1);
		param.setDuration(200);
		
		container.setScheduleDiagramParam(param);
				
	}
	
	

	/**
	 * Create the frame.
	 */
	public TraceViewer(String traceFileName) {
		
		frame = this;
		
		if(traceFileName == null){
			JFileChooser jfc = new JFileChooser();
			int answer = jfc.showOpenDialog(this);
			if(answer == JFileChooser.APPROVE_OPTION){
				File selected = jfc.getSelectedFile();
				traceFileName = selected.getAbsolutePath();
			}
			
		}
		 
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocation(0, 0);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		
		/*
		 * The very first thing is to parse the filename 
		 */
		TraceFileParser traceFileParser = new TraceFileParser(traceFileName);
		TaskSet taskSet = new TaskSet();
				
		container = new ScheduleDiagramContainer();
		
		GridDrawer 					grid 			 = new GridDrawer(container);		
		TaskScheduleDiagramDrawer	scheduleDiagram  = new TaskScheduleDiagramDrawer(container);
		TaskLegendaDrawer			taskLegendDrawer = new TaskLegendaDrawer(container);
		
				
		container.setGrid(grid);
		container.setTaskLegendDrawer(taskLegendDrawer);
		container.setScheduleDiagramDrawer(scheduleDiagram);
		
		
		scheduleDiagram.setProcessor(1);
		taskLegendDrawer.setLegendWidth(100);		

										
		Time.setResolution(TimeUnit.NANOSECONDS);	
		
				
		Hashtable<Task, PriorityLinkedList<RTAIEvent>> events = traceFileParser.parse();
		Hashtable<String, Task> tasks = traceFileParser.getTasks();
		assert(events != null);
		
		for(SharedResource r: traceFileParser.getResources().values()){
			scheduleDiagram.addResource(r);
		}
		
		int position = 0;		
		
		for(String name:traceFileParser.getTaskNames()){
			
			if(!name.equals("ALL") || !name.equals("MAIN")){
				System.out.println("Trovato task " + name);
				scheduleDiagram.associateByName(name, position);
				tasks.get(name).setTaskId(position);
				tasks.get(name).setStaticPriority(position);
				scheduleDiagram.associateById(position,position++);
				taskSet.addTask(tasks.get(name));				
			}
		}
		
			
		ScheduleDiagramParam scheduleParam = new ScheduleDiagramParam(taskSet.size(), 1);
		container.setScheduleDiagramParam(scheduleParam);
		container.getScheduleDiagramParam().setTaskset(taskSet);
		
		PriorityLinkedList<RTAIEvent> rtaiOriginalEvts = events.get(tasks.get("ALL"));
		PriorityLinkedList<RTAIEvent> rtaiCopyEvents = new PriorityLinkedList<RTAIEvent>();
		
		TaskSet ts = traceFileParser.getTaskset();
		
		Time applicationStop = new Time(0);
		RTAIEvent evt = rtaiOriginalEvts.getFirst();
		while(evt  != null){
			if(evt.getType() == EventType.APPLICATION_STOP){
				applicationStop = evt.getTime();
				evt = rtaiOriginalEvts.getNext(evt);
				
				for(Task t: ts.getArrayList()){
					Time currentTime = t.getTaskParameters().getOffset();
					RTAIEvent release = new RTAIEvent(t.getName(), EventType.JOB_RELEASE, currentTime, t);
					rtaiCopyEvents.orderedInsert(release);		
					int i = 0;
					while(currentTime.getTimeValue() <= applicationStop.getTimeValue() && i < 1){
						currentTime = new Time(currentTime.getTimeValue() + t.getTaskParameters().getPeriod().getTimeValue());
						release = new RTAIEvent(t.getName(), EventType.JOB_RELEASE, currentTime, t);
						rtaiCopyEvents.orderedInsert(release);
						i++;
					}					
				}
				
				continue;
			}
			rtaiCopyEvents.orderedInsert(evt);			
			evt = rtaiOriginalEvts.getNext(evt);			
		}		
				
		evt = rtaiOriginalEvts.getFirst();
		
			
		
		evt = rtaiCopyEvents.getFirst();
		while(evt  != null){			
			container.addElement(evt);
			evt = rtaiCopyEvents.getNext(evt);
		}
		
		
		/*
		 * Just prints events 
		 */
		evt = rtaiCopyEvents.getFirst();
		while(evt  != null){		
			System.out.println(evt);
			evt = rtaiCopyEvents.getNext(evt);			
		}
		
		
		JScrollPane scrollPane = new JScrollPane(container/*,ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED*/);		
		scrollPane.setAutoscrolls(true);
				
		container.setAutoscrolls(true);
		contentPane.add(scrollPane);
		contentPane.setAutoscrolls(true);
		
		int time  = (int)applicationStop.getTimeValue(TimeUnit.MILLISECONDS);
		int pfu   = container.getScheduleDiagramParam().getPixelForWidthCol();
		int nTask = 3;
		
		container.getScheduleDiagramParam().setDuration(applicationStop.getTimeValue(TimeUnit.MILLISECONDS));
		preferredSize = new Dimension(pfu*time + pfu*10, pfu*nTask + 1);
		size = new Dimension(pfu*time + pfu*10, pfu*nTask + 1);
//		container.setPreferredSize(preferredSize);
//		container.setSize(size);


	}
	
	private RTAIEvent handleTaskStart(RTAIEvent e){
		
		RTAIEvent preemption = null;
		
		if(runningTask != null){
			readyTasks.orderedInsert(runningTask);
			preemption = new RTAIEvent(runningTask.getName(), EventType.JOB_PREEMPTION, e.getTime(), runningTask);

			runningTask = e.getTask();
			return preemption;
		}	
		
		runningTask = e.getTask();
		return null;
		
			
	}
	

	
	private RTAIEvent handleTaskCompletion(RTAIEvent e){
		
		RTAIEvent resume = null;
		
		if(readyTasks.size() > 0){
			runningTask = readyTasks.popHead();			
			resume = new RTAIEvent(runningTask.getName(), EventType.JOB_RESUME, e.getTime(), runningTask);		
			return resume;
		}	
		
		runningTask = null;
		return null;
	}
	
	
	
	
	
	
}