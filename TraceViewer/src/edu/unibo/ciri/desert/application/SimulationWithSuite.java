package edu.unibo.ciri.desert.application;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.concurrent.TimeUnit;



import edu.unibo.ciri.desert.config.Configuration;
import edu.unibo.ciri.desert.config.InvalidPropertyConfigurationException;
import edu.unibo.ciri.desert.config.SimulationConfig;
import edu.unibo.ciri.desert.config.SuiteConfig;
import edu.unibo.ciri.desert.simulation.management.SimulationSuite;
import edu.unibo.ciri.desert.simulation.management.SimulationSuiteZL;
import edu.unibo.ciri.desert.simulation.results.SimulationResults;
import edu.unibo.ciri.realtime.model.DataSet;
import edu.unibo.ciri.realtime.model.io.TextDataSetSortedReader;
import edu.unibo.ciri.realtime.model.time.Time;

public class  SimulationWithSuite {
		
	
	public static void main(String[] args){
		
		Time.setResolution(TimeUnit.MICROSECONDS);			
				
		TextDataSetSortedReader reader = new TextDataSetSortedReader();
		
		try {
			Configuration.loadConfigurationFromFile();
		} catch (InvalidPropertyConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String datasetPath = Configuration.getDatasetFileNames()[0];
		
		reader.setFileName(datasetPath);
		reader.readDataSet();
		
        
		int parallelSimulation = Configuration.getParallelSimulationThreads();
		
		DataSet ds = reader.getDataSet();
				
		System.out.println("Readed " + ds.size() + " taskset");
		
		SuiteConfig suiteConf = new SuiteConfig();
						
		suiteConf.setParallelSimulation(parallelSimulation);
		
		SimulationConfig[] conf = new SimulationConfig[parallelSimulation];
		for(int i = 0; i < parallelSimulation; i++){
			conf[i] = new SimulationConfig();
			conf[i].setProcessors(2);
		}
				
		SimulationSuite ss = new SimulationSuite(suiteConf, conf, ds);
//		SimulationSuiteZL ss = new SimulationSuiteZL(suiteConf, conf, ds);
		
		ArrayList<SimulationResults> results = ss.getResults();
		
		try {		
		    
		    Hashtable<String,Integer> resultTable = new Hashtable<String,Integer>();
		
			for(int i = 0; i < results.size(); i++){
				
				System.out.println(results.get(i).getTaskSet().getTasksetName() 
						+ "\t " + (results.get(i).getTaskSet().getUtilizationString())
						+ "\t " + (results.get(i).getEndSimTime() - results.get(i).getStartSimTime())
						+ "\t " + results.get(i).getTotalPreemption()
						+ "\t " + results.get(i).getTotalMissed());
				
								
				
				//se non � gi� presente, lo dovr� creare nella hashtable, con valore 0/1 a seconda del "successo"
				if( ! resultTable.containsKey( results.get(i).getTaskSet().getUtilizationString())){		
					if (results.get(i).getTotalMissed()==0){		
						resultTable.put(results.get(i).getTaskSet().getUtilizationString(), 1);	
					}
					else{
						resultTable.put(results.get(i).getTaskSet().getUtilizationString(), 0);
					}
				}
				//se gi� presente, e non ha mancato deadline lo conteggio incrementando il valore nella hashtable
				else{											
					if (results.get(i).getTotalMissed()==0){
						int vecchio = (Integer) resultTable.get( results.get(i).getTaskSet().getUtilizationString() );
						resultTable.put(results.get(i).getTaskSet().getUtilizationString(), vecchio+1);
					}

				}
						
			}
			
			
			System.out.println("U: "+resultTable.toString());
			
			
			
		}catch (Exception e) {	
			System.err.println ("Error in writing to file result");	
			e.printStackTrace();
		}//catch
		
	}//main
	
}//class

