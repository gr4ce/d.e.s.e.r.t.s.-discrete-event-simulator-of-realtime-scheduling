package edu.unibo.ciri.desert.application;



import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;


import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import javax.swing.UIManager;

import edu.unibo.ciri.desert.config.SimulationConfig;
import edu.unibo.ciri.desert.event.EventType;
import edu.unibo.ciri.desert.graphics.diagram.schedule.ScheduleDiagramContainer;
import edu.unibo.ciri.desert.graphics.diagram.schedule.ScheduleDiagramParam;
import edu.unibo.ciri.desert.graphics.diagram.schedule.drawers.GridDrawer;
import edu.unibo.ciri.desert.graphics.diagram.schedule.drawers.TaskScheduleDiagramDrawer;
import edu.unibo.ciri.desert.simulation.management.Simulation;
import edu.unibo.ciri.desert.simulation.management.SimulationException;
import edu.unibo.ciri.desert.simulation.results.SimulationResults;
import edu.unibo.ciri.realtime.model.DataSet;
import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.model.io.TasksetFileParser;
import edu.unibo.ciri.realtime.model.io.TextDataSetReader;
import edu.unibo.ciri.realtime.model.time.EventCollector;
import edu.unibo.ciri.realtime.model.time.Time;
import edu.unibo.ciri.realtime.policies.scheduling.GlobalEDFSchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.FixedPriorityAssignment;
import edu.unibo.ciri.realtime.policies.scheduling.FixedPrioritySchedulingPolicy;
import edu.unibo.ciri.realtime.policies.staticsorting.RMSortingPolicy;


public class SingleGraphicalSimulation extends JFrame {

	/**
	 * 
	 */
	private static final long 			serialVersionUID = 1L;
	private JPanel 						contentPane;
	private ScheduleDiagramContainer 	container;
	
	ArrayList<TaskSet> 	tasksets 	= new ArrayList<TaskSet>();
	ArrayList<Thread> 	threads 	= new ArrayList<Thread>();
	
	int 				currentTaskset 	= -1;
	int 				runningThread 	= 0;
	
	TaskSet 			ts;
	TasksetFileParser 	taskFileParser;
	
//	private JScrollBar scrollBar;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SingleGraphicalSimulation frame = new SingleGraphicalSimulation();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}
	
	public void setDiagramParam(TaskSet ts){
		
		this.ts = ts;
		
		ScheduleDiagramParam param = new ScheduleDiagramParam(ts, -1);
		param.setDuration(200);
		
		container.setScheduleDiagramParam(param);
				
	}
	
	

	/**
	 * Create the frame.
	 */
	public SingleGraphicalSimulation() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocation(0, 0);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		EventType[] values = EventType.values();
		for(int i = 0; i < values.length; i++){
			System.out.println(values[i].ordinal() + " " +values[i].name());
		}
		
		container = new ScheduleDiagramContainer();
		
		GridDrawer 					grid 			= new GridDrawer(container);		
		TaskScheduleDiagramDrawer	scheduleDiagram = new TaskScheduleDiagramDrawer(container);
		
		container.setGrid(grid);
		container.setScheduleDiagramDrawer(scheduleDiagram);				
		
		SimulationResults.setTableHeader(String.format("%-25s%-20s%-12s%-20s","EVENT","STATE","TRIGG_TIME","TARGET"));
		SimulationResults.setTableRowFormatForSingleEvent("%-25s%-20s%-12d%-20s");
		SimulationResults.setTableRowFormatForMultipleEvent("%12s  %-25s%-20s%-12s%-20s");
						
		Time.setResolution(TimeUnit.MICROSECONDS);		
		
		TextDataSetReader reader = new TextDataSetReader();
		reader.setFileName("D:/target/input/dataset/current.dat");
//		reader.setFileName("k:/target/input/dataset/current.dat");
		reader.readDataSet();
		
		DataSet ds = reader.getDataSet();
		TaskSet ts = ds.next();
//		ts.getTask(0).getTaskParameters().getOffset().setTimeValue(0);
//		ts.getTask(1).getTaskParameters().getOffset().setTimeValue(0);
//		ts.getTask(2).getTaskParameters().getOffset().setTimeValue(0);
	
					
		System.out.println("Readed " + ds.size() + " taskset");
		
		Simulation sim = new Simulation();
			
		RMSortingPolicy rm = new RMSortingPolicy();
		
		
		/*
		 * Uncomment this block of code for scheduling with RM policy
		 */
//		FixedPriorityAssignment fpa = rm.getStaticPriorityAssignment(ts);
//		FixedPrioritySchedulingPolicy fps = new FixedPrioritySchedulingPolicy(rm);
//		fps.setPriorityAssignment(fpa);
//		sim.setSchedulingPolicy(fps);

		
		/*
		 * Uncomment this block of code for scheduling with PTS policy
		 */
//		FixedPriorityAssignment fpa = rm.getStaticPriorityAssignment(ts);
//		PreemptionThresholdSchedulingPolicy pts = new PreemptionThresholdSchedulingPolicy(rm);
//		pts.setPriorityAssignment(fpa);
//		PreemptionThresholdAssignment pta = new PreemptionThresholdAssignment(ts.getSize());
//		for(int i = 0; i < ts.getSize(); i++){
//			
//			Task t = fpa.getTaskByPriority(i);
//			System.out.print(t.getName() + "\t");
//			System.out.print(i + "\t");
//			System.out.print(pta.getThreshold(t.getTaskId()) + "\t");
//			System.out.println(t.getTaskId() + "\t");	
//			
//		}
		

		/*
		 * Uncomment this block of code for scheduling with FPZL policy
		 */
//		rm	= new RMSortingPolicy();		
//		FixedPriorityAssignment fpa = rm.getStaticPriorityAssignment(ts);
//		ZeroLaxityHandler zlHandler = new ZeroLaxityHandler(ts.getSize(), sim);
//		FixedPriorityZeroLaxitySchedulingPolicy fpzl = new FixedPriorityZeroLaxitySchedulingPolicy(rm, zlHandler);
//		fpzl.setPriorityAssignment(fpa);
//		sim.setSchedulingPolicy(fpzl);		


		/*
		 * Uncomment this block of code for scheduling with EDF policy
		 */
		GlobalEDFSchedulingPolicy edf = new GlobalEDFSchedulingPolicy();
		sim.setSchedulingPolicy(edf); 
		
		
		/*
		 * Uncomment this block of code for scheduling with EDZL policy  (and listeners under...)
		 */
//		preemptionthresholdschedulingpolicy pts = new preemptionthresholdschedulingpolicy(rm);
//		fixedpriorityassignment fpa = rm.getstaticpriorityassignment(ts);
//		pts.setpriorityassignment(fpa);
//		preemptionthresholdassignment pta = new preemptionthresholdassignment(ts.getsize());
//		for(int i = 0; i < ts.getsize(); i++){
//			if(ts.gettask(i).getname().equals("task_1"))
//				pta.addtaskthresholdpair(ts.gettask(i), 1);
//			else
//				pta.addtaskthresholdpair(ts.gettask(i), fpa.getpriority(ts.gettask(i)));
//		}
//		pts.setpreemptionthresholdassigment(pta);
//		zerolaxityhandler zlhandler = new zerolaxityhandler(ts.getsize(), sim);
//		earliestdeadlinezerolaxityschedulingpolicy edzl = new earliestdeadlinezerolaxityschedulingpolicy(zlhandler);
//		sim.setschedulingpolicy(edzl);	
		
		/*
		 * Uncomment this block of code for scheduling with FPZL policy (and listeners under...)
		 */
//		FixedPriorityAssignment fpa = rm.getStaticPriorityAssignment(ts);
//		PreemptionThresholdAssignment pta = new PreemptionThresholdAssignment(ts.getSize());
//		ZeroLaxityHandler zlHandler = new ZeroLaxityHandler(ts.getSize(), sim);
//		FixedPriorityZeroLaxitySchedulingPolicy fpsp 	= new FixedPriorityZeroLaxitySchedulingPolicy(rm, zlHandler);
//		fpsp.setPriorityAssignment(fpa);
//		sim.setSchedulingPolicy(fpsp);
		
		sim.setTaskset(ts);
		
		System.out.println(ts.toString());		
		
		/*
		 * Questa sezione di codice serve per ottenere i task ordinati
		 * secondo RM. Le priorit� del taskset non vengono toccate.
		 * La necessit� � quella di avere un ordine con cui visualizzare
		 * i task nel diagramma temporale. In alto ci sar� quello a periodo minimo 
		 */
		FixedPriorityAssignment 	spa = rm.getStaticPriorityAssignment(ts);
		Task[] sortedByRMPolicyTasks 	= spa.getSortedTaskArray();
		for(int i = 0; i < sortedByRMPolicyTasks.length; i++){
			scheduleDiagram.associateTaskToRow(sortedByRMPolicyTasks[i], i);
		}
		
		SimulationConfig sc = new SimulationConfig();
		sc.setProcessors(2);
		sc.setEnableTracingEvent(true);
		sc.setEnableTracingTime(true);
		sim.setSimulationConfig(sc);
		
		
		
		EventCollector ec = new EventCollector();
		sim.setEventCollector(ec);
		ec.setScheduleDiagramContainer(container);
		scheduleDiagram.setProcessor(sim.getSimulationConfig().getProcessors());
//		egd.setScheduleDiagramContainer(container);
		setDiagramParam(ts);
		
		try {
			sim.init();
		} catch (SimulationException e) {
			e.printStackTrace();
		}
		
		
		
		
		
//		sim.getEventFactory().setInterestedListenerToEventType(EventType.JOB_START, pts);
//		sim.getEventFactory().setInterestedListenerToEventType(EventType.JOB_PREEMPTION, pts);
//		sim.getEventFactory().setInterestedListenerToEventType(EventType.JOB_RESUME, pts);
//		sim.getEventFactory().setInterestedListenerToEventType(EventType.JOB_COMPLETION, pts);
		
//		sim.getEventFactory().setInterestedListenerToEventType(type, listener)
		
		
		/*
		 *  Uncomment this block of code for scheduling with ZERO LAXITY policy
		 */
//		sim.getEventFactory().setInterestedListenerToEventType(EventType.JOB_RELEASE, 			zlHandler);			
//		sim.getEventFactory().setInterestedListenerToEventType(EventType.JOB_START, 			zlHandler);			
//		sim.getEventFactory().setInterestedListenerToEventType(EventType.JOB_RESUME, 			zlHandler);			
//		sim.getEventFactory().setInterestedListenerToEventType(EventType.JOB_WAIT, 				zlHandler);			
//		sim.getEventFactory().setInterestedListenerToEventType(EventType.JOB_PREEMPTION,		zlHandler);			
//		sim.getEventFactory().setInterestedListenerToEventType(EventType.JOB_COMPLETION, 		zlHandler);			
//		sim.getEventFactory().setInterestedListenerToEventType(EventType.JOB_ZERO_LAXITY, 		zlHandler);			
//		sim.getEventFactory().setInterestedListenerToEventType(EventType.JOB_DEADLINE, 			zlHandler);	

		long start = System.currentTimeMillis();
		
		try {
			sim.start();
		} catch (SimulationException e) {
			e.printStackTrace();
		}
		
		long duration = System.currentTimeMillis() - start;
		
		System.out.println("Duration = "+duration);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		System.out.println(sim.getTimeLine().toString());
		
		
		JScrollPane scrollPane = new JScrollPane(container/*,ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED*/);		
		scrollPane.setAutoscrolls(true);
		
		
		
		container.setAutoscrolls(true);
		contentPane.add(scrollPane);
		contentPane.setAutoscrolls(true);
		
		int time  = (int)ts.getHyperperiod().getTimeValue(TimeUnit.MILLISECONDS);
//		int time  = 200;
		int pfu   = container.getScheduleDiagramParam().getPixelForWidthCol();
		int nTask = ts.size();
		
		container.setPreferredSize(new Dimension(pfu*time + pfu*10, pfu*nTask + 1));
		container.setSize(new Dimension(pfu*time + pfu*10, pfu*nTask + 1));
		

		
		

	}
	
	
	
	
	
	
}