package edu.unibo.ciri.desert.resource;

import edu.unibo.ciri.realtime.model.Job;

public class Processor extends Resource{
	
	public Processor(int resourceId) {
		super(resourceId);
	}
	
	public Processor(int resourceId, String name){
		super(resourceId,name,ResourceType.Processor);
	}	

	private Job lastRunning    = null;
	private Job currentRunning = null;	
	
	public Job currentRunning(){
		return currentRunning;
	}
	
	public void clear(){
		lastRunning = null;
		currentRunning = null;		
	}
	
	public void setRunning(Job j){
		if((currentRunning != null)/* && !(currentRunning.getTask().getName().equals("SCHEDULER"))*/)
			lastRunning = currentRunning;
		currentRunning = j;
		takeUp((int)j.getUniqueId());
	}
	
	public Job lastRunning(){
		return lastRunning;
	}
	
	public boolean isFree(){
		return !isBusy();
	}
	
	public void free(){
		super.free();
		if((currentRunning != null)/* && !(currentRunning.getTask().getName().equals("SCHEDULER"))*/)
			lastRunning = currentRunning;
		currentRunning = null;		
	}
	
	
	public boolean isBusy(){
		if(ownerId == -1)
			return false;
		return true;
	}

}
