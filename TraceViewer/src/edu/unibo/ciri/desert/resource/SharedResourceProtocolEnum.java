package edu.unibo.ciri.desert.resource;

public enum SharedResourceProtocolEnum {
	
	NO_PROTO,
	NON_PREEMPTIVE_CRITICAL_SECTION,
	FULL_PRIORITY_INHERITANCE,
	PRIORITY_CEILING,
	ADAPTIVE_DYNAMIC_PRIORITY_CEILING;
	
	private static String protocolNames[] = {
			"None",
			"Non Preemptive Critical Section",
			"Full priority inheritance",
			"Priority Ceiling",
			"Adaptive Dynamic Priority Ceiling"
	};
	
	private SharedResourceProtocolEnum(){
		
	}
	
	public static String getSharedResourceProtocolName(SharedResourceProtocolEnum proto){
		return protocolNames[proto.ordinal()];
	}
	
	public static SharedResourceProtocolEnum getSharedResourceProtocolEnum(String protocolName){
				
		for(SharedResourceProtocolEnum proto:  SharedResourceProtocolEnum.values()){
			if(protocolName.equals(SharedResourceProtocolEnum.getSharedResourceProtocolName(proto)))
					return proto;
		}
		
		return null;		
	}
	
	public static String[] getSharedResourceProtocolNames(){
		return protocolNames;
	}
	
	
	

}
