package edu.unibo.ciri.desert.resource;

import edu.unibo.ciri.realtime.model.Job;

public abstract class AbstractPlatform {
	
	private AllocationPolicy    policy;
	
	public abstract boolean 	isIdle();
	
	public abstract boolean 	isBusy();
	
	public abstract int 		getProcessorNumber();
	
	public abstract Job 		getJobRunningOn(int procId);
	
	public abstract void 		setJobRunningOn(Job j, int procId);
	
	public abstract Processor 	getProcessor(int index);

}
