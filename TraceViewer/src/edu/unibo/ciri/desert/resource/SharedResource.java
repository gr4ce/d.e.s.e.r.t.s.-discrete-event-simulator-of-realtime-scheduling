package edu.unibo.ciri.desert.resource;

import java.awt.Color;

public class SharedResource {
	
	private int    id;
	private String name;
	private Color  color;
	
	public SharedResource(int id, String name, Color c){
		setId(id);
		setName(name);
		this.color = c;
	}
	
	public Color getColor(){
		return color;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString(){
		return name + " " + id + " " + color;
	}
	
	

}
