package edu.unibo.ciri.desert.resource;

public abstract class Resource {
	
	public enum	ResourceType {Processor,ProcessorCluster,Platform,CriticalSection};
		
	public int 			resourceId;
	public String		resourceName;
	

	public ResourceType resourceType;	

	public int			ownerId;
		
	public Resource(int resourceId){		
		this.resourceId   = resourceId;
		this.ownerId      = -1;
	}	
	
	public Resource(int resourceId, String name, ResourceType type){
		this.resourceId   = resourceId;
		this.resourceName = name;
		this.resourceType = type;
		this.ownerId      = -1;
	}
	
	
	
	public int getResourceId(){
		return this.resourceId;
	}
	
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}
	
	public String getResourceName(){
		return this.resourceName;
	}
	
	public int getOwnerId(){
		return this.ownerId;
	}
	
	public abstract boolean isFree();
	
	public abstract boolean isBusy();
	
	public void takeUp(int ownerId){
		this.ownerId = ownerId;
	}
	
	public ResourceType getResourceType() {
		return resourceType;
	}

	public void setResourceType(ResourceType resourceType) {
		this.resourceType = resourceType;
	}
	
	public void free(){
		ownerId = -1;		
	}
	
	public void clear(){
		ownerId = -1;
	}

}
