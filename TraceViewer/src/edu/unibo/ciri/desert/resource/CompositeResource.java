package edu.unibo.ciri.desert.resource;

import java.util.List;

public abstract class CompositeResource extends Resource{
	 
	private List<Resource> resourceList;

	public CompositeResource(int resourceId) {
		super(resourceId);		
	}
	
	public CompositeResource(int resourceId, String resName, ResourceType type){
		super(resourceId,resName, type);
	}
	
	public void addResource(Resource res){
		resourceList.add(res);
	}
	
	//XXX may be you want to throw an exception
	public Resource getResource(int resourceId){
		for(Resource r: resourceList){
			if(r.getResourceId() == resourceId)
				return r;
		}
		return null;
	}
	
	//XXX may be you want to throw an exception
	public Resource getResource(String resourceName){
		for(Resource r: resourceList){
			if(r.getResourceName().equals(resourceName))
				return r;
		}
		return null;
	}
	
	public Resource[] getResources(Resource[] resources){
		return resourceList.toArray(resources);
	}
	
	public Resource[] getResources(){
		Resource[] resources = new Resource[resourceList.size()];
		return resourceList.toArray(resources);
	}
	
	public int size(){
		return resourceList.size();
	}	
	
	//XXX may be you want to throw an exception
	public void removeResource(Resource res){
		resourceList.add(res);
	}
	

}
