package edu.unibo.ciri.desert.resource;

import edu.unibo.ciri.realtime.model.Job;

public class VirtualPlatform {
	
	private Processor processors[];	
	private int maxProc;
	private int freeProcessors;
	
	public VirtualPlatform(int maxProcessors){
		maxProc = maxProcessors;
		processors = new Processor[maxProcessors];
		freeProcessors = maxProcessors;
		for(int i = 0; i < maxProcessors; i++){
			processors[i] = new Processor(i, "core"+i);
		}
	}	
	
	public boolean isIdle(){
		return freeProcessors == maxProc;
	}
	
	public boolean hasFreeProcessor(){		
		return freeProcessors > 0;
	}
	
	public int getFreeProcessorId(){
		for(int i = 0; i < maxProc; i++)
			if(processors[i].isFree())
				return i;
		return -1;
	}
	
	public boolean isProcessorFree(int nProc){
		return processors[nProc].isFree();
	}
	
	public int getMaxProcessor(){
		return maxProc;
	}
	
	public Job getJobRunningOn(int nProc){
		return processors[nProc].currentRunning();
	}
	
	public void setJobRunningOn(Job j, int nProc){
		freeProcessors--;
		assert(freeProcessors >= 0);		
		assert(nProc < maxProc);
		processors[nProc].setRunning(j);
	}
	
	public Processor getProcessor(int index){
		return processors[index];
	}
	
	public void freeProcessor(int nProc){
		processors[nProc].free();
		freeProcessors++;
		assert(freeProcessors <= maxProc);
	}
	
	public void clear(){
		freeProcessors = maxProc;
		for(int i = 0; i < maxProc; i++){
			processors[i].clear();
		}
	}
	
	public String toString(){
		return "<not set>";
	}
}
