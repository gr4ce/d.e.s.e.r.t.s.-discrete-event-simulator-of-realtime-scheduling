package edu.unibo.ciri.desert.graphics.diagram.schedule.drawers;


import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import edu.unibo.ciri.desert.graphics.diagram.schedule.ScheduleDiagramContainer;


public class GridDrawer extends JPanel {
	
	
	private ScheduleDiagramContainer container;
	
	private static final long serialVersionUID = 3703194789981691053L;
	
	public GridDrawer(ScheduleDiagramContainer container){
		this.container = container;
	}		
	 
	public void paint(Graphics g) {		
		
		int offset = container.getTaskLegendDrawer().getLegendWidth();
		
		int pixelHeightUnit = (int)container.getScheduleDiagramParam().getPixelForHeightRow();
		int pixelWidthUnit  = (int)container.getScheduleDiagramParam().getPixelForWidthCol();
		
		int nTask = container.getScheduleDiagramParam().getTasksetSize();
		long time = container.getScheduleDiagramParam().getDuration();
		
		
		int baseLine = nTask + 1;
		int freqInstantInidication = 5;
		
		for(int i = 0; i <= nTask; i++){
			g.setColor(Color.black);
			g.drawLine(offset, i*pixelHeightUnit, (int) (offset + time*pixelHeightUnit), i*pixelHeightUnit);
		}	
//		g.drawLine(offset, baseLine*pixelHeightUnit, (int)(offset + time*pixelHeightUnit), baseLine*pixelHeightUnit);
		
		for(int j = 0; j <= time; j++){
			g.setColor(Color.black);
			
			
			if(j % freqInstantInidication == 0){
				g.drawLine(offset + j*pixelWidthUnit, 0, offset + j*pixelWidthUnit, nTask*pixelWidthUnit + 10);
				g.drawString((new Long(j)).toString()+"ms", offset + j*pixelWidthUnit - 5,  baseLine*pixelHeightUnit );
			}
			else
				g.drawLine(offset + j*pixelWidthUnit, 0, offset + j*pixelWidthUnit, nTask*pixelWidthUnit);
				
		}
		
	}

}
