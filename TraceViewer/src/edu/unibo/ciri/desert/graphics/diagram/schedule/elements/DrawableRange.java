package edu.unibo.ciri.desert.graphics.diagram.schedule.elements;

import java.awt.Color;

import edu.unibo.ciri.desert.event.RTAIEvent;
import edu.unibo.ciri.realtime.model.time.Time;



public class DrawableRange implements Comparable<DrawableRange> {
	
	public  enum RangeType{ EXECUTION, READY, PREEMPTED, IDLE, RESOURCE, BLOCKED}
	
	private long 		start;
	private long 		end;
	private int 		taskId;
	private int 		resId;
	
	private RangeType 	type;	

	public DrawableRange(long start,long end, int taskId, int resourceId, RangeType t){		
		this.start	=	start;
		this.end	=	end;
		this.taskId	=	taskId;
		this.resId	=	resourceId;
		this.type   =   t;
	}
	
	public DrawableRange(Time start, Time end, int taskId, int resourceId, RangeType t){		
		this.start	=	start.getTimeValue();
		this.end	=	end.getTimeValue();
		this.taskId	=	taskId;
		this.resId	=	resourceId;
		this.type   =   t;
	}
	
	public int compareTo(DrawableRange e) {
		return 0;
	}

	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public long getEnd() {
		return end;
	}

	public void setEnd(long end) {
		this.end = end;
	}

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public int getResourceId() {
		return resId;
	}

	public void setResourceId(int resourceId) {
		this.resId = resourceId;
	}
	
	public RangeType getType() {
		return type;
	}

	public void setType(RangeType type) {
		this.type = type;
	}
	
	public String toString(){
		return "" + type.toString() + " " + taskId;
	}
	
	public DrawableRange parseRange(RTAIEvent left, RTAIEvent right){
		return null;
	}
	
	public Color getColor(){
		return Color.blue;
	}
	
	public long getStartTime(){
		return -1;
	}
	
	public long getEndTime(){
		return -1;
	}
	
}
