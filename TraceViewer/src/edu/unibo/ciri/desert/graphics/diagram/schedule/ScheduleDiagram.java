package edu.unibo.ciri.desert.graphics.diagram.schedule;

public abstract class ScheduleDiagram {
	
	private ScheduleDiagramContainer scheduleDiagramContainer;
	
	
	public abstract long getPixelHeightForUnit();
	
	public abstract long getPixelWidthForUnit();
	
	public abstract long getUnitResolution();
	
	
	
	
	public void setScheduleDiagramContainer(ScheduleDiagramContainer scheduleDiagramContainer){
		this.scheduleDiagramContainer = scheduleDiagramContainer;
	}
	
	public ScheduleDiagramContainer setSchedulerDiagramContainer(){
		return scheduleDiagramContainer;
		
	}
	

}
