package edu.unibo.ciri.desert.graphics.diagram.schedule.elements;

import edu.unibo.ciri.collection.Stack;
import edu.unibo.ciri.desert.resource.Processor;
import edu.unibo.ciri.desert.resource.SharedResource;

public class TaskContextHandler {
	
	
	private Processor 				currentProcessor;
	private Stack<SharedResource> 	resources = new Stack<SharedResource>();
	
	public void setCurrentProcessor(Processor proc){
		currentProcessor = proc;
	}
	
	public Processor getCurrentProcessor(){
		return currentProcessor;
	}
	
	public void enterResourceContext(SharedResource r){
		resources.push(r);
	}
	
	public void exitResourceContext(){
		resources.pop();
	}
	
	public SharedResource getCurrentResource(){
		return resources.readTop();
	}
	
	public boolean holdaResource(){
		if(resources.size() > 0)
			return true;
		return false;
	}

}
