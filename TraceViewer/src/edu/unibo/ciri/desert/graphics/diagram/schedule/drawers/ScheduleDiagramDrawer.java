package edu.unibo.ciri.desert.graphics.diagram.schedule.drawers;

import java.awt.Color;
import java.awt.Graphics;



public interface ScheduleDiagramDrawer{
	
	void draw();
	
	void associateByName(String name, int position);
	
	void associateById(int id, int position);
	
	int getPositionByName(String name);
	
	int getPositionById(int id);
	
	Color[] getProcessorMap();
	
	Graphics getGraphicObj();

	void setGraphicObj(Graphics graphicObj);

}
