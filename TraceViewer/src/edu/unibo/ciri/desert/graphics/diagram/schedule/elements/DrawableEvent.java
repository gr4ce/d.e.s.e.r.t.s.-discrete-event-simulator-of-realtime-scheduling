package edu.unibo.ciri.desert.graphics.diagram.schedule.elements;

import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.event.EventType;

import edu.unibo.ciri.realtime.model.time.Time;
import edu.unibo.ciri.realtime.model.time.TimeInstant;


public class DrawableEvent implements Comparable<DrawableEvent> {	
	
	TimeInstant time;
	String 		name;
	Time   		gridResolution;
	EventType   type;
	
	public DrawableEvent(Event e){
		
		time = new TimeInstant(e.getEventTime().getTimeValue());
		name = e.getTask().getName();
		type = e.getEventType();
	}
	
	public DrawableEvent(TimeInstant t, String taskName){
		
		time = t;
		name = taskName;
	}
	
	public void setGridResolution(Time t){
		this.gridResolution = t;
	}
	

	public TimeInstant getTime(){
		return time;
	}

	public String getName() {
		return name;
	}

	public EventType getType() {
		return type;
	}

	public int compareTo(DrawableEvent e) {		
		if(this.type.equals(EventType.JOB_RELEASE)){
			return 1;
		}else{
			return 0;
		}
	}
}
