package edu.unibo.ciri.desert.graphics.diagram.schedule;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.Box.Filler;

import edu.unibo.ciri.desert.event.RTAIEvent;
import edu.unibo.ciri.desert.graphics.diagram.schedule.drawers.GridDrawer;
import edu.unibo.ciri.desert.graphics.diagram.schedule.drawers.ScheduleDiagramDrawer;
import edu.unibo.ciri.desert.graphics.diagram.schedule.drawers.TaskLegendaDrawer;
import edu.unibo.ciri.desert.graphics.diagram.schedule.elements.DrawableRange;
import edu.unibo.ciri.desert.simulation.results.SimulationTrace;
import edu.unibo.ciri.realtime.model.Job;


public class ScheduleDiagramContainer extends JPanel{
	
	
	private static final long serialVersionUID = 7407338012714704870L;
	private int counter = 0;
	private int taskLegendWidht = 0;
	
	/*
	 * All the macro element reference
	 */
	private GridDrawer 				gridDrawer;
	private TaskLegendaDrawer       taskLegendDrawer;
	private ScheduleDiagram			scheduleDiagram;
	private ScheduleDiagramDrawer	scheduleDiagramDrawer;
	private ScheduleDiagramParam	scheduleDiagramParam;
	
	
	/*
	 * All the elements to be drawed 
	 */
	private ArrayList<RTAIEvent> 		events;
	private ArrayList<Job> 				jobs;
	private ArrayList<DrawableRange> 	ranges;
	private SimulationTrace				trace;
	
	
	public ScheduleDiagramContainer(){
		events = new ArrayList<RTAIEvent>();
		jobs   = new ArrayList<Job>();
		ranges = new ArrayList<DrawableRange>();
	}

	public void setScheduleDiagram(ScheduleDiagram scheduleDiagram) {
		this.scheduleDiagram = scheduleDiagram;
	}

	public ScheduleDiagram getScheduleDiagram() {
		return scheduleDiagram;
	}

	public void setGrid(GridDrawer grid) {
		this.gridDrawer = grid;
	}

	public GridDrawer getGrid() {
		return gridDrawer;
	}

	public void setScheduleDiagramDrawer(ScheduleDiagramDrawer scheduleDiagramDrawer) {
		this.scheduleDiagramDrawer = scheduleDiagramDrawer;
	}

	public ScheduleDiagramDrawer getScheduleDiagramDrawer() {
		return scheduleDiagramDrawer;
	}
	
	public void addElement(DrawableRange drawableRange) {
		this.ranges.add(drawableRange);
	}
	
	public void addElement(RTAIEvent collectedEvent) {
		this.events.add(collectedEvent);
	}
	
	public void addElement(Job drawableRange) {
		this.jobs.add(drawableRange);
	}

	public void setScheduleDiagramParam(ScheduleDiagramParam scheduleDiagramParam) {
		this.scheduleDiagramParam = scheduleDiagramParam;
	}

	public ScheduleDiagramParam getScheduleDiagramParam() {
//		if(scheduleDiagramParam == null)
//			scheduleDiagramParam = new ScheduleDiagramParam(4,1);
		return scheduleDiagramParam;
	}

	public ArrayList<DrawableRange> getDrawableRanges(){
		return ranges;
	}
	
	public ArrayList<RTAIEvent> getCollectedEvents(){
		return events;
	}
	
	public ArrayList<Job> getDrawableJobs(){
		return jobs;
	}
	
	@Override
	public void paint(Graphics g){
			
		g.setColor(Color.white);
		g.fillRect(0, 0, 10000, 10000);
		scheduleDiagramDrawer.setGraphicObj(g);		
		scheduleDiagramDrawer.draw();
		taskLegendDrawer.paint(g);
		gridDrawer.paint(g);
		
		
	}

	public TaskLegendaDrawer getTaskLegendDrawer() {
		return taskLegendDrawer;
	}

	public void setTaskLegendDrawer(TaskLegendaDrawer taskLegendDrawer) {
		this.taskLegendDrawer = taskLegendDrawer;
	}

	public int getTaskLegendWidht() {
		return taskLegendWidht;
	}

	public void setTaskLegendWidht(int taskLegendWidht) {
		this.taskLegendWidht = taskLegendWidht;
	}
	
}
