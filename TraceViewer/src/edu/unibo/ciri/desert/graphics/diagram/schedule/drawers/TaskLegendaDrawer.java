package edu.unibo.ciri.desert.graphics.diagram.schedule.drawers;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

import edu.unibo.ciri.desert.graphics.diagram.schedule.ScheduleDiagramContainer;
import edu.unibo.ciri.realtime.model.TaskSet;

public class TaskLegendaDrawer extends JPanel{
	
	
	private static final long serialVersionUID = -5694606483565596265L;
	private ScheduleDiagramContainer container;
	private int legendWidth = 100;

	
	public TaskLegendaDrawer(ScheduleDiagramContainer container){
		this.container = container;
	}	
	
	 
	public void paint(Graphics g) {		
		
		TaskSet ts = container.getScheduleDiagramParam().getTaskset();
		
		int pixelHeightUnit = (int)container.getScheduleDiagramParam().getPixelForHeightRow();
		int pixelWidthUnit  = (int)container.getScheduleDiagramParam().getPixelForWidthCol();
		
		int  nTask = container.getScheduleDiagramParam().getTasksetSize();
		int verticalOffsetDown = 20;
		
		int baseLine = nTask + 1;
		
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, legendWidth, nTask*pixelHeightUnit);
		
		for(int i = 0; i <= nTask; i++){
			g.setColor(Color.black);
			g.drawLine(0, i*pixelHeightUnit, legendWidth, i*pixelHeightUnit);
		}	
//		g.drawLine(0, baseLine*pixelHeightUnit, legendWidth, baseLine*pixelHeightUnit);
		
		for(int j = 0; j < nTask; j++){
			g.setFont(new Font("Times", Font.BOLD, 16));
			g.drawString(ts.getTask(j).getName(), 10,  j*pixelHeightUnit + verticalOffsetDown);
		}
		
		
		
	}


	public int getLegendWidth() {
		return legendWidth;
	}


	public void setLegendWidth(int legendWidth) {
		this.legendWidth = legendWidth;
	}

}
