package edu.unibo.ciri.desert.graphics.diagram.schedule.elements;

import edu.unibo.ciri.desert.event.EventType;

public class TracedEvent {
		
	private long 		time;
	private EventType 	type;
	private long		jobUniqueId;
	private int			jobInstance;
	private int			processorId;
	private int			taskId;
	

	public TracedEvent(){
		
		this.time        = -1;
		this.type        = EventType.NONE;
		this.jobUniqueId = -1;
		this.processorId = -1;
		this.taskId		 = -1; 
		
	}
	
	public TracedEvent(long time, EventType type, long jobId, int processorId, int taskId, int jobInstance){
		
		this.time        = time;
		this.type        = type;
		this.jobUniqueId = jobId;
		this.processorId = processorId;
		this.taskId		 = taskId; 
		this.jobInstance = jobInstance;
		
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public EventType getType() {
		return type;
	}

	public void setType(EventType type) {
		this.type = type;
	}

	public long getJobId() {
		return jobUniqueId;
	}

	public void setJobId(long jobId) {
		this.jobUniqueId = jobId;
	}

	public int getProcessorId() {
		return processorId;
	}

	public void setProcessorId(int processorId) {
		this.processorId = processorId;
	}

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public void setJobInstance(int jobInstance) {
		this.jobInstance = jobInstance;
	}

	public int getJobInstance() {
		return jobInstance;
	}
	
	
}
