package edu.unibo.ciri.desert.graphics.diagram.schedule.drawers;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Hashtable;

import edu.unibo.ciri.desert.event.EventType;
import edu.unibo.ciri.desert.event.RTAIEvent;
import edu.unibo.ciri.desert.graphics.diagram.schedule.ScheduleDiagramContainer;
import edu.unibo.ciri.desert.graphics.diagram.schedule.ScheduleDiagramParam;
import edu.unibo.ciri.desert.graphics.diagram.schedule.elements.DrawableRange;
import edu.unibo.ciri.desert.graphics.diagram.schedule.elements.TaskContextHandler;
import edu.unibo.ciri.desert.graphics.diagram.schedule.elements.DrawableRange.RangeType;
import edu.unibo.ciri.desert.resource.Processor;
import edu.unibo.ciri.desert.resource.Resource;
import edu.unibo.ciri.desert.resource.SharedResource;

import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.time.CollectedEvent;
import edu.unibo.ciri.realtime.model.time.Time;



public class TaskScheduleDiagramDrawer  implements ScheduleDiagramDrawer {	
	
	private ScheduleDiagramContainer 	container		= null;	
	private Graphics					graphicObj		= null;
	
	public  Color procMap[];
	public  Color defaultProcMap[] = { Color.red, Color.orange, Color.blue};	
	
	public  Color resMap[];
	public  SharedResource sr[] = new SharedResource[20];
	
	private Hashtable<String, SharedResource> resourcesTable = new Hashtable<String, SharedResource>();
	
	public void setResourcesTable(Hashtable<String, SharedResource> resourcesTable) {
		this.resourcesTable = resourcesTable;
	}

	private static Hashtable<String, Integer> positionMap = new Hashtable<String, Integer>();	
	private static int positionMapById[] = new int[30];	
	 
	public TaskScheduleDiagramDrawer(ScheduleDiagramContainer container){
		this.container 	= container;
		this.graphicObj = container.getGraphics(); 
	}
	
	public void setProcessor(int n){
		procMap = new Color[n];
		for(int i = 0; i < n; i++)
			setColorForCpu(i, defaultProcMap[i]);
	}
	
	public void addResource(SharedResource res){
		System.out.println("Adding resource: "+res);
		sr[res.getId()] = res;
	}
	
	public void setColorForCpu(int n, Color c){
		procMap[n] = c;
	}
		
	public void associateByName(String name, int position){
		positionMap.put(name, position);		
	}	
	
	public void associateTaskToRow(Task t, int position){
		associateById(t.getTaskId(), position);
	}
	
	public void associateById(int id, int position){
		positionMapById[id] = position;
	}	
	
	public int getPositionByName(String name){
		return positionMap.get(name).intValue();
	}
		
	public int getPositionById(int id){
		return positionMapById[id];
	}
	
	
	
	private int transformInPixel(long value){
		
		ScheduleDiagramParam		param			= container.getScheduleDiagramParam();
		
		int 						pixelForUnit	= param.getPixelForWidthCol();	
		Time						unitResolution	= param.getUnitResolution();
		
		return (int)((value*(long)pixelForUnit)/unitResolution.getTimeValue());
	}
	
	@Override
	public void draw(){		
		
		ArrayList<RTAIEvent> events  = container.getCollectedEvents();	
		ArrayList<DrawableRange>  ranges  = computeDrwableRanges(events);
		
		for(int i = ranges.size() - 1; i >= 0 ; i--){
			drawRange(ranges.get(i));			
		}
		
		for(int i = 0; i < events.size() ; i++){
			drawEvent(events.get(i));			
		}
		
		
		
	}
	
	private ArrayList<DrawableRange> computeDrwableRanges(ArrayList<RTAIEvent> events ){		
		
		
		int numberOfTasks  = positionMapById.length;
		ArrayList<RTAIEvent> collectedEventsPerTask[] = new ArrayList[numberOfTasks];
		ArrayList<DrawableRange> ranges = new ArrayList<DrawableRange>();
		
		for(int i = 0; i < numberOfTasks; i++){
			collectedEventsPerTask[i] = new ArrayList<RTAIEvent>(); 
		}
		
		for(int j = 0; j < events.size(); j++){
			
			RTAIEvent ce = events.get(j);			
			collectedEventsPerTask[positionMap.get(ce.getName())].add(ce);
		}
		
		Processor p  = new Processor(0);

		
		
		for(int i = 0; i < numberOfTasks; i++){
			
			boolean resourceRequested = false;
			boolean taskBlocked		  = false;
			
			TaskContextHandler tch = new TaskContextHandler();
			tch.setCurrentProcessor(p);
			
			for(int j = 0; j < collectedEventsPerTask[i].size() - 1; j++){
				
				RTAIEvent ceLeft  = collectedEventsPerTask[i].get(j);
				RTAIEvent ceRight = collectedEventsPerTask[i].get(j + 1);
							
				DrawableRange range = null;
			
				switch(ceLeft.getType()){
				
					case APPLICATION_START:
						continue;
					case JOB_RELEASE:
						if(!(ceRight.getType() == EventType.JOB_START || ceRight.getType() == EventType.JOB_RELEASE))
							System.out.println("Found a" + ceRight.toString());;
						range = new DrawableRange(ceLeft.getTime(), ceRight.getTime(), i, p.getResourceId(), RangeType.READY);							
						break;
					case JOB_START:
						if(!(ceRight.getType() == EventType.JOB_PREEMPTION 
							|| ceRight.getType() == EventType.JOB_COMPLETION 
							|| ceRight.getType() == EventType.SHARED_RESOURCE_REQUEST))
							System.out.println("Found a" + ceRight.toString());
						range = new DrawableRange(ceLeft.getTime(), ceRight.getTime(), i, p.getResourceId(), RangeType.EXECUTION);
						break;
					case JOB_WAIT:
						assert(ceRight.getType() == EventType.JOB_START);
						range = new DrawableRange(ceLeft.getTime(), ceRight.getTime(), i, -1, RangeType.READY);
						break;
					case JOB_PREEMPTION:
						if(!resourceRequested || (resourceRequested && !taskBlocked))
							range = new DrawableRange(ceLeft.getTime(), ceRight.getTime(), i, -1, RangeType.PREEMPTED);
						else if(resourceRequested && taskBlocked)
							range = new DrawableRange(ceLeft.getTime(), ceRight.getTime(), i, -1, RangeType.BLOCKED);
						else if(ceRight.getType() == EventType.JOB_RELEASE)
							range = new DrawableRange(ceLeft.getTime(), ceRight.getTime(), i, -1, RangeType.PREEMPTED);
						break;
					case JOB_RESUME:				
						if(taskBlocked)
							taskBlocked = false;
						if(!tch.holdaResource()){
//							assert(ceRight.getType() == EventType.JOB_PREEMPTION || ceRight.getType() == EventType.JOB_COMPLETION);
							range = new DrawableRange(ceLeft.getTime(), ceRight.getTime(), i, 0, RangeType.EXECUTION);
						}
						else{
							assert(ceRight.getType() == EventType.JOB_PREEMPTION || ceRight.getType() == EventType.SHARED_RESOURCE_RELEASE);
							range = new DrawableRange(ceLeft.getTime(), ceRight.getTime(), i, tch.getCurrentResource().getId(), RangeType.RESOURCE);
						}
						break;
					case JOB_COMPLETION:
//						if(!(ceRight.getType() == EventType.JOB_START || 
//								ceRight.getType() == EventType.JOB_WAIT ||
//								ceRight.getType() == EventType.JOB_RELEASE))
//							{
//								System.out.println("Found a" + ceRight.toString());
//								assert(false);
//							};
						if(ceRight.getType() == EventType.JOB_PREEMPTION){
							collectedEventsPerTask[i].remove(ceRight);
							break;
						}
						range = new DrawableRange(ceLeft.getTime(), ceRight.getTime(), i, -1, RangeType.IDLE);
						break;
					case SHARED_RESOURCE_REQUEST:
						assert(ceRight.getType() == EventType.SHARED_RESOURCE_ACCESS_GRANTED || 
							ceRight.getType() == EventType.SHARED_RESOURCE_ACCESS_DENIED);
						resourceRequested = true;
						tch.enterResourceContext(ceRight.getSharedResource());
						continue;
					case SHARED_RESOURCE_ACCESS_GRANTED:
						range = new DrawableRange(ceLeft.getTime(), ceRight.getTime(), i, tch.getCurrentResource().getId(), RangeType.RESOURCE);
						break;
					case SHARED_RESOURCE_ACCESS_DENIED:
						range = new DrawableRange(ceLeft.getTime(), ceRight.getTime(), i, 0, RangeType.BLOCKED);
						taskBlocked = true;
						break;
					case SHARED_RESOURCE_RELEASE:
						resourceRequested = false;
						assert(ceRight.getType() == EventType.JOB_PREEMPTION 
								|| ceRight.getType() == EventType.JOB_COMPLETION 
								|| ceRight.getType() == EventType.SHARED_RESOURCE_REQUEST 
								|| ceRight.getType() == EventType.SHARED_RESOURCE_RELEASE);
						tch.exitResourceContext();
						if(tch.holdaResource())
							range = new DrawableRange(ceLeft.getTime(), ceRight.getTime(), i, tch.getCurrentResource().getId(), RangeType.RESOURCE);
						else{	
							if(ceRight.getType() == EventType.JOB_RELEASE)
								range = new DrawableRange(ceLeft.getTime(), ceRight.getTime(), i, -1, RangeType.READY);
							else
								range = new DrawableRange(ceLeft.getTime(), ceRight.getTime(), i, 0, RangeType.EXECUTION);
						}
						break;
					default:
						System.out.println("Something wrong in parsing ranges");
						assert(false);
							
				}
				
				if(range == null)
					continue;
				ranges.add(range);
				
			}
			
		}
		
		return ranges;
		
		
	}
	
	private boolean isTaskStateEvent(CollectedEvent e){
		int taskId  = e.getTaskId();
		EventType t = e.getType();
		if( taskId != -1 && 
				(t == EventType.JOB_WAIT || 
				 t == EventType.JOB_START ||
				 t == EventType.JOB_PREEMPTION ||
				 t == EventType.JOB_RESUME ||
				 t == EventType.JOB_COMPLETION))
			return true;
		return false;
	}
	
	
	private void drawRange(DrawableRange range){
		
		int x,y,width,height;
		long start,end;		
		int taskLegendWidht = container.getTaskLegendDrawer().getLegendWidth();
		
		ScheduleDiagramParam	param	= container.getScheduleDiagramParam();			
		
//		if(range.getType() == RangeType.PREEMPTED || range.getType() == RangeType.READY || range.getType() == RangeType.BLOCKED ){
//			y 		= param.getPixelForWidthCol()*getPositionById(range.getTaskId()) + param.getPixelForHeightRow() /2; 		
//			height 	= param.getPixelForHeightRow() /2;
//		}else{
//			y 		= param.getPixelForWidthCol()*getPositionById(range.getTaskId()); 		
//			height 	= param.getPixelForHeightRow();
//		}
		
		y 		= param.getPixelForHeightRow()*getPositionById(range.getTaskId()); 		
		height 	= param.getPixelForHeightRow();
		
		start 	= range.getStart();
		end		= range.getEnd();
		
		
		if( (end - start) > 0){
			
			x = transformInPixel(start) + taskLegendWidht;			
			width = transformInPixel(end - start);
			
		
			switch(range.getType()){			
				case EXECUTION:
					graphicObj.setColor(defaultProcMap[range.getResourceId()]);
					break;
				case IDLE:
					graphicObj.setColor(Color.WHITE);
					break;
				case PREEMPTED:
					graphicObj.setColor(Color.GREEN);
					break;
				case READY:
					graphicObj.setColor(Color.GREEN);
					break;
				case RESOURCE:
					graphicObj.setColor(sr[range.getResourceId()].getColor());
					break;
				case BLOCKED:
					graphicObj.setColor(Color.CYAN);
					break;
			}			
			graphicObj.fillRect(x, y, width, height);		
			
		}
	}
	
	
	private void drawEvent(RTAIEvent e){
		
		EventType 	type 			= e.getType();
		long 	  	time 			= e.getTime().getTimeValue();
		int       	id   			= positionMap.get(e.getTask().getName());
		int 		taskLegendWidht = container.getTaskLegendDrawer().getLegendWidth();
		
		ScheduleDiagramParam		param			= container.getScheduleDiagramParam();
		int 						pixelForUnit	= param.getPixelForWidthCol();	
				
		int x,y,height,width;
		
		switch(type){
		
			case JOB_RELEASE:				
				
				y 		= this.getPositionById(id) * pixelForUnit;		
				x 		= transformInPixel(time) + taskLegendWidht;
				height 	= pixelForUnit;							
				width 	= 3;
				
				graphicObj.setColor(Color.black);
				graphicObj.fillRect(x, y, width, height);
				
				break;
				
			case JOB_DEADLINE:
				
				y 		= this.getPositionById(id) * pixelForUnit;
				x 		= transformInPixel(time) + taskLegendWidht;		
				height 	= pixelForUnit;	
				
				graphicObj.setColor(Color.black);			
				graphicObj.fillOval(x+6, y+6, (height-12), (height-12));
				
				break;
				
			case DELAYED_RELEASE:
				
				y 		= this.getPositionById(id) * pixelForUnit;
				x 		= transformInPixel(time) + taskLegendWidht;					
				height 	= pixelForUnit;						
				width 	= 2;
				
				graphicObj.setColor(Color.gray);
				graphicObj.fillRect(x, 1+y, width, (height/3-2));
				graphicObj.fillRect(x, 1+y+(height/3), width, (height/3-2));
				graphicObj.fillRect(x, 1+y+(height/3)*2, width, (height/3-1));
				
				break;
								
				
		}
		
	}
	

	@Override
	public Color[] getProcessorMap() {
		return procMap;
	}

	public Graphics getGraphicObj() {
		return graphicObj;
	}

	public void setGraphicObj(Graphics graphicObj) {
		this.graphicObj = graphicObj;
	}
	
	
	
}
