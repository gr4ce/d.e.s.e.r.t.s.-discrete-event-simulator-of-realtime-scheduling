package edu.unibo.ciri.desert.graphics.diagram.schedule;

import java.util.concurrent.TimeUnit;

import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.model.time.Time;

public class ScheduleDiagramParam {
	
	private TaskSet		taskset;
	private int			cpus;
	private long		duration;
	private int 		tasksetSize;
	
	private int 		pixelForHeightRow	= 30;
	private int			pixelForWidthCol	= 30;		
	
	private Time 		unitResolution =  new Time(1,TimeUnit.MILLISECONDS);;
	
	public ScheduleDiagramParam(TaskSet ts, int nCpu){
		setTaskset(ts);
		setCpus(nCpu);		
	}
	
	public ScheduleDiagramParam(int tasksetSize,int nCpu){
		setTasksetSize(tasksetSize);
		setCpus(nCpu);		
	}

	public int getTasksetSize() {
		return tasksetSize;
	}

	public void setTasksetSize(int tasksetSize) {
		this.tasksetSize = tasksetSize;
	}

	public void setTaskset(TaskSet taskset) {
		this.taskset = taskset;
	}


	public TaskSet getTaskset() {
		return taskset;
	}


	public void setCpus(int cpus) {
		this.cpus = cpus;
	}


	public int getCpus() {
		return cpus;
	}

	public void setPixelForWidthCol(int pixelForWidthCol) {
		this.pixelForWidthCol = pixelForWidthCol;
	}

	public int getPixelForWidthCol() {
		return pixelForWidthCol;
	}

	public void setPixelForHeightRow(int pixelForHeightRow) {
		this.pixelForHeightRow = pixelForHeightRow;
	}

	public int getPixelForHeightRow() {
		return pixelForHeightRow;
	}

	public void setUnitResolution(Time unitResolution) {
		this.unitResolution = unitResolution;
	}

	public Time getUnitResolution() {
		return unitResolution;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public long getDuration() {
		return duration;
	}



}
