package edu.unibo.ciri.desert.simulation.management;

import java.io.PrintStream;
import java.util.Vector;


import edu.unibo.ciri.desert.config.SimulationConfig;
import edu.unibo.ciri.desert.config.StaticConfig;

import edu.unibo.ciri.desert.datastructure.TimeLineRedBlackTree;

import edu.unibo.ciri.desert.event.EventType;

import edu.unibo.ciri.desert.event.management.EventManager;
import edu.unibo.ciri.desert.event.management.MultipleQueueEventFactory;

import edu.unibo.ciri.desert.resource.VirtualPlatform;
import edu.unibo.ciri.desert.scheduler.TimeStepFullPreemptiveScheduler;
import edu.unibo.ciri.desert.simulation.core.Simulator;
import edu.unibo.ciri.desert.simulation.results.SimulationResults;
import edu.unibo.ciri.desert.simulation.results.SuccessRatioFunction;

import edu.unibo.ciri.model.task.job.JobFactory;
import edu.unibo.ciri.realtime.model.DataSet;
import edu.unibo.ciri.realtime.model.time.EventCollector;
import edu.unibo.ciri.realtime.policies.scheduling.EDZLSchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.ZeroLaxityHandler;


public class CyclicZLSimulation extends Simulation implements Runnable {

	private Vector<SimulationResults> results = new Vector<SimulationResults>();
	int run = 1;
	
	protected SimulationSuiteZL		simulationSuiteZL 	= null;
	protected ZeroLaxityHandler     zlHandler;
	PrintStream ps;
	
	/* 
	 * per impostare EDZL o FPZL bisogna commentare/scommentare 3 sezioni di codice
	 * due in run() a seconda dell'utilizzo del reuse o meno
	 * una in init()   
	 * (oltre alle dichiarazioni immediatamente qui sotto)	
	 */
	
	/*		
	 * 		EDZL 	
	 * */
	EDZLSchedulingPolicy edzl=null;
	
	
	
	@Override
	public void run() {

		DataSet ds = simulationSuiteZL.getDataSet();
		
		SuccessRatioFunction successRatio = simulationSuiteZL.getSuccessRatioFunction();
		
		try {
			init();
		} catch (SimulationException e) {
			System.out.println("[SIMULATION][INFO] - Nothing left to do... terminating!!");
			return;
		}

		while (true) {

			SimulationResults result = new SimulationResults(taskset);
			results.add(result);

			eventFactory.setInterestedListenerToEventType(	EventType.JOB_DEADLINE, result);
			eventFactory.setInterestedListenerToEventType(	EventType.JOB_PREEMPTION, result);

			simulatorThread = new Thread(simulator);
			simulatorThread.setName("SIMULATOR(" + this.getId() + ") run("+(run++)+")");			
			
			result.setStartSimTime(System.currentTimeMillis());
			simulatorThread.start();

			try {
				if (StaticConfig.PRINT_SIMULATION_INFO)
					System.out.println("[SIMUALTION = "+Thread.currentThread().getName()+"][INFO] - Waiting for termination of " + simulatorThread.getName());
							
				simulatorThread.join();
				result.setEndSimTime(System.currentTimeMillis());
				
				boolean success = result.getTotalMissed() > 0 ? false : true;
				
				successRatio.addOccurrence(taskset.getUtilizationString(), success, ps);
				
				if (StaticConfig.PRINT_SIMULATION_INFO)
					System.out.println("[SIMUALTION = "+Thread.currentThread().getName()+"][INFO] - has terminated");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			
			synchronized (ds) {
				if (ds.hasNext()) {
					taskset = ds.next();
					System.out.println("[SIMULATION][INFO] Imported "+ taskset.getTasksetName());
				} else {
					state = SimulationState.TERMINATED;
					System.out.println("[SIMULATION][INFO] - Nothing left to do... terminating!!");
					return;
				}
			}
			
			if(StaticConfig.SIMULATOR_REUSE_RESOURCES){
				
				simulator.clear();
				if (StaticConfig.PRINT_SIMULATION_INFO)
					System.out.println("[SIMULATION][INFO] - simulator cleared");
	
				timeLine.clear();
				if (StaticConfig.PRINT_SIMULATION_INFO)
					System.out.println("[SIMULATION][INFO] - timeline cleared");
	
//				eventFactory = new MultipleQueueEventFactory(this, 400);
				eventFactory.clear();
				if (StaticConfig.PRINT_SIMULATION_INFO)
					System.out.println("[SIMULATION][INFO] - event factory cleared");
	
				eventManager.clear();
				if (StaticConfig.PRINT_SIMULATION_INFO)
					System.out.println("[SIMULATION][INFO] - event manager cleared");
	
				jobFactory.setStaticSize(taskset.size());
				jobFactory.clear();	
				
				
				scheduler.clear();
				if (StaticConfig.PRINT_SIMULATION_INFO)
					System.out.println("[SIMULATION][INFO] - scheudler cleared");
				
				platform.clear();
				if (StaticConfig.PRINT_SIMULATION_INFO)
					System.out.println("[SIMULATION][INFO] - event manager cleared");
				
				
				//zerolaxityhandler
				zlHandler.clear();
				zlHandler.setSize(taskset.size());
//				zlHandler.setSimulation(this);
//				this.setZLHandler(zlHandler);
				
				
				/*		
				 * 		EDZL 	
				 * */
				//XXX a che diavolo servono ste due righe di codice???? bhooooo
//				edzl.setZLHandler(zlHandler);
//				this.setSchedulingPolicy(edzl);
				
				/* 
				 * 		FPZL con RM	
				 * */
//				fpa = rm.getStaticPriorityAssignment(taskset);
//				pta = new PreemptionThresholdAssignment(taskset.getSize());
//				fpsp.setZLHandler(zlHandler);
//				fpsp.setPriorityAssignment(fpa);
//				this.setSchedulingPolicy (fpsp);
				

				
			}else{ //SIMULATOR_REUSE_RESOURCES == false;
				
				simulator = new Simulator(this);
				if (StaticConfig.PRINT_SIMULATION_INFO)
					System.out.println("[SIMULATION][INFO] - simulator cleared");
	
				timeLine = new TimeLineRedBlackTree();
				if (StaticConfig.PRINT_SIMULATION_INFO)
					System.out.println("[SIMULATION][INFO] - timeline created");
	
				eventFactory = new MultipleQueueEventFactory(this);
				if (StaticConfig.PRINT_SIMULATION_INFO)
					System.out.println("[SIMULATION][INFO] - event factory created");
	
				eventManager = new EventManager(this);
				if (StaticConfig.PRINT_SIMULATION_INFO)
					System.out.println("[SIMULATION][INFO] - event manager created");
				
				
				//zerolaxityhandler
				zlHandler = new ZeroLaxityHandler(this);	
				this.setZLHandler(zlHandler);
				
				
				/*		
				 * 			EDZL
				 * 	*/
				edzl = new EDZLSchedulingPolicy(zlHandler);
				this.setSchedulingPolicy (edzl);
				
				/*	
				 * 		FPZL con RM	
				 * */
//				rm = new RMSortingPolicy();
//				fpa = rm.getStaticPriorityAssignment(taskset);
//				pta = new PreemptionThresholdAssignment(taskset.getSize());
//				fpsp 	= new FixedPriorityZeroLaxitySchedulingPolicy(rm, zlHandler);
//				fpsp.setPriorityAssignment(fpa);
//				this.setSchedulingPolicy (fpsp);
				
	
				jobFactory 	= new JobFactory(this.schedulingPolicy, this.taskset);
				platform 	= new VirtualPlatform(simulationConfig.getProcessors());
				scheduler 	= new TimeStepFullPreemptiveScheduler(this);
				
			}
			
			if (StaticConfig.PRINT_SIMULATION_INFO)
				System.out.println("[SIMULATION][INFO] - job factory cleared whit taskset size = " +taskset.size());


//			timeKeeper = new TimeKeeper();
//			simulator = new Simulator(this);
			associateListenersToEvents();
			if(run % 400 == 0)
				System.gc();
						
		}
		
	}//run

	
	
	
	public void init() throws SimulationException {

		if (StaticConfig.PRINT_SIMULATION_INFO)
			System.out.println("[SIMULATION][INFO] Simulation initialize...");

		if (taskset == null) {
			
			DataSet ds = simulationSuiteZL.getDataSet();
			assert (ds != null);
			
			synchronized (ds) {
				if (ds.hasNext()) {
					taskset = ds.next();
					System.out.println("Starting simulation of "+ taskset.getTasksetName());
				} else {
					state = SimulationState.TERMINATED;
					System.out.println("[SIMULATION][INFO] - Nothing left to do... terminating!!");
					throw new SimulationException("No more taskset in dataset");
				}
			}
		}

		if (StaticConfig.PRINT_SIMULATION_INFO)
			System.out.println("[SIMULATION][INFO] Successfully imported "
					+ taskset.getTasksetName());

		simulator = new Simulator(this);
		timeLine = new TimeLineRedBlackTree();
		if (StaticConfig.PRINT_SIMULATION_INFO)
			System.out.println("[SIMULATION][INFO] - simulator created");

		eventFactory = new MultipleQueueEventFactory(this);

		eventManager = new EventManager(this);
		if (StaticConfig.PRINT_SIMULATION_INFO)
			System.out.println("[SIMULATION][INFO] - event manager created");
		
		
		//ZERO LAXITY
		zlHandler = new ZeroLaxityHandler(this);	
		this.setZLHandler(zlHandler);
		
		/*	
		 * 		EDZL	
		 * */
		edzl = new EDZLSchedulingPolicy(zlHandler);
		this.setSchedulingPolicy (edzl);
		
		/*	
		 * 		FPZL con RM	
		 * */
//		RMSortingPolicy rm = new RMSortingPolicy();
//		FixedPriorityAssignment fpa = rm.getStaticPriorityAssignment(taskset);
//		PreemptionThresholdAssignment pta = new PreemptionThresholdAssignment(taskset.getSize());
//		FixedPriorityZeroLaxitySchedulingPolicy fpsp 	= new FixedPriorityZeroLaxitySchedulingPolicy(rm, zlHandler);
//		fpsp.setPriorityAssignment(fpa);
//		this.setSchedulingPolicy (fpsp);

		
		

		jobFactory = new JobFactory(schedulingPolicy, taskset);

		if (StaticConfig.PRINT_SIMULATION_INFO)
			System.out.println("[SIMULATION][INFO] - object factories created");

		simulatorThread = new Thread(simulator);
		simulatorThread.setName("SIMULATOR_ENGINE(" + this.getId() + ")");
		if (StaticConfig.PRINT_SIMULATION_INFO)
			System.out.println("[SIMULATION][INFO] - thread "
					+ simulatorThread.getName() + " created");

		eventCollector = new EventCollector();

		associateListenersToEvents();

		state = SimulationState.INITIALIZED;
		if (StaticConfig.PRINT_SIMULATION_INFO)
			System.out.println("[SIMULATION][INFO] Switching to INITIALIZED state");

	}

	public Vector<SimulationResults> getAllSimulationsResults() {
		return results;
	}
	
	

	private void associateListenersToEvents() {

		eventFactory.setInterestedListenerToEventType(EventType.JOB_COMPLETION,			eventManager);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_RELEASE,			eventManager);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_DEADLINE,		 	eventManager);
		eventFactory.setInterestedListenerToEventType(EventType.SIMULATION_HALT,		eventManager);
		eventFactory.setInterestedListenerToEventType(EventType.SIMULATION_SOFT_STOP, 	eventManager);
		
		eventFactory.setInterestedListenerToEventType(EventType.JOB_RELEASE, 			eventCollector);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_START, 				eventCollector);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_PREEMPTION,			eventCollector);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_RESUME, 			eventCollector);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_COMPLETION, 		eventCollector);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_PRIORITY_CHANGE, 	eventCollector);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_WAIT, 				eventCollector);		
		
		eventFactory.setInterestedListenerToEventType(EventType.JOB_RELEASE,       		scheduler);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_COMPLETION,    		scheduler);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_PRIORITY_CHANGE, 	scheduler);
		eventFactory.setInterestedListenerToEventType(EventType.TIME_STEP_FORWARD,   	scheduler);

		//listener per zero laxity handler
		eventFactory.setInterestedListenerToEventType(EventType.JOB_RELEASE, 			zlHandler);		
		eventFactory.setInterestedListenerToEventType(EventType.JOB_START, 				zlHandler);		
		eventFactory.setInterestedListenerToEventType(EventType.JOB_RESUME, 			zlHandler);			
		eventFactory.setInterestedListenerToEventType(EventType.JOB_WAIT, 				zlHandler);			
		eventFactory.setInterestedListenerToEventType(EventType.JOB_PREEMPTION,			zlHandler);			
		eventFactory.setInterestedListenerToEventType(EventType.JOB_COMPLETION, 		zlHandler);			
		eventFactory.setInterestedListenerToEventType(EventType.JOB_ZERO_LAXITY, 		zlHandler);			

		// factory.setInterestedListenerToEventType(EventType.JOB_COMPLETION,
		// consoleDisplay);
		// factory.setInterestedListenerToEventType(EventType.JOB_RELEASE,
		// consoleDisplay);
		// factory.setInterestedListenerToEventType(EventType.JOB_START,
		// consoleDisplay);
		// factory.setInterestedListenerToEventType(EventType.JOB_PREEMPTION,
		// consoleDisplay);
		// factory.setInterestedListenerToEventType(EventType.JOB_RESUME,
		// consoleDisplay);
		// factory.setInterestedListenerToEventType(EventType.START_TRANSACTION,
		// consoleDisplay);
		// factory.setInterestedListenerToEventType(EventType.END_TRANSACTION,
		// consoleDisplay);

		// factory.setInterestedListenerToEventType(EventType.JOB_DEADLINE,
		// fileLog);
		// factory.setInterestedListenerToEventType(EventType.JOB_COMPLETION,
		// fileLog);
		// factory.setInterestedListenerToEventType(EventType.JOB_RELEASE,
		// fileLog);
		// factory.setInterestedListenerToEventType(EventType.JOB_START,
		// fileLog);
		// factory.setInterestedListenerToEventType(EventType.JOB_RESUME,
		// fileLog);
		// factory.setInterestedListenerToEventType(EventType.SIMULATION_HALT,
		// fileLog);
		// factory.setInterestedListenerToEventType(EventType.SIMULATION_SOFT_STOP,
		// fileLog);
		// factory.setInterestedListenerToEventType(EventType.TIME_ADVANCE,
		// fileLog);

		// eventFactory.setInterestedListenerToEventType(EventType.JOB_COMPLETION,
		// simulationResults);

		// eventFactory.setInterestedListenerToEventType(EventType.JOB_START,
		// simulationResults);
		// eventFactory.setInterestedListenerToEventType(EventType.JOB_RESUME,
		// simulationResults);
		// eventFactory.setInterestedListenerToEventType(EventType.JOB_RELEASE,
		// simulationResults);
		// eventFactory.setInterestedListenerToEventType(EventType.SIMULATION_HALT,
		// simulationResults);
		// eventFactory.setInterestedListenerToEventType(EventType.SIMULATION_SOFT_STOP,
		// simulationResults);

		// factory.setInterestedListenerToEventType(EventType.JOB_COMPLETION,
		// logger);
		// factory.setInterestedListenerToEventType(EventType.JOB_DEADLINE,
		// logger);
		// factory.setInterestedListenerToEventType(EventType.JOB_PREEMPTION,
		// logger);
		// factory.setInterestedListenerToEventType(EventType.JOB_START,
		// logger);
		// factory.setInterestedListenerToEventType(EventType.JOB_RESUME,
		// logger);
		// factory.setInterestedListenerToEventType(EventType.JOB_RELEASE,
		// logger);
		// factory.setInterestedListenerToEventType(EventType.SIMULATION_HALT,
		// logger);
		// factory.setInterestedListenerToEventType(EventType.SIMULATION_SOFT_STOP,
		// logger);

		// factory.setInterestedListenerToEventType(EventType.OVERHEAD_CONTEXT_SWITCH_START,
		// overHeadHandler);
		// factory.setInterestedListenerToEventType(EventType.OVERHEAD_CONTEXT_SWITCH_END,
		// overHeadHandler);

		eventFactory.setInterestedListenerToEventType(EventType.JOB_RELEASE,eventFactory);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_COMPLETION,eventFactory);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_START,	eventFactory);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_PREEMPTION,	eventFactory);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_RESUME,	eventFactory);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_PRIORITY_CHANGE, eventFactory);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_WAIT,eventFactory);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_DEADLINE,eventFactory);

		eventFactory.setInterestedListenerToEventType(EventType.JOB_COMPLETION, jobFactory);
				
//		eventFactory.setInterestedListenerToAllEventType(eventFactory);
//		eventFactory.removeInterestedListenerToEventType(EventType.TIME_STEP_FORWARD, eventFactory);
		
	}

	
	public void setSimulationSuiteZL (SimulationSuiteZL ss){
		simulationSuiteZL = ss;
	}	
	
	public void setZLHandler (ZeroLaxityHandler zlHandler){
		this.zlHandler = zlHandler;
	}
	
	public void setPrintStream(PrintStream ps){
		this.ps=ps;
	}




	public void setSimulationConfig(SimulationConfig simulationConfig) {
		// TODO Auto-generated method stub
		
	}
	
}