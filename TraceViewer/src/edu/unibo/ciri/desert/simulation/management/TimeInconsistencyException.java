package edu.unibo.ciri.desert.simulation.management;

public class TimeInconsistencyException extends Exception {
	
	public TimeInconsistencyException(){
		super();
	}
	
	public TimeInconsistencyException(String message){
		super(message);
	}

}
