package edu.unibo.ciri.desert.simulation.management;


import java.util.ArrayList;



import edu.unibo.ciri.desert.config.SimulationConfig;
import edu.unibo.ciri.desert.config.StaticConfig;
import edu.unibo.ciri.desert.config.SuiteConfig;
import edu.unibo.ciri.desert.resource.VirtualPlatform;
import edu.unibo.ciri.desert.scheduler.TimeStepFullPreemptiveScheduler;
import edu.unibo.ciri.desert.simulation.results.SimulationResults;
import edu.unibo.ciri.desert.simulation.results.SuccessRatioFunction;
import edu.unibo.ciri.realtime.model.DataSet;



public class SimulationSuiteZL {
	
	private SuiteConfig						suiteConfig = null;
	private SimulationConfig[] 				config		= null;
	private DataSet							dataset		= null;
	private CyclicZLSimulation[]			simulation	= null;
	private ArrayList<SimulationResults>	results		= new ArrayList<SimulationResults>();
	
	private long	startTime	= 0;
	private long	endTime		= 0;
	
	private SuccessRatioFunction successRatio = new SuccessRatioFunction();

	
	public SimulationSuiteZL(SuiteConfig suiteConf, SimulationConfig[] conf, DataSet ds){
		
		suiteConfig = suiteConf;
		config 		= conf;
		dataset 	= ds;
		
		int parallelSimulation = suiteConfig.getParallelSimulation();
		
		simulation 	= new CyclicZLSimulation[parallelSimulation];
		
		Thread controller[] = new Thread[parallelSimulation];
		
		if(dataset.size() < parallelSimulation){
			parallelSimulation = dataset.size(); 
		}
		
		for(int i = 0; i < parallelSimulation; i++){
			
			//TODO qui puoi leggere la configurazione e creare l'oggetto simulation con
			//     i parametri specifici
			simulation[i] = new CyclicZLSimulation();
			simulation[i].setId(i);
			simulation[i].setSimulationSuiteZL(this);
			simulation[i].setPlatform (new VirtualPlatform(config[i].getProcessors()));
			simulation[i].setScheduler(new TimeStepFullPreemptiveScheduler(simulation[i]));
			
			

			try {
				simulation[i].setSimulationConfig(config[i]);
				simulation[i].init();
				
				
				controller[i] = new Thread(simulation[i]);
				controller[i].setName("SIMULATION_CONTROLLER("+ i +")");
				
				if(StaticConfig.PRINT_SIMULATION_SUITE_INFO)
					System.out.println("[SIMULATION SUITE][INFO] - Simulation " + controller[i].getName() +"["+ simulation[i].getId() +"]"+ " initialized");
				
				
				controller[i].start();
				
				
				if(StaticConfig.PRINT_SIMULATION_SUITE_INFO)
					System.out.println("[SIMULATION SUITE][INFO] - Simulation " + controller[i].getName() +"["+ simulation[i].getId() +"]"+ " started");

				
			} catch (SimulationException e) {
				if(StaticConfig.PRINT_SIMULATION_SUITE_INFO)
					System.out.println("[SIMULATION SUITE][INFO] - Simulation " + controller[i].getName() +"["+ simulation[i].getId() +"]"+ " terminated because: "+ e.getMessage());
				parallelSimulation--;
//				e.printStackTrace();
			}
		}
		
		for(int i = 0; i < parallelSimulation; i++){
			try {
				if(StaticConfig.PRINT_SIMULATION_SUITE_INFO)
					System.out.println("[SIMULATION SUITE][INFO] - Waitinig termination " + simulation[i].getId() );
				controller[i].join();
				if(StaticConfig.PRINT_SIMULATION_SUITE_INFO)
					System.out.println("[SIMULATION SUITE][INFO] - Simulation " + controller[i].getName() +"["+ simulation[i].getId() +"]"+ " terminated" );
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		for(int i = 0; i < parallelSimulation; i++){
			results.addAll(simulation[i].getAllSimulationsResults());
//			results.add(simulation[i].getSimulationResults());
		}
		
		
		
		
	}
	
	public DataSet getDataSet(){
		return dataset;
	}
	
	public SuccessRatioFunction getSuccessRatioFunction(){
		return successRatio;
	}
	
	

	public ArrayList<SimulationResults> getResults() {
		return results;
	}

}