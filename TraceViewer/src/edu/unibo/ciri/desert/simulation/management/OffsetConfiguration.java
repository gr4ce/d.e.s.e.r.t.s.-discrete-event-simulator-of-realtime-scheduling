package edu.unibo.ciri.desert.simulation.management;

import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskSet;

public class OffsetConfiguration {

	private long[] startTimes;
	
	public OffsetConfiguration(int n){
		
		startTimes = new long[n];
		
		for(int i = 0; i < n; i++)
			startTimes[i] = 0;			
		
	}
	
	public void setOffsetByTaskId(int taskId, long value){
		startTimes[taskId] = value;
	}
	
	public long getOffsetByTaskId(int taskId){
		return startTimes[taskId];
	}
	
	public long getOffsetByTask(Task t){
		return getOffsetByTaskId(t.getTaskId());
	}
	
	public void applyOffset(TaskSet ts){
		for(int i = 0; i < ts.size(); i++){
			ts.getTask(i).getTaskParameters().getOffset().setTimeValue(startTimes[i]);			
		}
	}
}
