package edu.unibo.ciri.desert.simulation.management;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;

import policy.scheduling.RMPolicy;


import edu.unibo.ciri.desert.config.Configuration;
import edu.unibo.ciri.desert.config.SimulationConfig;
import edu.unibo.ciri.desert.config.StaticConfig;
import edu.unibo.ciri.desert.config.SuiteConfig;
import edu.unibo.ciri.desert.resource.VirtualPlatform;
import edu.unibo.ciri.desert.scheduler.TimeStepFullPreemptiveScheduler;
import edu.unibo.ciri.desert.simulation.results.SimulationResults;
import edu.unibo.ciri.desert.simulation.results.SuccessRatioFunction;
import edu.unibo.ciri.realtime.model.DataSet;
import edu.unibo.ciri.realtime.policies.scheduling.GlobalEDFSchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.FixedPriorityAssignment;
import edu.unibo.ciri.realtime.policies.scheduling.FixedPrioritySchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.SchedulingPolicyEnum;
import edu.unibo.ciri.realtime.policies.scheduling.SortingPolicyEnum;
import edu.unibo.ciri.realtime.policies.staticsorting.RMSortingPolicy;



public class SimulationSuite {
	
	private SuiteConfig						suiteConfig = null;
	private SimulationConfig[] 				config		= null;
	private DataSet							dataset		= null;
	private CyclicSimulation[]				simulation	= null;
	private ArrayList<SimulationResults>	results		= new ArrayList<SimulationResults>();
	
	private long	startTime	= 0;
	private long	endTime		= 0;
	
	private SuccessRatioFunction successRatio = new SuccessRatioFunction();
	
	public SimulationSuite(SuiteConfig suiteConf, SimulationConfig[] conf, DataSet ds){
		
		suiteConfig = suiteConf;
		config 		= conf;
		dataset 	= ds;
		
		String fileNameSuccessRatio	= Configuration.getResutlsPath()+ "sr.res"; 
		FileOutputStream out = null;
		PrintStream ps = null;
		try {
			 out = new FileOutputStream(fileNameSuccessRatio);
		     ps = new PrintStream(out);
		} 
		catch (FileNotFoundException e1) {	e1.printStackTrace();		} 
		
		int parallelSimulation = suiteConfig.getParallelSimulation();
		
		simulation 	= new CyclicSimulation[parallelSimulation];
		
		Thread controller[] = new Thread[parallelSimulation];
		
		if(dataset.size() < parallelSimulation){
			
			parallelSimulation = dataset.size(); 
		}
		
		System.out.println("Parallel simulation "+parallelSimulation);
		
		for(int i = 0; i < parallelSimulation; i++){
			
			//TODO qui puoi leggere la configurazione e creare l'oggetto simulation con
			//     i parametri specifici
			simulation[i] = new CyclicSimulation();
			simulation[i].setId(i);
			simulation[i].setSimulationSuite(this);
			simulation[i].setPlatform (new VirtualPlatform(config[i].getProcessors()));
			simulation[i].setScheduler(new TimeStepFullPreemptiveScheduler(simulation[i]));
			
			RMSortingPolicy rm = new RMSortingPolicy();			
			FixedPrioritySchedulingPolicy fps = new FixedPrioritySchedulingPolicy(rm);			
			simulation[i].setSchedulingPolicyEnum(SchedulingPolicyEnum.FIXED_PRIORITY_RATE_MONOTONIC);
			simulation[i].setPrintStream(ps);
//			ZeroLaxityHandler zlhandler = new ZeroLaxityHandler(5, simulation[i]);
//			EarliestDeadlineZeroLaxitySchedulingPolicy policy= new EarliestDeadlineZeroLaxitySchedulingPolicy(zlhandler);
//			simulation[i].setZLHandler(zlhandler);
//			simulation[i].edzl=policy;
//			simulation[i].setSchedulingPolicy(new EarliestDeadlineZeroLaxitySchedulingPolicy(zlhandler));
			
			try {
				simulation[i].setSimulationConfig(config[i]);
				simulation[i].init();				
				
				controller[i] = new Thread(simulation[i]);
				controller[i].setName("SIMULATION_CONTROLLER("+ i +")");
				
				if(StaticConfig.PRINT_SIMULATION_SUITE_INFO)
					System.out.println("[SIMULATION SUITE][INFO] - Simulation " + controller[i].getName() +"["+ simulation[i].getId() +"]"+ " initialized");
				
				
				controller[i].start();
				
				
				if(StaticConfig.PRINT_SIMULATION_SUITE_INFO)
					System.out.println("[SIMULATION SUITE][INFO] - Simulation " + controller[i].getName() +"["+ simulation[i].getId() +"]"+ " started");

				
			} catch (SimulationException e) {
				if(StaticConfig.PRINT_SIMULATION_SUITE_INFO)
					System.out.println("[SIMULATION SUITE][INFO] - Simulation " + controller[i].getName() +"["+ simulation[i].getId() +"]"+ " terminated because: "+ e.getMessage());
				parallelSimulation--;
//				e.printStackTrace();
			}
		}
		
		for(int i = 0; i < parallelSimulation; i++){
			try {
				if(StaticConfig.PRINT_SIMULATION_SUITE_INFO)
					System.out.println("[SIMULATION SUITE][INFO] - Waitinig termination " + simulation[i].getId() );
				controller[i].join();
				if(StaticConfig.PRINT_SIMULATION_SUITE_INFO)
					System.out.println("[SIMULATION SUITE][INFO] - Simulation " + controller[i].getName() +"["+ simulation[i].getId() +"]"+ " terminated" );
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		for(int i = 0; i < parallelSimulation; i++){
			results.addAll(simulation[i].getAllSimulationsResults());
//			results.add(simulation[i].getSimulationResults());
		}
		
		try {
			out.close();
			ps.close();
		} 
		catch (IOException e) {e.printStackTrace();}
	}
	
	public SuccessRatioFunction getSuccessRatioFunction(){
		return successRatio;
	}
	
	public DataSet getDataSet(){
		return dataset;
	}

	

	public ArrayList<SimulationResults> getResults() {
		return results;
	}

}
