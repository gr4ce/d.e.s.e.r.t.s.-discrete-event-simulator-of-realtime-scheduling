package edu.unibo.ciri.desert.simulation.management;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.model.time.TimeInstant;


public class OffsetGenerator {
	
	private ArrayList<OffsetConfiguration> config = new ArrayList<OffsetConfiguration>();
	private int index = 0;
	
	public OffsetGenerator(TaskSet ts){
		
		TimeInstant maxPeriod = new TimeInstant(0);
		
		for(int i = 0; i < ts.size(); i++){
			long period = ts.getTask(i).getTaskParameters().getPeriod().getTimeValue();
			if(period > maxPeriod.getTimeValue())
				maxPeriod.setTimeValue(period);
		}
		
		int step = (int)maxPeriod.getTimeValue(TimeUnit.MILLISECONDS);
		
		for(int offset0 = 0; offset0 < step; offset0++){
			
			for(int offset1 = 0; offset1 < step; offset1++){
				
				for(int offset2 = 0; offset2 < step; offset2++){
					
					OffsetConfiguration oc = new OffsetConfiguration(3);
					
					oc.setOffsetByTaskId(0, offset0 * 1000);
					oc.setOffsetByTaskId(1, offset1 * 1000);
					oc.setOffsetByTaskId(2, offset2 * 1000);
					
					config.add(oc);
					
				}
				
			}
			
		}
		
	}
	
	public OffsetConfiguration getNext(){
		return config.get(index++);
	}
	
	public boolean hasNext(){
		return index < config.size();
	}
	
	
}
