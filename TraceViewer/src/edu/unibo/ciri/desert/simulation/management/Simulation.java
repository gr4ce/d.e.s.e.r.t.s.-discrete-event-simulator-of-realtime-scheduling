package edu.unibo.ciri.desert.simulation.management;

import java.util.List;

import edu.unibo.ciri.desert.config.SimulationConfig;
import edu.unibo.ciri.desert.config.StaticConfig;
import edu.unibo.ciri.desert.datastructure.TimeLine;
import edu.unibo.ciri.desert.datastructure.TimeLineDoubleLinkedList;
import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.event.Event.EventState;
import edu.unibo.ciri.desert.event.EventType;
import edu.unibo.ciri.desert.event.management.EventFactory;
import edu.unibo.ciri.desert.event.management.EventListener;
import edu.unibo.ciri.desert.event.management.EventManager;
import edu.unibo.ciri.desert.event.management.MultipleQueueEventFactory;
import edu.unibo.ciri.desert.resource.VirtualPlatform;
import edu.unibo.ciri.desert.scheduler.PriorityScheduler;
import edu.unibo.ciri.desert.scheduler.TimeStepFullPreemptiveScheduler;
import edu.unibo.ciri.desert.simulation.core.Simulator;
import edu.unibo.ciri.desert.simulation.results.SimulationResults;
import edu.unibo.ciri.desert.simulation.results.SimulationTrace;
import edu.unibo.ciri.model.task.job.JobFactory;
import edu.unibo.ciri.realtime.model.DataSet;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.model.time.EventCollector;
//import edu.unibo.ciri.realtime.policies.scheduling.SchedulingPolicyEnum;
import edu.unibo.ciri.realtime.policies.scheduling.SortingPolicyEnum;
import edu.unibo.ciri.realtime.policies.scheduling.base.SchedulingPolicy;

public  class Simulation implements Runnable{
	
	public enum SimulationState {
		UNINITIALIZED,
		INITIALIZED,
		RUNNING,
		PAUSED,
		TERMINATED
	}
	
	protected int					id					= -1;
	protected SimulationState 		state				= SimulationState.UNINITIALIZED;	
		
	protected VirtualPlatform		platform			= null;	
	
	protected Simulator 			simulator			= null;
	
	protected SchedulingPolicy		schedulingPolicy	= null;
//	protected SchedulingPolicyEnum  schedulingPolicyE   = null;
	protected SortingPolicyEnum		sortingPolicyEnum   = null;
	

	protected PriorityScheduler 	scheduler			= null;
	
	protected EventManager			eventManager		= null;
	
	protected TimeLine 				timeLine 			= null;
	
	protected JobFactory			jobFactory			= null;
	protected EventFactory			eventFactory		= null;
	
	private   SimulationResults   	simulationResults	= null;
	
	protected Thread 				simulatorThread		= null;
	
	protected TaskSet				taskset				= null;
	
	protected SimulationSuite		simulationSuite 	= null;
	
	protected EventCollector		eventCollector		= null;
	
	protected SimulationConfig		simulationConfig	= null;
	
	protected SimulationTrace		trace				= new SimulationTrace();
	
	public Simulation(){
		
	}	
		
	public Simulation(VirtualPlatform platform, PriorityScheduler scheduler, SchedulingPolicy policy, TaskSet taskset){
		
		this.platform  			= platform;
		this.scheduler 			= scheduler;
		this.schedulingPolicy 	= policy;		
		this.taskset 			= taskset;
	}
	
		
	public void setTaskset(TaskSet t){
		
		this.taskset = t;
	}
	
//	public SchedulingPolicyEnum getSchedulingPolicyEnum() {
//		return schedulingPolicyE;
//	}

//	public void setSchedulingPolicyEnum(SchedulingPolicyEnum schedulingPolicy) {
//		this.schedulingPolicyE = schedulingPolicy;
//	}
	
	public SortingPolicyEnum getSortingPolicyEnum() {
		return sortingPolicyEnum;
	}

	public void setSortingPolicyEnum(SortingPolicyEnum sortingPolicyEnum) {
		this.sortingPolicyEnum = sortingPolicyEnum;
	}
	
	
	public void start() throws SimulationException{
		
		switch(state){
		
			case UNINITIALIZED:
				init();
				break;
				
			case INITIALIZED:
				if(StaticConfig.PRINT_SIMULATION_INFO)
					System.out.println("[SIMULATION][INFO] Starting simulation!");
				break;
				
			case PAUSED:
				throw new SimulationException("Wrong state! Can't call start when simulation is paused, use resume()");
								
			case RUNNING:
				throw new SimulationException("Wrong state! This call has no effects");				
				
			case TERMINATED:
				throw new SimulationException("Wrong state! This call has no effects, simulation must be cleared");	
				
		}
		
		if(StaticConfig.PRINT_SIMULATION_INFO)
			System.out.println("[SIMULATION][INFO] Signal simulator to start!");
		
		simulationResults.setStartSimTime(System.currentTimeMillis());
		simulatorThread.start();
		
									
		try {
			simulatorThread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		simulationResults.setEndSimTime  (System.currentTimeMillis());
		
		
	}
	
	public void pause() throws SimulationException{
		
		if(!simulatorThread.isAlive())
			throw new SimulationException("Simulator Engine is dead");
		
		try {
			simulatorThread.wait();
		} catch (InterruptedException e) {
			throw new SimulationException("Simulation thread interrupted");
		}
		
	}
	
	public void resume() throws SimulationException{
		
		if(!simulatorThread.isAlive())
			throw new SimulationException("Simulator Engine is dead");
		
		simulatorThread.notify();
	}
	
	public void stop() throws SimulationException{}
	
	
	public void init() throws SimulationException{
		
		if(StaticConfig.PRINT_SIMULATION_INFO)
			System.out.println("[SIMULATION][INFO] Simulation initialize...");
		
		if(taskset == null){
			DataSet d = simulationSuite.getDataSet();
			assert(d!= null);
			synchronized (d) {
				if(d.hasNext()){
					taskset = d.next();
				}
				else{
					throw new SimulationException("No more taskset to simulate");
				}			 
			}
		}
		
		if(StaticConfig.PRINT_SIMULATION_INFO)
			System.out.println("[SIMULATION][INFO] Successfully imported "+taskset.getTasksetName());
		
		simulator 		= new Simulator(this);
		timeLine 		= new TimeLineDoubleLinkedList();		
		if(StaticConfig.PRINT_SIMULATION_INFO)
			System.out.println("[SIMULATION][INFO] - simulator created");
				
		eventFactory	= new MultipleQueueEventFactory(this);
		
		eventManager 	= new EventManager(this);
		if(StaticConfig.PRINT_SIMULATION_INFO)
			System.out.println("[SIMULATION][INFO] - event manager created");
		
		jobFactory		= new JobFactory(schedulingPolicy, taskset);
		
		if(StaticConfig.PRINT_SIMULATION_INFO)
			System.out.println("[SIMULATION][INFO] - object factories created");
		
		simulationResults = new SimulationResults(taskset);
		if(StaticConfig.PRINT_SIMULATION_INFO)
			System.out.println("[SIMULATION][INFO] - created results object store");
		
		
		simulatorThread = new Thread(simulator);
		simulatorThread.setName("SIMULATOR_ENGINE("+ taskset.getTasksetName() +")");
		if(StaticConfig.PRINT_SIMULATION_INFO)
			System.out.println("[SIMULATION][INFO] - thread "+ simulatorThread.getName() + " created");
		
		platform = new VirtualPlatform(simulationConfig.getProcessors());
				
		scheduler = new TimeStepFullPreemptiveScheduler(this);
		
		associateListenersToEvents();
		
		simulationResults.setTrace(trace);
		
		state = SimulationState.INITIALIZED;
		if(StaticConfig.PRINT_SIMULATION_INFO)
			System.out.println("[SIMULATION][INFO] Switching to INITIALIZED state");
		
	}
	
	/**
	 * Clears all the entity held by this simulation. In particular a call to <code>clear()</code>
	 * cause calling the <code>clear()</code> method in:
	 * - platform
	 * - simulator
	 * - scheduler
	 * - timeline
	 * - factories 
	 * - results
	 * */
	public void clear() throws SimulationException{
		
		/*
		 * Clear processors state
		 */
		platform.clear();	
		
		/*
		 * Clear simulator state (not the associated thread)
		 */
		simulator.clear();
		
		/*
		 * Clear the scheduler state (all queues are emptied)
		 */
		scheduler.clear();
		
		/*
		 * Clear the results
		 */
		simulationResults.clear();
		
		
	}
	
	public EventManager getEventManager() {
		return eventManager;
	}

	public VirtualPlatform getPlatform() {
		return platform;
	}

	public void setPlatform(VirtualPlatform platform) {
		this.platform = platform;
	}

	public SchedulingPolicy getSchedulingPolicy() {
		return schedulingPolicy;
	}

	public void setSchedulingPolicy(SchedulingPolicy schedulingPolicy) {
		this.schedulingPolicy = schedulingPolicy;
	}

	public PriorityScheduler getScheduler() {
		return scheduler;
	}

	public void setScheduler(PriorityScheduler scheduler) {
		this.scheduler = scheduler;
	}

	public Simulator getSimulator() {
		return simulator;
	}

	public SimulationResults getSimulationResults() {
		return simulationResults;
	}

	public TaskSet getTaskset() {
		return taskset;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public EventFactory getEventFactory() {
		return eventFactory;
	}

	public JobFactory getJobFactory() {
		return jobFactory;
	}

	public void setTimeLine(TimeLine timeLine) {
		this.timeLine = timeLine;
	}

	public TimeLine getTimeLine() {
		return timeLine;
	}

	public SimulationState getState() {
		return state;
	}
	
	public void setState(SimulationState s) {
		state = s;
	}

	public void setSimulationSuite(SimulationSuite ss){
		simulationSuite = ss;
	}

	public void setEventCollector(EventCollector display) {
		this.eventCollector = display;
	}

	public EventCollector getEventCollector() {
		return eventCollector;
	}

	public SimulationConfig getSimulationConfig() {
		return simulationConfig;
	}
	
	
	private void associateListenersToEvents() {

		eventFactory.setInterestedListenerToEventType(EventType.JOB_COMPLETION,			eventManager);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_RELEASE,			eventManager);
//		eventFactory.setInterestedListenerToEventType(EventType.JOB_DEADLINE,		 	eventManager);
		eventFactory.setInterestedListenerToEventType(EventType.SIMULATION_HALT,		eventManager);
		eventFactory.setInterestedListenerToEventType(EventType.SIMULATION_SOFT_STOP, 	eventManager);
		
		
//		eventFactory.setInterestedListenerToAllEventType(trace);
		
//		eventFactory.setInterestedListenerToEventType(EventType.JOB_COMPLETION, 		trace);
//		eventFactory.setInterestedListenerToEventType(EventType.JOB_DEADLINE, 			trace);
//		eventFactory.setInterestedListenerToEventType(EventType.JOB_PREEMPTION, 		trace);
//		eventFactory.setInterestedListenerToEventType(EventType.JOB_RELEASE, 			trace);
//		eventFactory.setInterestedListenerToEventType(EventType.JOB_RESUME, 			trace);
//		eventFactory.setInterestedListenerToEventType(EventType.JOB_WAIT, 				trace);
//		eventFactory.setInterestedListenerToEventType(EventType.JOB_START, 				trace);
		
		
		eventFactory.setInterestedListenerToEventType(EventType.JOB_RELEASE,       		scheduler);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_COMPLETION,    		scheduler);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_PRIORITY_CHANGE, 	scheduler);
		eventFactory.setInterestedListenerToEventType(EventType.TIME_STEP_FORWARD,   	scheduler);
		
		
		eventFactory.setInterestedListenerToEventType(EventType.JOB_RELEASE, 			eventCollector);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_START, 				eventCollector);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_PREEMPTION,			eventCollector);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_RESUME, 			eventCollector);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_COMPLETION, 		eventCollector);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_WAIT, 				eventCollector);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_DEADLINE,			eventCollector);
//		
//		ZeroLaxityHandler zlHandler = new ZeroLaxityHandler(taskset.getSize());							//giacomo
//		eventFactory.setInterestedListenerToEventType(EventType.JOB_RELEASE, 			zlHandler);		//
//		eventFactory.setInterestedListenerToEventType(EventType.JOB_START, 				zlHandler);		//
//		eventFactory.setInterestedListenerToEventType(EventType.JOB_PREEMPTION,			zlHandler);		//
//		eventFactory.setInterestedListenerToEventType(EventType.JOB_RESUME, 			zlHandler);		//
//		eventFactory.setInterestedListenerToEventType(EventType.JOB_COMPLETION, 		zlHandler);		//
////		eventFactory.setInterestedListenerToEventType(EventType.JOB_PRIORITY_CHANGE,zlHandler);		//
//		eventFactory.setInterestedListenerToEventType(EventType.JOB_WAIT, 				zlHandler);		//
//				


		
//		factory.setInterestedListenerToEventType(EventType.JOB_COMPLETION, 	  		consoleDisplay);
//		factory.setInterestedListenerToEventType(EventType.JOB_RELEASE, 	  		consoleDisplay);
//		factory.setInterestedListenerToEventType(EventType.JOB_START, 	  	  		consoleDisplay);
//		factory.setInterestedListenerToEventType(EventType.JOB_PREEMPTION, 	  		consoleDisplay);
//		factory.setInterestedListenerToEventType(EventType.JOB_RESUME, 	      		consoleDisplay);
//		factory.setInterestedListenerToEventType(EventType.START_TRANSACTION, 		consoleDisplay);
//		factory.setInterestedListenerToEventType(EventType.END_TRANSACTION,   		consoleDisplay);
		
		
//		factory.setInterestedListenerToEventType(EventType.JOB_DEADLINE, 			fileLog);
//		factory.setInterestedListenerToEventType(EventType.JOB_COMPLETION, 			fileLog);
//		factory.setInterestedListenerToEventType(EventType.JOB_RELEASE, 			fileLog);
//		factory.setInterestedListenerToEventType(EventType.JOB_START, 				fileLog);
//		factory.setInterestedListenerToEventType(EventType.JOB_RESUME, 				fileLog);
//		factory.setInterestedListenerToEventType(EventType.SIMULATION_HALT, 		fileLog);
//		factory.setInterestedListenerToEventType(EventType.SIMULATION_SOFT_STOP, 	fileLog);
//		factory.setInterestedListenerToEventType(EventType.TIME_ADVANCE, 			fileLog);
		
			
		
//		eventFactory.setInterestedListenerToEventType(EventType.JOB_COMPLETION, 		simulationResults);
//		eventFactory.setInterestedListenerToEventType(EventType.JOB_DEADLINE,   		simulationResults);
//		eventFactory.setInterestedListenerToEventType(EventType.JOB_PREEMPTION, 		simulationResults);
//		eventFactory.setInterestedListenerToEventType(EventType.JOB_START, 				simulationResults);
//		eventFactory.setInterestedListenerToEventType(EventType.JOB_RESUME, 			simulationResults);
//		eventFactory.setInterestedListenerToEventType(EventType.JOB_RELEASE, 			simulationResults);
//		eventFactory.setInterestedListenerToEventType(EventType.SIMULATION_HALT, 		simulationResults);
//		eventFactory.setInterestedListenerToEventType(EventType.SIMULATION_SOFT_STOP, 	simulationResults);
	
//		factory.setInterestedListenerToEventType(EventType.JOB_COMPLETION, 				logger);
//		factory.setInterestedListenerToEventType(EventType.JOB_DEADLINE, 				logger);
//		factory.setInterestedListenerToEventType(EventType.JOB_PREEMPTION, 				logger);
//		factory.setInterestedListenerToEventType(EventType.JOB_START, 					logger);
//		factory.setInterestedListenerToEventType(EventType.JOB_RESUME, 					logger);
//		factory.setInterestedListenerToEventType(EventType.JOB_RELEASE, 				logger);
//		factory.setInterestedListenerToEventType(EventType.SIMULATION_HALT, 			logger);
//		factory.setInterestedListenerToEventType(EventType.SIMULATION_SOFT_STOP, 		logger);
				
//		factory.setInterestedListenerToEventType(EventType.OVERHEAD_CONTEXT_SWITCH_START, 	  overHeadHandler);
//		factory.setInterestedListenerToEventType(EventType.OVERHEAD_CONTEXT_SWITCH_END, 	  overHeadHandler);		
		
		
		eventFactory.setInterestedListenerToEventType(EventType.JOB_COMPLETION, 	eventFactory);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_DEADLINE, 		eventFactory);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_PREEMPTION, 	eventFactory);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_PRIORITY_CHANGE,eventFactory);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_RELEASE, 		eventFactory);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_RESUME, 		eventFactory);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_WAIT, 			eventFactory);
		eventFactory.setInterestedListenerToEventType(EventType.JOB_START, 			eventFactory);
		eventFactory.setInterestedListenerToEventType(EventType.PLATFORM_IDLE,	 	eventFactory);		
		
		
		if(StaticConfig.PRINT_LISTENER_INTERESTED_TO_EVENTS){
			EventType types[] = EventType.values();
			
			for(int i = 0; i < types.length; i++){
				System.out.println("Event Type: "+ types[i].getTypeName());
				System.out.print("Listeners: ");
				for(EventListener listener: eventFactory.getListenersInterestedInEventType(types[i])){
					System.out.print( listener.getName() + " ");
				}	
				System.out.println("");
			}
		}
		
	}
	
	public void fire(Event e){
		
		e.setState(EventState.HANDLING);
		
		if(StaticConfig.USE_LISTENER_IN_EVENTS)
			e.fire();
		
		if(StaticConfig.USE_LISTENER_BY_EVENT_TYPE){
			for(EventListener listener: eventFactory.getListenersInterestedInEventType(e.getEventType())){
				if(StaticConfig.PRINT_LISTENER_FIRING_EVENT)
					System.out.println("[FIRING][LISTENER = " + listener.getName() + "] is about to fire an event " + e.getEventType().getTypeName() );
				listener.handle(e);	
			}
		}
//		e.setState(EventState.HANDLED);
	}
	
	public void setSimulationConfig(SimulationConfig conf){
		this.simulationConfig = conf;
	}

	@Override
	public void run() {
		
		try {
			init();
		} catch (SimulationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			start();
		} catch (SimulationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
