package edu.unibo.ciri.desert.simulation.results;

import java.io.PrintStream;
import java.text.NumberFormat;
import java.util.Hashtable;
import java.util.Locale;


public class SuccessRatioFunction {
	
	public class SuccessRatioFunctionValue{
		
		private int tasksetSimulatedCount = 0;
		private int tasksetSuccessCount   = 0;
		
		public int getSimulatedCount(){
			return tasksetSimulatedCount;
		}	
		
		public int getSuccessCount(){
			return tasksetSuccessCount;
		}
		
		public double getRatio(){
			return ((double)tasksetSuccessCount)/tasksetSimulatedCount;
		}
		
		public void addOccurrence(boolean success){
			if(success)
				tasksetSuccessCount++;
			tasksetSimulatedCount++;
		}
		
	}
	
	public Hashtable<String, SuccessRatioFunctionValue> table = new Hashtable<String, SuccessRatioFunctionValue>();
	
	public NumberFormat nf =  NumberFormat.getInstance(Locale.ITALY);
	
	public SuccessRatioFunction(){
		
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
	}
	
	public synchronized void addOccurrence(String utilization, boolean success, PrintStream ps){
		
		SuccessRatioFunctionValue srfv = table.get(utilization);
		if(srfv == null){
			srfv = new SuccessRatioFunctionValue();
			table.put(utilization,srfv);
		}
		srfv.addOccurrence(success);
		
		System.out.print("\t\t\t\t\t\t"+utilization + " " + nf.format(srfv.getRatio()) + " " );
		System.out.println(srfv.getSuccessCount() + "/" + srfv.getSimulatedCount() );
		
		//da stampare su file
//		ps.println( nf.format(srfv.getRatio()) + "\t" + srfv.getSuccessCount() + "\t" + srfv.getSimulatedCount() );
	}
	
	public double getSuccessRatio(String utilization){
		
		SuccessRatioFunctionValue srfv = table.get(utilization);
		
		//XXX lanciare eccezione
		if(srfv == null)
			return -1;
				
		return srfv.getRatio(); 
	}

}
