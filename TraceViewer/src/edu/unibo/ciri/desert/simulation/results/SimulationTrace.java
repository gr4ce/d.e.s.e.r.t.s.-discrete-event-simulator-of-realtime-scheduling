package edu.unibo.ciri.desert.simulation.results;

import java.util.Vector;

import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.event.management.EventListener;
import edu.unibo.ciri.desert.graphics.diagram.schedule.elements.TracedEvent;
import edu.unibo.ciri.realtime.model.Task;


public class SimulationTrace implements EventListener{
	
	private final int INITIAL_SIZE 		= 500;
	private Vector<TracedEvent> events 	= new Vector<TracedEvent>(INITIAL_SIZE);
	private TracedEvent[] tmpEvents 	= new TracedEvent[INITIAL_SIZE];
	private int currentRecordingEvent   = 0;
	
	
	public SimulationTrace(){
		
		for(int i = 0; i < INITIAL_SIZE; i++ ){
			tmpEvents[i] = new TracedEvent();
		}
		
	}

	@Override
	public String getName() {		
		return "SimulationTracer";
	}

	@Override
	public void handle(Event event) {
		
		if(currentRecordingEvent == INITIAL_SIZE)
			saveAndReallocate();
		
		/*
		 * These are present anyway 
		 */
		tmpEvents[currentRecordingEvent].setType(event.getEventType());
		tmpEvents[currentRecordingEvent].setTime(event.getEventTimeValue());
		
		Task target = event.getTask();		
		if(target != null){
			tmpEvents[currentRecordingEvent].setJobId(target.getActiveJob().getUniqueId());
			tmpEvents[currentRecordingEvent].setProcessorId(target.getActiveJob().getCpu());
			tmpEvents[currentRecordingEvent].setJobInstance(target.getActiveJob().getProgInstanceNumber());
			tmpEvents[currentRecordingEvent].setTaskId(target.getTaskId());
		}	
		
		currentRecordingEvent++;
		
	}
	
	private void saveAndReallocate(){
		
		for(int i = 0; i < INITIAL_SIZE; i++ ){
			events.add(tmpEvents[i]);
			tmpEvents[i] = new TracedEvent();
		}
		currentRecordingEvent = 0;
		
	}

	public Vector<TracedEvent> getEvents() {
		return events;
	}

	
}
