package edu.unibo.ciri.desert.simulation.results;

import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.event.management.EventListener;
import edu.unibo.ciri.realtime.model.TaskSet;


public class SimulationResults implements EventListener{
	
	private long eventHandled 		= 0;
	private long eventSuppressed	= 0;
	private long eventTriggered		= 0;
	private long eventCreated		= 0;
	
	private int  totalMissed  		= 0;
	private int  totalOverrun 		= 0;
	private long totalPreemption 	= 0;
	
	private long startSimTime		= 0;
	private long endSimTime			= 0;
		
	private long virtualSimTime		= 0;
	
	
	
	private TaskSet ts							= null;
	
	private SimulationTrace	trace				= null;
	
	
	
	public SimulationResults(TaskSet taskset){
		this.ts = taskset;
	}
	
	public TaskSet getTaskSet(){
		return this.ts;
	}
	
	public long getEventSuppressed() {
		return eventSuppressed;
	}

	public void setEventSuppressed(long eventSuppressed) {
		this.eventSuppressed = eventSuppressed;
	}

	public long getEventTriggered() {
		return eventTriggered;
	}

	public void setEventTriggered(long eventTriggered) {
		this.eventTriggered = eventTriggered;
	}

	public long getStartSimTime() {
		return startSimTime;
	}

	public void setStartSimTime(long startSimTime) {
		this.startSimTime = startSimTime;
	}

	public long getEndSimTime() {
		return endSimTime;
	}

	public void setEndSimTime(long endSimTime) {
		this.endSimTime = endSimTime;
	}

	public long getVirtualSimTime() {
		return virtualSimTime;
	}

	public void setVirtualSimTime(long virtualSimTime) {
		this.virtualSimTime = virtualSimTime;
	}

	public void setEventHandled(long eventHandled) {
		this.eventHandled = eventHandled;
	}

	private static String tableRowFormatForSingleEvent   = "Table row format for single event not set!!";
	private static String tableRowFormatForMultipleEvent = "Table row format for multiple event not set!!";
	
	public static String getTableRowFormatForMultipleEvent() {		
		return tableRowFormatForMultipleEvent;
	}

	public static void setTableRowFormatForMultipleEvent(
			String tableRowFormatForMultipleEvent) {
		SimulationResults.tableRowFormatForMultipleEvent = tableRowFormatForMultipleEvent;
	}

	private static String tableHeader = "Table header not set!!";
	
	public static String getTableHeader() {
		return tableHeader;
	}

	public static void setTableHeader(String tableHeader) {
		SimulationResults.tableHeader = tableHeader;
	}

	public static String getTableRowFormatForSingleEvent() {
		return tableRowFormatForSingleEvent;
	}

	public static void setTableRowFormatForSingleEvent(String tableFormat) {
		SimulationResults.tableRowFormatForSingleEvent = tableFormat;
	}

	public void incTotalOverrun(){
		totalOverrun++;
	}
	
	public int getTotalOverrun(){
		return totalOverrun;
	}
	
	public void incTotalMissed(){
		totalMissed++;
	}
	
	public int getTotalMissed(){
		return totalMissed;
	}
	
	public void incEventHandled(){
		eventHandled++;
	}

	public long getEventHandled(){
		return eventHandled;
	}
	
	public long getTotalPreemption(){
		return totalPreemption;
	} 
	
	public void incTotalPreemption(){
		totalPreemption++;
	}
	
	public void setTotalPreemption(long preemption){
		this.totalPreemption = preemption;
	}
	
	private void handleJobPreemption(Event e){
		incTotalPreemption();
	}
	
	private void handleJobMigration(Event e){
		
	}
	
	private void handleJobDeadline(Event e){
		incTotalMissed();
	}

	private void handleJobOverrun(Event e){
		
	}
	
	
	@Override
	public void handle(Event event) {
		
		switch(event.getEventType()){
			case JOB_PREEMPTION:
				handleJobPreemption(event);
				break;
			case JOB_MIGRATION:
				handleJobMigration(event);
				break;
			case JOB_DEADLINE:
				handleJobDeadline(event);
				break;
			case JOB_OVERRUN:
				handleJobOverrun(event);
				break;
		}
		
	}

	public void clear(){
		
		eventHandled 		= 0;
		eventSuppressed		= 0;
		eventTriggered		= 0;
		
		totalMissed  		= 0;
		totalOverrun 		= 0;
		totalPreemption 	= 0;
		
		startSimTime		= 0;
		endSimTime			= 0;
			
		virtualSimTime		= 0;
		
	}

	public void setEventCreated(long eventCreated) {
		this.eventCreated = eventCreated;
	}

	public long getEventCreated() {
		return eventCreated;
	}

	@Override
	public String getName() {		
		return "SimulationResults";
	}

	public void setTrace(SimulationTrace trace) {
		this.trace = trace;
	}

	public SimulationTrace getTrace() {
		return trace;
	}

	
	

	

	
}
