package edu.unibo.ciri.desert.analysis.schedulability.test.multiprocessor;

import edu.unibo.ciri.desert.analysis.schedulability.test.SchedulabilityTest;
import edu.unibo.ciri.desert.resource.VirtualPlatform;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.policies.scheduling.base.SchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.base.StaticSortingAlgorithm;

public class EdfTestGoosens2003 implements SchedulabilityTest{
	
	private TaskSet ts 	= null;
	private int m 		= -1;		
	private boolean isSchedulable = false;

	@Override
	public void setTaskset(TaskSet t) {
		this.ts = t;		
	}

	@Override
	public void setPlatform(VirtualPlatform p) {
		this.m = p.getMaxProcessor();		
	}

	@Override
	public void setSchedulingPolicy(SchedulingPolicy sp) {
		
	}

	@Override
	public void setStaticSortingPolicy(StaticSortingAlgorithm ssp) {
		
	}

	@Override
	public void runTest() {
		
		if(ts == null)
			; //thwor an exception
			
		if(m == -1);
			; //thwor an exception
		
		int n = ts.size();
		double tasksetUtilization = ts.getUtilization();
		
				
		double maxTaskUtilization = 0;
		double currentTaskUtilization;
		
		double taskComputation, taskPeriod;
		
		//Find max utilization among tasks
		for(int i = 0; i < n; i++){
			
			taskComputation = ts.getTask(i).getTaskParameters().getComputation().getTimeValue();
			taskPeriod = ts.getTask(i).getTaskParameters().getPeriod().getTimeValue();
			
			
			currentTaskUtilization = taskComputation / taskPeriod;
			
//			System.out.println("U_t = " + currentTaskUtilization);
			
			if(currentTaskUtilization > maxTaskUtilization)
				maxTaskUtilization = currentTaskUtilization;
			
		}
		
		double proc = m;
		double u_edf = (1F - maxTaskUtilization) * proc  + maxTaskUtilization;
//		System.out.println("U_EDF = " + u_edf);
		
		if( tasksetUtilization < u_edf )
			isSchedulable = true;
		else
			isSchedulable = false;
		
		
	}

	@Override
	public boolean isSchedulable() {
		return isSchedulable;
	}

	@Override
	public boolean isSchedulable(int taskId) {
		// TODO Auto-generated method stub
		return false;
	}

}
