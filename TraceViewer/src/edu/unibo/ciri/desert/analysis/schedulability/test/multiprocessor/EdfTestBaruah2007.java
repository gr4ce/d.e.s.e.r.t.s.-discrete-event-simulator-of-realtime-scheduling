package edu.unibo.ciri.desert.analysis.schedulability.test.multiprocessor;

import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;



import util.SortingAlgorithm.SortingOrder;
import edu.unibo.ciri.collection.PriorityLinkedList;
import edu.unibo.ciri.desert.analysis.schedulability.test.SchedulabilityTest;
import edu.unibo.ciri.desert.resource.VirtualPlatform;
import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.policies.scheduling.base.SchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.base.StaticSortingAlgorithm;
import edu.unibo.ciri.util.sort.algorithm.QuickSort;

/**
 * @author 			Giuseppe Salvatore
 * @date   			26/01/2012
 * @current_version 0.1
 * 
 * This class implements the schedulability test presented in the work of Sanjoy Baruah
 * published in 2007 titled "Techniques for Multiprocessor Global Schedulability" 
 *
 */
public class EdfTestBaruah2007 implements SchedulabilityTest{
		
	private TaskSet ts;
	private int m;		
	private boolean isSchedulable = false;	
	private int currentArraySize = -1;
	
	private SortedSet<Long> A[];
	private long Abound[];
	private long I_diff[];
	private Long Ak_values[];
	private Long[] computations;
	private QuickSort<Long> sorter;
	
	public EdfTestBaruah2007(){
		sorter = new QuickSort<Long>();
		sorter.setComparator(new Comparator<Long>() {
			
			@Override
			public int compare(Long l1, Long l2){
				if(l1 > l2)
					return 1;
				if(l1 < l2)
					return -1;
				return 0;
			}
			
		});
	}
	
	@Override
	public void setTaskset(TaskSet t) {
		ts = t;		
	}

	@Override
	public void setPlatform(VirtualPlatform p) {
		m = p.getMaxProcessor();		
	}

	@Override
	public void setSchedulingPolicy(SchedulingPolicy sp) {		
	}

	@Override
	public void setStaticSortingPolicy(StaticSortingAlgorithm ssp) {
			
	}
	
	@Override
	public boolean isSchedulable() {
		return isSchedulable;
	}
	
	private void init(int size){
		
		currentArraySize = size;
		
		A = new TreeSet[size];
		Abound = new long[size];
		I_diff = new long[size];
		computations = new Long[size];
		
	}
	
	@Override
	public void runTest() {								
			
		
		int n = ts.size();
		
		if(n > currentArraySize){
			init(n);
		}
				
		isSchedulable = false;
				
		/* For each task compute its A set made by all time values to be checked
		 * in the schedulability check. These values must be less then an upper
		 * bound computed by <code>compute_Ak_bound()</code> method 
		 */
		for(int k = 0; k < n; k++){
			A[k] = new TreeSet<Long>();
			Abound[k] = compute_Ak_bound(k);
//			System.out.println("Abound["+k+"] = " + Abound[k]);
		}
		
		
		long t;
		int s;

		long I_nc = 0L;
		long I_ci = 0L;
		
		
		/* For each task do a schedulability check */
		for(int k = 0; k < n; k++){
			
			t = 0L;
			s = 0;
			
			A[k].add(0L);
			Task tk = ts.getTask(k);
			long Tk = tk.getTaskParameters().getPeriod().getTimeValue();
			long Dk = tk.getTaskParameters().getDeadline().getTimeValue();
			long Ck = tk.getTaskParameters().getComputation().getTimeValue();
			
			
			t = s*Tk + Dk;
			while(t <= Abound[k]){
				A[k].add(t);
				s++;
				
				t = s*Tk + Dk;
			}
			
//			System.out.println("Abound = "+Abound[k]);
//			System.out.println("A["+k+"]: " + A[k]);
			
			Ak_values = A[k].toArray(new Long[A[k].size()]);
			PriorityLinkedList<Long> ll = new PriorityLinkedList<Long>();
			
			for(int j = 0; j < Ak_values.length; j++){
				
				long sum_interference_nc = 0, sum_interference_diff = 0;
				
				for(int i = 0; i < n; i++){
					I_ci = compute_interference_with_carry_in(i, k, Ak_values[j], Ck, Dk);
					I_nc = compute_interference_with_no_carry(i, k, Ak_values[j], Ck, Dk);
//					System.out.println("Task i=" + i + " interfers CI over Task k=" + k + " by "+I_ci[i]);
//					System.out.println("Task i=" + i + " interfers NC over Task k=" + k + " by "+I_nc[i]);
					sum_interference_nc += I_ci;
					                            
					I_diff[i] = I_ci - I_nc;
					ll.orderedInsert(I_diff[i]);
					
				}
				
				for(int v = 0 ; v < m - 1; v++){
					Long val = ll.getFirst();
					sum_interference_diff += val;
					ll.removeElementAt(0);
				}
				
				if( sum_interference_nc + sum_interference_diff > m * (Ak_values[j] + Dk - Ck) ){
//					System.out.println(sum_interference_nc + " + " + 
//							sum_interference_diff + " > "  + 
//							m + "  * (" + Ak_values[j] + " + "+ Dk  +" - " +  Ck + ")" );
					isSchedulable = false;
					return;
				}
			}
		}
				
		isSchedulable = true;
		return;
				
	}

	
	

	
	/* Also known as I1(task_i) */
	private long compute_interference_with_no_carry(int i, int k, long Ak, long Ck, long Dk){
		if(i != k)
			return Math.min(DBF(ts.getTask(i), Ak + Dk), Ak + Dk - Ck);
		else			
			return Math.min(DBF(ts.getTask(i), Ak + Dk) - Ck, Ak);
				
	}
	
	/* Also known as I2(task_i) */
	private long compute_interference_with_carry_in(int i, int k, long Ak, long Ck, long Dk){
		if(i != k)
			return Math.min(DBF_2(ts.getTask(i), Ak + Dk), Ak + Dk - Ck);
		else			
			return Math.min(DBF_2(ts.getTask(i), Ak + Dk) - Ck, Ak);
	}
	
	
	
	private long compute_Ak_bound(int k){
		
		Task tk = ts.getTask(k);
		long Dk = tk.getTaskParameters().getDeadline().getTimeValue();
		long Ck = tk.getTaskParameters().getComputation().getTimeValue();
		
		long Ti,Di;
		double Ui,internal_sum = 0;
		
		
		for(int i = 0; i < ts.size(); i++){
			
			Task ti = ts.getTask(i);
			Ti = ti.getTaskParameters().getPeriod().getTimeValue();
			Di = ti.getTaskParameters().getDeadline().getTimeValue();
			Ui = ti.getTaskParameters().getComputation().getTimeValue() / (double)Ti;
			
			internal_sum += (Ti - Di)*Ui;
//			System.out.println(" ( Ti = " + Ti +  " - Di = " + Di + " ) *Ui = "+ Ui);
			
		}
//		System.out.println("sum( Ti - Di )*Ui = "+internal_sum);
		
		double Utot = ts.getUtilization();
		
		long Csum = compute_Csum();
		
		
		
		long result = (long)((Csum - Dk*(m - Utot) + internal_sum + m*Ck)/(m - Utot));
		
		
//		double resultd = ((Csum - Dk*(m - Utot) + internal_sum + m*Ck)/(m - Utot));
//		System.out.println(resultd + " == " + result);
//		
//		System.out.println("A[" + k + "] must be less then "+ Csum + " - " + Dk + " * ( " + m + 
//				" - " + Utot + " ) + " + internal_sum + " + " + m + "*" + Ck + " / " + "( " + m + 
//						" - " + Utot + ")");
		
		return result;
	}
	
	private long compute_Csum(){
		
		Long computationForAllTask = 0L;
		Long[] computations = new Long[ts.size()];
		
		/* Sort in a list the execution requirements of the n tasks */
		for(int i = 0; i < computations.length; i++){
			if( i < ts.size())
				computations[i] = ts.getTask(i).getTaskParameters().getComputation().getTimeValue();
			else
				computations[i] = 0L;
		}
		
		sorter.setArray(computations);		
		sorter.sort(SortingOrder.NON_INCREASING_ORDER);
		assert(sorter.isSorted(computations, SortingOrder.NON_INCREASING_ORDER));
		
		/* Sum the m-1 highest values of the execution requirements */
		for(int i = 0; i < (m - 1); i++){
//			System.out.println("c["+i+"] = "+ computations[i]);
			computationForAllTask += computations[i];
		}		
				
		return computationForAllTask.longValue();
	}
	
	private long DBF(Task tau_i, long timeBound){
		
		long C = tau_i.getTaskParameters().getComputation().getTimeValue();
		long T = tau_i.getTaskParameters().getPeriod().getTimeValue();
		long D = tau_i.getTaskParameters().getDeadline().getTimeValue();
		
		long diff = timeBound - D;
		long instances = diff / T;
		return Math.max(0L, (instances + 1) * C);
	}
	
	private long DBF_2(Task tau_i, long timeBound){
		
		long C = tau_i.getTaskParameters().getComputation().getTimeValue();
		long T = tau_i.getTaskParameters().getPeriod().getTimeValue();
		long t = timeBound;
		
		
		long instances = t / T;
		return instances * C + Math.min(C , t % T);
	}

	@Override
	public boolean isSchedulable(int taskId) {
		// TODO Auto-generated method stub
		return false;
	}
	
}
