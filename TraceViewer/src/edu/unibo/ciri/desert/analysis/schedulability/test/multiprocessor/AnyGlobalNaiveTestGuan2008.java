package edu.unibo.ciri.desert.analysis.schedulability.test.multiprocessor;

import java.util.Comparator;

import util.SortingAlgorithm.SortingOrder;
import edu.unibo.ciri.desert.analysis.schedulability.test.SchedulabilityTest;
import edu.unibo.ciri.desert.resource.VirtualPlatform;
import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.policies.scheduling.base.SchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.base.StaticSortingAlgorithm;
import edu.unibo.ciri.util.sort.algorithm.QuickSort;


public class AnyGlobalNaiveTestGuan2008 implements SchedulabilityTest {

	private TaskSet ts;
	private int m;
	private boolean isSchedulable;
	private QuickSort<Long> sorter;
	
	public AnyGlobalNaiveTestGuan2008(){
	
		sorter = new QuickSort<Long>();
		sorter.setComparator(new Comparator<Long>() {
			
			@Override
			public int compare(Long l1, Long l2){
				if(l1 > l2)
					return 1;
				if(l1 < l2)
					return -1;
				return 0;
			}
			
		});
	
	}
	
	@Override
	public void setTaskset(TaskSet t) {
		ts = t;		
	}

	@Override
	public void setPlatform(VirtualPlatform p) {
		m = p.getMaxProcessor();		
	}

	@Override
	public void setSchedulingPolicy(SchedulingPolicy sp) {		
	}

	@Override
	public void setStaticSortingPolicy(StaticSortingAlgorithm ssp) {
			
	}
	
	@Override
	public boolean isSchedulable() {
		return isSchedulable;
	}

	@Override
	public void runTest() {
				
		int n = ts.size();		
		
		long sumSortedClist =  compute_Csum();
		long sumAllC = 0;
		double Smin = Double.MAX_VALUE;
		
		for(int i = 0; i < ts.size(); i++){
			
			Task ti = ts.getTask(i);
			long Ci = ti.getTaskParameters().getComputation().getTimeValue();
			long Di = ti.getTaskParameters().getDeadline().getTimeValue();
			if(Di - Ci < Smin)
				Smin = Di - Ci;
			
			sumAllC += Ci;			
		}
		double Utot = ts.getUtilization();
		
		isSchedulable = false;
		
		if( Utot < m - (sumAllC + sumSortedClist) / Smin )
			isSchedulable = true;
		
		isSchedulable = true;
		
	}
	
	private long compute_Csum(){
		
		Long computationForAllTask = 0L;
		Long[] computations = new Long[ts.size()];
		
		/* Sort in a list the execution requirements of the n tasks */
		for(int i = 0; i < ts.size(); i++){
			computations[i] = ts.getTask(i).getTaskParameters().getComputation().getTimeValue();
		}
		
		sorter.setArray(computations);		
		sorter.sort(SortingOrder.NON_INCREASING_ORDER);
		assert(sorter.isSorted(computations, SortingOrder.NON_INCREASING_ORDER));
		
		/* Sum the m-1 highest values of the execution requirements */
		for(int i = 0; i < (m - 1); i++){
			computationForAllTask += computations[i];
		}		
				
		return computationForAllTask.longValue();
	}

	@Override
	public boolean isSchedulable(int taskId) {
		// TODO Auto-generated method stub
		return false;
	}
}
