package edu.unibo.ciri.desert.analysis.schedulability.test;

import edu.unibo.ciri.desert.resource.VirtualPlatform;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.policies.scheduling.base.SchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.base.StaticSortingAlgorithm;


public interface SchedulabilityTest {	
	
	void setTaskset(TaskSet t);
	
	void setPlatform(VirtualPlatform p);
	
	void setSchedulingPolicy(SchedulingPolicy sp);
	
	void setStaticSortingPolicy(StaticSortingAlgorithm ssp);
	
	void runTest();
	
		
	boolean isSchedulable();
	
	boolean isSchedulable(int taskId);
	
	boolean isSchedulable(TaskSet ts);
	
	
	String getName();

}
