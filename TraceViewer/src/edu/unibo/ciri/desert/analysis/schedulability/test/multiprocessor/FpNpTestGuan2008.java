package edu.unibo.ciri.desert.analysis.schedulability.test.multiprocessor;

import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;


import util.SortingAlgorithm.SortingOrder;
import edu.unibo.ciri.desert.analysis.schedulability.test.SchedulabilityTest;
import edu.unibo.ciri.desert.resource.VirtualPlatform;
import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.policies.scheduling.FixedPriorityAssignment;
import edu.unibo.ciri.realtime.policies.scheduling.base.SchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.base.StaticSortingAlgorithm;
import edu.unibo.ciri.util.sort.algorithm.QuickSort;

public class FpNpTestGuan2008 implements SchedulabilityTest {
	
	
	private TaskSet ts;
	private Task[] sortedTaskArray;
	private int m;
	
	private StaticSortingAlgorithm sortingPolicy;
	private FixedPriorityAssignment spa;
	private boolean isSchedulable = false;
	private QuickSort<Long> sorter;
	
	public FpNpTestGuan2008(){
		sorter = new QuickSort<Long>();
		sorter.setComparator(new Comparator<Long>() {
			
			@Override
			public int compare(Long l1, Long l2){
				if(l1 > l2)
					return 1;
				if(l1 < l2)
					return -1;
				return 0;
			}
			
		});
		
	}
	
	@Override
	public void setTaskset(TaskSet t) {
		ts = t;		
	}

	@Override
	public void setPlatform(VirtualPlatform p) {
		m = p.getMaxProcessor();		
	}

	@Override
	public void setSchedulingPolicy(SchedulingPolicy sp) {		
	}

	@Override
	public void setStaticSortingPolicy(StaticSortingAlgorithm ssp) {
		sortingPolicy = ssp;		
	}
	
	@Override
	public boolean isSchedulable() {
		return isSchedulable;
	}

	@Override
	public void runTest() {
		
		spa = sortingPolicy.getStaticPriorityAssignment(ts);
		sortedTaskArray = spa.getSortedTaskArray();
		
		
		int n = sortedTaskArray.length;
		
		SortedSet<Long> A[] = new TreeSet[n];
		long Abound[] = new long[n];
		
		/* For each task compute its A set made by all time values to be checked
		 * in the schedulability check. These values must be less then an upper
		 * bound computed by <code>compute_Ak_bound()</code> method 
		 */
			
		for(int k = 0; k < n; k++){
			
			Task tk = sortedTaskArray[k];
			long Tk = tk.getTaskParameters().getPeriod().getTimeValue();
			long Dk = tk.getTaskParameters().getDeadline().getTimeValue();
			long Ck = tk.getTaskParameters().getComputation().getTimeValue();
			
			A[k] = new TreeSet<Long>();
			Abound[k] = compute_Ak_bound(k, Dk - Ck);
//			System.out.println("Abound["+k+"] = " + Abound[k]);
			
			long t = 0L;
			int s = 0;
			
			A[k].add(0L);
						
			
			t = s*Tk + Dk;
			while(t <= Abound[k]){
				A[k].add(t);
				s++;				
				t = s*Tk + Dk;
			}
			
//			System.out.println("A["+k+"]: " + A[k]);
			Long Ak_values[] = A[k].toArray(new Long[A[k].size()]);
			 
			for(int j = 0; j < Ak_values.length; j++){
				if(schedCheck(k,Ak_values[j]) == false){
					isSchedulable = false;
					return;
				}
			}
		}
		isSchedulable = true;
		
	}




	private boolean schedCheck(int k, long Ak){
		
		Task tk = sortedTaskArray[k];
		
		long Ck = tk.getTaskParameters().getComputation().getTimeValue();
		long Dk = tk.getTaskParameters().getDeadline().getTimeValue();
		long Tk = tk.getTaskParameters().getDeadline().getTimeValue();
		
		long sum_Inc = 0;
		Long[] Idiff_list = new Long[sortedTaskArray.length];
		
		for(int i = 0; i < sortedTaskArray.length; i++){
			
			long Inc = compute_interference_with_no_carry(i, k, Ak, Ck, Dk, Tk);
			sum_Inc += Inc;
			
			long Ici = compute_interference_with_carry_in(i, k, Ak, Ck, Dk, Tk);
			Idiff_list[i] = Ici - Inc;			
		}
		
		sorter.setArray(Idiff_list);		
		sorter.sort(SortingOrder.NON_INCREASING_ORDER);
		
		long Idiff_sum = 0;
		
		for(int j = 0; j < m - 1; j++){
			Idiff_sum += Idiff_list[j];
		}
		
		return Idiff_sum + sum_Inc < (Ak + Dk - Ck) * m; 
		
	}
	
	/* Also known as I1(task_i) */
	private long compute_interference_with_no_carry(int i, int k, long Ak, long Ck, long Dk, long Tk){
		
		long Sk = Dk - Ck;
		long Ci = sortedTaskArray[i].getTaskParameters().getComputation().getTimeValue();
		long Ti = sortedTaskArray[i].getTaskParameters().getPeriod().getTimeValue();
		long alpha2 = compute_alpha2(Ak,Sk,Ti);
		
		//For tasks with lower priority then task k
		if( i > k ){
			if( Ak == 0){
				return 0;
			} else if( Ak > 0 && Ak <= alpha2 ){
				return ((Ak + Sk)/Ti) * Ci; // I1,2(tau_i)
			} else{
				return ((Ak + Sk)/Ti) * Ci + Math.min(Ci, (Ak + Sk) % Ti); // I1,3(tau_i)
			}	
			
		} else if( i == k){
			return (Ak/Tk)*Ck; // I1,1(tau_i)
		} else{
			return ((Ak + Sk)/Ti) * Ci + Math.min(Ci, (Ak + Sk) % Ti); // I1,3(tau_i)
		}
					
	}
	
	
	/* Also known as I2(task_i) */
	private long compute_interference_with_carry_in(int i, int k, long Ak, long Ck, long Dk, long Tk){
		
		long Sk = Dk - Ck;
		long Ci = sortedTaskArray[i].getTaskParameters().getComputation().getTimeValue();
		long Ti = sortedTaskArray[i].getTaskParameters().getPeriod().getTimeValue();
		long Di = sortedTaskArray[i].getTaskParameters().getDeadline().getTimeValue();
		
		long v; 
		long w;
		
		//For tasks with higher priority then task k
		if( i == k ){

			return ((Ak + Dk)/Tk) * Ck + Math.min(Ck, (Ak + Dk) % Tk) - Ck;		
		} 
		
		if( i > k && Sk >= Ci){
			if(Ak == 0){
				return Ci - 1;
			}
			if(Ak > 0){
				v = Math.min(Ci, Math.max(0, (Ak-1) % Ti - (Ti - Di)));
				return (((Ak - 1)/Ti) + 1) * Ci + v;
			}
		}
		
		if( Ak + Sk <= Ci){
			return Ak + Sk;
		}else{
			assert( Ak + Sk > Ci );
			w = Math.min(Ci, Math.max(0, (Ak-Sk-Ci) % Ti - (Ti - Di)));
			return ((Ak + Sk - Ci)/Ti) * Ci + Ci + w;
		}
		
	}
	
	private long compute_alpha2_floor(long Ak, long Sk, long Ti){
		
		return (long)(Math.floor((Ak + Sk)/Ti)*Ti);
	}
	
	private long compute_alpha2(long Ak, long Sk, long Ti){
		
		long result = ((Ak + Sk)/Ti)*Ti;
		assert(result == compute_alpha2_floor(Ak, Sk, Ti));
		return result;
	}
	
	private long compute_Ak_bound(int k, long Sk){
					
		long sum_all_computation = 0;
			
		for(int i = 0; i < ts.size(); i++){
			
			Task ti = ts.getTask(i);
			long Ci = ti.getTaskParameters().getComputation().getTimeValue();			
			sum_all_computation += Ci;
			
		}
				
		double Utot = ts.getUtilization();
		
		long Csum = compute_Csum();
//		System.out.println("\ncsum = "+ Csum);
//		System.out.println("allsum = "+ sum_all_computation);
		double fraction = (Csum + sum_all_computation)/(m - Utot);
//		System.out.println("fraction = "+fraction);
//		System.out.println("Sk = "+Sk);
		
		long result = (long)( fraction - Sk);
						
		return result;
	}
	
	private long compute_Csum(){
		
		Long computationForAllTask = 0L;
		Long[] computations = new Long[ts.size()];
		
		/* Sort in a list the execution requirements of the n tasks */
		for(int i = 0; i < ts.size(); i++){
			computations[i] = ts.getTask(i).getTaskParameters().getComputation().getTimeValue();
		}
		
		sorter.setArray(computations);		
		sorter.sort(SortingOrder.NON_INCREASING_ORDER);
		assert(sorter.isSorted(computations, SortingOrder.NON_INCREASING_ORDER));
		
		/* Sum the m-1 highest values of the execution requirements */
		for(int i = 0; i < (m - 1); i++){
			computationForAllTask += computations[i];
		}		
				
		return computationForAllTask.longValue();
	}

	@Override
	public boolean isSchedulable(int taskId) {
		// TODO Auto-generated method stub
		return false;
	}
	

}
