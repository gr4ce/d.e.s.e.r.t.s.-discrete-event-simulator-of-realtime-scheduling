package edu.unibo.ciri.desert.user.listeners;

import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.event.EventType;

public class HandledEventInfo{
	
	private EventType eventType;	
	private long 	  eventTime;
	private String 	  targetTaskName = "none";	
	private int    	  targetJobId = -1;	
	
	public HandledEventInfo(){
		
	}		
	
	public HandledEventInfo(Event e){
		this.convertToHandledEventInfo(e);
	}
	
	public HandledEventInfo(EventType type, long time, String targetTaskName, int targetJobId) {
		super();
		this.eventTime 		= time;
		this.targetTaskName = targetTaskName;
		this.targetJobId	= targetJobId;
		this.eventType 		= type;
	}
		
			
	public String getTargetTaskName() {
		return targetTaskName;
	}


	public void setTargetTaskName(String targetTaskName) {
		this.targetTaskName = targetTaskName;
	}


	public int getTargetJobId() {
		return targetJobId;
	}


	public void setTargetJobId(int targetJobId) {
		this.targetJobId = targetJobId;
	}


	public long getEventTime() {
		return eventTime;
	}
	
	public void setEventTime(long eventTime) {
		this.eventTime = eventTime;
	}
	
	
	public EventType getEventType() {
		return eventType;
	}
	
	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	public void convertToHandledEventInfo(Event e){
		this.setEventTime(e.getEventTime().getTimeValue());
		if(e.getTask() != null)
			this.setTargetTaskName(e.getTask().getName());
		if(e.getJob() != null)
			this.setTargetJobId(e.getJob().getProgInstanceNumber());
		this.setEventType(e.getEventType());
	}
	
	public String toString(){
		return this.eventTime + "\t" + this.getEventType().name() + "\t" + this.targetTaskName + "_" + this.targetJobId;
	}
	

}
