package edu.unibo.ciri.desert.datastructure;

import edu.unibo.ciri.collection.DoubleLinkElementWrap;
import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.event.EventType;

public class TimeLineTransactionalLinkedList extends TimeLineDoubleLinkedList implements TimeLine{ 
	
	
	@Override
	public int orderedInsert(Event elem){		
		
		if(elem == null)
    		return -1;
    	
    	int position = 0;
    	
    	DoubleLinkElementWrap<Event> linkedElem = getInstance(elem);//crea un DoubleLinkElementWrap con content = a elem
    	
    	if(head.getNext() == tail){
    		insertBetween(linkedElem, head, tail);
    		return position;
    	}
    	    	
    	DoubleLinkElementWrap<Event> curr = head; 
    	
    	
    	//scorre tutta la lista
    	while((curr = curr.getNext())!= tail){
    		
    		long currentTime = curr.getContent().getEventTimeValue();
    		long eventTime   = linkedElem.getContent().getEventTimeValue();//istante di tempo dell'elemento da inserire
    		long deltaTime = currentTime - eventTime;
    		    		
    		if((elem.getEventType()==EventType.JOB_COMPLETION) && (elem.getEventTimeValue()==26350)){
    			System.out.println("DEBUG TRANSACTIONAL "+elem);
    		}
    		
    		if(deltaTime == 0){
    			
	    			//Is there a transaction activated before?
	    			if(curr.getContent().isType(EventType.START_TRANSACTION)){
	    				
	    				insertAfterEvent(curr, linkedElem);
	    				
	    			}else /*Check the need to create a transaction*/{
	    				
	    				if(findEventsInConflict(curr.getContent(),linkedElem.getContent())){
		    				Event startTransaction, endTransaction;
		    				startTransaction = factory.getEventInstance(currentTime, EventType.START_TRANSACTION, null);
		    				endTransaction   = factory.getEventInstance(currentTime, EventType.END_TRANSACTION,   null);
		    				
		    				DoubleLinkElementWrap<Event> startTransactionWrapper = getInstance(startTransaction); 
		    				insertBetween(startTransactionWrapper, curr.getPrev(), curr);
		    				insertAfterEvent(startTransactionWrapper, getInstance(endTransaction)); 
		    				insertAfterEvent(startTransactionWrapper, linkedElem);
		    				
	    				}
	    				else{
	    					insertAfterEvent(curr, linkedElem);
	    				}
	    				
	    			}
    			return position;
    			
    		}else if(deltaTime > 0){//l'istante corrente  successivo a quello dell'elemento da inserire
    			
    			insertBetween(linkedElem, curr.getPrev(), curr);
    			return position;
    			
    		}else /*deltaTime < 0*/{//l'istante corrente  precedente a quello dell'elemento da inserire
    			
    		}   			    		
    		position++;
    	}    	
    	insertBetween(linkedElem, curr.getPrev(), curr);
    	return position;
	}
	
	public void trigger(Event e){
		orderedInsert(e);

	}
	

	
	private boolean findEventsInConflict(Event start,Event toInsert){
		
		int numberOfRelease = 0;
		int numberOfCompletion = 0;
		int numberofChangePriority=0;
		long currTime = start.getEventTimeValue();
		Event curr;
		
		if(start != null){
			if(start.isType(EventType.JOB_COMPLETION))
				numberOfCompletion++;
			if(start.isType(EventType.JOB_RELEASE))
				numberOfRelease++;
			if(start.isType(EventType.JOB_PRIORITY_CHANGE))
				numberofChangePriority++;//per il momento ho considerato un overhead context switch end come una release
		}
		
		if(toInsert != null){
			if(toInsert.isType(EventType.JOB_COMPLETION))
				numberOfCompletion++;
			if(toInsert.isType(EventType.JOB_RELEASE))
				numberOfRelease++;
			if(toInsert.isType(EventType.JOB_PRIORITY_CHANGE))
				numberofChangePriority++;//per il momento ho considerato un overhead context switch end come una release
		}
		
		curr = start;
		while((curr = getNext(curr)) != null && curr.getEventTimeValue() == currTime){
			switch(curr.getEventType()){
				case JOB_RELEASE:
					numberOfRelease++;
					break;
				case JOB_PRIORITY_CHANGE:
					numberofChangePriority++;//per il momento ho considerato un overhead context switch end come una release
					break;
				case JOB_COMPLETION:
					numberOfCompletion++;
					break;
				default:
			}
		}
		
		if((numberOfCompletion > 0 && numberOfRelease > 0) | (numberofChangePriority>1) | (numberOfRelease >1) | (numberOfCompletion > 1) | (numberOfCompletion > 0 && numberofChangePriority>0) | (numberOfRelease > 0 && numberofChangePriority>0))
			return true;
		
		/*if(numberofChangePriority>1){
			return true;
		}*/
			
			
		return false;
	}
	
	private void insertAfterEvent(DoubleLinkElementWrap<Event> from, DoubleLinkElementWrap<Event> toInsert){
		
		if(toInsert.getContent().compareTo(from.getContent()) > 0){
//			System.out.println("\n"+toInsert.getContent() + " prioritario rispetto a " + from.getContent());
			insertBetween(toInsert, from.getPrev(), from);
			return;
		}
		
		DoubleLinkElementWrap<Event> tmp = from;
		while( (tmp = tmp.getNext()) != tail ){
			if(toInsert.getContent().compareTo(tmp.getContent()) > 0){
//				System.out.println("\n"+toInsert.getContent() + " prioritario rispetto a " + tmp.getContent());
				insertBetween(toInsert, tmp.getPrev(), tmp);
				return;
			}
		}
//		System.out.println("\n"+toInsert.getContent() + " prioritario rispetto a " + tmp.getContent());
		insertBetween(toInsert, tail.getPrev(), tail);
	}

}
