package edu.unibo.ciri.desert.datastructure;

import edu.unibo.ciri.desert.event.Event;

public interface TimeLine {
	
	public enum TimeLineOperation{ TRIGGER, SUPPRESS, CONTAINS, POP, GET_FIRST, GET_NEXT }
			
	void trigger(Event e);
	
	boolean suppress(Event e);
	
	boolean contains(Event e);
		
	Event pop();
	
	Event getFirst();
	
	Event getNext(Event e);
	
	void clear();
	
	
}
