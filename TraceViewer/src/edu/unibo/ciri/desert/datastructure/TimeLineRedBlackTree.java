package edu.unibo.ciri.desert.datastructure;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;


import edu.unibo.ciri.collection.PriorityLinkedList;
import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.realtime.model.time.TimeInstant;


public class TimeLineRedBlackTree implements TimeLine{
	
	private int size = 0;
	
	private int[] operations = new int[TimeLineOperation.values().length];
	private int maxNumberOfEventTriggered = 0;
	
	private TreeMap<TimeInstant, PriorityLinkedList<Event>> timeMap = new TreeMap<TimeInstant, PriorityLinkedList<Event>>();
	private TimeInstant tmpInstant = new TimeInstant();
	
	private Vector<PriorityLinkedList<Event>> freeLists = new Vector<PriorityLinkedList<Event>>();
	
	private static final boolean REUSE_LISTS = true;
	
	public void trigger(long timeVal, Event e){		
		
		operations[TimeLineOperation.TRIGGER.ordinal()]++;
		
		PriorityLinkedList<Event> list;		
		tmpInstant.setTimeValue(timeVal);
		
		
		if(!timeMap.containsKey(tmpInstant)){
			
			//TODO use a factory instead
			TimeInstant ti = new TimeInstant(timeVal);
			list = getList();
			assert(list != null);
//			e.setEventTime(ti);
			timeMap.put(ti, list);
			list.orderedInsert(e);
			size++;
			return;
		}
		
		list = timeMap.get(tmpInstant);	
		assert(list != null);
//		e.setEventTime(list.getFirst().getEventTime());
		list.orderedInsert(e);
		
//		System.out.println(this);
		
		size++;
		if(size > maxNumberOfEventTriggered){
			maxNumberOfEventTriggered = size;
		}
	}
	
	
	
	public int getEventNumber(){		
		return size;
	}
	
	public int getTimeInstantNumber(){
		return timeMap.size();
	}
	
	public Event getFirst(){
		
		operations[TimeLineOperation.GET_FIRST.ordinal()]++;
		
		if(timeMap.size() == 0)
			return null;
		
		TimeInstant timeInstant = timeMap.firstKey();
		
		assert(timeInstant != null);
		
		if(timeInstant != null){
			
			PriorityLinkedList<Event> list = timeMap.get(timeInstant);
			return list.getFirst();
			
		}
		
		return null;		
	}
	
	public Event pop(){
		
		operations[TimeLineOperation.POP.ordinal()]++;
		
		TimeInstant timeInstant;
		Event retVal;
		
		if(timeMap.isEmpty())
			return null;
		
		timeInstant = timeMap.firstKey();
		
		assert(timeInstant != null);
				
		if(timeInstant != null){
			
			PriorityLinkedList<Event> list = timeMap.get(timeInstant);
			
			if(list.size() == 1){
				if(REUSE_LISTS){
					retVal = list.popHead(); 
					freeList(timeMap.remove(timeInstant));
				}
				else{
					retVal = list.popHead(); 
					timeMap.remove(timeInstant);
				}				
			}
			else{
				assert(list.size() > 0);
				retVal = list.popHead();
			}
			
			size--;
			return retVal;
			
		}
		
		return null;
		
	}
	
	public String toString(){
		
		StringBuffer sb = new StringBuffer();
		Set<TimeInstant> set = timeMap.keySet();
		Iterator<TimeInstant> it = set.iterator();
		
		PriorityLinkedList<Event> pll;
		while(it.hasNext()){
			
			TimeInstant t = it.next();
			sb.append("\nTime = "+ t.getTimeValue() );
			pll = timeMap.get(t);
			
			Event e = pll.getFirst();
			while(e != null){
				
				sb.append( "\n\t" + e );
				e = pll.getNext(e);
			}			
		}
		sb.append("\n-----------------------------------------------------------------------\n");
		
		TimeLineOperation[] values = TimeLineOperation.values();
		for(int i = 0; i < operations.length; i++){
			sb.append(values[i].toString() + " = " + operations[i] + "\n");
		}
		sb.append("Max event in timeline = " + maxNumberOfEventTriggered + "\n");
		
		return sb.toString();
	}


	@Override
	public void trigger(Event e) {
		trigger(e.getEventTimeValue(), e);		
	}

	@Override
	public Event getNext(Event e) {
		
		operations[TimeLineOperation.GET_NEXT.ordinal()]++;
		
		if(!timeMap.containsKey(e.getEventTime()))
			throw new RuntimeException("Requested next event to an event that doesn't exists");
		
		PriorityLinkedList<Event> list = timeMap.get(e.getEventTime());
		
		return list.getNext(e);
	}

	@Override
	public boolean suppress(Event e) {
		
		operations[TimeLineOperation.SUPPRESS.ordinal()]++;
		
		if(!timeMap.containsKey(e.getEventTime()))
			return false;
		
		PriorityLinkedList<Event> list = timeMap.get(e.getEventTime());
		if(list.size() == 1 && list.removeElement(e)){
			
			if(REUSE_LISTS)
				freeList(timeMap.remove(e.getEventTime()));
			else
				timeMap.remove(e.getEventTime());
			size--;
			//TODO remember to free if using a factory
			return true;
		}
		
		if(list.removeElement(e)){
			assert(list.size() >= 1);
			size--;
			return true;
		}
		return false;
		
	}

	@Override
	public boolean contains(Event e) {
		
		operations[TimeLineOperation.CONTAINS.ordinal()]++;
		
		assert(e != null);
		assert(timeMap != null);
		
		if(!timeMap.containsKey(e.getEventTime()))
			return false;
		
		return timeMap.get(e.getEventTime()).contains(e);
			
	}
	
	@Override
	public void clear() {
		
		timeMap.clear();
		for(int i = 0; i < operations.length; i++)
			operations[i] = 0;
		size = 0;
	}


	private PriorityLinkedList<Event> getList(){
		
		int size = freeLists.size();
		
		if(REUSE_LISTS) {
			if (size > 0)
				return freeLists.remove( size - 1 );
			else
				return new PriorityLinkedList<Event>();
		} else
			return new PriorityLinkedList<Event>();
		
	}
	
	private void freeList(PriorityLinkedList<Event> list){
		
		assert(list.size() == 0);
//		list.clear();		
		
		if(!freeLists.contains(list)){
			freeLists.add(list);
		}
		else{
			assert(false);
		}
	}
	
	

}
