package edu.unibo.ciri.desert.datastructure;

import edu.unibo.ciri.desert.event.Event;

public interface EventList {
			
	void trigger(Event e);
	
	void retrigger(Event e, long time);
	
	Event pop();
	
	Event getFirst();
	
	Event getNext(Event e);
	
	boolean suppress(Event e);
	
	

}
