package edu.unibo.ciri.desert.datastructure;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;


import edu.unibo.ciri.collection.PriorityLinkedList;
import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.realtime.model.time.TimeInstant;


public class TimeLineTreeEventList implements EventList{
	
	private int size = 0;
	
	private TreeMap<TimeInstant, PriorityLinkedList<Event>> timeMap = new TreeMap<TimeInstant, PriorityLinkedList<Event>>();
	private TimeInstant tmpInstant = new TimeInstant();
	
	public void trigger(long timeVal, Event e){		
		
		PriorityLinkedList<Event> list;		
		tmpInstant.setTimeValue(timeVal);
		
		
		if(!timeMap.containsKey(tmpInstant)){
			
			//TODO use a factory instead
			TimeInstant ti = new TimeInstant(timeVal);
			list = new PriorityLinkedList<Event>();
			e.setEventTimeValue(ti);
			timeMap.put(ti, list);
			list.orderedInsert(e);
			size++;
			return;
		}
		
		list = timeMap.get(tmpInstant);		
		e.setEventTimeValue(list.getFirst().getEventTime());
		list.orderedInsert(e);
		
		size++;
	}
	
	public int getEventNumber(){		
		return size;
	}
	
	public int getTimeInstantNumber(){
		return timeMap.size();
	}
	
	public Event getFirst(){
		
		if(timeMap.size() == 0)
			return null;
		
		TimeInstant timeInstant = timeMap.firstKey();
		
		if(timeInstant != null){
			
			PriorityLinkedList<Event> list = timeMap.get(timeInstant);
			return list.getFirst();
			
		}
		
		return null;		
	}
	
	public Event pop(){
		
		TimeInstant timeInstant = timeMap.firstKey();
				
		if(timeInstant != null){
			
			PriorityLinkedList<Event> list = timeMap.get(timeInstant);
			
			if(list.size() == 1){
				timeMap.remove(timeInstant);
				//TODO remember to free if using factory
			}
			size--;
			return list.popHead();
			
				
		}
		
		return null;
		
	}
	
	public String toString(){
		
		StringBuffer sb = new StringBuffer();
		Set<TimeInstant> set = timeMap.keySet();
		Iterator<TimeInstant> it = set.iterator();
		
		PriorityLinkedList<Event> pll;
		while(it.hasNext()){
			
			TimeInstant t = it.next();
			sb.append(t.getTimeValue() + " instance: " + t + "\n\t");
			pll = timeMap.get(t);
			
			Event e = pll.getFirst();
			while(e != null){
				
				sb.append( e.getEventType() + " ");
				e = pll.getNext(e);
			}
			
			sb.append("\n");
		}
		
		return sb.toString();
	}


	@Override
	public void trigger(Event e) {
		trigger(e.getEventTimeValue(), e);		
	}

	@Override
	public Event getNext(Event e) {
		
		if(!timeMap.containsKey(e.getEventTime()))
			throw new RuntimeException("Requested next event to an event that doesn't exists");
		
		PriorityLinkedList<Event> list = timeMap.get(e.getEventTime());
		
		return list.getNext(e);
	}

	@Override
	public boolean suppress(Event e) {
		
		if(!timeMap.containsKey(e.getEventTime()))
			return false;
		
		PriorityLinkedList<Event> list = timeMap.get(e.getEventTime());
		if(list.size() == 1 && list.removeElement(e)){
			timeMap.remove(e.getEventTime());
			size--;
			//TODO remember to free if using a factory
			return true;
		}
		
		if(list.removeElement(e)){
			size--;
			return true;
		}
		return false;
		
	}

	@Override
	public void retrigger(Event e, long time) {
		// TODO Auto-generated method stub
		
	}


	
	

}
