package edu.unibo.ciri.desert.datastructure;


import edu.unibo.ciri.collection.DoubleLinkElementWrap;
import edu.unibo.ciri.collection.PriorityLinkedList;
import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.event.Event.EventState;
import edu.unibo.ciri.desert.event.management.EventFactory;
import edu.unibo.ciri.realtime.model.time.TimeInstant;

public class TimeLineDoubleLinkedList  extends PriorityLinkedList<Event> implements TimeLine{	
	
	EventFactory factory;
	
	public TimeLineDoubleLinkedList(){
		super();
	}
	
	public boolean isEmpty() {
		return super.size() == 0 ? true: false;
	}
	
	public void setEventFactory(EventFactory factory){
		this.factory = factory;
	}
	
	@Override	
	public void trigger(Event ev) {		
		orderedInsert(ev);
		ev.setState(EventState.TRIGGERED);
	}
	
	@Override
	public boolean suppress(Event ev){
		
		if(!super.contains(ev))
			return false;
		
		super.removeElement(ev);
		
		return true;
	}

	public Event inspectHead(){
		return super.getFirst();
	}

	@Override
	public Event pop() {
		return  super.popHead();		
	}
	
	@Override
	public Event getNext(Event event){
		return super.getNext(event);
	}
	
	
	public boolean removeEvent(Event event){		
		return super.removeElement(event);		
	}
	
	public Event getEventAtTime(TimeInstant ti){
		
		DoubleLinkElementWrap<Event> elem = super.head;
		long evtTime,queryTime = ti.getTimeValue();
		
		
		while((elem = elem.getNext()) != tail){
			evtTime = elem.getContent().getEventTimeValue();
			if(evtTime == queryTime)
				return elem.getContent();
		}
		
		return null;
	}

	@Override 
	public int size() {
		return super.size();
	}

	@Override
	public String toString() {
		return super.toString();
	}






}
