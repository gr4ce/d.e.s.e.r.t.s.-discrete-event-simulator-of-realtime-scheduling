package edu.unibo.ciri.desert.event;



public enum EventType {
	
		JOB_COMPLETION,
		JOB_DEADLINE,
		JOB_DELAYED,
		JOB_PREEMPTION,
		JOB_PRIORITY_CHANGE,	
		JOB_AFFINITY_CHANGE,
		JOB_RELEASE,
		JOB_RESUME,
		JOB_START,			
		JOB_MIGRATION,
		JOB_OVERRUN,
		JOB_WAIT,		
		JOB_BLOCKED,
		APPLICATION_START,
		APPLICATION_STOP,
		SIMULATION_START,
		SIMULATION_HALT,
		SIMULATION_SOFT_STOP,		
		JOB_ZERO_LAXITY,
		MULTIPLE_EVENT,
		START_TRANSACTION,
		END_TRANSACTION,
		ENABLE_PREEMPTION,
		DISABLE_PREEMPTION,
		OVERHEAD_CONTEXT_SWITCH_START,
		OVERHEAD_CONTEXT_SWITCH_END,
		OVERHEAD_MIGRATION_START,
		OVERHEAD_MIGRATION_END,
		SCHEDULER_RELEASE,
		SCHEDULER_COMPLETION,
		DELAYED_RELEASE,
		TIME_STEP_FORWARD,
		PLATFORM_IDLE,
		SHARED_RESOURCE_REQUEST,
		SHARED_RESOURCE_ACCESS_GRANTED,
		SHARED_RESOURCE_ACCESS_DENIED,
		SHARED_RESOURCE_RELEASE,
		APERIODIC_REQUEST_FIRED,
		NONE;
		
		private int eventPriority[] = {
			5, // JOB_COMPLETION 
			3, // JOB_DEADLINE 
			0, // JOB_DELAYED 
			0, // JOB_PREEMPTION 
			1, // JOB_PRIORITY_CHANGE 	
			0, // JOB_AFFINITY_CHANGE
			2, // JOB_RELEASE 
			6, // JOB_RESUME
			0, // JOB_START
			0, // JOB_MIGRATION	
			0, // JOB_WAIT		
			0, // JOB_BLOCKED
			0, // JOB_OVERRUN
			5, // APPLICATION_START
			0, // APPLICATION_STOP
			0, // SIMULATION_START
			3, // SIMULATION_HALT
			3, // SIMULATION_SOFT_STOP	
			2, // ZERO_LAXITY						
			5, // MULTIPLE_EVENT
			6, // START_TRANSACTION
			0, // END_TRANSACTION
			5, // ENABLE_PREEMPTION
			5, // DIABLE_PREEMPTION
			2, // OVERHEAD CONTEXT SWITCH START
			2, // OVERHEAD CONTEXT SWITCH END
			2, // OVERHEAD MIGRATION START
			2, // OVERHEAD MIGRATION END
			2, // SCHEDULER_RELEASE
			4, // SCHEDULER_COMPLETION
			0, // DELAYED_RELEASE
			0, // TIME_STEP_FORWARD
			0, // PLATFORM_IDLE
			0, // SHARED_RESOURCE_REQUEST
			0, // SHARED_RESOURCE_ACCESS_GRANTED
			0, // SHARED_RESOURCE_ACCESS_DENIED
			0, // SHARED_RESOURCE_RELEASEÙ
			0, // APERIODIC_REQUEST_FIRED
			0, // NONE
		};
		
		private String eventName[] = {
			"Job Completion",
			"Job Deadline",
			"Job Dealyed",
			"Job Preemption",
			"Priority Change",
			"Affinity Change",
			"Job Release",
			"Job Resume",
			"Job Start",
			"Job Migration",
			"Job Overrun",
			"Job Wait",								
			"Job Blocked",
			"Application Start",
			"Application Stop",
			"Simulation Start",
			"Simulation Halt",
			"Simulation Soft stop",
			"Zero Laxity",
			"MultipleEvent",
			"Start Transaction",
			"End Transaction",
			"Enable Preemption",
			"Disable Preemption",
			"Overhead Context Switch Start",
			"Overhead Context Switch End",
			"Overhead Migration Start",
			"Overhead Migration End",
			"Scheduler Release",
			"Scheduler Completion",
			"Delayed Release",
			"Time Step Forward",
			"Platform Idle",
			"Shared Resource Request",
			"Shared Resource Access Granted",
			"Shared Resource Access Denied",
			"Shared Resource Release",
			"Aperiodic Request Fired",
			"None"			
		};
		
		public int getTypePriority(){
			return eventPriority[this.ordinal()];
		}
			
		
		public String getTypeName(){
			return eventName[this.ordinal()];
		}

}
