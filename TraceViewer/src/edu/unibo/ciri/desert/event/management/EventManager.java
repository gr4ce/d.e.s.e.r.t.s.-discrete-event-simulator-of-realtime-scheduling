package edu.unibo.ciri.desert.event.management;




import edu.unibo.ciri.model.task.job.JobFactory;
import edu.unibo.ciri.realtime.model.Job;
import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskParameters;
import edu.unibo.ciri.realtime.model.Job.JobState;
import edu.unibo.ciri.realtime.model.time.Time;
import edu.unibo.ciri.realtime.model.time.TimeInstant;
import edu.unibo.ciri.realtime.model.time.TimeVisualization;
import edu.unibo.ciri.realtime.policies.overrun.OverrunPolicy.OverrunPolicyEnum;
import edu.unibo.ciri.desert.config.StaticConfig;
import edu.unibo.ciri.desert.datastructure.TimeLine;
import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.event.EventType;
import edu.unibo.ciri.desert.event.Event.EventState;
import edu.unibo.ciri.desert.simulation.management.Simulation;



public class EventManager implements EventListener{
	
	private Simulation simulation		= null;
	private EventFactory eventFactory   = null;
	
//	private Event timeStepForward = new Event(new TimeInstant(0), null, EventType.TIME_STEP_FORWARD);
	
	public EventManager(Simulation sim){
		simulation = sim;
		eventFactory = simulation.getEventFactory();
		assert(eventFactory != null);
	}
	
	public void clear(){		
//		timeStepForward.clear();
	}
	
	//TODO accedere direttamente alla Timeline dalla Simulation
	public Event popEvent(){
		return simulation.getTimeLine().pop();
	}
	
	//TODO accedere direttamente alla Timeline dalla Simulation
	public Event getNextEvent(){
		return simulation.getTimeLine().getFirst();
	}
	
	//TODO accedere direttamente alla Timeline dalla Simulation
	public Event getFirst(){
		return simulation.getTimeLine().getFirst();
	}

	public void trigger(Event ev) {
		ev.setState(EventState.TRIGGERED);
		simulation.getTimeLine().trigger(ev);
	}
	
	public boolean retrigger(Event ev,long time){
		
		if(!simulation.getTimeLine().contains(ev))
			return false;
		
		simulation.getTimeLine().suppress(ev);
		System.out.println("ops!! this point is not supposed to be reached!!");
		ev.setEventTimeValue(time,Time.getResolution());
		ev.setState(EventState.RETRIGGERED);
		simulation.getTimeLine().trigger(ev);
		
		return true;
	}
	
	public boolean suppress(Event ev){
		
		if(!simulation.getTimeLine().contains(ev) )
			return false;
		
		if(StaticConfig.SUPPRESS_BY_REMOVING)			
			simulation.getTimeLine().suppress(ev);
				
		ev.setState(EventState.SUPPRESSED);
		
		if(StaticConfig.SUPPRESS_BY_REMOVING)
			eventFactory.freeEvent(ev);
		
		
		return true;			
	}
	
		
	
	
	protected void handleSimulationHalt(Event event){
				
		System.out.println(event.getName() + " at time " + TimeVisualization.getString(event.getEventTime()) );		
		simulation.getTimeLine().clear();
		
	}
	
	protected void handleJobCompletion(Event event) {		
	
		Job completingJob = event.getJob();
		if(!completingJob.hasMissed())
			suppress(completingJob.getEvent(EventType.JOB_DEADLINE));
		
	}




	private void handleJobRelease(Event event) {	/// E' L'UNICO PRIVATO!???
		
		long 				currentTime 	= event.getEventTime().getTimeValue();
		
		Task 				task 			= event.getTask();
		TaskParameters 		taskParameters 	= task.getTaskParameters();		
		
		long 				period  		= taskParameters.getPeriod().getTimeValue();
		long 				deadline 		= taskParameters.getDeadline().getTimeValue();		
		
		OverrunPolicyEnum 	overrunPolicy 	= taskParameters.getOverrunPolicy();
		
		TimeLine			timeLine		= simulation.getTimeLine();
		EventFactory 		eventFactory 	= simulation.getEventFactory();
		JobFactory			jobFactory		= simulation.getJobFactory();
		
		
		Job 				releasingJob;
		Job 				activeJob 		= task.getActiveJob();
		

		Event 				nextJobRelease;
		
		if(activeJob != null){
			
			activeJob.setOverrun();
			
			switch(overrunPolicy){
				
				case SKIP:
					//Triggering next job release 
					nextJobRelease = eventFactory.getEventInstance(currentTime + period, EventType.JOB_RELEASE, task);
					nextJobRelease.addListener(this);
					trigger(nextJobRelease);
					return;
					
				case ASAP:
					//Triggering next job release 
					long delta = 1000;
					nextJobRelease = eventFactory.getEventInstance(currentTime + delta, EventType.JOB_RELEASE, task);
					nextJobRelease.addListener(this);
					trigger(nextJobRelease);					
					return;
					
				case SHIFT_ALL:
					delta = 1000;
					Event e = timeLine.getFirst();
					while(e != null){
						if(e.getEventType() == EventType.JOB_RELEASE && e.getState() != EventState.TO_RETRIGGER){
							e.setState(EventState.TO_RETRIGGER);
						}
						
						e = timeLine.getNext(e);
					}
					
					e = timeLine.getFirst();
					while(e != null){
						if(e.getEventType() == EventType.JOB_RELEASE && e.getState() == EventState.TO_RETRIGGER){
							retrigger(e,e.getEventTime().getTimeValue() + delta);
							e = timeLine.getFirst();
						}
						
						e = timeLine.getNext(e);
					}
					
					nextJobRelease = eventFactory.getEventInstance(currentTime + delta, EventType.JOB_RELEASE, task);
					nextJobRelease.addListener(this);
					trigger(nextJobRelease);					
					return;
					
				case NAIVE:
					break;
			}
		
		}
		
		releasingJob = jobFactory.releaseNewJob(task);
		task.releaseNewJob(releasingJob);
		releasingJob.setState(JobState.RELEASED);
		releasingJob.getInfo().setActivationTime(currentTime);		
		releasingJob.getEvent(EventType.JOB_RELEASE);
		event.setJob(releasingJob);
		
		//Triggering next job release	//� UGUALE A QUELLI DEGLI SWITCH, SERVE PER LA NAIVE CHE HA LA BREAK???
		nextJobRelease = eventFactory.getEventInstance(currentTime + period,EventType.JOB_RELEASE ,task);
		nextJobRelease.addListener(this);
		trigger(nextJobRelease);
		
		
		//Triggering next job deadline
		Event jobDeadline = eventFactory.getEventInstance(currentTime + deadline,EventType.JOB_DEADLINE, task);
		jobDeadline.addListener(this);
		trigger(jobDeadline);
		releasingJob.setEvent(jobDeadline);
		releasingJob.getEvent(EventType.JOB_DEADLINE);	//PERCH� ANCHE QUESTA???
		jobDeadline.setJob(releasingJob);

				
	}
	
	protected void handleSimulationSoftStop(Event event) {
		
		TimeLine timeLine = simulation.getTimeLine();
		System.out.println("SIMULATION STOP "+event);
			
		
		Event curr,next = timeLine.getFirst();
		EventType et;
		
		/*
		 * This find all the job releases triggered in the time line ad suppress them.
		 * If this is done all running and waiting job will terminate their execution and no 
		 * other job instances will be created, then executed
		 */
		
		while((curr = next) != null){
			
			et = curr.getEventType();
			next = timeLine.getNext(curr);			
			switch(et){
				case JOB_RELEASE:
					timeLine.suppress(curr);					
					break;
				default:
					;
			}			
		}		
	}
	
	protected void handleJobDeadline(Event e){
		
		long time = e.getEventTimeValue();
		if(e.getState() == EventState.SUPPRESSED)
			return;
		System.out.println("Oh Shit!! I miss a deadline at time: "+time);
		trigger( new Event(new TimeInstant(time + 1),null,EventType.SIMULATION_HALT));
	}
	
	
	@Override
	public void handle(Event event) {
		
		if(StaticConfig.PRINT_FIRING_EVENT)
			System.out.println("[EVENT MANAGER][INFO] Handling "+event.getEventType() + " "+"t = "+ event.getEventTimeValue());
		
		switch (event.getEventType()) {
			case JOB_RELEASE:
				handleJobRelease(event);
				break;
			case JOB_COMPLETION:
				handleJobCompletion(event);
				break;
			case SIMULATION_HALT:
				handleSimulationHalt(event);
				break;
			case SIMULATION_SOFT_STOP:
				handleSimulationSoftStop(event);
				break;
			case JOB_DEADLINE:
				handleJobDeadline(event);
				break;
			default:
				assert(false);
		}
			
	}

	

	public void setSimulation(Simulation simulation) {
		this.simulation = simulation;
	}

	public Simulation getSimulation() {
		return simulation;
	}

	
	@Override
	public String getName() {
		
		return "EventManager";
	}
	

}
