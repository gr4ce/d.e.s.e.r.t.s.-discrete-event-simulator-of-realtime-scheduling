package edu.unibo.ciri.desert.event.management;


import java.util.ArrayList;
import java.util.List;

import java.util.Vector;

import edu.unibo.ciri.collection.Queue;
import edu.unibo.ciri.desert.config.StaticConfig;
import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.event.EventType;
import edu.unibo.ciri.desert.event.Event.EventState;
import edu.unibo.ciri.desert.simulation.management.Simulation;
import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.time.TimeInstant;

public class MultipleQueueEventFactory extends EventFactory {
	
	private Vector<Queue<Event>> freeEventQueues = new Vector<Queue<Event>>(EventType.values().length);
//	private Vector<java.util.Queue<Event>> freeEvent = new Vector<java.util.Queue<Event>>(EventType.values().length);
		
	private ArrayList<ArrayList<EventListener>> listeners = new ArrayList<ArrayList<EventListener>>();
	
			
	public MultipleQueueEventFactory(Simulation sim){
		
		super(sim);	
		System.out.println("[EVENT FACTORY][INFO] - Creating a new EventFactory");
		initQueues();
		initListeners();
	}
	
	public MultipleQueueEventFactory(Simulation sim, int initialQueueCapacity){
		
		super(sim);
		System.out.println("[EVENT FACTORY][INFO] - Creating a new EventFactory");		
		EventType[] types = EventType.values();
		initQueues();
		for(int i = 0; i < freeEventQueues.size(); i++){
			for(int j = 0; j < initialQueueCapacity; j++){
				Event e = new Event(new TimeInstant(0), null, types[i] );				
				freeEventQueues.get(types[i].ordinal()).offer(e);				
			}			
		}
		
		initListeners();
		
		
	}
	
	private void initListeners(){
		
		EventType[] types = EventType.values();
		for(int i = 0; i < types.length; i++){
			listeners.add(new ArrayList<EventListener>());
		}
		
	}
		
	private void initQueues(){	
		
		EventType[] types = EventType.values();
			
		for(int i = 0; i < types.length; i++){	
			if(StaticConfig.PRINT_EVENT_FACTORY_INFO)
				System.out.println("[EVENT FACTORY][INFO] - Creating an Event Queue for "+types[i]);
			freeEventQueues.add(new Queue<Event>());
		}		
	}
	
	@Override
	public Event getEventInstance(long timeValue, EventType type, Task target ){
		
		Event e = null;
		Queue<Event> currentQueue = freeEventQueues.get(type.ordinal());
		
			
		
		if(currentQueue.size() > 0){	
//			System.out.print("[EVENT_FACTORY] - Reusing " + type.name() + " queue before="+currentQueue.size());
			e = currentQueue.poll();	
//			System.out.println(" after="+currentQueue.size());
			assert(e.getEventType() == type);
			e.setTask(target);
			e.getEventTime().setTimeValue(timeValue);			
		}	
		else{
			e = new Event(new TimeInstant(timeValue),target, type);	
			if(StaticConfig.USE_LISTENER_IN_EVENTS)
				setDefaultListenersInEvent(e, type);
			if(StaticConfig.PRINT_NEW_ALLOCATION_EVENT_INFO)
				System.out.println("\t\t\t\t[NEW OJBECT ALLOCATION] - " + e);
		}			
		
		e.setState(EventState.CREATED);		
					
		return e;
	}
	

	

	
	@Override
	public void setInterestedListenerToEventType(EventType type, EventListener listener){
	
		if(StaticConfig.USE_LISTENER_BY_EVENT_TYPE){
			
			if(StaticConfig.PRINT_EVENT_FACTORY_INFO)
				System.out.println("[EVENT FACTORY][INFO] - Set " + listener.toString() + " interested in " + type);
			
			listeners.get(type.ordinal()).add(listener);
		}
		
		
		if(StaticConfig.USE_LISTENER_IN_EVENTS){
			
			if(StaticConfig.PRINT_EVENT_FACTORY_INFO)
				System.out.println("[EVENT FACTORY][INFO] - Set " + listener.toString() + " interested in " + type);
		
			super.setInterestedListenerToEventType(type, listener);
			
			Queue<Event> qe = freeEventQueues.get(type.ordinal());
			
			
//			Iterator<Event> it = qe.iterator();			
//			Event inQueue = qe.peek();
//			while(it.hasNext()){
//				inQueue = it.next();
//				inQueue.addListener(listener);
//			}
			
			Event inQueue = qe.getFirst();
			while(inQueue != null){				
				inQueue.addListener(listener);
				inQueue = qe.getNext(inQueue);
			}
			
			
		}
	

	}	
	
	@Override
	public void freeEvent(Event event){
//		System.out.print("[EVENT_FACTORY] - Freeing event type "+event.getEventType().name());
		event.setState(EventState.FREE);
		freeEventQueues.get(event.getEventType().ordinal()).offer(event);		
//		System.out.println(" now the queue contains " + freeEventQueues.get(event.getEventType().ordinal()).size());
	}	

	@Override
	public void handle(Event event) {		
		freeEvent(event);		
	}

	@Override
	public void clear(){
		
//		super.clear();
		
		EventType[] types = EventType.values();
		
//		for(int i = 0; i < freeEventQueues.size(); i++){
//			System.out.println("The queue containing " + types[i].toString() + " has " + freeEventQueues.get(types[i].ordinal()).size() +  " elements!");					
//		}
		
//		for(int i = 0; i < types.length; i++){
//			System.out.println(types[i]);
//			System.out.println("Queue length = "+freeEvent.get(i).size());
//			freeEvent.get(i).clear();						
//		}
				
		
		for(int i = 0; i < types.length; i++){
			listeners.get(i).clear();
		}
		
		
		
	}
	
	public void setInterestedListenerToAllEventType(EventListener listener){
		
		EventType[] types =EventType.values();
		for(int i = 0; i < types.length; i++){
			setInterestedListenerToEventType(types[i],listener);
		}
	}

	@Override
	public List<EventListener> getListenersInterestedInEventType(EventType type) {
		return listeners.get(type.ordinal());
	}


	@Override
	public String getName() {		
		return "MultipleQueueEventFactory";
	}
	
}
