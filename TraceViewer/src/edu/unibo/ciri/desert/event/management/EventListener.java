package edu.unibo.ciri.desert.event.management;

import edu.unibo.ciri.desert.event.Event;


public interface EventListener{
	
	public String getName();
	
	public void handle(Event event);

}
