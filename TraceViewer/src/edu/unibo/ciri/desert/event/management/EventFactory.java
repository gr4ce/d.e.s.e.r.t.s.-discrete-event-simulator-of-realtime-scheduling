package edu.unibo.ciri.desert.event.management;


import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import edu.unibo.ciri.collection.Queue;
import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.event.EventType;
import edu.unibo.ciri.desert.event.Event.EventState;
import edu.unibo.ciri.desert.simulation.management.Simulation;
import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.time.Time;
import edu.unibo.ciri.realtime.model.time.TimeInstant;


public abstract class EventFactory implements EventListener{
	
	private Queue<Event> 	freeEvent	= new Queue<Event>();
	private Simulation 		simulation 	= null;
		
	private Vector<ArrayList<EventListener>>	interestedList = new Vector<ArrayList<EventListener>>(EventType.values().length);
	
	
	private int incEventCreationCapacity = 5;
	
	public EventFactory(Simulation sim){
		setSimulation(sim);
		
		for(int i = 0; i < EventType.values().length; i++)
			interestedList.add(i, new ArrayList<EventListener>());
	}
	
	public EventFactory(Simulation sim, int numberOfTasks){		
		
		setSimulation(sim);
		
		if(simulation.getSimulationConfig().isEnableTracingEvent())
			incEventCreationCapacity = 1000;
		
		long eventInstanceCreated = 0;
		
		for(int i = 0; i < EventType.values().length; i++)
			interestedList.add(i, new ArrayList<EventListener>());
		
		for(int i = 0; i < numberOfTasks; i++){			
			freeEvent.offer(new Event(new TimeInstant(0),null, EventType.NONE));
			eventInstanceCreated++;						
		}
		simulation.getSimulationResults().setEventCreated(eventInstanceCreated);
	}
	
	
	
	
	public Event getEventInstance(long timeValue, EventType type, Task target ){
		
		Event e;
				
		if(freeEvent.size() > 0){
			
			e = freeEvent.poll();		
			e.setEventType(type);
			e.setTask(target);
			e.setEventTimeValue(timeValue, Time.getResolution());			
		}	
		else{
			
			long eventInstanceCreated = simulation.getSimulationResults().getEventCreated();
			
			e = new Event(new TimeInstant(timeValue),target, type);
			eventInstanceCreated++;
			
			for(int i = 0; i < incEventCreationCapacity; i++){
				freeEvent.offer(new Event(new TimeInstant(0),null, EventType.NONE));
				eventInstanceCreated++;				
			}
			simulation.getSimulationResults().setEventCreated(eventInstanceCreated);						
		}			
		
		e.setState(EventState.CREATED);
		e.removeAllListeners();
		setDefaultListenersInEvent(e, type);
					
		return e;
	}

	public Simulation getSimulation() {
		return simulation;
	}

	public void setSimulation(Simulation simulation) {
		this.simulation = simulation;
	}

	protected void setDefaultListenersInEvent(Event e, EventType type){
		
		ArrayList<EventListener> queue = interestedList.get(type.ordinal());
		
		for(EventListener list: queue)
			e.addListener(list);
		
	}
	
	public void setInterestedListenerToEventType(EventType type, EventListener listener){
								
		assert(listener != null);
		
		interestedList.get(type.ordinal()).add(listener);
					
	}
	
	
	
	public void setInterestedListenerToAllEventType(EventListener listener){
		
		for(int i = 0; i < EventType.values().length; i++){
			interestedList.get(i).add(listener);
		}
	}
	
	
	public void removeInterestedListenerToEventType(EventType type, EventListener listener){
		interestedList.get(type.ordinal()).remove(listener);
	}
	
	public abstract List<EventListener> getListenersInterestedInEventType(EventType type);
	
	public boolean removeListener(EventListener listener){
		
		int removed = 0;
		
		for(int i = 0; i < EventType.values().length; i++){
			if(interestedList.get(i).remove(listener))
				removed++;
		}
				
		if(removed > 0)
			return true;
		
		return false;		
	}
	
	public void freeEvent(Event event){
		freeEvent.offer(event);
		event.setEventType(EventType.NONE);
		event.setState(EventState.FREE);
	}
	

	@Override
	public void handle(Event event) {
		freeEvent(event);		
		event.removeAllListeners();
	}


	public void clear(){
		
//		freeEvent.clear();
		
		for(int i = 0; i < EventType.values().length; i++){
			interestedList.get(i).clear();
		}
		
	}


	

}
