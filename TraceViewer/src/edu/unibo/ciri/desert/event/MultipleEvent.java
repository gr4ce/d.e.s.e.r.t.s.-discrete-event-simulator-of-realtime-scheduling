package edu.unibo.ciri.desert.event;


import edu.unibo.ciri.collection.PriorityLinkedList;
import edu.unibo.ciri.desert.event.management.EventFactory;
import edu.unibo.ciri.desert.event.management.EventListener;
import edu.unibo.ciri.desert.simulation.results.SimulationResults;
import edu.unibo.ciri.realtime.model.time.Time;
import edu.unibo.ciri.realtime.model.time.TimeInstant;

public class MultipleEvent extends Event {
	
	private PriorityLinkedList<Event> events = new PriorityLinkedList<Event>();
	
	public MultipleEvent(){
		super();
		this.setEventType(EventType.MULTIPLE_EVENT);
	}
	
	public MultipleEvent(long timeValue){
		super();
		this.setEventTimeValue(timeValue, Time.getResolution());
	}
	
	public MultipleEvent(
			TimeInstant eventAbsoluteTime) {
		super(eventAbsoluteTime, null, EventType.MULTIPLE_EVENT);
	}
	
	public synchronized void add(Event e){
		events.orderedInsert(e);
	}
	
	public synchronized PriorityLinkedList<Event> getList(){
		return events;
	}
	
	public void free(EventFactory f){
		Event e = events.getFirst();
		while(e != null){
			f.freeEvent(e);
			e = events.getNext(e);
		}		
		events.clear();		
	}
	
	@Override
	public void fire() {		
		for(EventListener el: getListeners()){			
			el.handle(this);
		}
	}

	public String toString(){
		return String.format(SimulationResults.getTableRowFormatForSingleEvent(),"Event transaction","HANDLING",getEventTime().getTimeValue(),"none"); 
	}
}
