package edu.unibo.ciri.desert.event;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


import edu.unibo.ciri.desert.config.StaticConfig;
import edu.unibo.ciri.desert.event.management.EventListener;
import edu.unibo.ciri.desert.simulation.core.Simulator;
import edu.unibo.ciri.desert.simulation.results.SimulationResults;
import edu.unibo.ciri.desert.user.listeners.Tracer;
import edu.unibo.ciri.realtime.model.Job;
import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.time.TimeInstant;

@SuppressWarnings("unused")
public class Event implements Comparable<Event> {
		
	private ArrayList<EventListener> listeners = new ArrayList<EventListener>();
	
	private static int currentEventId = 0;
	
	private int eventId;
	
	private String eventName;
	
	/**
	 * Consider the idea to have a system clock with increasing 
	 * absolute time. So every entity in the system will see this
	 * time 
	 */
	private TimeInstant eventTime;
	
	
	/**
	 * 
	 */
	private int staticPriority;
	
		
	
	/**
	 * 
	 */
	private EventType eventType;
	
		
	/**
	 * 
	 */
	protected Simulator simulator;
	
	private int happendOnCpu;
	
	public void setCpu(int cpu){
		this.happendOnCpu = cpu;
	}
	
	public int getCpu(){
		return this.happendOnCpu;
	}
	
	//TEST///////
	/*Mi serve per simulare il jitter, dato che lo simulo ritardando la release � necessario tenere traccia
	 * dell'originale istante di quest'ultima in modo da poter calcolare correttamente la prossima release*/
	private long originalTime=0;
	
	public void setOriginalTime(long t){
		this.originalTime=t;
	}
	
	public long getOriginalTime(){
		return this.originalTime;
	}
	
	
	
	public  void addListener(EventListener el){
		if(!listeners.contains(el))
			listeners.add(el);
	}
	
	public void clearListeners(){
		listeners.clear();
	}
	
	public  void removeListener(EventListener el){
		listeners.remove(el);
	}
	
	public void removeAllListeners(){
		listeners.clear();
	}
	
	protected  ArrayList<EventListener> getListeners(){
		return listeners;
	}	

	public void fire(){		
		
		for(int i = 0; i < listeners.size(); i++){
			EventListener listener = listeners.get(i);
			if(StaticConfig.PRINT_LISTENER_FIRING_EVENT)
				System.out.println("[FIRING][LISTENER = " + listener.getName() + "] is about to fire an event " + this.getEventType().getTypeName() );
			listener.handle(this);
		}
			
	}
	
	
	/**
	 * Be carefull to assign the priority to an event.
	 * If there is more than one event triggered at the same time
	 * the priority will be used to sort these events
	 */	

	private Task targetTask;
	
	private Job targetJob;
	
	private EventState state;
	
	public enum EventState {CREATED,HANDLING,TRIGGERED,TO_RETRIGGER,RETRIGGERED,HANDLED,SUPPRESSED,FREE};
	
		
	public Event(){
		
		this.eventId = currentEventId++;
		this.eventTime = new TimeInstant(0);
		this.targetTask = null;
		this.staticPriority = -1;
		this.eventName = "<null>";
		this.eventType = EventType.NONE;
		this.state = EventState.CREATED;
	}
	
			
	public Event(TimeInstant eventAbsoluteTime, Task target, EventType type) {
		
		this.eventId = currentEventId++;
		this.eventTime = eventAbsoluteTime;
		this.targetTask = target;
		setEventType(type);
		this.state = EventState.CREATED;
		
	}
	
	public void clear(){
		setEventTimeValue(0, null);
		setTask(null);
		setEventType(EventType.NONE);
		setState(EventState.CREATED);
	}
	
	public Task getTask(){
		return this.targetTask;
	}
	
	public void setTask(Task target){
		this.targetTask = target;
	}
	
	public Job getJob(){
		return this.targetJob;
	}
	
	public void setJob(Job target){
		this.targetJob = target;
	}
	
	public int getEventStaticPriority(){
		return staticPriority;
	}
	
	public void setEventStaticPriority(int prio){
		this.staticPriority = prio;
	}
			
	public TimeInstant getEventTime() {
		return eventTime;
	}
	
	public long getEventTimeValue(TimeUnit unit){
		return eventTime.getTimeValue(unit);
	}
	
	public long getEventTimeValue(){
		return eventTime.getTimeValue();
	}
	
	public void setEventTimeValue(TimeInstant ti) {
		this.eventTime.setTimeValue(ti.getTimeValue());
	}
	
	public void setEventTime(TimeInstant ti){
		this.eventTime = ti;
	}
	
	public void setEventTimeValue(long timeValue, TimeUnit unit){
		this.eventTime.setTimeValue(timeValue, unit);
	}
	
	public void setSimulator(Simulator s){
		simulator = s;
	}

	public String getName(){
		return this.eventName;
	}
	
	public void setName(String name){
		this.eventName = name;
	}
	
	public EventType getEventType() {
		return eventType;
	}
		
	
	public int getEventId() {
		return eventId;
	}

	
	public boolean isType(EventType eventType){
		return this.getEventType() == eventType ? true : false;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
		this.staticPriority = eventType.getTypePriority();
		this.eventName = eventType.getTypeName();
	}
	
	public EventState getState() {
		return state;
	}
	

	public void setState(EventState state) {
		this.state = state;	
	}
	

	
	public String toString(){
		
		Task target = getTask();
		String targetName;
		if(target != null)
			targetName = target.getName();
		else
			targetName = "none";
		
		StringBuffer sb = new StringBuffer();
		sb.append("Event");
		sb.append("\n");
		
		sb.append("id: ");
		sb.append(getEventId());
		sb.append("\n");
		
		sb.append("type: ");
		sb.append(getEventType());
		sb.append("\n");
		
		sb.append("time: ");
		sb.append(eventTime.getTimeValue());
		sb.append("\n");
		
		sb.append("state: ");
		sb.append(getState());
		sb.append("\n");
		
		sb.append("task: ");
		if(target != null)
			sb.append(target.getName());
		else
			sb.append("none");
		sb.append("\n");
		
		if(target != null && target.getActiveJob() != null){
			sb.append("job id: ");			
			sb.append(target.getActiveJob().getUniqueId());
			sb.append("\n");
			
			sb.append("job instance: ");
			sb.append(target.getActiveJob().getProgInstanceNumber());
			sb.append("\n");
		}
		
		return sb.toString();
		
//		return String.format(SimulationResults.getTableRowFormatForSingleEvent(),this.getEventType().getTypeName(),this.getState().toString(),getEventTime().getTimeValue(),targetName); 
	}


	/**
	 * Ritorna
	 * 1 se this è prioritario rispetto a event
	 */
	@Override
	public int compareTo(Event event) {
		
		//TODO provare a far fare il confronto direttamente a TimeInstant
		//che implementa l'interfaccia Comparable
		
		long thisTime  = this.getEventTimeValue(); 
		long otherTime = event.getEventTimeValue();
		
		int thisPrio   = this.getEventType().getTypePriority();
		int otherPrio  = event.getEventType().getTypePriority();
		
		int thisTargetPrio = 0;
		int otherTargetPrio = 0;
		
		if(		this.getTask() != null &&
				event.getTask() != null &&
				this.getTask().getActiveJob() != null &&
				event.getTask().getActiveJob() != null ){
			
			thisTargetPrio  = this.getTask().getActiveJob().getCurrentPriority();
			otherTargetPrio = event.getTask().getActiveJob().getCurrentPriority();
		}
		
		
		if( thisTime == otherTime )
			if( thisPrio == otherPrio )
				return thisTargetPrio - otherTargetPrio;
			else
				return thisPrio - otherPrio;			
		else{
			if(otherTime > thisTime)
				return 1;
			else if(otherTime < thisTime)
				return -1;
			else
				return 0;
		}
		
	}
	
	
}
