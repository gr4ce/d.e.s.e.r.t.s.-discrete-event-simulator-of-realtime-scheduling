package edu.unibo.ciri.desert.event;

import edu.unibo.ciri.desert.resource.Processor;
import edu.unibo.ciri.desert.resource.SharedResource;
import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.time.Time;

public class RTAIEvent implements Comparable<RTAIEvent> {
	
	private Task      		task = null;
	private EventType 		type = null;
	private Time	  		time = null;
	private String    		name = null;
	private Processor 		proc = null;
	private SharedResource 	res  = null;
	
	public RTAIEvent(){
		
	}	
	
	public RTAIEvent(String name, EventType type, Time time, Task task){		
		setName(name);
		setTask(task);
		setTime(time);
		setType(type);		
	}
	
	public RTAIEvent(String name, EventType type, Time time, Task task, Processor p){
		setName(name);
		setTask(task);
		setTime(time);
		setType(type);	
		setProcessor(p);
	}
	
	public RTAIEvent(String name, EventType type, Time time, Task task, SharedResource sr){
		setName(name);
		setTask(task);
		setTime(time);
		setType(type);	
		setSharedResource(sr);
	}
	
	public RTAIEvent(String name, EventType type, Time time, Task task, Processor p, SharedResource sr){
		setName(name);
		setTask(task);
		setTime(time);
		setType(type);	
		setProcessor(p);
		setSharedResource(sr);
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public EventType getType() {
		return type;
	}

	public void setType(EventType type) {
		this.type = type;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public Processor getProcessor() {
		return proc;
	}

	public void setProcessor(Processor proc) {
		this.proc = proc;
	}

	public SharedResource getSharedResource() {
		return res;
	}

	public void setSharedResource(SharedResource res) {
		this.res = res;
	}

	@Override
	public int compareTo(RTAIEvent event) {
		
		long thisTime  = time.getTimeValue(); 
		long otherTime = event.getTime().getTimeValue();
					
		if(otherTime > thisTime)
			return 1;
		
		if(otherTime < thisTime)
			return -1;
		
		
		/*
		 * If we are here events have the same time. Use the event type priority to decide  
		 */
		int comparison = this.getTask().compareTo(event.getTask());
		if(comparison != 0)
			return comparison;
		
		
		System.out.println("Ouch... same time, same task... " + this.getType().getTypeName() + " prio = " + this.getType().getTypePriority() +
				" " + event.getType().getTypeName() + " prio = " + event.getType().getTypePriority());
		
		/*
		 * If we are here events have the same time. Use the event type priority to decide  
		 */
		if(this.getType().getTypePriority() > event.getType().getTypePriority())
			return 1;
		
		if(this.getType().getTypePriority() < event.getType().getTypePriority())
			return -1;
		
		return 0;
		
	}
	
	@Override
	public String toString(){
		
		return time.getTimeValue() + " " + task +  " " + type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public RTAIEvent clone(){
		return new RTAIEvent(name, type, time, task, proc, res);
	}
	
	

}
