package edu.unibo.ciri.desert.scheduler;


import java.util.concurrent.TimeUnit;

import edu.unibo.ciri.collection.PriorityLinkedList;
import edu.unibo.ciri.collection.PriorityLinkedList.PolicyForTieInsetion;
import edu.unibo.ciri.desert.config.StaticConfig;
import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.event.EventType;
import edu.unibo.ciri.desert.event.Event.EventState;
import edu.unibo.ciri.desert.resource.VirtualPlatform;
import edu.unibo.ciri.desert.simulation.management.Simulation;
import edu.unibo.ciri.realtime.model.Job;
import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.Job.JobState;


public class TimeStepFullPreemptiveScheduler extends PriorityScheduler {

	protected PriorityLinkedList<Job> runningJobs;
	protected PriorityLinkedList<Job> reschedulingJobs;
	protected PriorityLinkedList<Job> waitingJobs;	

	public TimeStepFullPreemptiveScheduler(Simulation sim) {
		
		super(sim);
		
		reschedulingJobs= new PriorityLinkedList<Job>(PolicyForTieInsetion.INSERT_AFTER);
		waitingJobs 	= new PriorityLinkedList<Job>(PolicyForTieInsetion.INSERT_AFTER);
		runningJobs 	= new PriorityLinkedList<Job>(PolicyForTieInsetion.INSERT_AFTER);		
	}



	private void partialHandleJobCompletion(Event event) {

		/*
		 * Come nel caso precedente ma a questo punto bisogna prendere solamente delle 
		 * decisioni parziali e confermarle nel momento in cui la transazione sarà completata.
		 * Una di queste, consiste nel dichiarare libera la cpu sulla quale stava eseguendo il job
		 * che ha terminato. 
		 */
		Task task 		  = event.getTask();
		Job completingJob = task.getActiveJob();
		long currentTime  = event.getEventTimeValue();
		
		VirtualPlatform platform = simulation.getPlatform(); 
		
		boolean hasOverrunPendingJobs = task.hasPendingJobs();
		
		if (!runningJobs.removeElement(completingJob)) {
			System.out.println(completingJob);
			System.out.println(event);
			assert (false);
		}
		completingJob.setState(JobState.IDLE);
		
//		if(completingJob.getRemainingExecutionTime() != 0){
//			System.out.println("remaining before = " + completingJob.getRemainingExecutionTime());
//			System.out.println("current time = " + currentTime);
//			System.out.println(completingJob);
//		}
		
		completingJob.setEndExecutionTime(currentTime);
		
		if(completingJob.getRemainingExecutionTime() != 0){
			System.out.println("remaining after = " + completingJob.getRemainingExecutionTime());
			assert(false);
		}


		task.completeCurrentJob();
		platform.freeProcessor(completingJob.getCpu());

		/*
		 * Eventuali job che dovrebbero essere messi in esecuzione, vengono 
		 * memorizzati in una coda temporanea che verrà processata a termine transazione.
		 */
		if (waitingJobs.size() > 0)
			reschedulingJobs.orderedInsert(waitingJobs.popHead());		

		if (hasOverrunPendingJobs)
			reschedulingJobs.orderedInsert(task.getActiveJob());		
		
	}


	private void partialHandleJobRelease(Event event) {
		

		Task task = event.getTask();
		assert(task != null);
		
		Job jobToRelease = task.getActiveJob();
		assert(jobToRelease != null);
		
		/*
		 * Careful here!! Do not schedule new jobs if there are overrun jobs
		 * (from the same task) not yet completed
		 */
		if (!task.hasPendingJobs() && !jobToRelease.hasOverrun())
			reschedulingJobs.orderedInsert(jobToRelease);
	}
	
	private void reinsertJob(PriorityLinkedList<Job> listFrom,PriorityLinkedList<Job> listTo, Job j){

		if(reschedulingJobs.contains(j))
			return;
		
		if ( !listFrom.removeElement(j)){
			System.out.println("Element not founded  in priority change handler " + j.getTask().getName() );
			assert(false);
		}
		listTo.orderedInsert(j);
		
	}
	

	private void partialHandleJobPriorityChange(Event event){
		//System.out.println("partialHandleJobPriorityChange: "+event.getEventTimeValue()+" "+event.getTask().getName());
		Job j = event.getJob();
		
		switch ( j.getState() ) {

			case RUNNING:
				//lo vado a prendere dalla coda running e inserisco in quella rescheduling
				reinsertJob(runningJobs,reschedulingJobs,j);
				break;
			case READY:
				//lo vado a prendere dalla coda waiting e inserisco in quella rescheduling
				reinsertJob(waitingJobs,reschedulingJobs,j);
				return;
			case PREEMPTED:
				//lo vado a prendere dalla coda waiting e inserisco in quella rescheduling		
				reinsertJob(waitingJobs,reschedulingJobs,j);
				return;
			default:
				System.out.println("Founded in priority change handler " + j.getState() );
				assert(false);
		}//switch		
		
	}//partialHandleJobPriorityChange
	
	

	
	
	private void confirmSchedule() {
		
		if(StaticConfig.PRINT_SCHEDULING_ACTION){
			System.out.println("[SCHEDULER][ACTION = confirm] " + simulation.getSimulator().getCurrentTime().getTimeValue(TimeUnit.MILLISECONDS)  );			
		}

		Job currentReschedulingJob, lowPriorityRunningJob = null;
		VirtualPlatform platform = simulation.getPlatform();		

		while ((currentReschedulingJob = reschedulingJobs.popHead()) != null) {

			if (platform.hasFreeProcessor()) {
				
				int freeCpu = platform.getFreeProcessorId();
				
				if(StaticConfig.PRINT_SCHEDULING_ACTION){
					System.out.println("\t\t[PRE_SCHEDULE][ACTION = schedule to run] " + currentReschedulingJob +"{"+currentReschedulingJob.getInfo().getRemainingExecution()+"} on CPU_" + freeCpu);			
				}
				
				scheduleToRun(currentReschedulingJob, freeCpu);	
				
				if(StaticConfig.PRINT_SCHEDULING_ACTION){
					System.out.println("\t\t[POST_SCHEDULE][ACTION = schedule to run] " + currentReschedulingJob +"{"+currentReschedulingJob.getInfo().getRemainingExecution()+"} on CPU_" + freeCpu);			
				}
				
				continue;
			}
			
			assert (!platform.hasFreeProcessor());
			lowPriorityRunningJob = runningJobs.getLast();
			
			assert (lowPriorityRunningJob != null);
			
			if (currentReschedulingJob.compareTo(lowPriorityRunningJob) > 0){
				if(StaticConfig.PRINT_SCHEDULING_ACTION){
					System.out.println("\t\t[PRE_SCHEUDLE][ACTION = context switch] OUT = " + lowPriorityRunningJob + "{"+lowPriorityRunningJob.getInfo().getRemainingExecution()+"} IN = "+ currentReschedulingJob+"{"+currentReschedulingJob.getInfo().getRemainingExecution()+"}");
				}
				contextSwitch(lowPriorityRunningJob, currentReschedulingJob);
				if(StaticConfig.PRINT_SCHEDULING_ACTION){
					System.out.println("\t\t[POST_SCHEDULE][ACTION = context switch] OUT = " + lowPriorityRunningJob + "{"+lowPriorityRunningJob.getInfo().getRemainingExecution()+"} IN = "+ currentReschedulingJob+"{"+currentReschedulingJob.getInfo().getRemainingExecution()+"}");			
				}
			}
			else{
				if(StaticConfig.PRINT_SCHEDULING_ACTION){
					System.out.println("\t\t[PRE_SCHEUDLE][ACTION = schedule to wait] "+ currentReschedulingJob+"{"+currentReschedulingJob.getInfo().getRemainingExecution()+"}");		
				}
				scheduleToWait(currentReschedulingJob);	
				if(StaticConfig.PRINT_SCHEDULING_ACTION){
					System.out.println("\t\t[POST_SCHEUDLE][ACTION = schedule to wait] "+ currentReschedulingJob+"{"+currentReschedulingJob.getInfo().getRemainingExecution()+"}");		
				}
			}
			
			
			if(platform.isIdle()){				
				Event platformIdleEvent = simulation.getEventFactory().getEventInstance(simulation.getSimulator().getCurrentTime().getTimeValue(), EventType.PLATFORM_IDLE,null);
				simulation.fire(platformIdleEvent);
			}

		}
		
		
		//Test, quando la transizione è causata da più JOB_CHANGE_PRIORITY devo controllare che tra i waiting jobs non ci siano dei job più prioritari
//		if(waitingJobs.size()>0){
//			
//			while(waitingJobs.getFirst().compareTo(runningJobs.getLast())>0){
//				Job highPriorityWaitingjob = waitingJobs.getFirst(); 
//				waitingJobs.removeElementAt(0);
//				contextSwitch(runningJobs.getLast(), highPriorityWaitingjob);
//				
//			}
//			
//		}

	}


	public void handle(Event event) {	
		
		if(StaticConfig.PRINT_SCHEDULER_HANDLING){
			if(event.getTask() != null)
				System.out.println("[SCHEDULER][INFO] Handling "+ event.getEventType() + " of " + event.getTask().getName()+ " "+ event.getEventTimeValue());
			else
				System.out.println("[SCHEDULER][INFO] Handling "+ event.getEventType() + " "+ event.getEventTimeValue());
		}
		

		switch (event.getEventType()) {
			case TIME_STEP_FORWARD:
				confirmSchedule();
				break;
			case JOB_RELEASE:
				partialHandleJobRelease(event);
				break;
			case JOB_COMPLETION:
				partialHandleJobCompletion(event);
				break;
			case JOB_PRIORITY_CHANGE:
				partialHandleJobPriorityChange(event);
				break;
			default:
				System.out.println("Founded a " + event.getEventType());
				assert(false);
		}
	}
	
	private void contextSwitch(Job outJob, Job inJob) {
						
		int freeCpu = preempt(outJob);
		scheduleToRun(inJob, freeCpu);

	}

	private void scheduleToWait(Job waitingJob) {
		
		switch(waitingJob.getState()){
			case RUNNING:
				break;
			case PREEMPTED:
				waitingJobs.orderedInsert(waitingJob);
				return;
			case READY:
				waitingJobs.orderedInsert(waitingJob);
				return;				
			case RELEASED:
				waitingJob.setState(JobState.READY);
				break;
		}		
		waitingJobs.orderedInsert(waitingJob);
		
		Event waitEvent = simulation.getEventFactory().getEventInstance(simulation.getSimulator().getCurrentTime().getTimeValue(), EventType.JOB_WAIT, waitingJob.getTask());	
		waitingJob.setEvent(waitEvent);
		waitEvent.setJob(waitingJob);
		
		simulation.fire(waitEvent);
		
	}

	private int preempt(Job preemptedJob) {

		assert(preemptedJob.getState() == JobState.RUNNING);
		assert(preemptedJob.getEvent(EventType.JOB_COMPLETION) != null);
		
		VirtualPlatform platform 	= simulation.getPlatform();		
		long currTime 		= simulation.getSimulator().getCurrentTime().getTimeValue();
		
		/*
		 * Remove the job to be preempted from its running cpu
		 */
		int freeCpu = preemptedJob.getCpu();
		platform.freeProcessor(freeCpu);
		
		/*
		 * Remove the completion event from the job events and suppress it
		 */
		Event completionEvent = preemptedJob.getEvent(EventType.JOB_COMPLETION);
		assert(completionEvent != null);
		preemptedJob.setEvent(EventType.JOB_COMPLETION, null);
		if (!simulation.getEventManager().suppress(completionEvent))
			assert (false);
//		assert(completionEvent.getState() == EventState.SUPPRESSED);
		/*
		 * Update the queues state and change the job state
		 */
		preemptedJob.setState(JobState.PREEMPTED);
		if (!runningJobs.removeElement(preemptedJob))
			assert (false);
		waitingJobs.orderedInsert(preemptedJob);

		/*
		 * Update the queues state and change the job state
		 */
		Event jobPreemptionEvent = simulation.getEventFactory()
				.getEventInstance(currTime, EventType.JOB_PREEMPTION,
						preemptedJob.getTask());
		jobPreemptionEvent.setJob(preemptedJob);
		preemptedJob.setEvent(jobPreemptionEvent);
		
		/*
		 * Write time information
		 */
		preemptedJob.setEndExecutionTime(currTime);

		/*
		 * Fire the event so all the listeners can handle it
		 */
		simulation.fire(jobPreemptionEvent);

		return freeCpu;
	}

	private void scheduleToRun(Job jobToRun, int freeCpu) {
				
		long currTime = simulation.getSimulator().getCurrentTime().getTimeValue();		
		
		JobState currInJobState = jobToRun.getState();
		
		long remainingExecutionTime = jobToRun.getRemainingExecutionTime();
		assert(remainingExecutionTime >= 0);
		long triggerTime = currTime + remainingExecutionTime;
		if(triggerTime <= currTime){
			System.out.println("REMANING EXECUTION JOB: " + jobToRun + " " + remainingExecutionTime);			
			assert(false);
		}

		if (currInJobState == JobState.READY || currInJobState == JobState.RELEASED) {
			// currJobInfo.setStartTime(currTime,cpu);
			Event jobStart = simulation.getEventFactory().getEventInstance(
					currTime, EventType.JOB_START, jobToRun.getTask());
			jobStart.setCpu(freeCpu);
			jobStart.setJob(jobToRun);
			jobToRun.setCpu(freeCpu);
			simulation.getPlatform().setJobRunningOn(jobToRun, freeCpu);

			/*
			 * Write time information
			 */
			jobToRun.setStartExecutionTime(currTime);
			
			simulation.fire(jobStart);

			
		} else if (currInJobState == JobState.PREEMPTED) {
			// currJobInfo.addResumeTime(currTime,cpu);
			Event jobResume = simulation.getEventFactory().getEventInstance(
					currTime, EventType.JOB_RESUME, jobToRun.getTask());
			jobResume.setCpu(freeCpu);
			jobResume.setJob(jobToRun);
			jobToRun.setCpu(freeCpu);
			simulation.getPlatform().setJobRunningOn(jobToRun, freeCpu);

			/*
			 * Write time information
			 */
			jobToRun.setStartExecutionTime(currTime);
			
			simulation.fire(jobResume);

		} else {
			
			System.out.println(jobToRun);
			assert (false);
		}

		jobToRun.setState(JobState.RUNNING);
		runningJobs.orderedInsert(jobToRun);	
		

		
		
		Event completionEvent = simulation.getEventFactory().getEventInstance(triggerTime, EventType.JOB_COMPLETION,jobToRun.getTask());
		assert(completionEvent.getState() == EventState.CREATED);
		jobToRun.setEvent(completionEvent);
		completionEvent.setJob(jobToRun);
		simulation.getEventManager().trigger(completionEvent);
		

	}
	
	public void clear(){
		
		runningJobs.clear();
		waitingJobs.clear();
		reschedulingJobs.clear();	
	}
	
	@Override
	public String getName() {
		
		return "TimeStepFullPreemptiveScheduler";
	}

}
