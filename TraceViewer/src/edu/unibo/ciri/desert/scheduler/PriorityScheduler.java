package edu.unibo.ciri.desert.scheduler;

import edu.unibo.ciri.collection.PriorityLinkedList;
import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.event.management.EventListener;
import edu.unibo.ciri.desert.simulation.management.Simulation;
import edu.unibo.ciri.realtime.model.Job;

public abstract class PriorityScheduler implements EventListener{
		
	protected Simulation    			simulation;	
	protected PriorityLinkedList<Job> 	activeJobs;	
	
	public PriorityScheduler(Simulation sim){
		simulation = sim;
		activeJobs = new PriorityLinkedList<Job>();
	}	
		
	@Override
	public abstract void handle(Event event);

	public abstract void clear();
		

}
