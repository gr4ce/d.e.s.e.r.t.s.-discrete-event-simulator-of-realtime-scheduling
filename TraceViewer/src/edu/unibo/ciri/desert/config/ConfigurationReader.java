package edu.unibo.ciri.desert.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;


public class ConfigurationReader {
	
	private static File 				sourceConfigFile = null;
	private static String 				sourceConfigFileName = "./global.conf";
	private static BufferedReader 		br;
	private static FileInputStream 		fis;
	private static InputStreamReader 	isr;
	private static String 				bufferString;
	
	static{		
		
		if(sourceConfigFile == null){
			
			sourceConfigFile = new File(sourceConfigFileName);
			
			try {
				
				fis = new FileInputStream(sourceConfigFile);
				isr = new InputStreamReader(fis);
				br = new BufferedReader(isr);
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
		}	
		
		try {
			while((bufferString = br.readLine()) != null){
												
				if(bufferString.startsWith("BEGIN")){
					
					StringTokenizer st = new StringTokenizer(bufferString);
					
					if(!st.hasMoreTokens())
						assert(false);
					assert(st.nextToken().equals("BEGIN"));
					
					if(!st.hasMoreTokens())
						assert(false);
										
				}
				
			}	
			
			
		} catch (IOException e) {			
		}
		
	}
	
	
	
	

}
