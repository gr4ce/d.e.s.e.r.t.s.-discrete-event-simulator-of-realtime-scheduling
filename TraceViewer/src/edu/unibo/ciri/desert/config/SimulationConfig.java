package edu.unibo.ciri.desert.config;




public class SimulationConfig {
	
	private final int			defaultProcessors			= 1;
	private final String 		defaultSchedulingPolicy		= "FIXED-PRIORITY RATE MONOTONIC";
	private final boolean		defaultEnableTracingTime	= true;
	private final boolean		defaultEnableTracingEvent	= false;
	
	private int					processors;	
	private String			 	schedulingPolicy;	
	private boolean				enableTracingTime;
	private boolean 			enableTracingEvent;	
	
	public SimulationConfig(){
		
		setProcessors(defaultProcessors);
		setSchedulingPolicy(defaultSchedulingPolicy);
		setEnableTracingTime(defaultEnableTracingTime);		
		setEnableTracingEvent(defaultEnableTracingEvent);
	}

	public int getProcessors() {
		return processors;
	}

	public void setProcessors(int processors) {
		this.processors = processors;
	}

	public String getSchedulingPolicy() {
		return schedulingPolicy;
	}

	public void setSchedulingPolicy(String schedulingPolicy) {
		this.schedulingPolicy = schedulingPolicy;
	}

	public void setEnableTracingTime(boolean enableTracingTime) {
		this.enableTracingTime = enableTracingTime;
	}

	public boolean isEnableTracingTime() {
		return enableTracingTime;
	}

	public void setEnableTracingEvent(boolean enableTracingEvent) {
		this.enableTracingEvent = enableTracingEvent;
	}

	public boolean isEnableTracingEvent() {
		return enableTracingEvent;
	}
	
	

}
