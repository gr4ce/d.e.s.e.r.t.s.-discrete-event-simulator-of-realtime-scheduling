package edu.unibo.ciri.desert.config;



public class SuiteConfig {
	
	
	private final int defaultParallelSimulation	= 4;
	
	private int parallelSimulation;
	
	public SuiteConfig(){
		
		setParallelSimulation(defaultParallelSimulation);
	}

	public int getParallelSimulation() {
		return parallelSimulation;
	}

	public void setParallelSimulation(int parallelSimulation) {
		this.parallelSimulation = parallelSimulation;
	}


}
