package edu.unibo.ciri.desert.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Properties;
import java.util.StringTokenizer;

public class Configuration {
	
	
	/*
	 * This is a property in the configuration file named DATASET_FILE
	 * You can specify the file by which tasksets are provided to simulation
	 */	
	public static final String configFileName = "global.conf";
	
	private static Properties properties = new Properties();	
	
	
	/*
	 * All the enum fields of the configuration
	 */
	private static ScheulingPolicyName schedulingPolicyName;	
	public enum ScheulingPolicyName {
		FIXED_PRIORITY,
		EARLIEST_DEADLINE_FIRST,
		FIXED_PRIORITY_ZERO_LAXITY,
		EARLIEST_DEADLINE_ZERO_LAXITY
	};	
	
	
	private static ScheulingPolicyType schedulingPolicyType;	
	public enum ScheulingPolicyType {
		PARTITIONED,
		GLOBAL,
		SEMI_PARTITIONED
	};
	
	private static StaticPartitioningPolicyName staticPartitioningPolicyName;	
	public enum StaticPartitioningPolicyName{
		BEST_FIT,
		FIRST_FIT,
		USER_DEFINED
	}
	
	private static StaticSortingPolicyName staticSortingPolicyName;	
	public enum StaticSortingPolicyName{
		RATE_MONOTONIC, 
		DEADLINE_MONOTONIC, 
		SLACK_MONOTONIC,
		DKC
	}
	
	
		
	
	/*
	 * All the numeric fields
	 */
	private static final int DEFAULT_PARALLEL_SIMULATION_THREADS = 1;
	private static final int MAX_PARALLEL_SIMULATION_THREADS = Runtime.getRuntime().availableProcessors() * 2;	
	private static 	     int PARALLEL_SIMULATION_THREADS;
	
	
	
	/*
	 * All the string fields
	 */	
	/* The main working directory given as path name */
	private static final String DEFAULT_WORKING_PATH = "./simulator_workspace/";
	private static String workingPath;
	
	/* The dataset directory */
	private static final String DEFAULT_DATASET_DIR = "input/dataset/";
	private static String datasetDir;
	private static String datasetFiles[];
	
	/* The results directory */
	private static final String DEFAULT_RESULT_DIR  = "output/result/";
	private static String resultDir;
	
	
	private static final String DEFAULT_EVENTS_COLLECTED_FILE_NAME = "collected_events.dat";
	private static String eventsCollectedPathName; 
	
	
	/*
	 * All the boolean fields
	 */	
	private static boolean enableOverheadAccounting = false;
	private static boolean multipleDataset    = false;
	
	/* 
	 * Data collecting section 
	*/
	
	private static boolean collectActualSimulatedTime		= false;
	private static boolean collectScheduledSimulatedTime	= false;
	private static boolean collectResponseTime				= false;
	private static boolean collectPreemptionAndResumeEvents = false;
	private static boolean collectRealSimulationTime		= false;
	
	private static boolean collectMissDeadlineEvents		= false;
	private static boolean collectOverrunEvents				= false;
	
	private static boolean countPreemptionNumber			= false;
	private static boolean countMissDeadlineNumber			= false;
	private static boolean countOverrunNumber				= false;
	private static boolean countTotalFiredEvents			= false;
	private static boolean countFiredEventsByType			= false;
	private static boolean countTotalCanceledEvents			= false;
	
	
	
	public static void importAndCheckConfigurationPropertyValues() throws InvalidPropertyConfigurationException{
		
		String propertyName,propertyValue;
		int enumSize = 0;
		boolean valueSuccessfullyFound = false;


		propertyName  = "WORKING_PATH";
		propertyValue = properties.getProperty(propertyName);
		if(	propertyValue != null && !propertyValue.equals("")){
			File f = new File(propertyValue);
			if(!f.exists())
				throw new InvalidPropertyConfigurationException("The path specified in " + propertyName + " does not exist: " + propertyValue );
			workingPath = propertyValue;
		}
		else{
			workingPath = DEFAULT_WORKING_PATH;
		}
		
		
		propertyName  = "DATASET_DIR";
		propertyValue = properties.getProperty(propertyName);
		if(	propertyValue != null && !propertyValue.equals("")){
			File f = new File(workingPath + propertyValue);
			if(!f.exists())
				throw new InvalidPropertyConfigurationException("The path specified in " + propertyName + " does not exist: " + f.getAbsolutePath() );
			datasetDir = propertyValue;
		}
		else{
			datasetDir = DEFAULT_DATASET_DIR;
		}
		
		
		propertyName  = "RESULT_DIR";
		propertyValue = properties.getProperty(propertyName);
		if(	propertyValue != null && !propertyValue.equals("")){
			File f = new File(workingPath + propertyValue);
			if(!f.exists())
				throw new InvalidPropertyConfigurationException("The path specified in " + propertyName + " does not exist: " + f.getAbsolutePath() );
			resultDir = propertyValue;
		}
		else{
			resultDir = DEFAULT_RESULT_DIR;
		}
				
		
		//scanning the input dataset directory	
		
		File inputDatasetPath = new File(workingPath + datasetDir);
		if(!inputDatasetPath.exists() || !inputDatasetPath.isDirectory())
			throw new InvalidPropertyConfigurationException("Invalid dataset directory: "+inputDatasetPath);
				
		File files[] = inputDatasetPath.listFiles(new FilenameFilter() {
				
			@Override
			public boolean accept(File dir, String name) {
				if(name.endsWith(".dat"))
					return true;
				return false;
			}
		});
		datasetFiles = new String[files.length];
		for(int i = 0; i < files.length; i++){
			datasetFiles[i] = files[i].getAbsolutePath();
		}
			
		
		
	
		
					
		
		propertyName  = "REPLICATED_TRYAL";
		propertyValue = properties.getProperty(propertyName);
		if( propertyValue != null){
			if( !propertyValue.equalsIgnoreCase("true") && !propertyValue.equalsIgnoreCase("false"))			
				throw new InvalidPropertyConfigurationException("Property " + propertyName + " has an incorrect value: " + propertyValue );
		}
		
		
		propertyName  = "PARALLEL_SIMULATOR";
		propertyValue = properties.getProperty(propertyName);
		if( propertyValue != null){
			if( !propertyValue.equalsIgnoreCase("true") && !propertyValue.equalsIgnoreCase("false"))			
				throw new InvalidPropertyConfigurationException("Property " + propertyName + " has an incorrect value: " + propertyValue );
		}
						 
		propertyName  = "PARALLEL_SIMULATION_THREADS";
		propertyValue = properties.getProperty(propertyName);
		if(propertyValue == null){
			
			PARALLEL_SIMULATION_THREADS = MAX_PARALLEL_SIMULATION_THREADS;
			
		}else{
			
			try{
				PARALLEL_SIMULATION_THREADS = Integer.parseInt(propertyValue);
				if(PARALLEL_SIMULATION_THREADS < 1)
					throw new InvalidPropertyConfigurationException("Property " + propertyName + " has an incorrect value: " + propertyValue );
			}catch(NumberFormatException nfe){
				PARALLEL_SIMULATION_THREADS = DEFAULT_PARALLEL_SIMULATION_THREADS;
				throw new InvalidPropertyConfigurationException("Property " + propertyName + " has an incorrect value: " + propertyValue );
			}
			
		}
		
		
		propertyName  = "SCHEDULING_POLICY_NAME";
		propertyValue = properties.getProperty(propertyName);
		
		ScheulingPolicyName schedulingPolicyNameValues[] = ScheulingPolicyName.values();
		enumSize = schedulingPolicyNameValues.length;
		valueSuccessfullyFound = false;
		
		for(int i = 0; i < enumSize; i++)
			if(schedulingPolicyNameValues[i].toString().equals(propertyValue)){
				schedulingPolicyName = schedulingPolicyNameValues[i];
				valueSuccessfullyFound = true;
				break;
			}
		
		if(!valueSuccessfullyFound)
			throw new InvalidPropertyConfigurationException("Property " + propertyName + " has an incorrect value: " + propertyValue );
		
		
		propertyName  = "SCHEDULING_POLICY_TYPE";
		propertyValue = properties.getProperty(propertyName);
		
		ScheulingPolicyType schedulingPolicyTypeValues[] = ScheulingPolicyType.values();
		enumSize = schedulingPolicyTypeValues.length;
		valueSuccessfullyFound = false;
		
		for(int i = 0; i < enumSize; i++)
			if(schedulingPolicyTypeValues[i].toString().equals(propertyValue)){
				schedulingPolicyType = schedulingPolicyTypeValues[i];
				valueSuccessfullyFound = true;
				break;
			}
		
		if(!valueSuccessfullyFound)
			throw new InvalidPropertyConfigurationException("Property " + propertyName + " has an incorrect value: " + propertyValue );
		
		
		
		propertyName  = "STATIC_PARTITIONING_POLICY_NAME";		
		propertyValue = (new StringTokenizer(properties.getProperty(propertyName))).nextToken();
		
		StaticPartitioningPolicyName staticPartitioningPolicyNameValues[] = StaticPartitioningPolicyName.values();
		enumSize = staticPartitioningPolicyNameValues.length;
		valueSuccessfullyFound = false;
		
		for(int i = 0; i < enumSize; i++)
			if(staticPartitioningPolicyNameValues[i].toString().equals(propertyValue)){
				staticPartitioningPolicyName = staticPartitioningPolicyNameValues[i];
				valueSuccessfullyFound = true;
				break;
			}
		
		if(!valueSuccessfullyFound)
			throw new InvalidPropertyConfigurationException("Property " + propertyName + " has an incorrect value: " + propertyValue );
					
		
		
		propertyName  = "STATIC_SORTING_POLICY_NAME";
		propertyValue = (new StringTokenizer(properties.getProperty(propertyName))).nextToken();
		
		StaticSortingPolicyName staticSortingPolicyNameValues[] = StaticSortingPolicyName.values();
		enumSize = staticSortingPolicyNameValues.length;		
		valueSuccessfullyFound = false;		
		
		for(int i = 0; i < enumSize; i++)
			if(staticSortingPolicyNameValues[i].toString().equals(propertyValue)){
				staticSortingPolicyName = staticSortingPolicyNameValues[i];
				valueSuccessfullyFound = true;		
				break;
			}
		
		if(!valueSuccessfullyFound)
			throw new InvalidPropertyConfigurationException("Property " + propertyName + " has an incorrect value: " + propertyValue );
			
		
		
		propertyName  = "COLLECT_ACTUAL_SIMULATED_TIME";
		propertyValue = properties.getProperty(propertyName);
		valueSuccessfullyFound = true;	
		
		if(propertyValue.equalsIgnoreCase("true")){
			collectActualSimulatedTime = true;	
		}else if(propertyValue.equalsIgnoreCase("false")){
			collectActualSimulatedTime = false;
		}else{
			valueSuccessfullyFound = false;	
		}			
		if(!valueSuccessfullyFound)
			throw new InvalidPropertyConfigurationException("Property " + propertyName + " has an incorrect value: " + propertyValue );
		
		
		
		propertyName  = "COLLECT_SCHEDULED_SIMULATED_TIME";
		propertyValue = properties.getProperty(propertyName);
		valueSuccessfullyFound = true;	
		
		if(propertyValue.equalsIgnoreCase("true")){
			collectScheduledSimulatedTime = true;	
		}else if(propertyValue.equalsIgnoreCase("false")){
			collectScheduledSimulatedTime = false;
		}else{
			valueSuccessfullyFound = false;	
		}			
		if(!valueSuccessfullyFound)
			throw new InvalidPropertyConfigurationException("Property " + propertyName + " has an incorrect value: " + propertyValue );
		
		
		
		propertyName  = "COLLECT_REAL_SIMULATION_TIME";
		propertyValue = properties.getProperty(propertyName);
		valueSuccessfullyFound = true;	
		
		if(propertyValue.equalsIgnoreCase("true")){
			collectScheduledSimulatedTime = true;	
		}else if(propertyValue.equalsIgnoreCase("false")){
			collectScheduledSimulatedTime = false;
		}else{
			valueSuccessfullyFound = false;	
		}			
		if(!valueSuccessfullyFound)
			throw new InvalidPropertyConfigurationException("Property " + propertyName + " has an incorrect value: " + propertyValue );
		
		
		
		propertyName  = "COLLECT_RESPONSE_TIME";
		propertyValue = properties.getProperty(propertyName);
		valueSuccessfullyFound = true;	
		
		if(propertyValue.equalsIgnoreCase("true")){
			collectScheduledSimulatedTime = true;	
		}else if(propertyValue.equalsIgnoreCase("false")){
			collectScheduledSimulatedTime = false;
		}else{
			valueSuccessfullyFound = false;	
		}			
		if(!valueSuccessfullyFound)
			throw new InvalidPropertyConfigurationException("Property " + propertyName + " has an incorrect value: " + propertyValue );

		
		
		propertyName  = "COLLECT_PREEMPTION_AND_RESUME_EVENTS";
		propertyValue = properties.getProperty(propertyName);
		valueSuccessfullyFound = true;	
		
		if(propertyValue.equalsIgnoreCase("true")){
			collectPreemptionAndResumeEvents = true;	
		}else if(propertyValue.equalsIgnoreCase("false")){
			collectPreemptionAndResumeEvents = false;
		}else{
			valueSuccessfullyFound = false;	
		}			
		if(!valueSuccessfullyFound)
			throw new InvalidPropertyConfigurationException("Property " + propertyName + " has an incorrect value: " + propertyValue );
		 		 	
				

		propertyName  = "COLLECT_MISS_DEADLINE_EVENTS";
		propertyValue = properties.getProperty(propertyName);
		valueSuccessfullyFound = true;	
		
		if(propertyValue.equalsIgnoreCase("true")){
			collectMissDeadlineEvents = true;	
		}else if(propertyValue.equalsIgnoreCase("false")){
			collectMissDeadlineEvents = false;
		}else{
			valueSuccessfullyFound = false;	
		}			
		if(!valueSuccessfullyFound)
			throw new InvalidPropertyConfigurationException("Property " + propertyName + " has an incorrect value: " + propertyValue );
		
		
		
		propertyName  = "COLLECT_OVERRUN_EVENTS";
		propertyValue = properties.getProperty(propertyName);
		valueSuccessfullyFound = true;	
		
		if(propertyValue.equalsIgnoreCase("true")){
			collectOverrunEvents = true;	
		}else if(propertyValue.equalsIgnoreCase("false")){
			collectOverrunEvents = false;
		}else{
			valueSuccessfullyFound = false;	
		}			
		if(!valueSuccessfullyFound)
			throw new InvalidPropertyConfigurationException("Property " + propertyName + " has an incorrect value: " + propertyValue );
		 	
					

		propertyName  = "COUNT_PREEMPTION_NUMBER";
		propertyValue = properties.getProperty(propertyName);
		valueSuccessfullyFound = true;	
		
		if(propertyValue.equalsIgnoreCase("true")){
			countPreemptionNumber = true;	
		}else if(propertyValue.equalsIgnoreCase("false")){
			countPreemptionNumber = false;
		}else{
			valueSuccessfullyFound = false;	
		}			
		if(!valueSuccessfullyFound)
			throw new InvalidPropertyConfigurationException("Property " + propertyName + " has an incorrect value: " + propertyValue );
					
		
		
		propertyName  = "COUNT_MISS_DEADLINE_NUMBER";
		propertyValue = properties.getProperty(propertyName);
		valueSuccessfullyFound = true;	
		
		if(propertyValue.equalsIgnoreCase("true")){
			countMissDeadlineNumber = true;	
		}else if(propertyValue.equalsIgnoreCase("false")){
			countMissDeadlineNumber = false;
		}else{
			valueSuccessfullyFound = false;	
		}			
		if(!valueSuccessfullyFound)
			throw new InvalidPropertyConfigurationException("Property " + propertyName + " has an incorrect value: " + propertyValue );
		
		
						 
		propertyName  = "COUNT_OVERRUN_NUMBER";
		propertyValue = properties.getProperty(propertyName);
		valueSuccessfullyFound = true;	
		
		if(propertyValue.equalsIgnoreCase("true")){
			countOverrunNumber = true;	
		}else if(propertyValue.equalsIgnoreCase("false")){
			countOverrunNumber = false;
		}else{
			valueSuccessfullyFound = false;	
		}			
		if(!valueSuccessfullyFound)
			throw new InvalidPropertyConfigurationException("Property " + propertyName + " has an incorrect value: " + propertyValue );
		
				
		
		propertyName  = "COUNT_TOTAL_FIRED_EVENTS";
		propertyValue = (new StringTokenizer(properties.getProperty(propertyName))).nextToken();
		valueSuccessfullyFound = true;	
		
		if(propertyValue.equalsIgnoreCase("true")){
			countTotalFiredEvents = true;	
		}else if(propertyValue.equalsIgnoreCase("false")){
			countTotalFiredEvents = false;
		}else{
			valueSuccessfullyFound = false;	
		}			
		if(!valueSuccessfullyFound)
			throw new InvalidPropertyConfigurationException("Property " + propertyName + " has an incorrect value: " + propertyValue );
			
		
		
		propertyName  = "COUNT_FIRED_EVENTS_BY_TYPE";
		propertyValue = (new StringTokenizer(properties.getProperty(propertyName))).nextToken();
		valueSuccessfullyFound = true;	
		
		if(propertyValue.equalsIgnoreCase("true")){
			countFiredEventsByType = true;	
		}else if(propertyValue.equalsIgnoreCase("false")){
			countFiredEventsByType = false;
		}else{
			valueSuccessfullyFound = false;	
		}			
		if(!valueSuccessfullyFound)
			throw new InvalidPropertyConfigurationException("Property " + propertyName + " has an incorrect value: " + propertyValue );
		
		
		
		propertyName  = "COUNT_TOTAL_CANCELED_EVENTS";
		propertyValue = (new StringTokenizer(properties.getProperty(propertyName))).nextToken();
		valueSuccessfullyFound = true;	
		
		if(propertyValue.equalsIgnoreCase("true")){
			countFiredEventsByType = true;	
		}else if(propertyValue.equalsIgnoreCase("false")){
			countFiredEventsByType = false;
		}else{
			valueSuccessfullyFound = false;	
		}			
		if(!valueSuccessfullyFound)
			throw new InvalidPropertyConfigurationException("Property " + propertyName + " has an incorrect value: " + propertyValue );
			
				
		
	}
	
	public static void loadConfigurationFromFile(String fileName) throws IOException, InvalidPropertyConfigurationException{	
		
		properties.load(new FileInputStream(fileName));
		importAndCheckConfigurationPropertyValues();
	}
	
	public static void loadConfigurationFromFile() throws IOException, InvalidPropertyConfigurationException{	
		
		properties.load(new FileInputStream(configFileName));
		importAndCheckConfigurationPropertyValues();
	}
	
	

	public static ScheulingPolicyName getSchedulingPolicyName() {
		return schedulingPolicyName;
	}

	public static void setSchedulingPolicyName(
			ScheulingPolicyName schedulingPolicyName) {
		Configuration.schedulingPolicyName = schedulingPolicyName;
	}

	public static ScheulingPolicyType getSchedulingPolicyType() {
		return schedulingPolicyType;
	}

	public static void setSchedulingPolicyType(
			ScheulingPolicyType schedulingPolicyType) {
		Configuration.schedulingPolicyType = schedulingPolicyType;
	}

	public static StaticPartitioningPolicyName getStaticPartitioningPolicyName() {
		return staticPartitioningPolicyName;
	}

	public static void setStaticPartitioningPolicyName(
			StaticPartitioningPolicyName staticPartitioningPolicyName) {
		Configuration.staticPartitioningPolicyName = staticPartitioningPolicyName;
	}

	public static StaticSortingPolicyName getStaticSortingPolicyName() {
		return staticSortingPolicyName;
	}

	public static void setStaticSortingPolicyName(
			StaticSortingPolicyName staticSortingPolicyName) {
		Configuration.staticSortingPolicyName = staticSortingPolicyName;
	}

	public static int getParallelSimulationThreads() {
		return PARALLEL_SIMULATION_THREADS;
	}

	public static void setParallelSimulationThreads(int parallelSimulationThreads) {
		Configuration.PARALLEL_SIMULATION_THREADS = parallelSimulationThreads;
	}

	public static boolean isOverheadAccounting() {
		return enableOverheadAccounting;
	}

	public static void setOverheadAccounting(boolean overheadAccounting) {
		Configuration.enableOverheadAccounting = overheadAccounting;
	}
	
	public static boolean hasMultpleDataset(){
		return Configuration.multipleDataset;
	}
	
	public static String[] getDatasetFileNames(){
		return datasetFiles;
	}

	public static boolean isCollectActualSimulatedTime() {
		return collectActualSimulatedTime;
	}

	public static boolean isCollectScheduledSimulatedTime() {
		return collectScheduledSimulatedTime;
	}

	public static boolean isCollectResponseTime() {
		return collectResponseTime;
	}

	public static boolean isCollectPreemptionAndResumeTime() {
		return collectPreemptionAndResumeEvents;
	}

	public static boolean isCollectMissDeadlineEvents() {
		return collectMissDeadlineEvents;
	}

	public static boolean isCollectOverrunEvents() {
		return collectOverrunEvents;
	}

	public static boolean isCountPreemptionNumber() {
		return countPreemptionNumber;
	}

	public static boolean isCountMissDeadlineNumber() {
		return countMissDeadlineNumber;
	}

	public static boolean isCountOverrunNumber() {
		return countOverrunNumber;
	}

	public static boolean isCountTotalFiredEvents() {
		return countTotalFiredEvents;
	}

	public static boolean isCountFiredEventsByType() {
		return countFiredEventsByType;
	}

	public static boolean isCountTotalCanceledEvents() {
		return countTotalCanceledEvents;
	}

	public static boolean isCollectRealSimulationTime() {
		return collectRealSimulationTime;
	}

	public static String getResutlsPath(){
		return  workingPath + resultDir;
	}
	
	public static String getDatasetPath(){
		return  workingPath + datasetDir;
	}
	
	
}
