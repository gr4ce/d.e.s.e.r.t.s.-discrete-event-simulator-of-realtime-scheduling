package edu.unibo.ciri.desert.config;

public final class StaticConfig {
	
	public static final boolean DEBUG 							= false;
	public static final boolean LOGGING 						= false;
	public static final boolean PRINT_SIMULATION_INFO 			= false;
	public static final boolean PRINT_SIMULATOR_INFO 			= false;
	
	/*
	 * Dataset manager
	 */
	public static final boolean PRINT_DATASET_INFO 				= false;
	
	/*
	 * Simulation info
	 */
	public static final boolean PRINT_SIMULATION_SUITE_INFO		= false;
	public static final boolean SIMULATOR_REUSE_RESOURCES 		= true;
	
	/*
	 * Event Management info 
	 */
	public static final boolean PRINT_FIRING_EVENT 				= false;
	public static final boolean PRINT_ISTANTANEUS_FIRING_EVENT 	= false;
	public static final boolean PRINT_TRIGGERING_EVENT			= false;
	public static final boolean PRINT_SUPPRESSING_EVENT			= false;
	public static final boolean PRINT_LISTENER_FIRING_EVENT		= false;
	public static final boolean PRINT_LISTENER_INTERESTED_TO_EVENTS = true;
	
	public static final boolean PRINT_EVENT_FACTORY_INFO 		= false;
	public static final boolean PRINT_ZERO_LAXITY_EVENT 		= false;
	
	public static final boolean PRINT_NEW_ALLOCATION_JOB_INFO	= false;
	public static final boolean PRINT_NEW_ALLOCATION_EVENT_INFO = false;

	
	public static final boolean PRINT_JOBFACTORY_NEW_ALLOCATION = false;
	
	/*
	 * Scheduler info
	 */
	public static final boolean PRINT_SCHEDULER_HANDLING		= false;
	public static final boolean PRINT_SCHEDULING_ACTION			= false;

	/*
	 * Factory management 
	 */
	public static final boolean JOB_FACTORY_REUSE_RESOURCES		= true;
	public static final boolean USE_LISTENER_IN_EVENTS			= false;
	public static final boolean USE_LISTENER_BY_EVENT_TYPE		= true;
	
	/*
	 * EventManager management 
	 */
	public static final boolean SUPPRESS_BY_REMOVING			= true;
}
