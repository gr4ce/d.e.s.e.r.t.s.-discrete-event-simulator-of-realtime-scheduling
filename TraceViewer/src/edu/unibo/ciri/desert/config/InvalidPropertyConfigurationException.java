package edu.unibo.ciri.desert.config;

public class InvalidPropertyConfigurationException extends Exception {
		
	private static final long serialVersionUID = 3389576996007729450L;

	public InvalidPropertyConfigurationException(String s){
		super(s);
	}
}
