package edu.unibo.ciri.model.task.job;


import edu.unibo.ciri.collection.Queue;
import edu.unibo.ciri.desert.config.StaticConfig;
import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.event.EventType;
import edu.unibo.ciri.desert.event.management.EventListener;
import edu.unibo.ciri.realtime.model.Job;
import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.model.Job.JobState;
import edu.unibo.ciri.realtime.policies.scheduling.base.SchedulingPolicy;


public class JobFactory implements EventListener{
	
	private final  int maxJobListSize = 5; 
	
	private SchedulingPolicy policy;
	private int staticSize;
	private int oldSize = 0;
	private Task[] tasks;
	private int[]  instanceNumber;	
	
	private int current_id = 0;
	
	private Queue<Job> freeJobs = new Queue<Job>();
	
	public JobFactory(SchedulingPolicy sp, TaskSet ts){
		
		setSchedulingPolicy(sp);
		
		staticSize = ts.size();		
		
		tasks = new Task[staticSize];
		instanceNumber = new int[staticSize];	
		
		
		for(int j = 0; j < ts.size(); j++){			
			tasks[j] = ts.getTask(j);
			tasks[j].setTaskId(j);
			instanceNumber[j] = 0;			
		}
		
		
		
		for(int i= 0; i < maxJobListSize; i++){
			Job j = new Job();
			j.setUniqueId(current_id++);
			j.setCompareRule(policy.getPriorityComparator());
			freeJobs.offer(j);
			if(StaticConfig.PRINT_NEW_ALLOCATION_JOB_INFO)
				System.out.println("\t\t\t\t[JOBFACTORY][NEW ALLOCATION] - Job " + j.getUniqueId());
		}
		
		
	}
	
	
	public Job releaseNewJob(Task t){
		
		Job newJob;
		
		if(freeJobs.size() > 0){
			
			newJob = freeJobs.poll();
			newJob.setTask(t);
						
		}else{								
			
			newJob = new Job(current_id++,t);
			newJob.setCompareRule(policy.getPriorityComparator());
			if(StaticConfig.PRINT_NEW_ALLOCATION_JOB_INFO)
				System.out.println("\t\t\t\t[JOBFACTORY][NEW ALLOCATION] - Job " + newJob);
		}
		
		assert(!newJob.hasMissed());
		assert(!newJob.hasOverrun());
		
		assert(newJob.getStartExecutionTime() 	  == -1);
		assert(newJob.getEndExecutionTime() 	  == -1);
		assert(newJob.getRemainingExecutionTime() == -1);
		
		newJob.setProgInstanceNumber(instanceNumber[t.getTaskId()]++);
		newJob.setRemainingExecutionTime(t.getTaskParameters().getComputation().getTimeValue());		
		newJob.setState(JobState.INITIALIZED);
		
		return newJob;
	}
	
	
	public void setSchedulingPolicy(SchedulingPolicy sp){
		this.policy = sp;
	}
	
	public void setStaticSize(int size){
		this.staticSize = size;
	}
	
	public void clear(){
				
		if(oldSize != staticSize){
			tasks = new Task[staticSize];
			instanceNumber = new int[staticSize];
		}
				
	}

	@Override
	public void handle(Event event) {

		EventType type = event.getEventType();
		
		switch(type){
			
			case JOB_COMPLETION:
				handleJobCompletion(event);
				break;
		}
		
	}
	
	private void handleJobCompletion(Event e){
		
		Job j = e.getJob();
		j.clear();
		freeJobs.offer(j);
		
	}

	@Override
	public String getName() {
		
		return "JobFactory";
	}

}
