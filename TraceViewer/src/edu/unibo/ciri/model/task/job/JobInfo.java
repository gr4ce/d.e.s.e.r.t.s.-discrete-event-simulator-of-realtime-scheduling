package edu.unibo.ciri.model.task.job;

import java.util.ArrayList;

import edu.unibo.ciri.desert.simulation.management.TimeInconsistencyException;

public class JobInfo {
	
	public enum JobInfoState{CREATED, FILLING, STORED};

	private long activationTime 		= -1;
	private long finishingTime  		= -1;
	private long startTime				= -1;
	private long currentPreemptionTime 	= -1;
	private long currentResumeTime 		= -1;
	
	private long remainingExecution		= -1;
	private long computationTime		= -1;
	private long responseTime			= -1;
	
	private long preemptionTime[]; 
	private long resumeTime[];
	private int  processor[];	
	
	private JobInfoState state;	
	
	private ArrayList<Long> preemptionBuff;
	private ArrayList<Long> resumeBuff;
	private ArrayList<Integer> processors;
	
	@Deprecated
	private static int numberOfExceptionCaught = 0;
	
	public JobInfo() {		
		preemptionBuff = new ArrayList<Long>();
		resumeBuff     = new ArrayList<Long>();
		processors     = new ArrayList<Integer>();
		state = JobInfoState.CREATED;		
	}
	

	public long getRemainingExecution() {
		return remainingExecution;
	}

	public void setRemainingExecution(long remainingExecution) {
		this.remainingExecution = remainingExecution;
		this.computationTime = remainingExecution;
	}
	
	public void addPreemptionTime(long value) throws TimeInconsistencyException{
		
		if(remainingExecution == -1 ){
			assert(false);
			throw new TimeInconsistencyException("Remaining execution time not initialized");
		}
		if(startTime == -1 ){
			assert(false);
			throw new TimeInconsistencyException("Remaining execution time not initialized");
		}
					
		if(currentPreemptionTime == -1){
			if(value < startTime){
				assert(false);
				throw new TimeInconsistencyException();
			}
			remainingExecution -= (value - startTime);
			assert(remainingExecution >= 0);
			
		}
		else{
			assert(currentResumeTime != -1);
			if(value < currentResumeTime){
				assert(false);
				throw new TimeInconsistencyException();
			}
			remainingExecution -= (value - currentResumeTime);
			assert(remainingExecution >= 0);
		}
		if(remainingExecution < 0){
			
			throw new TimeInconsistencyException();
		}
		currentPreemptionTime = value;		
		preemptionBuff.add(new Long(value));
	}
	
	public void addResumeTime(long value, int processor) throws TimeInconsistencyException{
		
		currentResumeTime = value;
		if((currentPreemptionTime == currentResumeTime)){
			numberOfExceptionCaught++;
			if(numberOfExceptionCaught < 10)
				throw new TimeInconsistencyException("DEBUG: "+currentPreemptionTime+ "  "+currentResumeTime);				
			else
				assert(false);
			
		}
		assert(currentPreemptionTime < currentResumeTime);
		resumeBuff.add(new Long(value));
		this.processors.add(new Integer(processor));
	}

	public long getActivationTime() {
		return activationTime;
	}

	public void setActivationTime(long activationTime) {
		state = JobInfoState.FILLING;
		assert(currentPreemptionTime == -1);
		assert(currentResumeTime == -1);
		this.activationTime = activationTime;
	}

	public long getFinishingTime() {		
		return finishingTime;
	}

	public void setFinishingTime(long finishingTime) {
		if(currentPreemptionTime == -1){
			remainingExecution -= finishingTime - startTime;
			assert(finishingTime > startTime);
		}
		else{
			remainingExecution -= finishingTime - currentResumeTime;
			assert(finishingTime > currentResumeTime );
		}
		
		if(remainingExecution != 0){
//			assert(false);
		}
		this.finishingTime = finishingTime;
		
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime, int processor) {
		this.processors.add(new Integer(processor));
		this.startTime = startTime;
		assert(startTime >= activationTime);
	}

	public long[] getPreemptionTime() {
		return preemptionTime;
	}

	public void setPreemptionTime(long[] preemptionTime) {
		this.preemptionTime = preemptionTime;
	}

	public long[] getResumeTime() {
		return resumeTime;
	}
	
	public int[] getProcessors(){
		return processor;
	}

	public void setResumeTime(long[] resumeTime, int processor) {
		this.resumeTime = resumeTime;		
	}
	
	public long storeJobInfo(){
		
		int dataSize = preemptionBuff.size(); 
		preemptionTime = new long[dataSize];
		for(int i = 0; i < dataSize; i ++){
			preemptionTime[i] = preemptionBuff.get(i).longValue();
		}	
		
		dataSize = resumeBuff.size(); 
		resumeTime = new long[dataSize];
		for(int i = 0; i < dataSize; i ++){
			resumeTime[i] = resumeBuff.get(i).longValue();
		}	
		
		responseTime = finishingTime - activationTime;
		
		processor = new int[processors.size()];
		for(int i = 0; i < processors.size(); i++){
			processor[i] = processors.get(i).intValue();
			assert(processor[i] >=0);
		}
				
		state = JobInfoState.STORED;
//		reset();
		
		return responseTime;
	}
	
	
	
	public long getResponseTime() {
		return responseTime;
	}

	

	public JobInfoState getJobInfoState(){
		return state;
	}
	
	public String toString(){
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("STATE     : " + state.toString());
		sb.append("\n");
		sb.append("ACTIVATION: " + getActivationTime());
		sb.append("\n");
		sb.append("START     : " + getStartTime());
		sb.append("\n");
		
		for(int i = 0; i < preemptionTime.length; i++){
			sb.append(" PREEMPT  : " + preemptionTime[i]);
			sb.append("\n");
			sb.append(" RESUME   : " + resumeTime[i]);
			sb.append("\n");
		}
		
		sb.append("FINISH    : " + getFinishingTime());
		
		
		return sb.toString();
	}
	
	public void clear(){
		
		activationTime 			= -1;
		finishingTime  			= -1;
		startTime				= -1;
		currentPreemptionTime 	= -1;
		currentResumeTime 		= -1;
		remainingExecution 		= -1;
		responseTime 			= -1;
		
		preemptionBuff.clear();
		resumeBuff.clear();
		processors.clear();
	}
	
	public void reset(){
		
		currentPreemptionTime = -1;
		currentResumeTime = -1;
		remainingExecution = -1;
		preemptionBuff.clear();
		resumeBuff.clear();
		processors.clear();
		preemptionTime = null;
		resumeTime = null;		
		state = JobInfoState.CREATED;	
	}
	
}
