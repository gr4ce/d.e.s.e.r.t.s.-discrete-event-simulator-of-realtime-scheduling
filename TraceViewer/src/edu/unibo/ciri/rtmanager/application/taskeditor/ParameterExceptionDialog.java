package edu.unibo.ciri.rtmanager.application.taskeditor;



import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



@SuppressWarnings("serial")
public class ParameterExceptionDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private ParameterExceptionDialog thisFrame;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		try {
//			ParameterExceptionDialog dialog = new ParameterExceptionDialog();
//			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//			dialog.setVisible(true);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * Create the dialog.
	 */
	public ParameterExceptionDialog(JFrame jf, String message) {
		
		super(jf);
		setBounds(100, 100, 283, 140);
		getContentPane().setLayout(null);
		contentPanel.setBounds(0, 0, 277, 113);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);
		thisFrame = this;
		{
			JButton okButton = new JButton("OK");
			okButton.setBounds(91, 76, 80, 25);
			okButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					thisFrame.setVisible(false);
				}
			});
			contentPanel.add(okButton);
			okButton.setActionCommand("OK");
			getRootPane().setDefaultButton(okButton);
		}
		{
			JLabel lblNewLabel = new JLabel(message);
			lblNewLabel.setBounds(12, 22, 285, 52);
			contentPanel.add(lblNewLabel);
			lblNewLabel.setIcon(new ImageIcon(ParameterExceptionDialog.class.getResource("/com/sun/java/swing/plaf/windows/icons/Error.gif")));
			lblNewLabel.setVerticalAlignment(SwingConstants.TOP);
		}
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

	
}
