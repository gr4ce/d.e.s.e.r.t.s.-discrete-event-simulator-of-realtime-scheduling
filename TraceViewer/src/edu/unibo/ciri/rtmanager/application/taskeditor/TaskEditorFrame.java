package edu.unibo.ciri.rtmanager.application.taskeditor;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableModel;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.text.ParseException;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;


import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskParameters;
import edu.unibo.ciri.rtmanager.application.tabletest.DeleteActionListener;
import edu.unibo.ciri.rtmanager.application.tabletest.EditActionListener;
import edu.unibo.ciri.rtmanager.application.tabletest.RTWindow;
import edu.unibo.ciri.rtmanager.application.tabletest.TasksetTableModel;
import edu.unibo.ciri.rtos.parser.TaskExecutionPattern;
import edu.unibo.ciri.rtos.rtai.RtaiCodeGenerator;

import javax.swing.JTextPane;
import javax.swing.JScrollPane;


@SuppressWarnings("serial")
public class TaskEditorFrame extends JFrame {

	
	public enum TableAction{ INSERT, UPDATE, DELETE};
	
	private JPanel contentPane;
	private final JPanel panel = new JPanel();
	private JTextField taskNameTextField;
	private TableAction tableAction;
	
	private JTextField offsetValue;
	private TimeUnitComboBox offsetCombo;
	
	private JTextField deadlineValue;
	private TimeUnitComboBox deadlineCombo;
	
	private JTextField periodValue;
	private TimeUnitComboBox periodCombo;
	private JFrame thisJFrame;
	private JLabel taskNameLabel;
	
	private TaskModelComboBox comboBox;
	
	
	private JTextPane taskExecutionPane;
	private TasksetTableModel theModel;
	private Task theTask;
	
	JButton okButton;
	
	/**
	 * Create the frame.
	 * @wbp.parser.constructor
	 */
	public TaskEditorFrame(TasksetTableModel model) {
		
		theModel = model;	
		thisJFrame = this;
		
		setTitle("Task editor");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension windowSize = new Dimension(536, 258);
		double xCenterPosition = (screenSize.getWidth() - windowSize.getWidth())/ 2;
		double yCenterPosition = (screenSize.getHeight() - windowSize.getHeight()) / 2;
		thisJFrame.setBounds((int)xCenterPosition, (int) yCenterPosition, (int)windowSize.getWidth(), (int)windowSize.getHeight());
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		taskNameTextField = new JTextField();
		taskNameTextField.setBounds(79, 14, 152, 19);
		panel.add(taskNameTextField);
		taskNameTextField.setColumns(10);
		
		taskNameLabel = new JLabel("Name");
		taskNameLabel.setDisplayedMnemonic('N');
		taskNameLabel.setBounds(12, 16, 49, 15);
		panel.add(taskNameLabel);
		
		JLabel lblTaskModel = new JLabel("Model");
		lblTaskModel.setBounds(12, 47, 91, 15);
		panel.add(lblTaskModel);
		
		comboBox = new TaskModelComboBox();
		comboBox.setBounds(79, 45, 152, 19);
		panel.add(comboBox);
		
		offsetCombo = new TimeUnitComboBox();
		offsetCombo.setSelectedIndex(1);
		offsetCombo.setBounds(169, 76, 63, 19);
		panel.add(offsetCombo);
		
		offsetValue = new JTextField();
		offsetValue.setHorizontalAlignment(SwingConstants.CENTER);
		offsetValue.setText("0");
		offsetValue.setColumns(10);
		offsetValue.setBounds(79, 76, 82, 19);
		panel.add(offsetValue);
		
		JLabel offsetLabel = new JLabel("Offset");
		offsetLabel.setBounds(12, 78, 91, 15);
		panel.add(offsetLabel);
		
		deadlineCombo = new TimeUnitComboBox();
		deadlineCombo.setSelectedIndex(1);
		deadlineCombo.setBounds(169, 107, 63, 19);
		panel.add(deadlineCombo);
		
		deadlineValue = new JTextField();
		deadlineValue.setHorizontalAlignment(SwingConstants.CENTER);
		deadlineValue.setColumns(10);
		deadlineValue.setBounds(79, 107, 82, 19);
		panel.add(deadlineValue);
		
		JLabel deadlineLabel = new JLabel("Deadline");
		deadlineLabel.setBounds(11, 109, 91, 15);
		panel.add(deadlineLabel);
		
		periodCombo = new TimeUnitComboBox();
		periodCombo.setSelectedIndex(1);
		periodCombo.setBounds(167, 138, 65, 19);
		panel.add(periodCombo);
		
		periodValue = new JTextField();
		periodValue.setHorizontalAlignment(SwingConstants.CENTER);
		periodValue.setColumns(10);
		periodValue.setBounds(80, 138, 81, 19);
		panel.add(periodValue);
		
		JLabel periodLabel = new JLabel("Period");
		periodLabel.setBounds(12, 140, 91, 15);
		panel.add(periodLabel);
		
		JLabel lblTaskExecutionPattern = new JLabel("Task execution pattern");
		lblTaskExecutionPattern.setDisplayedMnemonic('N');
		lblTaskExecutionPattern.setBounds(243, 16, 174, 15);
		panel.add(lblTaskExecutionPattern);
		
		
		Task task = new Task("dummy");
		theTask = task;	
		
		TaskParameters tp = new TaskParameters();
		task.setTaskParameters(tp);
		
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				thisJFrame.setVisible(false);
			}
		});	
		cancelButton.setBounds(401, 184, 107, 25);
		panel.add(cancelButton);
		
		okButton = new JButton("OK");
		
		class DoTableAction implements ActionListener{
			
			private Task        targetTask;
			
			public DoTableAction(Task t) {
				targetTask = t;
			}
			
			@Override
			public void actionPerformed(ActionEvent e) {
				parseFormAndPerformAction(targetTask);
				thisJFrame.setVisible(false);
			}
			
		}
		
		ActionListener doAction = new DoTableAction(task);
		okButton.addActionListener(doAction);		
		
		okButton.setBounds(282, 184, 107, 25);
		panel.add(okButton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(243, 40, 265, 128);
		panel.add(scrollPane);
		
		taskExecutionPane = new JTextPane();
		scrollPane.setViewportView(taskExecutionPane);
	}
	
	/**
	 * Create the frame.
	 */
	public TaskEditorFrame(Task task, TasksetTableModel model) {
		
		theModel = model;			
		theTask  = task;
		TaskParameters tp = task.getTaskParameters();
		thisJFrame = this;
		
		setTitle("Task editor");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension windowSize = new Dimension(536, 258);
		double xCenterPosition = (screenSize.getWidth() - windowSize.getWidth())/ 2;
		double yCenterPosition = (screenSize.getHeight() - windowSize.getHeight()) / 2;
		thisJFrame.setBounds((int)xCenterPosition, (int) yCenterPosition, (int)windowSize.getWidth(), (int)windowSize.getHeight());
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		taskNameTextField = new JTextField(task.getName());
		taskNameTextField.setBounds(79, 14, 152, 19);
		panel.add(taskNameTextField);
		taskNameTextField.setColumns(10);
		
		taskNameLabel = new JLabel("Name");
		taskNameLabel.setDisplayedMnemonic('N');
		taskNameLabel.setBounds(12, 16, 49, 15);
		panel.add(taskNameLabel);
		
		JLabel lblTaskModel = new JLabel("Model");
		lblTaskModel.setBounds(12, 47, 91, 15);
		panel.add(lblTaskModel);
		
		comboBox = new TaskModelComboBox();
		comboBox.setBounds(79, 45, 152, 19);
		panel.add(comboBox);
		
		
		/*
		 * Offset stuffs
		 */
		offsetCombo = new TimeUnitComboBox();
		offsetCombo.setSelectedIndex(getIndexFromCombo(tp.getOffset().getPreferredTimeUnit()));
		offsetCombo.setBounds(169, 76, 63, 19);
		panel.add(offsetCombo);
		
		offsetValue = new JTextField();
		offsetValue.setHorizontalAlignment(SwingConstants.CENTER);
		offsetValue.setText(""+tp.getOffset().getValueInPreferredTimeUnitRepresentation());
		offsetValue.setColumns(10);
		offsetValue.setBounds(79, 76, 82, 19);
		panel.add(offsetValue);
		
		JLabel offsetLabel = new JLabel("Offset");
		offsetLabel.setBounds(12, 78, 91, 15);
		panel.add(offsetLabel);
		
		
		/*
		 * Deadline stuffs
		 */
		deadlineCombo = new TimeUnitComboBox();
		deadlineCombo.setSelectedIndex(getIndexFromCombo(tp.getDeadline().getPreferredTimeUnit()));
		deadlineCombo.setBounds(169, 107, 63, 19);
		panel.add(deadlineCombo);
		
		deadlineValue = new JTextField();
		deadlineValue.setHorizontalAlignment(SwingConstants.CENTER);
		deadlineValue.setColumns(10);
		deadlineValue.setText(""+tp.getDeadline().getValueInPreferredTimeUnitRepresentation());
		deadlineValue.setBounds(79, 107, 82, 19);
		panel.add(deadlineValue);
		
		JLabel deadlineLabel = new JLabel("Deadline");
		deadlineLabel.setBounds(11, 109, 91, 15);
		panel.add(deadlineLabel);
		
		
		/*
		 * Period stuffs
		 */
		periodCombo = new TimeUnitComboBox();
		periodCombo.setSelectedIndex(getIndexFromCombo(tp.getPeriod().getPreferredTimeUnit()));
		periodCombo.setBounds(167, 138, 65, 19);
		panel.add(periodCombo);
		
		periodValue = new JTextField();
		periodValue.setHorizontalAlignment(SwingConstants.CENTER);
		periodValue.setColumns(10);
		periodValue.setText(""+tp.getPeriod().getValueInPreferredTimeUnitRepresentation());
		periodValue.setBounds(80, 138, 81, 19);
		panel.add(periodValue);
		
		JLabel periodLabel = new JLabel("Period");
		periodLabel.setBounds(12, 140, 91, 15);
		panel.add(periodLabel);
		
		JLabel lblTaskExecutionPattern = new JLabel("Task execution pattern");
		lblTaskExecutionPattern.setDisplayedMnemonic('N');
		lblTaskExecutionPattern.setBounds(243, 16, 174, 15);
		panel.add(lblTaskExecutionPattern);
		
		
		
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				thisJFrame.setVisible(false);
			}
		});	
		cancelButton.setBounds(401, 184, 107, 25);
		panel.add(cancelButton);
		
		okButton = new JButton("OK");
		
		class DoTableAction implements ActionListener{
			
			private Task        targetTask;
			
			public DoTableAction(Task t) {
				targetTask = t;
			}
			
			@Override
			public void actionPerformed(ActionEvent e) {
				parseFormAndPerformAction(targetTask);
				thisJFrame.setVisible(false);
			}
			
		}
		ActionListener doAction = new DoTableAction(task);
		okButton.addActionListener(doAction);		
		
		okButton.setBounds(282, 184, 107, 25);
		panel.add(okButton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(243, 40, 265, 128);
		panel.add(scrollPane);
		
		taskExecutionPane = new JTextPane();
		taskExecutionPane.setText(task.getTaskPseudoCode());
		scrollPane.setViewportView(taskExecutionPane);
	}
	
	
	private void parseFormAndPerformAction(Task theTask){
		
		try{
						
			if(taskNameTextField.getText() == null || taskNameTextField.getText().equals(""))
				throw new ParseException("Please, provide a non empty name for task", 0);
			
			if(taskNameTextField.getText().contains(" "))
				throw new ParseException("Please, provide a task name without spaces", 0);		
			
			
//			String taskNameToUpdate = theTask.getName();
			theTask.setName(taskNameTextField.getText());
						
			TaskParameters tp = theTask.getTaskParameters();
			
			if(periodValue.getText().equals(""))
				throw new ParseException("Please, insert a valid period value", 0);
			try{
				tp.setPeriod(Long.parseLong(periodValue.getText()),getTimeUnitFromCombo(periodCombo));
			}catch(NumberFormatException nfe){
				throw new ParseException("Please, provide a valid value for period: " + nfe.getMessage(),0);	
			}			
			
			tp.setDeadline(Long.parseLong(deadlineValue.getText()), getTimeUnitFromCombo(deadlineCombo) );
			tp.setOffset(Long.parseLong(offsetValue.getText()),   getTimeUnitFromCombo(offsetCombo) );
			
			tp.setTaskModel(comboBox.getSelectedItem().toString());
			
			String taskPseudoCode = taskExecutionPane.getText();
			if(!taskPseudoCode.endsWith("\n"))
				taskPseudoCode += "\n";
			theTask.setTaskPlatformSpecCode(RtaiCodeGenerator.compileExecutionPattern(taskPseudoCode));
			theTask.setTaskPseudoCode(taskExecutionPane.getText());
			
			if(tableAction == TableAction.UPDATE){
				theModel.updateTask(theTask);
				RTWindow.outputPanelWriteLnInfo("Task " +  theTask.getName() + " has been modified");
			}
			
			if(tableAction == TableAction.INSERT){
				EditActionListener eal = new EditActionListener(theTask, theModel, this);
				DeleteActionListener dal = new DeleteActionListener(theTask, theModel);				
				theModel.addTask(theTask,eal,dal);
				RTWindow.outputPanelWriteLnInfo("Task " +  theTask.getName() + " successfully added to taskset");
			}
			
			System.out.println(theTask.getTaskPlatformSpecCode());
						
			
//			thisJFrame.setVisible(false);
			
		}catch(ParseException pe){
			JOptionPane.showMessageDialog(thisJFrame, pe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);			
		}catch (edu.unibo.ciri.rtos.parser.ParseException e) {
			JOptionPane.showMessageDialog(thisJFrame, "Error parsing task pseudo code!", "Task parsing error", JOptionPane.ERROR_MESSAGE);			
		}
		
	}
	
	private TimeUnit getTimeUnitFromCombo(TimeUnitComboBox cmb){
		
		switch(cmb.getSelectedIndex()){
			case 0:
				return TimeUnit.SECONDS;
			case 1:
				return TimeUnit.MILLISECONDS;
			case 2:
				return TimeUnit.MICROSECONDS;
			case 3:
				return TimeUnit.NANOSECONDS;
			default:
				return TimeUnit.NANOSECONDS;
		}
		
	}
	
	private int getIndexFromCombo(TimeUnit tu){
		
		switch(tu){
			case SECONDS:
				return 0;
			case MILLISECONDS:
				return 1;
			case MICROSECONDS:
				return 2;
			case NANOSECONDS:
				return 2;
			default:
				return 2;
		}
		
	}
	
	
	
	

	public void doUpdate() {
		tableAction = TableAction.UPDATE;	
	}
	
	public void doInsert(){
		tableAction = TableAction.INSERT;		
	}
}
