package edu.unibo.ciri.rtmanager.application.taskeditor;

import javax.swing.JComboBox;

import edu.unibo.ciri.realtime.model.TaskParameters.TaskModelEnum;

@SuppressWarnings("serial")
public class TaskModelComboBox extends JComboBox{
		
	public TaskModelComboBox() {
		super(TaskModelEnum.values());
	}

}
