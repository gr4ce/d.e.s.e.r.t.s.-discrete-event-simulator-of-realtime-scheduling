package edu.unibo.ciri.rtmanager.application.taskeditor;

import javax.swing.JComboBox;

@SuppressWarnings("serial")
public class TimeUnitComboBox extends JComboBox{
	
	public TimeUnitComboBox() {		
		super(new String[]{"s", "ms", "us", "ns"});
	}

}
