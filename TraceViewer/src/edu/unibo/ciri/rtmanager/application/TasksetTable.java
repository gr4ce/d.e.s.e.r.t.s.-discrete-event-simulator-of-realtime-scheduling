package edu.unibo.ciri.rtmanager.application;

import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import edu.unibo.ciri.realtime.model.Task;

@SuppressWarnings("serial")
public class TasksetTable extends JTable{
	
	private Vector<Task> tasks;
		
	public TasksetTable(Vector<Vector<Object>> tasks, Vector<String> header){
		super(tasks, header);		
//		adjustSize();
	}
	
	public TasksetTable(AbstractTableModel model){
		super(model);
//		adjustSize();
	}
	
	public static Vector<Vector<Object>> getVectorized(Vector<Task> tasks){
		
		Vector<Vector<Object>> tasksInfo = new Vector<Vector<Object>>();
		
		for(Task t: tasks){
			Vector<Object> tasksParameters = new Vector<Object>();
			tasksParameters.add(t.getName());
			tasksParameters.add("Periodic");
			tasksParameters.add(t.getTaskParameters().getOffset());
			tasksParameters.add(t.getTaskParameters().getPeriod());
			tasksParameters.add(t.getTaskParameters().getDeadline());
			tasksParameters.add(t.getTaskParameters().getComputation());
			tasksInfo.add(tasksParameters);
		}
		
		return tasksInfo;		
	}
	
	
	
	private void adjustSize(){
		this.getColumnModel().getColumn(0).setPreferredWidth(100);
		this.getColumnModel().getColumn(0).setMinWidth(100);
		this.getColumnModel().getColumn(0).setMaxWidth(100);
		this.getColumnModel().getColumn(1).setPreferredWidth(100);
		this.getColumnModel().getColumn(1).setMinWidth(100);
		this.getColumnModel().getColumn(1).setMaxWidth(100);
		this.getColumnModel().getColumn(2).setPreferredWidth(100);
		this.getColumnModel().getColumn(2).setMinWidth(100);
		this.getColumnModel().getColumn(2).setMaxWidth(100);
		this.getColumnModel().getColumn(3).setPreferredWidth(100);
		this.getColumnModel().getColumn(3).setMinWidth(100);
		this.getColumnModel().getColumn(3).setMaxWidth(100);
		this.getColumnModel().getColumn(4).setPreferredWidth(100);
		this.getColumnModel().getColumn(4).setMinWidth(100);
		this.getColumnModel().getColumn(4).setMaxWidth(100);
		this.getColumnModel().getColumn(5).setMinWidth(75);
		this.getColumnModel().getColumn(5).setMaxWidth(75);
		this.getColumnModel().getColumn(6).setMinWidth(75);
		this.getColumnModel().getColumn(6).setMaxWidth(75);
	}
}
