package edu.unibo.ciri.rtmanager.application;

import java.util.Enumeration;
import java.util.Vector;

import javax.swing.ListSelectionModel;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;


public class TasksetTableColumnModel implements TableColumnModel{
	
	private Vector<TableColumn> columns = new Vector<TableColumn>();
	private Vector<TableColumnModelListener> columnModelListeners = new Vector<TableColumnModelListener>();
	private boolean columnSelectionAllowed = true;
	private ListSelectionModel listSelectionModel = null;
	
	public TasksetTableColumnModel(){
		
	}

	@Override
	public void addColumn(TableColumn aColumn) {
		columns.add(aColumn);		
	}

	@Override
	public void addColumnModelListener(TableColumnModelListener x) {
		columnModelListeners.add(x);		
	}

	@Override
	public TableColumn getColumn(int index) {
		return columns.get(index);
	}

	@Override
	public int getColumnCount() {
		return columns.size();
	}

	@Override
	public int getColumnIndex(Object columnIdentifier) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getColumnIndexAtX(int xPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getColumnMargin() {
		return 0;
	}

	@Override
	public boolean getColumnSelectionAllowed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Enumeration<TableColumn> getColumns() {
		return new Enumeration<TableColumn>() {
			
			private int position = 0;
			
			@Override
			public boolean hasMoreElements() {
				if(position < columns.size())
					return true;
				return false;
			}

			@Override
			public TableColumn nextElement() {
				return columns.get(position++);
			}
		};
	}

	@Override
	public int getSelectedColumnCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int[] getSelectedColumns() {		
		return null;
	}

	@Override
	public ListSelectionModel getSelectionModel() {
		return listSelectionModel;
	}

	@Override
	public int getTotalColumnWidth() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void moveColumn(int columnIndex, int newIndex) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeColumn(TableColumn column) {
		columns.remove(column);
		
	}

	@Override
	public void removeColumnModelListener(TableColumnModelListener x) {
		columnModelListeners.remove(x);
		
	}

	@Override
	public void setColumnMargin(int newMargin) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setColumnSelectionAllowed(boolean flag) {
		columnSelectionAllowed = flag;
	}

	@Override
	public void setSelectionModel(ListSelectionModel newModel) {
		listSelectionModel = newModel;		
	}

}
