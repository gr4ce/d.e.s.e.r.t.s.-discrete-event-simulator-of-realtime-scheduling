package edu.unibo.ciri.rtmanager.application.tabletest;

import java.awt.Component;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;

public class JButtonCellRender implements TableCellRenderer{
	
	private String name;
	
	public JButtonCellRender(String buttonName){
		name = buttonName;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		JButton button = (JButton)value;
	    if (isSelected) {
	      button.setForeground(table.getSelectionForeground());
	      button.setBackground(table.getSelectionBackground());
	    } else {
	      button.setForeground(table.getForeground());
	      button.setBackground(UIManager.getColor("Button.background"));
	    }
	    return button;
	}

}
