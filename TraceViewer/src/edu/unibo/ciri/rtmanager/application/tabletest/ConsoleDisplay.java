package edu.unibo.ciri.rtmanager.application.tabletest;

import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.CharArrayReader;
import java.io.CharArrayWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.nio.CharBuffer;

import javax.swing.JTextArea;
import javax.swing.JTextPane;

public class ConsoleDisplay implements Runnable{
	
	protected JTextArea theTextArea;
	protected JTextPane theTextPane;
	protected BufferedReader inReader = null;
	protected Reader errReader = null;
	private StringBuilder buf = new StringBuilder();
	private Process theShell = null;
	private PrintWriter shellCommandWriter = null;
	private String workDir = null;
	

	
	public ConsoleDisplay(JTextPane textPane){
		theTextPane = textPane;
	}
	
	public void initShell(String workingDir){
				
		if(theShell != null){
			if(workingDir != null)
				shellCommandWriter.println("cd " + workingDir);
			return;
		}
				
		try {	
			
			File wd;
			if(workingDir == null || !(new File(workingDir).exists())){
				Process p = Runtime.getRuntime().exec("pwd");				
				BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
				String pwdRes = br.readLine();
				System.out.println("PWD = " + pwdRes);
				wd = new File(pwdRes);
				p.destroy();
			}
			else{
				wd = new File(workingDir);
			}
			
			ProcessBuilder pb = new ProcessBuilder("/bin/bash");
			pb.redirectErrorStream(true);
			pb.directory(wd);
			theShell = pb.start();

			inReader = new BufferedReader(new InputStreamReader(theShell.getInputStream()));
		    shellCommandWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(theShell.getOutputStream())), true);
		    
		    new Thread(this).start();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

		
	public void execute(String command){
		
		shellCommandWriter.println(command);
		shellCommandWriter.flush();	
		
	}

	private void setText(final String text) {
	    EventQueue.invokeLater(new Runnable() {
	        public void run() {
	        	theTextPane.setText(text);
	        }
	    });
	}
	
	public void write(String text){
		buf.append(text);
		setText( buf.toString() );
	}

	@Override
	public void run() {	   
		String line;
	    try {
	        while( (line = inReader.readLine()) != null ) {
	            
	            buf.append(line + "\n");
	            setText( buf.toString() );
	        }
	    } catch ( IOException ioe ) {
	        buf.append("\n\nERROR:\n"+ioe.toString());
	        setText( buf.toString() );
	    } finally {
	        try {
	            inReader.close();
	        } catch ( IOException ioe ) {
	            ioe.printStackTrace();
	        }
	    }
	}
	
	public void clear(){
		buf = new StringBuilder();
		setText( buf.toString() );
	}

}
