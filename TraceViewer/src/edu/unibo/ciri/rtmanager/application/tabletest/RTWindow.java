package edu.unibo.ciri.rtmanager.application.tabletest;


import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;


import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;


import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.TimeUnit;

import javax.swing.JTable;

import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.model.io.TasksetReader;
import edu.unibo.ciri.realtime.model.io.TasksetWriter;
import edu.unibo.ciri.realtime.model.time.Time;
import edu.unibo.ciri.rtmanager.application.taskeditor.TaskEditorFrame;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;

import edu.unibo.ciri.rtos.ChoseRTOSActionListener;
import edu.unibo.ciri.rtos.RealTimeOperatinSystemEnum;
import edu.unibo.ciri.rtos.rtai.RtaiCodeGenerator;
import edu.unibo.ciri.rtos.rtai.RtaiGenerateAction;
import javax.swing.JTextPane;



import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import edu.unibo.ciri.rtmanager.application.taskeditor.TimeUnitComboBox;
import javax.swing.ButtonGroup;
import edu.unibo.ciri.desert.config.Configuration.ScheulingPolicyName;
import javax.swing.JToolBar;

public class RTWindow {

	public static JFrame frame;
	private JTable tasksetTable;
	private Object editorValue;
	private TaskEditorFrame currentFrame = null;
	private TasksetTableModel theModel;
//	private TaskSet taskSet = null;
	public static JTextPane outputTextPanel;
	
	private JLabel lblTotalTasksetUtilization;
	
	private static ConsoleDisplay theConsole;
	
	private RtaiCodeGenerator generator; 
	
	JButton editButton = new JButton("edit");
	JButton addButton  = new JButton("add");
	private JTextField txtTaskset;
	private JTextField textField;
	private JTextField textField_1;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	
	JCheckBox chckbxGenerate;
	JCheckBox chckbxCompile;
	JCheckBox chckbxRun;
	private final ButtonGroup applicationRunConfiguration = new ButtonGroup();
	private final ButtonGroup schedulingPolicy = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
//		try {
//            // Set System L&F
//        UIManager.setLookAndFeel(
//        		com.sun.java.swing.plaf.gtk.);
//	    } 
//	    catch (UnsupportedLookAndFeelException e) {
//	       e.printStackTrace();
//	    }
//	    catch (ClassNotFoundException e) {
//	    	e.printStackTrace();
//	    }
//	    catch (InstantiationException e) {
//	       e.printStackTrace();
//	    }
//	    catch (IllegalAccessException e) {
//	    	e.printStackTrace();
//	    }
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Time.setResolution(TimeUnit.NANOSECONDS);
					
					RTWindow window = new RTWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public RTWindow() {
		initialize();
	}
	
	private static void writeOutput(final String s){
//		outputTextPanel.setText(outputTextPanel.getText() + s); 	
		theConsole.write(s);
	}
	
	
	
	public static void outputPanelWriteLnInfo(String s){
		writeOutput("[INFO] - " + s + "\n");
	}
	
	public static ConsoleDisplay getOutputDisplay(){
		return theConsole;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame = new JFrame();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension windowSize = new Dimension(800, 600);
		double xCenterPosition = (screenSize.getWidth() - windowSize.getWidth())/ 2;
		double yCenterPosition = (screenSize.getHeight() - windowSize.getHeight()) / 2;
		frame.setBounds((int)xCenterPosition, (int) yCenterPosition, 1101, 602);
		frame.setLocationRelativeTo(null);
		frame.setTitle("RealTime Application Manager");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);		
		
		JPanel tasksetPanel = new JPanel();
		tasksetPanel.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Taskset editor", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
		tasksetPanel.setBounds(12, 12, 510, 254);
		frame.getContentPane().add(tasksetPanel);
		tasksetPanel.setLayout(null);
				
				
		/**
		 * What you surely need to create your custom JTable are three models
		 * 1) A TableModel
		 * 2) A TableColumnModel
		 * 3) A ListSelectionModel
		 * We create the frist two model leaving the third empty because we don't need
		 * a selection model
		 */
		theModel	= new TasksetTableModel();
//		taskSet = theModel.getTakset();
		TasksetTableColumnModel ttcm 	= new TasksetTableColumnModel();
		
		
		/**
		 * When you create a TableColumnModel you can (actually you have to) add your TableColumn
		 * instances... so, let's do so!
		 * But first we have to create TableCoulmns. You can create special purpose table column
		 * with their specific render and editor. 
		 */
		
		/*
		 * Column 0 needed for the selection
		 */
		JButtonCellRender jbcrEdit = new JButtonCellRender("edit");
		JButtonCellRender jbcrDelete = new JButtonCellRender("del");
		DefaultTableCellRenderer defaultRender;
		TableColumn tc;		
		
		tc = new TableColumn(0,120);
		tc.setHeaderValue("Name");
		defaultRender = new DefaultTableCellRenderer();
		defaultRender.setHorizontalAlignment(JLabel.CENTER);
		tc.setCellRenderer(defaultRender);
		ttcm.addColumn(tc);
		
		tc = new TableColumn(1,100);
		tc.setHeaderValue("Model");
		defaultRender = new DefaultTableCellRenderer();
		defaultRender.setHorizontalAlignment(JLabel.CENTER);
		tc.setCellRenderer(defaultRender);
		ttcm.addColumn(tc);
		
		tc = new TableColumn(2,100);
		tc.setHeaderValue("Offset");
		defaultRender = new DefaultTableCellRenderer();
		defaultRender.setHorizontalAlignment(JLabel.CENTER);
		tc.setCellRenderer(defaultRender);
		ttcm.addColumn(tc);
		
		tc = new TableColumn(3,100);
		tc.setHeaderValue("Deadline");
		defaultRender = new DefaultTableCellRenderer();
		defaultRender.setHorizontalAlignment(JLabel.CENTER);
		tc.setCellRenderer(defaultRender);
		ttcm.addColumn(tc);
		
		tc = new TableColumn(4,100);
		tc.setHeaderValue("Period");
		defaultRender = new DefaultTableCellRenderer();
		defaultRender.setHorizontalAlignment(JLabel.CENTER);
		tc.setCellRenderer(defaultRender);
		ttcm.addColumn(tc);
		
		tc = new TableColumn(5,100);
		tc.setHeaderValue("Execution");
		defaultRender = new DefaultTableCellRenderer();
		defaultRender.setHorizontalAlignment(JLabel.CENTER);
		tc.setCellRenderer(defaultRender);
		ttcm.addColumn(tc);
		
		tc = new TableColumn(6,120);
		tc.setCellRenderer(jbcrEdit);
		tc.setHeaderValue("Edit");
		ttcm.addColumn(tc);
		
		tc = new TableColumn(7,120);
		tc.setCellRenderer(jbcrDelete);
		tc.setHeaderValue("Delete");
		ttcm.addColumn(tc);
		
		tasksetTable = new JTable(theModel,ttcm);
		tasksetTable.setBounds(12, 30, 441, 160);
		tasksetTable.addMouseListener(new JTableButtonMouseListener(tasksetTable));
		
		JScrollPane scrollPane = new JScrollPane(tasksetTable);
		scrollPane.setBounds(12, 71, 486, 134);
		tasksetPanel.add(scrollPane);
		
		scrollPane.setViewportView(tasksetTable);
		tasksetTable.setFillsViewportHeight(true);
		tasksetPanel.add(scrollPane);
		
		JButton btnAddTask = new JButton("Add task");
		btnAddTask.setBounds(252, 217, 117, 25);
		btnAddTask.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				currentFrame = new TaskEditorFrame(theModel);
				currentFrame.doInsert();
				currentFrame.setVisible(true);
				
			}
		});
		tasksetPanel.add(btnAddTask);
		
		tasksetTable.getModel().addTableModelListener(new TableModelListener() {
			
			@Override
			public void tableChanged(TableModelEvent arg0) {
				
				String displayUtilization = "Total taskset utilization: ";							
				
				lblTotalTasksetUtilization.setText(displayUtilization + theModel.getTaskset().getUtilization());
				
			}
		});
		
		JButton btnLoadTaskset = new JButton("Load Taskset");
		
		btnLoadTaskset.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser jfc = new JFileChooser();
				
				int returnVal = jfc.showOpenDialog(RTWindow.frame);
			    if(returnVal == JFileChooser.APPROVE_OPTION) {
			    	theModel.setTaskset(TasksetReader.read(jfc.getSelectedFile()));
			    	TaskSet ts = theModel.getTaskset();
			    	theConsole.write("[INFO] - Successfully readed "+ ts.getTasksetName() + "\n");
			    	theConsole.write(ts.toString());
			    	txtTaskset.setText(ts.getTasksetName());
			    	generator.setTaskset(ts);
					
			    	
			    }
			}
		});
		btnLoadTaskset.setBounds(252, 34, 117, 25);
		tasksetPanel.add(btnLoadTaskset);
		
		JButton btnSaveTaskset = new JButton("Save Taskset");
		btnSaveTaskset.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser jfc = new JFileChooser();
				
				int returnVal = jfc.showSaveDialog(RTWindow.frame);
				if(returnVal == JFileChooser.APPROVE_OPTION){
					
					TasksetWriter tw = new TasksetWriter(jfc.getSelectedFile(), theModel.getTaskset());
				}
				
			}
		});
		btnSaveTaskset.setBounds(381, 34, 117, 25);
		tasksetPanel.add(btnSaveTaskset);
		
		txtTaskset = new JTextField();
		txtTaskset.setText("taskset");
		txtTaskset.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				theModel.getTaskset().setTasksetName(txtTaskset.getText());		
			}
			
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				theModel.getTaskset().setTasksetName(txtTaskset.getText());	
			}
			
			@Override
			public void changedUpdate(DocumentEvent arg0) {
				theModel.getTaskset().setTasksetName(txtTaskset.getText());					
			}			
		});
		
		txtTaskset.setBounds(12, 34, 228, 25);
		tasksetPanel.add(txtTaskset);
		txtTaskset.setColumns(10);
		
		lblTotalTasksetUtilization = new JLabel("Total taskset utilization: 0 ");
		lblTotalTasksetUtilization.setFont(new Font("Dialog", Font.BOLD, 14));
		lblTotalTasksetUtilization.setBounds(12, 222, 251, 20);
		tasksetPanel.add(lblTotalTasksetUtilization);
		
		JButton btnNewButton = new JButton("Clear table");
		btnNewButton.setBounds(381, 217, 117, 25);
		tasksetPanel.add(btnNewButton);
		
		
		JPanel outputPanel = new JPanel();
		outputPanel.setBorder(new TitledBorder(null, "Output", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		outputPanel.setBounds(12, 278, 1075, 283);
		frame.getContentPane().add(outputPanel);
		outputPanel.setLayout(null);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(12, 22, 1051, 249);
		outputPanel.add(scrollPane_1);
		
		outputTextPanel = new JTextPane();
		outputTextPanel.setForeground(Color.GREEN);
		outputTextPanel.setFont(new Font("Courier 10 Pitch", Font.PLAIN, 14));
		outputTextPanel.setBackground(Color.BLACK);
		outputTextPanel.setEditable(false);
		scrollPane_1.setViewportView(outputTextPanel);
		theConsole = new ConsoleDisplay(outputTextPanel);
		
		JPanel applicationConfigPanel = new JPanel();
		applicationConfigPanel.setLayout(null);
		applicationConfigPanel.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Application configuration", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
		applicationConfigPanel.setBounds(534, 12, 258, 119);
		frame.getContentPane().add(applicationConfigPanel);
		
		textField = new JTextField();
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		textField.setText("1");
		textField.setBounds(70, 88, 38, 19);
		applicationConfigPanel.add(textField);
		textField.setColumns(10);
		
		JRadioButton rdbtnRunFor = new JRadioButton("Run for");
		applicationRunConfiguration.add(rdbtnRunFor);
		rdbtnRunFor.setBounds(12, 32, 76, 23);
		applicationConfigPanel.add(rdbtnRunFor);
		
		JRadioButton rdbtnRunForAn = new JRadioButton("Run for an hyperperiod");
		applicationRunConfiguration.add(rdbtnRunForAn);
		rdbtnRunForAn.setBounds(12, 59, 187, 23);
		applicationConfigPanel.add(rdbtnRunForAn);
		
		JLabel lblInstances = new JLabel("instances");
		lblInstances.setBounds(116, 88, 83, 19);
		applicationConfigPanel.add(lblInstances);
		
		JRadioButton rdbtnRunInstances = new JRadioButton("Run");
		rdbtnRunInstances.setSelected(true);
		applicationRunConfiguration.add(rdbtnRunInstances);
		rdbtnRunInstances.setBounds(12, 86, 54, 23);
		applicationConfigPanel.add(rdbtnRunInstances);
		
		textField_1 = new JTextField();
		textField_1.setText("1");
		textField_1.setHorizontalAlignment(SwingConstants.CENTER);
		textField_1.setColumns(10);
		textField_1.setBounds(92, 34, 44, 19);
		applicationConfigPanel.add(textField_1);
		
		TimeUnitComboBox timeUnitComboBox = new TimeUnitComboBox();
		timeUnitComboBox.setSelectedIndex(1);
		timeUnitComboBox.setBounds(147, 34, 63, 19);
		applicationConfigPanel.add(timeUnitComboBox);
		
		JPanel systemConfigPanel = new JPanel();
		systemConfigPanel.setBounds(804, 12, 283, 254);
		frame.getContentPane().add(systemConfigPanel);
		systemConfigPanel.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "System configuration", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
		systemConfigPanel.setLayout(null);
		
		JComboBox rtosComboChooser = new JComboBox();
		rtosComboChooser.setModel(new DefaultComboBoxModel(RealTimeOperatinSystemEnum.values()));
		rtosComboChooser.setBounds(12, 33, 145, 25);
		systemConfigPanel.add(rtosComboChooser);
		
		JButton btnCompile = new JButton("Go!");
		generator = new RtaiCodeGenerator();
		RtaiGenerateAction rga = new RtaiGenerateAction(generator);
		btnCompile.addActionListener(rga);
		btnCompile.setBounds(181, 217, 89, 25);
		systemConfigPanel.add(btnCompile);
		
		chckbxGenerate = new JCheckBox("Generate");
		chckbxGenerate.setSelected(true);
		chckbxGenerate.setBounds(12, 167, 80, 23);
		chckbxGenerate.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(chckbxGenerate.isSelected())
					generator.setGenerateCode(true);
				else
					generator.setGenerateCode(false);
			}
		});
		systemConfigPanel.add(chckbxGenerate);
		
		chckbxCompile = new JCheckBox("Compile");
		chckbxCompile.setSelected(true);
		chckbxCompile.setBounds(12, 195, 80, 23);		
		chckbxCompile.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(chckbxGenerate.isSelected())
					generator.setCompileCode(true);
				else
					generator.setCompileCode(false);				
			}
		});
		systemConfigPanel.add(chckbxCompile);
		
		chckbxRun = new JCheckBox("Execute");
		chckbxRun.setSelected(true);
		chckbxRun.setBounds(12, 222, 80, 23);
		chckbxRun.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(chckbxRun.isSelected())
					generator.setRunApplication(true);
				else
					generator.setRunApplication(false);
				
			}
		});
		systemConfigPanel.add(chckbxRun);
		
		JButton btnRTOSConfiguration = new JButton("Configure");
		ChoseRTOSActionListener al = new ChoseRTOSActionListener(rtosComboChooser);
		btnRTOSConfiguration.addActionListener(al);
		btnRTOSConfiguration.setBounds(169, 33, 101, 25);
		systemConfigPanel.add(btnRTOSConfiguration);
		
		JCheckBox chckbxEnableBusyWait = new JCheckBox("Enable busy wait calibration");
		chckbxEnableBusyWait.setSelected(true);
		chckbxEnableBusyWait.setBounds(12, 66, 241, 23);
		systemConfigPanel.add(chckbxEnableBusyWait);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Scheduling Policy", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
		panel.setBounds(534, 143, 258, 123);
		frame.getContentPane().add(panel);
		
		JRadioButton rdbtnSingleCpu = new JRadioButton("Single CPU");
		rdbtnSingleCpu.setSelected(true);
		schedulingPolicy.add(rdbtnSingleCpu);
		rdbtnSingleCpu.setBounds(12, 42, 99, 23);
		panel.add(rdbtnSingleCpu);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(12, 86, 128, 25);
		panel.add(comboBox);
		
		JRadioButton rdbtnMultiprocessor = new JRadioButton("Multiprocessor");
		schedulingPolicy.add(rdbtnMultiprocessor);
		rdbtnMultiprocessor.setBounds(124, 42, 122, 23);
		panel.add(rdbtnMultiprocessor);
		
		JButton button = new JButton("Configure");
		button.setBounds(147, 86, 99, 25);
		panel.add(button);
	}
}
