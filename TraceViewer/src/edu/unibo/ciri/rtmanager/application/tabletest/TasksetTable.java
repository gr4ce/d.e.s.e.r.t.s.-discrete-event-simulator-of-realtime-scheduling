package edu.unibo.ciri.rtmanager.application.tabletest;

import javax.swing.JTable;
import javax.swing.table.TableModel;

public class TasksetTable extends JTable{

	private static final long serialVersionUID = -2270843365976430650L;

	public TasksetTable(TableModel tm){
		super(tm);		
	}
	
	public TasksetTable(TableModel tm, TasksetTableColumnModel ttcm){
		super(tm,ttcm);	
	}
}
