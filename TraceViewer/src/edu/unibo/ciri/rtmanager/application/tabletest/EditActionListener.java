package edu.unibo.ciri.rtmanager.application.tabletest;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.rtmanager.application.taskeditor.TaskEditorFrame;

public class EditActionListener implements ActionListener{


	private TaskEditorFrame     theFrame;
	
	public EditActionListener(Task task, TasksetTableModel model, TaskEditorFrame frame){
		theFrame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		theFrame.doUpdate();		
		theFrame.setVisible(true);
	}

}
