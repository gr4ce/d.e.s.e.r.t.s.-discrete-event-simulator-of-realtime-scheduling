package edu.unibo.ciri.rtmanager.application.tabletest;

import java.awt.Component;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;



public class TasksetTableColumnModel extends DefaultTableColumnModel implements TableColumnModel { 

	private Vector<TableColumn> tableColumns = new Vector<TableColumn>();
//	private ListSelectionModel listSelectionModel;
//	private boolean selectionAllowed = false;
//	private Vector<TableColumnModelListener> tableColumnModelListeners = new Vector<TableColumnModelListener>();
	
	
	public TasksetTableColumnModel(){		
		
	}
	
	@Override
	public void addColumn(TableColumn aColumn) {
		tableColumns.add(aColumn);		
	}

//	@Override
//	public void addColumnModelListener(TableColumnModelListener x) {
//		tableColumnModelListeners.add(x);
//	}

	@Override
	public TableColumn getColumn(int columnIndex) {
		return tableColumns.get(columnIndex);
	}

	@Override
	public int getColumnCount() {		
		return tableColumns.size();
	}

//	@Override
//	public int getColumnIndex(Object columnIdentifier) {
//		for(TableColumn c: tableColumns){
//			if(c.getIdentifier().equals(columnIdentifier))
//				return c.getModelIndex();
//		}
//		return 0;
//	}

//	@Override
//	public int getColumnIndexAtX(int xPosition) {
//		return tableColumns.get(xPosition).getModelIndex();
//	}

//	@Override
//	public int getColumnMargin() {
//		// TODO Auto-generated method stub
//		return 0;
//	}

//	@Override
//	public boolean getColumnSelectionAllowed() {
//		return selectionAllowed;
//	}

	@Override
	public Enumeration<TableColumn> getColumns() {
		return new Enumeration<TableColumn>() {
			
			int index = 0;
			@Override
			public boolean hasMoreElements() {
				if(index < tableColumns.size())
					return true;
				return false;
			}

			@Override
			public TableColumn nextElement() {
				return tableColumns.get(index++);
			}
		};
	}

//	@Override
//	public int getSelectedColumnCount() {
//		return 0;
//	}

//	@Override
//	public int[] getSelectedColumns() {
//		return new int[]{0};
//	}

//	@Override
//	public ListSelectionModel getSelectionModel() {		
//		return listSelectionModel;
//	}

//	@Override
//	public int getTotalColumnWidth() {
//		return 0;
//	}

//	@Override
//	public void moveColumn(int columnIndex, int newIndex) {
//		// TODO Auto-generated method stub
//		
//	}

//	@Override
//	public void removeColumn(TableColumn column) {
//		//we don't want to use this!	
//	}

//	@Override
//	public void removeColumnModelListener(TableColumnModelListener x) {
//		tableColumnModelListeners.remove(x);
//	}
//
//	@Override
//	public void setColumnMargin(int newMargin) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void setColumnSelectionAllowed(boolean flag) {
//		selectionAllowed = flag;		
//	}
//
//	@Override
//	public void setSelectionModel(ListSelectionModel newModel) {
//		listSelectionModel = newModel;
//		
//	}

}
