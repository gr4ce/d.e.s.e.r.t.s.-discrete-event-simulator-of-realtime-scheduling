package edu.unibo.ciri.rtmanager.application.tabletest;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskParameters;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.rtmanager.application.taskeditor.TaskEditorFrame;
import edu.unibo.ciri.rtos.parser.ParseException;
import edu.unibo.ciri.rtos.rtai.RtaiCodeGenerator;

@SuppressWarnings("serial")
public class TasksetTableModel extends AbstractTableModel{

	private Vector<String> header = new Vector<String>();	
	private Vector<Object[]> data = new Vector<Object[]>();
	private boolean editingMode = false;
	private TaskSet taskSet = new TaskSet();
	
	public TasksetTableModel(){
		header.add("Sel");
		header.add("Name");
		header.add("Model");
		header.add("Offset");
		header.add("Period");
		header.add("Execution");
		header.add("Action");	
		header.add("Action");
		header.add("taskId");
//		data.add(new Object[]{null,null,null,null,null,null,null,null});
	}
	

		
	@Override
	public String getColumnName(int index){
		return header.get(index);
	}

	@Override
	public int getColumnCount() {
		return header.size();
	}

	@Override
	public int getRowCount() {
		return data.size();
	}

	@Override
	public Object getValueAt(int arg0, int arg1) {
		return data.get(arg0)[arg1];
	}
	
	@Override
	public boolean isCellEditable(int row, int col){
		return editingMode;
	}
	
	@Override 
	public void setValueAt(Object val, int row, int col){
		if(isCellEditable(row, col) && row >= 0 && row < data.size() && col < getColumnCount() ){
			data.get(row)[col] = val;
			fireTableCellUpdated(row, col);
		}
		else
			System.out.println("Error trying to edit the table: setValue()");
	}
	
	public void addTask(Task t, EditActionListener eal, DeleteActionListener dal){
				
		JButton edit = new JButton("edit");
		edit.addActionListener(eal);
		
		JButton delete = new JButton("del");
		delete.addActionListener(dal);
		
		TaskParameters tp = t.getTaskParameters();
		
		data.add(new Object[]{t.getName(),tp.getTaskModel(),tp.getOffset(),tp.getDeadline(),tp.getPeriod(),"OK",edit,delete,t.getTaskId(),});
		
		fireTableDataChanged();
		
		taskSet.addTask(t);
	}
	
	public TaskSet getTaskset(){
		return taskSet;
	}
	
	public void removeTask(int row){
		data.remove(row);
	}
	
	public void removeTask(String name){
		for(int i = 0; i < data.size(); i++){
			String theName = (String) data.get(i)[0];
			if(theName.equals(name)){
				enableEditingMode(true);
				data.remove(i);
				taskSet.removeTask(theName);
				this.fireTableRowsDeleted(i, i);
				enableEditingMode(false);
				return;
			}
				
		}
	}

	
	@Override
	public Class<?> getColumnClass(int arg0) {
		return header.get(arg0).getClass();
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
		super.addTableModelListener(l);		
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		super.removeTableModelListener(l);
	}


	public void updateTask(Task theTask) {
		
		for(int i = 0; i < data.size(); i++){
			int taskId = (Integer) data.get(i)[8]; 
			
			if(theTask.getTaskId() == taskId){
				enableEditingMode(true);
				System.out.println("Updating the value of row "+ i + " with "+theTask.getName());
				Object[] theData = data.get(i);
				TaskParameters tp = theTask.getTaskParameters();
				setValueAt(theTask.getName(),i, 0);
				setValueAt(tp.getTaskModel(),i, 1);
				setValueAt(tp.getOffset()   ,i, 2);
				setValueAt(tp.getDeadline() ,i, 3);
				setValueAt(tp.getPeriod()   ,i, 4);
				
				fireTableRowsUpdated(i, i);
				enableEditingMode(false);
				return;
			}
				
		}
	}
	
	public void clear(){
		data = new Vector<Object[]>();
		taskSet = new TaskSet();
	}
	
	public void setTaskset(TaskSet ts){
		
		data = new Vector<Object[]>();
		taskSet = new TaskSet();
		taskSet.setTasksetName(ts.getTasksetName());		
		
		for(Task theTask: ts.getArrayList()){
			try {
				theTask.setTaskPlatformSpecCode(RtaiCodeGenerator.compileExecutionPattern(theTask.getTaskPseudoCode()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			EditActionListener eal = new EditActionListener(theTask, this, new TaskEditorFrame(theTask, this));
			DeleteActionListener dal = new DeleteActionListener(theTask, this);
			this.addTask(theTask, eal, dal);
		}
		fireTableDataChanged();
		
	}
	
	public void enableEditingMode(boolean val){
		this.editingMode = val;
	}

	
	
	

}
