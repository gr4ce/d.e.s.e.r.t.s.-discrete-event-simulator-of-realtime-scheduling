package edu.unibo.ciri.rtmanager.application.tabletest;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JTable;

import edu.unibo.ciri.realtime.model.Task;

public class DeleteActionListener implements ActionListener{

	private Task 				theTask;
	private TasksetTableModel 	theModel;
	
	public DeleteActionListener(Task task, TasksetTableModel model){
		theTask = task;
		theModel = model;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		int res = JOptionPane.showConfirmDialog(JOptionPane.getFrameForComponent(RTWindow.frame), "Are you sure you want to delete "+theTask.getName(), "Delete task?", JOptionPane.OK_CANCEL_OPTION);
		if(res == JOptionPane.OK_OPTION){
			RTWindow.outputPanelWriteLnInfo("Task " +  theTask.getName() + " has been removed");
			theModel.removeTask(theTask.getName());
		}
	}

}
