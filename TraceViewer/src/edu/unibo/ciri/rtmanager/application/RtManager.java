package edu.unibo.ciri.rtmanager.application;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import edu.unibo.ciri.desert.application.RtaiApplicationCreator.ExecutionPatternButtonColumn;
import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskParameters;
import edu.unibo.ciri.realtime.model.time.Time;
import edu.unibo.ciri.rtmanager.application.taskeditor.TaskEditorFrame;

import javax.swing.table.DefaultTableModel;
import javax.swing.ScrollPaneConstants;

public class RtManager {

	private JFrame mainApplicationframe;
	private TaskEditorFrame currentFrame = null;
	
	private TableModel model;
	private Task currentTask;
	
	private Hashtable<Task, JFrame> taskHTable = new Hashtable<Task,JFrame>(); 
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		Time.setResolution(TimeUnit.NANOSECONDS);
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RtManager window = new RtManager();					
					window.mainApplicationframe.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public RtManager() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		/*
		 * The main application frame part 
		 */
		mainApplicationframe = new JFrame();
		mainApplicationframe.setTitle("RealTime Application Manager");
		mainApplicationframe.setBounds(100, 100, 800, 600);
		mainApplicationframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		/*
		 * The main application panel part 
		 */
		JPanel mainApplicationpanel = new JPanel();
		mainApplicationframe.getContentPane().add(mainApplicationpanel);
		mainApplicationpanel.setLayout(null); //with this you can position your component wherever you want
		
		/*
		 * The task input panel 
		 */
		JPanel taskInputPanel = new JPanel();
		taskInputPanel.setBorder(new TitledBorder(null, "Task parameters", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		taskInputPanel.setBounds(12, 12, 600, 549);
		mainApplicationpanel.add(taskInputPanel);	
		taskInputPanel.setLayout(null); //with this you can position your component wherever you want
		
		/*
		 * Setup for task table 
		 */
		Vector<Task> tasks = new Vector<Task>();
		Task dummy = new Task("...");
		TaskParameters tp = new TaskParameters();
		tp.setDeadline	 (new Time(0, TimeUnit.MILLISECONDS));
		tp.setOffset	 (new Time(0, TimeUnit.MILLISECONDS));
		tp.setPeriod	 (new Time(0, TimeUnit.MILLISECONDS));
		tp.setComputation(new Time(0, TimeUnit.NANOSECONDS));
		dummy.setTaskParameters(tp);
		tasks.add(dummy);
		
		Vector<String> header = new Vector<String>();
		header.add("Name");
		header.add("Model");
		header.add("Offset");
		header.add("Period");
		header.add("Deadline");
		header.add("Execution");
		header.add("Action");
		
		/*
		 * The task input table 
		 */
		
		TasksetTable taskTable = new TasksetTable(new AbstractTableModel() {
			
			@Override
			public Object getValueAt(int arg0, int arg1) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public int getRowCount() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public int getColumnCount() {
				// TODO Auto-generated method stub
				return 0;
			}
		
			
			
		});
//		TasksetTable taskTable = new TasksetTable(TasksetTable.getVectorized(tasks), header);

		
		taskTable.setSize(432, 79);
		JScrollPane scrollPane = new JScrollPane(taskTable);
		scrollPane.setBounds(12, 30, 576, 79);
		taskInputPanel.add(scrollPane);
		
		scrollPane.setViewportView(taskTable);
		taskTable.setFillsViewportHeight(true);
		
		
		
		
		
		Action edit = new AbstractAction() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {							
							currentFrame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				
			}
		}; 
		
		Action delete = new AbstractAction() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {							
							currentFrame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				
			}
		}; 
		
		@SuppressWarnings("serial")
		Action add = new AbstractAction() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
											
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							if(currentFrame == null){
								currentTask  = new Task("dummy");								
								currentFrame = new TaskEditorFrame(currentTask);
								taskHTable.put(currentTask, currentFrame);
//								currentFrame.setTable(model);								
								currentFrame.setVisible(true);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				
			}
		};
		
//		ExecutionButtonColumn buttonColumn = new ExecutionButtonColumn(taskTable, add, 6);
	}

}
