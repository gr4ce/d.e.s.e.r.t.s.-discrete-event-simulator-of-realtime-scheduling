package edu.unibo.ciri.rtmanager.application;

import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import edu.unibo.ciri.realtime.model.time.Time;

@SuppressWarnings("serial")
public class JTable extends AbstractTableModel implements TableModel {

	private Vector<String> 		columnNames = new Vector<String>();
	private Vector<Object[]> 	elements 	= new Vector<Object[]>();
	private Vector<Class<?>>	columnClass = new Vector<Class<?>>();
	private Object[] 			dummy 		= {"...", null, null, null, null, null, new JButton("add")};
	
	
	public JTable() {
		
		columnNames.add("Name");
		columnNames.add("Model");
		columnNames.add("Offset");
		columnNames.add("Deadline");
		columnNames.add("Period");
		columnNames.add("Execution");
		columnNames.add("Action");
		
		columnClass.add(String.class);
		columnClass.add(String.class);
		columnClass.add(Time.class);
		columnClass.add(Time.class);
		columnClass.add(Time.class);
		columnClass.add(JButton.class);
		columnClass.add(JButton.class);
		
		elements.add(dummy);
		
	}
	
	@Override
	public int getColumnCount() {		
		return columnNames.size();
	}

	@Override
	public int getRowCount() {		
		return elements.size();
	}

	@Override
	public Object getValueAt(int row, int col) {
		return elements.get(row)[col];
	}
	
    public void setValueAt(Object value, int row, int col) {
    	elements.get(row)[col] = value;
    }

	@Override
	public void addTableModelListener(TableModelListener arg0) {
		super.addTableModelListener(arg0);
		
	}
	
	@Override
	public void removeTableModelListener(TableModelListener arg0) {
		super.addTableModelListener(arg0);		
	}

	@Override
	public Class<?> getColumnClass(int index) {
		return columnClass.get(index);
	}

	@Override
	public String getColumnName(int index) {
		return columnNames.get(index);
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {
		return true;
	}

	

}
