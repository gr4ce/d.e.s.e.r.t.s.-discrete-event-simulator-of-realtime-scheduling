package edu.unibo.ciri.rtos;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import edu.unibo.ciri.rtos.rtai.RtaiConfigurationWindow;

public class ChoseRTOSActionListener implements ActionListener {

	JComboBox rtosSelection;
	
	RtaiConfigurationWindow configWindow = null;
	
	public ChoseRTOSActionListener(JComboBox comboSelection){
		rtosSelection = comboSelection;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		
		RealTimeOperatinSystemEnum[] rtos = RealTimeOperatinSystemEnum.values();
		int selectedIndex =  rtosSelection.getSelectedIndex();
		
		switch(selectedIndex){
			case 0:
				if(configWindow == null){
					configWindow = new RtaiConfigurationWindow();					
				}
				configWindow.setVisible(true);
				break;
			case 1:
			case 2:
			case 3:
		}

	}

}
