package edu.unibo.ciri.rtos;

public enum RealTimeOperatinSystemEnum {
	RTAI,
	Linux_PREEMPT_RT,
	VxWorks,
	Neutrino
}
