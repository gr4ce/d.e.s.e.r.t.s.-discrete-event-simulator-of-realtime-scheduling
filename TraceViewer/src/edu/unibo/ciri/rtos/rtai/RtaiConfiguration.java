package edu.unibo.ciri.rtos.rtai;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;


import edu.unibo.ciri.desert.resource.SharedResourceProtocolEnum;
import edu.unibo.ciri.realtime.model.time.Time;

public class RtaiConfiguration extends Properties {

	private static final long serialVersionUID = 1L;
	
	private final String rtaiConfigurationFilePath = "config/rtaiConfiguration.xml";
	
	private File xmlRtaiConfigurationFile = new File(rtaiConfigurationFilePath);
	
	private SharedResourceProtocolEnum 	proto = null;
	private String		   				schedulerModes[] = {"One Shot", "Periodic"};
	private int 						schedulerModeIndex;
	private Time						schedulerPeriod;
	
	
	public RtaiConfiguration(){
		
		load();
		setSharedResourceProtocol(getProperty("SharedResourceProtocol"));
		setSchedulerMode(getProperty("SchedulerMode"));
		setSchedulerPeriod(getProperty("SchedulerPeriodValue"), getProperty("SchedulerPeriodTimeUnit"));
		
	}	
	
	
	public void setSharedResourceProtocol(String protoName){
		
		proto = SharedResourceProtocolEnum.getSharedResourceProtocolEnum(protoName);
		
		if(proto != null)
			setProperty("SharedResourceProtocol", protoName);		
	}
	
	public void setSchedulerMode(String schedMode){
		
		if(schedMode.equals(schedulerModes[0])){
			schedulerModeIndex = 0;
		}else if(schedMode.equals(schedulerModes[1])){
			schedulerModeIndex = 1;
		}else
			return;
		
		setProperty("SchedulerMode", schedMode);
	}
	
	public void setSchedulerPeriod(String value, String unit){		
		
		schedulerPeriod = new Time(Long.parseLong(value), unit);
		setProperty("SchedulerPeriodValue", value);
		setProperty("SchedulerPeriodTimeUnit", unit);
	}
	
	public Time getSchedulerPeriod(){
		return schedulerPeriod;
	}
	
	public String getSchedulerMode(){
		return schedulerModes[schedulerModeIndex];
	}
	
	public SharedResourceProtocolEnum getSharedResourceProtocolEnum(){
		return proto;
	}
	
	public void save(){
			
		FileOutputStream fos;
		
		try {
			
			fos = new FileOutputStream(xmlRtaiConfigurationFile);
					
			storeToXML(fos, "");
			fos.flush();
			fos.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void load(){
		
		FileInputStream fis;
		
		try {
			
			fis = new FileInputStream(xmlRtaiConfigurationFile);
					
			loadFromXML(fis);
			fis.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
