package edu.unibo.ciri.rtos.rtai;


import java.awt.EventQueue;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JRadioButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;


import edu.unibo.ciri.rtmanager.application.taskeditor.TimeUnitComboBox;
import edu.unibo.ciri.util.common.GroupButtonUtil;

import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;

public class RtaiConfigurationWindow extends JFrame {

	
	private static final long serialVersionUID = -6341522901348240714L;

	private JPanel contentPane;
	
	private final ButtonGroup sharedResourcesButtonGroup = new ButtonGroup();
	private final ButtonGroup schedulerModeButtonGroup = new ButtonGroup();
	
	
	/*
	 * Scheduler mode 
	 */
	private JRadioButton rdbtnOneShot;
	private JRadioButton rdbtnPeriodic;
	
	
	/*
	 * Scheduler period
	 */
	private JTextField schedulerPeriodValue;
	private TimeUnitComboBox schedulerPeriodTimeUnit;
	
	
	/*
	 * Shared resources protocol
	 */
	private JRadioButton rdbtnNone;
	private JRadioButton rdbtnDynamicPriorityCeiling;
	private JRadioButton rdbtnFullPriorityInheritance;
	
	
	private RtaiConfiguration rtaiConfig = null;
	
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RtaiConfigurationWindow frame = new RtaiConfigurationWindow();
					frame.setVisible(true);
			
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RtaiConfigurationWindow() {
		
		setTitle("RTAI Configuration");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(487, 212);
		setLocationRelativeTo(null);
		setResizable(false);
		
		RtaiConfiguration config = new RtaiConfiguration();
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel sharedResourceProtocolPanel = new JPanel();
		sharedResourceProtocolPanel.setBorder(new TitledBorder(null, "Shared resource protocol", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		sharedResourceProtocolPanel.setBounds(250, 12, 216, 120);
		contentPane.add(sharedResourceProtocolPanel);
		sharedResourceProtocolPanel.setLayout(null);
		
		
		/*
		 * Shared resource protocol  
		 */
		rdbtnNone = new JRadioButton("None");
		sharedResourcesButtonGroup.add(rdbtnNone);
		rdbtnNone.setBounds(8, 22, 185, 23);
		sharedResourceProtocolPanel.add(rdbtnNone);
		
		rdbtnDynamicPriorityCeiling = new JRadioButton("Dynamic Priority Ceiling");
		sharedResourcesButtonGroup.add(rdbtnDynamicPriorityCeiling);
		rdbtnDynamicPriorityCeiling.setBounds(8, 76, 185, 23);
		sharedResourceProtocolPanel.add(rdbtnDynamicPriorityCeiling);		
	
		rdbtnFullPriorityInheritance = new JRadioButton("Full priority inheritance");
		sharedResourcesButtonGroup.add(rdbtnFullPriorityInheritance);
		
		rdbtnFullPriorityInheritance.setBounds(8, 49, 185, 23);
		sharedResourceProtocolPanel.add(rdbtnFullPriorityInheritance);
		
		switch(config.getSharedResourceProtocolEnum()){
			case FULL_PRIORITY_INHERITANCE:
				rdbtnFullPriorityInheritance.setSelected(true);
				break;
			case ADAPTIVE_DYNAMIC_PRIORITY_CEILING:
				rdbtnDynamicPriorityCeiling.setSelected(true);
				break;
			case NO_PROTO:
				rdbtnNone.setSelected(true);
				break;
			default:
				//throw exeception
		}
		
		
		/*
		 * Scheduler execution mode
		 */
		JPanel schedulerModePanel = new JPanel();
		schedulerModePanel.setBorder(new TitledBorder(null, "Scheduler mode", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		schedulerModePanel.setBounds(12, 12, 226, 120);
		contentPane.add(schedulerModePanel);
		schedulerModePanel.setLayout(null);
		
		rdbtnOneShot = new JRadioButton("One Shot");
		schedulerModeButtonGroup.add(rdbtnOneShot);		
		rdbtnOneShot.setBounds(8, 24, 103, 23);
		schedulerModePanel.add(rdbtnOneShot);
		
		rdbtnPeriodic = new JRadioButton("Periodic");
		schedulerModeButtonGroup.add(rdbtnPeriodic);
		rdbtnPeriodic.setBounds(8, 51, 88, 23);
		schedulerModePanel.add(rdbtnPeriodic);
		
		if(config.getSchedulerMode().equals("One Shot")){
			rdbtnOneShot.setSelected(true);
		}else if(config.getSchedulerMode().equals("Periodic")){
			rdbtnPeriodic.setSelected(true);
		}else;
			//throw exeception;
		
			
		/*
		 * Scheduler period
		 */
		schedulerPeriodValue = new JTextField();
		
		schedulerPeriodValue.setText( config.getSchedulerPeriod().getValueInPreferredTimeUnitRepresentation() + "");
				
		schedulerPeriodValue.setHorizontalAlignment(SwingConstants.CENTER);
		schedulerPeriodValue.setColumns(10);
		schedulerPeriodValue.setBounds(73, 89, 70, 19);
		
		schedulerModePanel.add(schedulerPeriodValue);
		
		schedulerPeriodTimeUnit = new TimeUnitComboBox();
		schedulerPeriodTimeUnit.setModel(new DefaultComboBoxModel(new String[] {"s", "ms", "us", "ns"}));
		schedulerPeriodTimeUnit.setSelectedItem(config.getSchedulerPeriod().getPreferredTimeUnitRepresentationString());
		schedulerPeriodTimeUnit.setBounds(155, 89, 58, 19);
		schedulerModePanel.add(schedulerPeriodTimeUnit);
		
		JLabel lblPeriod = new JLabel("Period");
		lblPeriod.setBounds(14, 91, 49, 15);
		schedulerModePanel.add(lblPeriod);
		
		JButton btnCancel = new JButton("Cancel");
		class CancelButton implements ActionListener{

			JFrame f;
			public CancelButton(JFrame frameToBeClosed){
				f = frameToBeClosed;
			}
			
			@Override
			public void actionPerformed(ActionEvent e) {
				f.setVisible(false);				
			}			
		}		
		btnCancel.addActionListener(new CancelButton(this));		
		btnCancel.setBounds(370, 144, 96, 25);
		contentPane.add(btnCancel);
		
		JButton btnSave = new JButton("Save");
		class SimplePropertySaveButton implements ActionListener{

			JFrame f;
			public SimplePropertySaveButton(JFrame frameToBeClosed){
				f = frameToBeClosed;
			}
			
			@Override
			public void actionPerformed(ActionEvent e) {
				((RtaiConfigurationWindow)f).saveAsProperty();		
				f.setVisible(false);
			}			
		}
		btnSave.addActionListener(new SimplePropertySaveButton(this));
		btnSave.setBounds(264, 144, 96, 25);
		contentPane.add(btnSave);
	}
	

	public void saveAsProperty() {
		
		if(rtaiConfig == null)
			rtaiConfig = new RtaiConfiguration();
		rtaiConfig.setSchedulerMode(GroupButtonUtil.getSelectedButtonText(schedulerModeButtonGroup));
		rtaiConfig.setSchedulerPeriod(schedulerPeriodValue.getText(), (String)schedulerPeriodTimeUnit.getSelectedItem());
		rtaiConfig.setSharedResourceProtocol(GroupButtonUtil.getSelectedButtonText(sharedResourcesButtonGroup));
		rtaiConfig.save();

	}
}
