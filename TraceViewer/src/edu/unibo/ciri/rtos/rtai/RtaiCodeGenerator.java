package edu.unibo.ciri.rtos.rtai;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskParameters;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.rtmanager.application.tabletest.ConsoleDisplay;
import edu.unibo.ciri.rtmanager.application.tabletest.RTWindow;
import edu.unibo.ciri.rtos.parser.TaskExecutionPattern;


public class RtaiCodeGenerator {
	
	private TaskSet ts;
	

	private String outputSourcesFolder = "build/";
	private String outputMakeFolder    = "build/";
	private String outputBinFolder     = "build/";
	private String outputTracesFolder  = "build/";
	
	private boolean generateCode   = false;
	private boolean compileCode    = false;
	private boolean runApplication = false;
	
	private final String headers[] = {
			"global_defs",
			"resources",
			"realtime_task",
			"calibration",
			"periodic_model",
			"aperiodic_model",
			"scheduling_policies"
			
	};
	
	private final String sources[] = {
			"base_module",
			"resources",
			"realtime_task",
			"calibration",
			"periodic_model",
			"aperiodic_model",
			"scheduling_policies"
			
	};
	
	
	
	public void setTaskset(TaskSet t){
		ts = t;		
	}
	
	private void prepareBuildEnvironment(){
		
		outputSourcesFolder = "build/";
		outputMakeFolder 	= "build/";
		outputBinFolder 	= "build/";
		outputTracesFolder	= "build/";
		
		outputSourcesFolder += ts.getTasksetName() + "/src/" ;
		outputMakeFolder 	+= ts.getTasksetName() + "/src/.make/";
		outputBinFolder 	+= ts.getTasksetName() + "/bin/";
		outputTracesFolder  += ts.getTasksetName() + "/traces/";
		
	}
	
	public void generate(){
		
		ConsoleDisplay display = RTWindow.getOutputDisplay();		
		display.initShell(null);
		
		System.out.println("Generating taskset:\n" +ts);
		
		prepareBuildEnvironment();
				
		/*
		 * Generate application sources 
		 */
		if(generateCode){
			
			display.execute("pwd");
			if(new File("build/"+ts.getTasksetName()).exists()){
				int answer = JOptionPane.showConfirmDialog(RTWindow.frame, "<html>The " + ts.getTasksetName() + " folder already exists. <br/>If you continue all files will be overwritten, are you happy with that?</html>", "Warning!", JOptionPane.YES_NO_OPTION);
				if(answer != JOptionPane.YES_OPTION)
					return;			
			}else{			
				new File(outputSourcesFolder).mkdirs();
				new File(outputMakeFolder).mkdirs();
				new File(outputBinFolder).mkdirs();
				new File(outputTracesFolder).mkdirs();			
			}		
			
//			display.execute("cp -rf templates/rtai/src/* build/" + applicationName + "/src/");
//			display.execute("cd build/" + applicationName + "/src/");
//			display.execute("rm -vf *.c");
//			display.execute("rm -vf *.h");
//			display.execute("cd ../../..");
			
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			RTWindow.outputPanelWriteLnInfo("Automatic code generation: template folder is templates/rtai/src/");
			RTWindow.outputPanelWriteLnInfo("Automatic code generation: output folder is build/" + ts.getTasksetName() + "/");
			
			generate_global_defs_h("templates/rtai/src/global_defs.h", outputSourcesFolder + "global_defs.h");
			RTWindow.outputPanelWriteLnInfo("Successfully generated global_defs.h");
			
			generate_base_module_c("templates/rtai/src/base_module.c", outputSourcesFolder + "base_module.c");		
			RTWindow.outputPanelWriteLnInfo("Successfully generated base_module.c");
			
			generate_resources_h("templates/rtai/src/resources.h", outputSourcesFolder + "resources.h");
			RTWindow.outputPanelWriteLnInfo("Successfully generated resources.h");
			
			generate_resources_c("templates/rtai/src/resources.c", outputSourcesFolder + "resources.c");
			RTWindow.outputPanelWriteLnInfo("Successfully generated resources.c");	
			
			generate_realtime_task_h("templates/rtai/src/realtime_task.h", outputSourcesFolder + "realtime_task.h");
			RTWindow.outputPanelWriteLnInfo("Successfully generated periodic_task.h");
			
			generate_aperiodic_model_h("templates/rtai/src/aperiodic_model.h", outputSourcesFolder + "aperiodic_model.h");
			RTWindow.outputPanelWriteLnInfo("Successfully generated aperiodic_model.h");
			
			generate_aperiodic_model_c("templates/rtai/src/aperiodic_model.c", outputSourcesFolder + "aperiodic_model.c");
			RTWindow.outputPanelWriteLnInfo("Successfully generated aperiodic_model.c");
			
			generate_realtime_task_c("templates/rtai/src/realtime_task.c", outputSourcesFolder + "realtime_task.c");
			RTWindow.outputPanelWriteLnInfo("Successfully generated periodic_task.c");
			
			generate_calibration_h("templates/rtai/src/calibration.h", outputSourcesFolder + "calibration.h");
			RTWindow.outputPanelWriteLnInfo("Successfully generated calibration.h");
			
			generate_calibration_c("templates/rtai/src/calibration.c", outputSourcesFolder + "calibration.c");
			RTWindow.outputPanelWriteLnInfo("Successfully generated calibration.c");
			
			generate_makefiles("templates/rtai/src/.make/", outputMakeFolder);
			
//			String result = JOptionPane.showInputDialog(RTWindow.frame, "Please insert a name for the trace output file");
					
			display.execute("cp templates/rtai/bin/* build/" + ts.getTasksetName() + "/bin/");
			display.execute("cp templates/rtai/src/scheduling_policies* build/" + ts.getTasksetName() + "/src/");
			display.execute("cp templates/rtai/src/sporadic_model* build/" + ts.getTasksetName() + "/src/");
			display.execute("cp templates/rtai/src/periodic_model* build/" + ts.getTasksetName() + "/src/");
			display.execute("cd build/" + ts.getTasksetName() + "/bin/");
			display.execute("mkdir .module");
			display.execute("cd ../../..");
			
		}
		
		/*
		 * Compile application sources		 
		 */		
		if(compileCode){
			display.execute("cd build/" + ts.getTasksetName() + "/bin/");
			display.execute("./build");
			display.execute("cd ../../..");
		}
		
		
		/*
		 * Run application 		 
		 */		
		if(runApplication){
			String traceFileName = Calendar.getInstance().getTime().toString() + "-" + ts.getTasksetName() + ".trace";
			traceFileName = traceFileName.replaceAll("( )+", "_");
			System.out.println("TraceFile = " + traceFileName);
			display.execute("cd build/" + ts.getTasksetName() + "/bin/");
			display.execute("export SUDO_ASKPASS=/usr/bin/ssh-askpass");
			display.execute("sudo -s");
			display.execute("dmesg -c > /dev/null");
			display.execute("export PATH=$PATH:/usr/realtime/bin");
			display.execute("./run "+ts.getTasksetName());
			display.execute("exit");
			display.execute("./display --events");		
			display.execute("./extract ../traces/" + traceFileName);
			display.execute("./traceviewer ../traces/" + traceFileName + " &");
			display.execute("cd ../../..");
		}
				
		RTWindow.outputPanelWriteLnInfo("Automatic code generation: output folder is build/" + ts.getTasksetName() + "/");
	}
	
	private void generate_makefiles(String templateFolderName, String outputFolderName) {
		
		String gnuMakefile = "GNUmakefile";
		String makefile = "Makefile";
		
		/*
		 * Checking if folder exists
		 */
		File makeFolder = new File(outputFolderName);
		if(!makeFolder.exists()){
			makeFolder.mkdir();
		}
		
		/*
		 * Preparing the input file
		 */
		File templateFile = new File(templateFolderName + gnuMakefile);
		FileInputStream fis;
		InputStreamReader isr;
		String bufferString;
		
		/*
		 * Preparing the outpu file 
		 */
		BufferedReader br;
		FileWriter output; 		
		
		try {
			fis = new FileInputStream(templateFile);
			isr = new InputStreamReader(fis);
			br = new BufferedReader(isr);
			
			output = new FileWriter(outputFolderName + gnuMakefile);			
		
			while((bufferString = br.readLine()) != null){												
				
				if(bufferString.startsWith("#@COMPILER_STATEMENTS")){
					
					String makeAllStatement = "all: ";
					
					for(String source: sources){
						makeAllStatement += source + ".o ";
					}
					
					output.write(makeAllStatement + "\n\n");
					
					for(String source: sources){
						output.write(createMakeStatement(source));
					}					
					
				}else{					
					output.write(bufferString+"\n");					
				}								
			}
			
			output.close();
			
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
		catch (IOException e) {		
			e.printStackTrace();
		}
		
		
		templateFile = new File(templateFolderName + makefile);
		
		try {
			fis = new FileInputStream(templateFile);
			isr = new InputStreamReader(fis);
			br = new BufferedReader(isr);
			
			output = new FileWriter(outputFolderName + makefile);			
		
			while((bufferString = br.readLine()) != null){												
				
				if(bufferString.startsWith("#@COMPILER_STATEMENTS")){
					
					String makeStatement = "obj-m += "+ts.getTasksetName()+".o\n";
					makeStatement += ts.getTasksetName() + "-objs := ";
					
					for(String source: sources){
						makeStatement += source + ".o ";
					}
					
					output.write(makeStatement + "\n\n");
														
				}else{					
					output.write(bufferString+"\n");					
				}								
			}
			
			output.close();
			
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
		catch (IOException e) {		
			e.printStackTrace();
		}
	}

	private String createMakeStatement(String source) {
		return source + ".o: " + source + ".c\n\t$(CC) $(MODULE_CFLAGS) -c $<;\n\n";
	}

	//SECTION: Aperiodic request definition
	
	private void generate_calibration_c(String templateFileName, String outputFileName) {
		
		/*
		 * Preparing the input file
		 */
		File templateFile = new File(templateFileName);
		FileInputStream fis;
		InputStreamReader isr;
		String bufferString;
		
		/*
		 * Preparing the outpu file 
		 */
		BufferedReader br;
		FileWriter outputFileWriter; 		
		
		try {
			fis = new FileInputStream(templateFile);
			isr = new InputStreamReader(fis);
			br = new BufferedReader(isr);
			
			outputFileWriter = new FileWriter(outputFileName);		
		
			while((bufferString = br.readLine()) != null){												
				outputFileWriter.write(bufferString+"\n");							
			}
			
			outputFileWriter.close();
			
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
		catch (IOException e) {		
			e.printStackTrace();
		}
	}

	private void generate_calibration_h(String templateFileName, String outputFileName) {
		
		/*
		 * Preparing the input file
		 */
		File templateFile = new File(templateFileName);
		FileInputStream fis;
		InputStreamReader isr;
		String bufferString;
		
		/*
		 * Preparing the outpu file 
		 */
		BufferedReader br;
		FileWriter outputFileWriter; 
		
		
		try {
			fis = new FileInputStream(templateFile);
			isr = new InputStreamReader(fis);
			br = new BufferedReader(isr);
			
			File outputFile = new File(outputFileName);
			if(!outputFile.exists()){
				outputFile.createNewFile();
			}
			outputFileWriter = new FileWriter(outputFile);	
					
		
			while((bufferString = br.readLine()) != null){
												
				outputFileWriter.write(bufferString+"\n");							
			}
			
			outputFileWriter.close();
			
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
		catch (IOException e) {		
			e.printStackTrace();
		}
	}

	private void generate_aperiodic_model_c(String templateFileName, String outputFileName) {
		
		/*
		 * Preparing the input file
		 */
		File templateFile = new File(templateFileName);
		FileInputStream fis;
		InputStreamReader isr;
		String bufferString;
		
		/*
		 * Preparing the outpu file 
		 */
		BufferedReader br;
		FileWriter outputFileWriter; 
		
		
		try {
			fis = new FileInputStream(templateFile);
			isr = new InputStreamReader(fis);
			br  = new BufferedReader(isr);
			
			outputFileWriter = new FileWriter(outputFileName);			
		
			while((bufferString = br.readLine()) != null){			
				
				if(bufferString.startsWith("//SECTION: Aperiodic requests")){
					
					//TODO inserire le richisete in maniera parametrica
					outputFileWriter.write("#define MAX_REQUESTS 4\n\n");
					outputFileWriter.write("void request_1_code(void *param){\n\tbusy_wait_ms(1);\n}\n\n");
					outputFileWriter.write("void request_2_code(void *param){\n\tbusy_wait_ms(2);\n}\n\n");
					outputFileWriter.write("void request_3_code(void *param){\n\tbusy_wait_ms(3);\n}\n\n");
					
					outputFileWriter.write(createAperiodicRequestList(ts));
					
				}else if(bufferString.startsWith("//SECTION: Bind aperiodic request code")){
				
					outputFileWriter.write("\taperiodic_request_list[0].request_code = request_1_code;\n");
					outputFileWriter.write("\taperiodic_request_list[1].request_code = request_2_code;\n");
					outputFileWriter.write("\taperiodic_request_list[2].request_code = request_3_code;\n");
					outputFileWriter.write("\taperiodic_request_list[3].request_code = request_1_code;\n");
					
				}else{
					outputFileWriter.write(bufferString+"\n");
				}
			}
			
			outputFileWriter.close();
			
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
		catch (IOException e) {		
			e.printStackTrace();
		}
		
	}
	
	private String createAperiodicRequestList(TaskSet ts2) {
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("rt_aperiodic_request aperiodic_request_list[] = {\n");
		//TODO le stringhe seguenti devono essere fatte seguendo questa logica:
		//     bisogna utilizzare il tempo assoluto della richiesta 
		//     lasciare a NULL il task che viene inserito in fase di bind
		//     inserire il codice della richiesta che sarà fatto con nome richiesta o con 
		//     un nome fisso + un indice progressivo della richiesta
		sb.append("{ms_to_ns(0),  NULL, NULL},\n");
		sb.append("{ms_to_ns(7),  NULL, NULL},\n");
		sb.append("{ms_to_ns(16), NULL, NULL},\n");
		sb.append("{ms_to_ns(25), NULL, NULL},\n");
		sb.append("};\n\n");
		
		return sb.toString();
	}

	private void generate_aperiodic_model_h(String templateFileName, String outputFileName) {
		
		/*
		 * Preparing the input file
		 */
		File templateFile = new File(templateFileName);
		FileInputStream fis;
		InputStreamReader isr;
		String bufferString;
		
		/*
		 * Preparing the outpu file 
		 */
		BufferedReader br;
		FileWriter outputFileWriter; 
		
		
		try {
			fis = new FileInputStream(templateFile);
			isr = new InputStreamReader(fis);
			br = new BufferedReader(isr);
			

			outputFileWriter = new FileWriter(outputFileName);			
		
			while((bufferString = br.readLine()) != null){
				
														
					outputFileWriter.write(bufferString+"\n");							
			}
			
			outputFileWriter.close();
			
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
		catch (IOException e) {		
			e.printStackTrace();
		}
		
	}
	
	private void generate_realtime_task_c(String templateFileName, String outputFileName) {
		
		/*
		 * Preparing the input file
		 */
		File templateFile = new File(templateFileName);
		FileInputStream fis;
		InputStreamReader isr;
		String bufferString;
		
		/*
		 * Preparing the outpu file 
		 */
		BufferedReader br;
		FileWriter outputFileWriter; 
		
		
		try {
			fis = new FileInputStream(templateFile);
			isr = new InputStreamReader(fis);
			br = new BufferedReader(isr);
			

			outputFileWriter = new FileWriter(outputFileName);			
		
			while((bufferString = br.readLine()) != null){
				
				if(bufferString.startsWith("//SECTION: Periodic task initalization")){
					
					for(Task t: ts.getArrayList()){
						outputFileWriter.write(createTaskInstanciationLine(t));
					}
					
				}else if(bufferString.startsWith("//SECTION: Aperiodic task initalization")){
					
					//TODO fare in modo che l'indice e la priorità siano assegnate parametricamente
					outputFileWriter.write("\ttask = create_aperiodic_task(4,\"APER\", 3);\n");
					outputFileWriter.write("\tbind_aperiodic_request_to_realtime_task(task);\n");					
					
				}else if(bufferString.startsWith("//SECTION: Task definition")){
					
					for(Task t: ts.getArrayList()){
						outputFileWriter.write(createTaskFunctionDefinition(t));
					}
					
				}else												
					outputFileWriter.write(bufferString+"\n");							
			}
			
			outputFileWriter.close();
			
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
		catch (IOException e) {		
			e.printStackTrace();
		}
		
	}

	private void generate_realtime_task_h(String templateFileName, String outputFileName) {
		
		/*
		 * Preparing the input file
		 */
		File templateFile = new File(templateFileName);
		FileInputStream fis;
		InputStreamReader isr;
		String bufferString;
		
		/*
		 * Preparing the outpu file 
		 */
		BufferedReader br;
		FileWriter outputFileWriter; 
		
		
		try {
			fis = new FileInputStream(templateFile);
			isr = new InputStreamReader(fis);
			br = new BufferedReader(isr);
			
			outputFileWriter = new FileWriter(outputFileName);			
		
			while((bufferString = br.readLine()) != null){			
				
				if(bufferString.startsWith("//SECTION: Number of tasks")){
					
					outputFileWriter.write("#define NUMBER_OF_TASKS " + ts.size() + "\n");
					
				}else if(bufferString.startsWith("//SECTION: Task code declaration")){
					
					for(Task task: ts.getArrayList()){
						outputFileWriter.write(createTaskFunctionDeclaration(task));
					}
					
				}else{
					outputFileWriter.write(bufferString+"\n");
				}
			}
			
			outputFileWriter.close();
			
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
		catch (IOException e) {		
			e.printStackTrace();
		}
		
	}

	public void generate_base_module_c(String templateFileName, String outputFileName){	
		
		/*
		 * Preparing the input file
		 */
		File templateFile = new File(templateFileName);
		FileInputStream fis;
		InputStreamReader isr;
		String bufferString;
		
		/*
		 * Preparing the outpu file 
		 */
		BufferedReader br;
		FileWriter outputFileWriter; 
		
		
		try {
			fis = new FileInputStream(templateFile);
			isr = new InputStreamReader(fis);
			br = new BufferedReader(isr);
			

			outputFileWriter = new FileWriter(outputFileName);
			
		
			while((bufferString = br.readLine()) != null){
										
				if(bufferString.startsWith("//SECTION: Timer initialization")){
					
					outputFileWriter.write("\trt_set_oneshot_mode();\n");
					outputFileWriter.write("\tstart_rt_timer(us_to_ns(500));\n\n");
					//TODO Da scrivere in base alla configurazione di rtai... per ora inizializzato staticamente
					
				}else if(bufferString.startsWith("//SECTION: Calibration")){
					
					outputFileWriter.write("\tcalibrate_loop();\n\n");
					//TODO Da scrivere se la calibrazione è abilitata
					
				}else if(bufferString.startsWith("//SECTION: Scheduling policy")){
					
					outputFileWriter.write("\tset_scheduling_policy(RATE_MONOTONIC);\n\n");
					//TODO Da scrivere in base alla politica di scheduling
					
				}else if(bufferString.contains("//SECTION: Register main task")){
					
					outputFileWriter.write("\tunibo_tracer_register_task(rt_whoami(), \"MAIN\");"+"\n");
					outputFileWriter.write("\trt_register(MAIN_TASK, (void*) rt_whoami(), 0, NULL);" + "\n");
					outputFileWriter.write("unibo_trace_event(APPLICATION_START, rt_whoami(), get_start_time_ns(), NULL);\n");
					outputFileWriter.write("unibo_trace_event(APPLICATION_STOP, rt_whoami(), get_start_time_ns() + ms_to_ns(APPLICATION_STOP_TIME_MS), NULL);\n");
										
				}else{					
					outputFileWriter.write(bufferString+"\n");					
				}				
			}
			
			outputFileWriter.close();
			
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
		catch (IOException e) {		
			e.printStackTrace();
		}			
	}

	public void generate_global_defs_h(String templateFileName, String outputFileName){
		
		/*
		 * Preparing the input file
		 */
		File templateFile = new File(templateFileName);
		FileInputStream fis;
		InputStreamReader isr;
		String bufferString;
		
		/*
		 * Preparing the outpu file 
		 */
		BufferedReader br;
		FileWriter outputFileWriter; 
		
		try {
			
			fis = new FileInputStream(templateFile);
			isr = new InputStreamReader(fis);
			br = new BufferedReader(isr);
			
			outputFileWriter = new FileWriter(outputFileName);
		
			while((bufferString = br.readLine()) != null){
												
				if(bufferString.startsWith("//SECTION: Application name")){				
					
					outputFileWriter.write("#define MODULE_NAME \""+ ts.getTasksetName() +"\"");				
					
				}else if(bufferString.startsWith("//SECTION: Stop mode")){
					
					outputFileWriter.write("#define STOP_MODE FIXED_USER_DEFINED_TIME");
					//TODO Definire il modo di stop da file di configurazione 
					
				}else if(bufferString.startsWith("//SECTION: Stop time")){
					
					outputFileWriter.write("#define APPLICATION_STOP_TIME_MS 60");
					//TODO Definire il tempo di stop da file di configurazione
					
				}else{
					
					outputFileWriter.write(bufferString+"\n");					
				}				
			}
			
			outputFileWriter.close();
			fis.close();
			isr.close();
			br.close();
			
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
		catch (IOException e) {		
			e.printStackTrace();
		}
		
	}
	
	public void generate_resources_c(String templateFileName, String outputFileName){
				
		/*
		 * Preparing the input file
		 */
		File templateFile = new File(templateFileName);
		FileInputStream fis;
		InputStreamReader isr;
		String bufferString;
		
		/*
		 * Preparing the outpu file 
		 */
		BufferedReader br;
		FileWriter outputFileWriter; 
		
		
		try {
			fis = new FileInputStream(templateFile);
			isr = new InputStreamReader(fis);
			br = new BufferedReader(isr);
			

			outputFileWriter = new FileWriter(outputFileName);
		
			while((bufferString = br.readLine()) != null){
												
				if(bufferString.startsWith("//SECTION: Register resources")){
															
					ArrayList<String> res = ts.getUsedResoruces();
					int index = 0;
					for(String res_name: res){
						outputFileWriter.write("\tunibo_tracer_register_semaphore(&resources[" + (index++) + "],\""+ res_name +"\");\n");
					}
					
				}else{					
					outputFileWriter.write(bufferString+"\n");					
				}				
			}
			
			outputFileWriter.close();
			fis.close();
			isr.close();
			br.close();
			
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
		catch (IOException e) {		
			e.printStackTrace();
		}
		
	}
	
	public void generate_resources_h(String templateFileName, String outputFileName){
		
		/*
		 * Preparing the input file
		 */
		File templateFile = new File(templateFileName);
		FileInputStream fis;
		InputStreamReader isr;
		String bufferString;
		
		/*
		 * Preparing the outpu file 
		 */
		BufferedReader br;
		FileWriter outputFileWriter; 
		
		
		try {
			
			fis = new FileInputStream(templateFile);
			isr = new InputStreamReader(fis);
			br = new BufferedReader(isr);
			

			outputFileWriter = new FileWriter(outputFileName);	
		
			while((bufferString = br.readLine()) != null){
				
				if(bufferString.startsWith("//SECTION: Resources declaration")){					
										
					ArrayList<String> res = ts.getUsedResoruces();
					int index = 0;
					for(String s: res){
						outputFileWriter.write("#define " + s + " " + (index++) + "\n");
					}
					outputFileWriter.write("#define NUMBER_OF_RESOURCES " + res.size() + "\n");
					outputFileWriter.write("#define RES_TYPE RES_SEM|PRIO_Q\n");
					
				}else{					
					outputFileWriter.write(bufferString+"\n");					
				}				
			}
			
			outputFileWriter.close();
			fis.close();
			isr.close();
			br.close();
			
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
		catch (IOException e) {		
			e.printStackTrace();
		}		
	}
	
	

	private String createTaskInstanciationLine(Task t){
				
		StringBuffer line = new StringBuffer();
		TaskParameters tp = t.getTaskParameters();
		
			
		line.append("\tcreate_periodic_task(");
		line.append(t.getTaskId());
		line.append(",\""+ t.getName() +"\",");
		line.append(t.getTaskId()+",");//PRIORITY
		
		TimeUnit tu = tp.getOffset().getPreferredTimeUnit();		
		switch(tu){
			case MILLISECONDS:
				line.append("ms_to_ns(" + tp.getOffset().getTimeValue(TimeUnit.MILLISECONDS)+"),");
				break;
			case MICROSECONDS:
				line.append("us_to_ns(" + tp.getOffset().getTimeValue(TimeUnit.MICROSECONDS)+"),");
				break;			
			default:	
				line.append(tp.getOffset().getTimeValue(TimeUnit.NANOSECONDS)+",");
		}
		
		
		tu = tp.getDeadline().getPreferredTimeUnit();
		switch(tu){
			case MILLISECONDS:
				line.append("ms_to_ns(" + tp.getDeadline().getTimeValue(TimeUnit.MILLISECONDS)+"),");
				break;
			case MICROSECONDS:
				line.append("us_to_ns(" + tp.getDeadline().getTimeValue(TimeUnit.MICROSECONDS)+"),");
				break;			
			default:	
				line.append(tp.getDeadline().getTimeValue(TimeUnit.NANOSECONDS)+",");
		}
	
		
		tu = tp.getPeriod().getPreferredTimeUnit();
		switch(tu){
			case MILLISECONDS:
				line.append("ms_to_ns(" + tp.getPeriod().getTimeValue(TimeUnit.MILLISECONDS)+"),");
				break;
			case MICROSECONDS:
				line.append("us_to_ns(" + tp.getPeriod().getTimeValue(TimeUnit.MICROSECONDS)+"),");
				break;			
			default:	
				line.append(tp.getPeriod().getTimeValue(TimeUnit.NANOSECONDS)+",");
		}

		line.append(t.getName()+"_execution_pattern);\n");
		
		return line.toString();
	}
	
	private String createTaskFunctionDefinition(Task t){
		
		StringBuffer function = new StringBuffer();
		String taskName = t.getName();
		
		function.append("void 	" + taskName + "_execution_pattern(void *param){\n\n");
		function.append("\trealtime_task *task = (realtime_task*) param;\n\n");
		function.append(t.getTaskPlatformSpecCode() + "\n");
		function.append("}\n\n");
		
		return function.toString();
	}
	
	private String createTaskFunctionDeclaration(Task task) {
		
		StringBuffer function = new StringBuffer();
		String taskName = task.getName();
		
		function.append("void " + taskName + "_execution_pattern(void *param);\n");
		
		return function.toString();
	}
	
	public static String compileExecutionPattern(String pattern) throws edu.unibo.ciri.rtos.parser.ParseException{
		
		TaskExecutionPattern parser = new TaskExecutionPattern( new java.io.StringReader(pattern));
		try {
			parser.parse();
		} catch (edu.unibo.ciri.rtos.parser.ParseException e) {
			e.printStackTrace();
			throw new edu.unibo.ciri.rtos.parser.ParseException();
		}		
		return parser.getCompiledString();
	}

	public void setGenerateCode(boolean generateCode) {
		this.generateCode = generateCode;
	}

	public void setCompileCode(boolean compileCode) {
		this.compileCode = compileCode;
	}

	public void setRunApplication(boolean runApplication) {
		this.runApplication = runApplication;
	}

	
}
