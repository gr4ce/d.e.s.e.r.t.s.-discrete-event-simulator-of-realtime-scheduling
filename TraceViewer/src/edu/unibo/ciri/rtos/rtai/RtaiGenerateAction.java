package edu.unibo.ciri.rtos.rtai;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.rtmanager.application.tabletest.RTWindow;

public class RtaiGenerateAction implements ActionListener{
	
	private RtaiCodeGenerator generator;
	
	public RtaiGenerateAction(RtaiCodeGenerator generator){
		this.generator = generator;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
				
		RTWindow.outputPanelWriteLnInfo("Starting application generation for RTAI 3.9 Operating System");
		generator.generate();
	}

}
