package edu.unibo.ciri.rtos.rtai;

public class CodeGeneratorException extends Exception {

	private static final long serialVersionUID = 6320512289009083224L;

	public CodeGeneratorException(String string) {
		super(string);
	}

}
