package edu.unibo.ciri.util.sort.algorithm;

import java.util.Comparator;

import util.SortingAlgorithm;
import util.SortingAlgorithm.SortingOrder;


public class QuickSort<E> implements SortingAlgorithm<E>{
	
	
	private Comparator<E> comparatorInstance;
	private E[] internalArray;
		
	public QuickSort(){
		
	}
	
	public QuickSort(Comparator<E> comp, E[] array){
		comparatorInstance = comp;
		internalArray = array;
	}
	
	public void setArray(E[] array){
		internalArray = array;
	}
	
	public E[] getArray(){
		return internalArray;
	}
	
	public <C extends Comparator<E>> void setComparator(C comp) {
		comparatorInstance = comp;
		
	}
	
	public Comparator<E> getComparator(){
		return comparatorInstance;
	}

	
	
	public void sort(SortingOrder order){
		
		int minIndex = 0;
		int maxIndex = internalArray.length - 1;
		
		if(order == SortingOrder.NON_DECREASING_ORDER)		
			sortNonDecreasing(internalArray, minIndex, maxIndex);
		else
			sortNonIncreasing(internalArray, minIndex, maxIndex);
		
	}
	
	public void sort(E[] array, SortingOrder order){
		
		if(order == SortingOrder.NON_DECREASING_ORDER)		
			sortNonDecreasing(array, 0, array.length - 1);
		else
			sortNonIncreasing(array, 0, array.length - 1);
		
		
	}
	
	private void sortNonIncreasing(E[] array, int lowIndex, int highIndex){
		
		if (highIndex <= lowIndex) 
			return;
		
		int j = partitionNonIncreasing(array, lowIndex, highIndex);
        
		sortNonIncreasing(array, lowIndex, j - 1);
		sortNonIncreasing(array, j + 1, highIndex);
        
	}
	
	private void sortNonDecreasing(E[] array, int lowIndex, int highIndex){
		
		if (highIndex <= lowIndex) 
			return;
		
		int j = partitionNonDecreasing(array, lowIndex, highIndex);
        
		sortNonDecreasing(array, lowIndex, j - 1);
		sortNonDecreasing(array, j + 1, highIndex);
        
	}
	
	private void swap(E[] array, int i, int j){
		
		int bound = array.length;
		
		//TODO just for debug...cosider to remove this code or to put in on an assertion piece of code
		if( i == j )
			return;
		
		if( i > j ){
			if( i > bound )
				throw new ArrayIndexOutOfBoundsException(i);
			if( j < 0 )
				throw new ArrayIndexOutOfBoundsException(j);
		}else{
			if( j > bound )
				throw new ArrayIndexOutOfBoundsException(j);
			if( i < 0 )
				throw new ArrayIndexOutOfBoundsException(i);
		}
		
		E tmp = array[j];
		array[j] = array[i];
		array[i] = tmp;
		
	}
	
	private int partitionNonDecreasing(E[] a, int lo, int hi) {
		
		int i = lo;
        int j = hi + 1;
        
        E v = a[lo];
        while (true) { 

            // find item on lo to swap
            while (lessThan(a[++i], v))
                if (i == hi) break;

            // find item on hi to swap
            while (lessThan(v, a[--j]))
                if (j == lo) break;      // redundant since a[lo] acts as sentinel

            // check if pointers cross
            if (i >= j) break;

            swap(a, i, j);
        }

        // put v = a[j] into position
        swap(a, lo, j);

        // with a[lo .. j-1] <= a[j] <= a[j+1 .. hi]
        return j;
        
	}
	
	
	private int partitionNonIncreasing(E[] a, int lo, int hi) {
		
		int i = lo;
        int j = hi + 1;
        
        E v = a[lo];
        while (true) { 

            // find item on lo to swap
            while (greaterThan(a[++i], v))
                if (i == hi) break;

            // find item on hi to swap
            while (greaterThan(v, a[--j]))
                if (j == lo) break;      // redundant since a[lo] acts as sentinel

            // check if pointers cross
            if (i >= j) break;

            swap(a, i, j);
        }

        // put v = a[j] into position
        swap(a, lo, j);

        // with a[lo .. j-1] <= a[j] <= a[j+1 .. hi]
        return j;
        
	}
	
	
	private boolean lessThan(E o1, E o2){
		
		assert(o1 != null && o2 != null && comparatorInstance != null);
		
		return (comparatorInstance.compare(o1,o2) < 0);
	}
	
	private boolean greaterThan(E o1, E o2){
		
		assert(o1 != null && o2 != null);
		
		return (comparatorInstance.compare(o1,o2) > 0);
	}
	

		
	public boolean isSorted(E[] array, SortingOrder order){
		
		int lo = 0, hi = array.length - 1;
		
		if(order == SortingOrder.NON_DECREASING_ORDER){
			for(int i = lo + 1; i <= hi; i++)
	            if (lessThan(array[i], array[i-1])) 
	            	return false;
		}
        else{
        	for(int i = lo + 1; i <= hi; i++)
	            if (greaterThan(array[i], array[i-1])) 
	            	return false;
        }
        
		return true;
	}

	

	

	
	
    
}
