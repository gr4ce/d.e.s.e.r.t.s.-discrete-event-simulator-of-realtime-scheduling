package edu.unibo.ciri.util.common;

import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;


public class GroupButtonUtil {

    public static String getSelectedButtonText(ButtonGroup buttonGroup) {
    	
        for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {
                return button.getText();
            }
        }

        return null;
    }
}