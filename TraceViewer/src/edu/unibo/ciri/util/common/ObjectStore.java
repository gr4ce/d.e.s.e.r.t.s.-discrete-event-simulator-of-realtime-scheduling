package edu.unibo.ciri.util.common;


public interface ObjectStore<E> {
	
	public E getInstance();	
	public void free(E instance);
	
}
