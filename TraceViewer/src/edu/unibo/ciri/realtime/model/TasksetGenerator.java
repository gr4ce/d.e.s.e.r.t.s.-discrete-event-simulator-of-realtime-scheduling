package edu.unibo.ciri.realtime.model;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import edu.unibo.ciri.collection.PriorityLinkedList;
import edu.unibo.ciri.realtime.model.time.Time;




@SuppressWarnings("unused")
public class TasksetGenerator {
	
	public enum DeadlineModel{IMPLICIT, CONSTRAINED, ARBITRARY};
	public enum TaskWeight{LIGHT, MODERATE, HEAVY};
	
	
	/*
	 * Predisporre i getters e setters asap
	 */
	public static double minLightWeight = 0.001;
	public static double maxLightWeight = 0.2;
	
	public static double minModerateWeight = 0.1;
	public static double maxModerateWeight = 0.4;
	
	public static double minHeavyWeight = 0.4;
	public static double maxHeavyWeight = 0.95;
		
	
	private DeadlineModel deadlineModel = DeadlineModel.IMPLICIT;
	
	private int numberOfTask;
	private boolean exactNumberOfTask;
	private double utilization;
	private boolean exactUtilization;
	
	private double minPeriod;
	private double maxPeriod;
	private double minDead;
	private double maxDead;
	private int Implicit;
	
	
	private TimeUnit periodResolution;
	private TimeUnit deadlineResolution;
	private TimeUnit computationResolution;
	
	
	
	public TasksetGenerator(){
		periodResolution = Time.getResolution();
		deadlineResolution = Time.getResolution();
		computationResolution = Time.getResolution();
	}
	
	private double getMaxBound(TaskWeight weight){
		switch(weight){
			case LIGHT:
				return maxLightWeight;
			case MODERATE:
				return maxModerateWeight;
			case HEAVY:
				return maxHeavyWeight;
			default:
				throw new NullPointerException("Wrong task weight: "+weight.toString());
				
		}
	}
	
	private double getMinBound(TaskWeight weight){
		switch(weight){
			case LIGHT:
				return minLightWeight;
			case MODERATE:
				return minModerateWeight;
			case HEAVY:
				return minHeavyWeight;
			default:
				throw new NullPointerException("Wrong task weight: "+weight.toString());
				
		}
	}

	
	public TaskSet generateTaskSet(int number_of_task, double utilization_factor, double min_period, double max_period, double min_dead, double max_dead, int implicit){
		
		numberOfTask = number_of_task;
		utilization = utilization_factor;
		minPeriod = min_period;
		maxPeriod = max_period;	
		minDead = min_dead;
		maxDead = max_dead;
		Implicit = implicit;
		Task t;
		
		class RMTask implements Comparable<RMTask>{
			
			private Task t;
			
			public RMTask(Task t){
				this.t = t;
			}
			
			public Task getTask(){
				return t;
			}
			
			public Time getPeriod(){
				return t.getTaskParameters().getPeriod();
			}

			@Override
			public int compareTo(RMTask task) {
				long thisPeriod = this.getPeriod().getTimeValue();
				long otherPeriod = task.getPeriod().getTimeValue();
				if(thisPeriod < otherPeriod)
					return 1;
				else return -1;
			}
			
		}; 
				
		double []vectUtilization = new double[numberOfTask];		
		TaskSet ts = new TaskSet(numberOfTask);		
		Task toInsert;
		ArrayList<TaskParameters> tp = new ArrayList<TaskParameters>();
		PriorityLinkedList<RMTask> list = new PriorityLinkedList<RMTask>();
		TaskParameters task_param = new TaskParameters();
		
		list.clear();
		
		boolean ok = false;
		boolean ok2 = true;
		int counter = 0;
		if (Implicit == 0) {
			while (ok == false) {
				vectUtilization = generateVectorUtilization();
				ok = true; 
				for(int i = 0; i < numberOfTask; i++){				
					if (vectUtilization[i] > 1){
						ok = false;
					}
				}
			}				
			for(int i = 0; i < numberOfTask; i++){
				tp.add(generateTask(vectUtilization[i]));			
				t = new Task("T"+i);
				t.setTaskParameters(tp.get(i));
				list.orderedInsert(new RMTask(t));
			}
		}
		else {
			while ( ok == false) {
				vectUtilization = generateVectorUtilization();
				ok = true;				
				for (int i = 0; i < numberOfTask && ok2; i++){
					ok2=false;
					counter = 0;
					tp.add(generateTask(vectUtilization[i]));
					while (ok2 == false && counter < 100) {
						ok2 = true;
						if (tp.get(i).getComputation().getTimeValue() > tp.get(i).getDeadline().getTimeValue()) {
							ok2 = false;
							counter++;
							tp.set(i,generateTask(vectUtilization[i]));
						}
					}				
				}
				if (counter == 100) {
					ok = false;
				}
			}
			for(int i = 0; i < numberOfTask; i++){			
				t = new Task("T"+i);
				t.setTaskParameters(tp.get(i));
				list.orderedInsert(new RMTask(t));
			}
			
		}
		
		for(int i = 0; i < numberOfTask; i++){
			toInsert = list.popHead().getTask();
			task_param = toInsert.getTaskParameters();
			task_param.setStaticPriority(numberOfTask - i);
			task_param.setThreshold(numberOfTask - i);
			ts.addTask(toInsert);
		}
		
		return ts;
	}
	
	
	private double[] generateVectorUtilization() {
		double sumUtilizzation = utilization;
		double nextSumUtilization;
		double []vectorUtilization = new double[numberOfTask];
		
		for (int i = 0; i < numberOfTask - 1; i++){
			nextSumUtilization = sumUtilizzation * (Math.pow(Math.abs(Math.random()), 1./(numberOfTask - i)));
			vectorUtilization[i] = sumUtilizzation - nextSumUtilization;
			sumUtilizzation = nextSumUtilization;
		}
		vectorUtilization[numberOfTask-1] = sumUtilizzation;
		return vectorUtilization;
	}
	
	
	private TaskParameters generateTask(double utilization_factor){
		
		double rangePeriod = maxPeriod - minPeriod;
		double rangeDead = maxDead - minDead;
		double convPeriodToMicro;
		double convCompToMicro;
		long generatedPeriod = (long)(Math.abs(Math.random()) * rangePeriod + minPeriod);
		long generatedDead;
		if (Implicit==0) {
			generatedDead = generatedPeriod;
		}
		else {			
			generatedDead = (long)(Math.abs(Math.random()) * rangeDead + minDead);
		}
		
		Time period = new Time(generatedPeriod, periodResolution);
		long generatedComputation = (long)(period.getTimeValue(computationResolution) * utilization_factor);
		TaskParameters tp = new TaskParameters();
		tp.setPeriod(generatedPeriod,periodResolution);
		tp.setComputation(generatedComputation,computationResolution);
		tp.setDeadline(generatedDead,deadlineResolution);	
		
		return tp;
	}
	
	
	public void setExactNumberOfTasks(int nTask){
		numberOfTask = nTask;
		exactNumberOfTask = true;
	}
	
	public void setMaxNumberOfTasks(int nTask){
		numberOfTask = nTask;
		exactNumberOfTask = false;
	}
	
	public void setExactUtilization(double util){
		utilization = util;
		exactUtilization = true;
	}
	
	public void setMaxUtilization(double util){
		utilization = util;
		exactUtilization = false;
	}
	
	public void setPeriodRange(Time tMin, Time tMax){
		
		assert( tMin.getTimeValue() < tMax.getTimeValue());
		
		minPeriod = tMin.getTimeValue();
		maxPeriod = tMax.getTimeValue();		
	}

	public TimeUnit getPeriodResolution() {
		return periodResolution;
	}

	public void setPeriodResolution(TimeUnit periodResolution) {
		this.periodResolution = periodResolution;
	}

	public TimeUnit getDeadlineResolution() {
		return deadlineResolution;
	}

	public void setDeadlineResolution(TimeUnit deadlineResolution) {
		this.deadlineResolution = deadlineResolution;
	}

	public TimeUnit getComputationResolution() {
		return computationResolution;
	}

	public void setComputationResolution(TimeUnit computationResolution) {
		this.computationResolution = computationResolution;
	}
	
}
