package edu.unibo.ciri.realtime.model;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import edu.unibo.ciri.realtime.model.time.Time;



public class TaskSet {
	
	private Task taskSet[];
	private ArrayList<Task> taskset = new ArrayList<Task>();
	
	private double utilization = -1;
	private double density = 0.0;
	
	private String utilizationString = null;
	
	private Time hyperperiod = null;
	private boolean isHyperperiodUptdate = false;
	
	private String tasksetName = "default_taskset_name";
	
	private int currentTaskId = 0;
	
	
	
	public TaskSet(){
	}
	
	public TaskSet(int numberOfTask){
		taskset = new ArrayList<Task>(numberOfTask);
	}
	
	public TaskSet(Task tasks[]){
		taskSet = tasks;
	}
	
	public int size(){
		return taskset.size();
	}
	
	public void addTask(Task t){
		t.setTaskId(currentTaskId++);
		taskset.add(t);
	}
	
	public boolean removeTask(Task t){
		if(taskset.remove(t)){
			updateTasksetData(t);
			return true;
		}
		return false;		
	}
	
	public boolean removeTask(int taskId){
		
		Task curr;
		for(int i = 0; i < taskset.size(); i++){
			curr = taskset.get(i);
			if(curr.getTaskId() == taskId){
				taskset.remove(curr);
				isHyperperiodUptdate = false;
				updateTasksetData(curr);
				return true;
			}
		}
		return false;
		
	}
	
	public boolean removeTask(String taskName){
		
		Task curr;
		for(int i = 0; i < taskset.size(); i++){
			curr = taskset.get(i);
			if(curr.getName().equals(taskName)){
				taskset.remove(curr);
				updateTasksetData(curr);
				return true;
			}
		}
		return false;
		
	}
	
	
	
	public Task getTask(int index){
		return taskset.get(index);
	}
	
	public Task getTask(String name){
		
		Task current;
		for(int k = 0; k < taskset.size(); k++){
			current = taskset.get(k);
			if(current.getName().equals(name))
				return current;
		}
		return null;
	}
	
	public ArrayList<Task> getArrayList(){
		return this.taskset;
	}
	
	public Task[] getTaskWithPriorityGreaterThenThreshold(int index){
		
		ArrayList<Task> tasks = new ArrayList<Task>();
		//modificato
		int thresholdLevel = taskset.get(index).getTaskParameters().getThreshold();
		
		Task current;
		//modificato
		for(int k = 0; k < taskset.size(); k++){
			//modificato
			current = taskset.get(k);
			int prio = current.getTaskParameters().getStaticPriority();
			if(prio > thresholdLevel){
				tasks.add(current);
			}
		}
		
		return tasks.toArray(new Task[tasks.size()]);
		
	}
	
	public Time getHyperperiod(){
		
		if(isHyperperiodUptdate)
			return this.hyperperiod;
		
		hyperperiod = new Time(taskset.get(0).getTaskParameters().getPeriod().getTimeValue(TimeUnit.MILLISECONDS),TimeUnit.MILLISECONDS);
		
		if(taskset.size() == 1)
			return hyperperiod;
		
		long longHp = hyperperiod.getTimeValue(TimeUnit.MILLISECONDS);
		
		for(int k = 1; k < taskset.size(); k++)
			longHp = lcm(longHp, taskset.get(k).getTaskParameters().getPeriod().getTimeValue(TimeUnit.MILLISECONDS));
		
		hyperperiod.setTimeValue(longHp,TimeUnit.MILLISECONDS); 
		isHyperperiodUptdate = true;
		
		return hyperperiod; 
		
	}
	
	public double getUtilization(){

		long computation;
		long period;
		
		if(utilization == -1){
			utilization = 0;
			for(int i = 0; i < taskset.size(); i++){
				computation = taskset.get(i).getTaskParameters().getComputation().getTimeValue();
				
				if(computation == 0){
					computation = taskset.get(i).getExecutionPattern().getTotalExecutionRequirement().getTimeValue();
				}
				period      = taskset.get(i).getTaskParameters().getPeriod().getTimeValue();
				utilization += computation  / (double)period;				
			}
		}
		
		return utilization;
	}
	
	public int getMaxPriority(){
		
		int max = -1;
		int currentPriority;
		
		for(int i = 0; i < taskset.size(); i++){
			currentPriority = taskset.get(i).getTaskParameters().getStaticPriority();
			if(currentPriority > max)
				max = currentPriority;
		}
		
		return max;
	}
	
	private long lcm(long a, long b){
		return Math.abs(a*b)/gcd(a,b);
	}
	
	private long gcd(long a, long b){
		if(b == 0)
			return a;
		return gcd(b, a % b);
	}
	
	public void perturbExecutionTime(){
		for(int i = 1; i < taskSet.length; i++){
			taskSet[i].getTaskParameters().enableComputationJitter();
		}
		
	}
	
	
	private void updateTasksetData(Task t){
		
		double computation = t.getTaskParameters().getComputation().getTimeValue();
		double period = t.getTaskParameters().getPeriod().getTimeValue();
		double deadline = t.getTaskParameters().getDeadline().getTimeValue();
		this.utilization -= computation / period;
		this.setDensity(this.getDensity() - computation /deadline);		
	}
	
	
	public TaskSet clone(){
		
		TaskSet cloneTaskSet = new TaskSet(this.size());
		Task cloneTask;
		
		for(int i = 0; i < size(); i++){
			cloneTask = new Task(taskSet[i].getName());
			cloneTask.setTaskParameters(taskSet[i].getTaskParameters());
			cloneTaskSet.addTask(cloneTask);
		}
		
		return cloneTaskSet;
	}

	public void setTasksetName(String tasksetName) {
		this.tasksetName = tasksetName;
	}

	public String getTasksetName() {
		return tasksetName;
	}
	
	public String toString(){
		
		StringBuffer sb = new StringBuffer(50 * size());
		for(int i = 0; i < size(); i++){
			Task t = taskset.get(i);
			sb.append( t);
//			sb.append( t.getTaskId() + " ");
//			sb.append( t.getTaskParameters().toString() + "\n");
		}
		
		return sb.toString();
	}

	public String getUtilizationString() {
		return utilizationString;
	}

	public void setUtilizationString(String utilizationString) {
		this.utilizationString = utilizationString;
	}

	public double getDensity() {
		return density;
	}

	public void setDensity(double density) {
		this.density = density;
	}
	
	public ArrayList<String> getUsedResoruces(){
		ArrayList<String> res = new ArrayList<String>();
		for(Task t: taskset){
			for(String s: t.getUsedResources()){
				if(!res.contains(s)){
					res.add(s);
				}
			}
		}
		return res;
	}


	
	
}
