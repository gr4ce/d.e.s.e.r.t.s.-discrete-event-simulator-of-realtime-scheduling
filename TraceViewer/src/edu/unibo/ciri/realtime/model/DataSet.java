package edu.unibo.ciri.realtime.model;

import java.util.ArrayList;
import java.util.Iterator;


import edu.unibo.ciri.desert.config.StaticConfig;


public class DataSet implements Iterator<TaskSet>{	
	
	private String 	name = "none";
	protected ArrayList<TaskSet> elements;	
	
	
	protected int currentTassketIndex = 0;
	
	
	public DataSet(){
		elements = new ArrayList<TaskSet>();
	}
	
	public DataSet(int initalCapacity){
		elements = new ArrayList<TaskSet>(initalCapacity);
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public int size(){
		return elements.size();
	}
		
	public TaskSet getTaskSet(int index){
		return elements.get(index);
	}
	
	public void addTaskSet(TaskSet ts){
		elements.add(ts);
	}	

	@Override
	public boolean hasNext() {
		
		int bound = Math.min(elements.size(), 500);
		
		if(currentTassketIndex < bound)
			return true;
		
		return false;
	}

	@Override
	public TaskSet next() {
		if(StaticConfig.PRINT_DATASET_INFO)
		System.out.println("\t\t\t[DATASET MANAGER] Requested "+elements.get(currentTassketIndex).getTasksetName() + " U=" +elements.get(currentTassketIndex).getUtilizationString());
		return  elements.get(currentTassketIndex++);		
	}

	@Override
	public void remove() {	
		
	}
	
}