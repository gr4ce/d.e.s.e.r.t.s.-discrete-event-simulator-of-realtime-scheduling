package edu.unibo.ciri.realtime.model;


import java.util.ArrayList;
import java.util.LinkedList;

import edu.unibo.ciri.collection.DoubleLinkedList;
import edu.unibo.ciri.realtime.model.Job.JobState;

public class Task implements Comparable<Task>{
	
	
	private String name;	
	private int taskId;
	private LinkedList<Job> executedJobs;
	private int currentStoredJob = 0;
	protected DoubleLinkedList<Job> activeJobs;
	private int nominalPriority;
	private TaskParameters taskParameters;
	private long worstCaseResponseTime = -1;
	private int worstCaseInstance = -1;
	private int lastCpu;
	private int staticPriority;
	private String taskPseudoCode;
	private String taskPlatformSpecCode;
	private ExecutionPattern ep = null;
	private ArrayList<String> usedResources = new ArrayList<String>();
	
	public Task(String name){
		this.setName(name);
		activeJobs = new DoubleLinkedList<Job>();
	}
	
	public int getTaskId(){
		return taskId;
	}
	
	public int getLastCpu(){
		return lastCpu;
	}
	
	public void setLastCpu(int n){
		this.lastCpu=n;
	}
	 	
	public void setTaskId(int id){
		this.taskId = id;
	}
	
	public LinkedList<Job> getExecutedJobs() {
		return executedJobs; 
	}
	
	public int getExecutedJobsLenght(){
		return currentStoredJob;
	}
	
	public Job getActiveJob(){
		return activeJobs.getHead();
	}
	
	public Job getNextActiveJob(Job j){
		return activeJobs.getNext(j);
	}

	public int getNominalPriority() {
		return nominalPriority;
	}

	public void setNominalPriority(int nominalPriority) {
		this.nominalPriority = nominalPriority;
	}

	public TaskParameters getTaskParameters() {
		return taskParameters;
	}

	public void setTaskParameters(TaskParameters taskParameters) {
		this.taskParameters = taskParameters;
	}

	public boolean updateWorstCaseResponseTime(long responseTime){
		if(responseTime > worstCaseResponseTime){
			worstCaseResponseTime = responseTime;		
			return true;
		}
		return false;
	}
	
	public long getWorstCaseResponseTime(){
		return this.worstCaseResponseTime;
	}
	
	public String getName(){
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void releaseNewJob(Job releasedJob){		
		int prio = this.getTaskParameters().getStaticPriority();				
		releasedJob.setCurrentPriority(prio);
		releasedJob.setState(JobState.INITIALIZED);
		activeJobs.enqueue(releasedJob);	
	}
	
	public void completeCurrentJob(){
		activeJobs.popHead();
	}
	
	
	public boolean hasPendingJobs(){
		return activeJobs.size() > 1 ? true : false;
	}
			
	public String toString(){
		String s = getName() + " ";
		if(taskParameters != null)
			s += taskParameters.toString();
		s += taskPseudoCode;
		return s;
	}
	
	public void setWorstCaseInstance(int ins){
		worstCaseInstance = ins;
	}
		
	public int getWorstCaseInstance(){
		return worstCaseInstance;
	}
	
	public String executedJobsToString(){
		StringBuffer sb = new StringBuffer();
		sb.append(this.name+"\n");
		for(Job j : executedJobs){
			sb.append("  job("+j.getProgInstanceNumber()+")\n");
			sb.append("    a = "+j.getInfo().getActivationTime()+"\n");
			sb.append("    s = "+j.getInfo().getStartTime()+"\n");
			for(int k = 0; k < j.getInfo().getPreemptionTime().length; k++){
				sb.append("     p = "+(j.getInfo().getPreemptionTime())[k]+"\n");
				sb.append("     r = "+(j.getInfo().getResumeTime())[k]+"\n");
			}
			sb.append("    f = "+j.getInfo().getFinishingTime()+"\n");			
		}
		return sb.toString();
	}

	@Override
	public int compareTo(Task o) {
		if(staticPriority < o.getStaticPriority())
			return 1;
		if(staticPriority > o.getStaticPriority())
			return -1;
		return 0;
	}

	public int getStaticPriority() {
		return staticPriority;
	}

	public void setStaticPriority(int staticPriority) {
		this.staticPriority = staticPriority;
	}

	public String getTaskPlatformSpecCode() {
		return taskPlatformSpecCode;
	}

	public void setTaskPlatformSpecCode(String taskPlatformSpecCode) {
		this.taskPlatformSpecCode = taskPlatformSpecCode;
	}

	public String getTaskPseudoCode() {
		return taskPseudoCode;
	}

	public void setTaskPseudoCode(String taskPseudoCode) {
		this.taskPseudoCode = taskPseudoCode;
		ep = new ExecutionPattern();
		ep.importTaskCode(taskPseudoCode);
		usedResources = ep.getUsedResources();
	}
	
	public ExecutionPattern getExecutionPattern(){
		return ep;
	}
	
	public ArrayList<String> getUsedResources() {
		return usedResources;
	}

	
	
}
