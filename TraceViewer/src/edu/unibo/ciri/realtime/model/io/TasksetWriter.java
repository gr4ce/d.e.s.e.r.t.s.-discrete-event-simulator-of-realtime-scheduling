package edu.unibo.ciri.realtime.model.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskParameters;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.model.time.Time;
import edu.unibo.ciri.rtmanager.application.tabletest.RTWindow;

public class TasksetWriter {
	
	private File f;
	
	private PrintWriter pw;
	private FileOutputStream fos;
	
	private StringBuffer header = new StringBuffer("");
	
	private String actualName;
	private String separator = System.getProperty("file.separator");
	
	
	public TasksetWriter(String dirName, String fileName) {
		int count = 0;
		actualName = dirName + separator;
		f = new File(actualName);
		
		if(!f.exists()) 
			f.mkdir();
	
		
		actualName = actualName + fileName + ".dat";
		
		f = new File(actualName);		
		
		
	
		try {
			System.out.println(actualName);
			if( !f.exists() )
				f.createNewFile();
			fos = new FileOutputStream(f);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		pw = new PrintWriter(fos);
	}
	
	public TasksetWriter(File file, TaskSet ts) {
		
		if(file.exists()){
			int answer = JOptionPane.showConfirmDialog(RTWindow.frame, "The file already exists, do you want to overwrite it?", "Save taskset", JOptionPane.YES_NO_OPTION);
			if(answer != JOptionPane.YES_OPTION)
				return;
		} else{
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		FileWriter fw;		
		try {
			fw = new FileWriter(file);
			fw.write("BEGIN TASKSET " + ts.getTasksetName() + "\n");
			
			for(Task t: ts.getArrayList()){
				TaskParameters tp = t.getTaskParameters();
				String taskString = t.getName() + ",";
				taskString += "MODEL PERIODIC,";
				taskString += "OFFSET " + tp.getOffset().getValueInPreferredTimeUnitRepresentation() + " " + Time.getTimeUnitString(tp.getOffset().getPreferredTimeUnit()) + "," ;
				taskString += "PERIOD " + tp.getPeriod().getValueInPreferredTimeUnitRepresentation() + " " + Time.getTimeUnitString(tp.getPeriod().getPreferredTimeUnit()) + ",";
				taskString += "DEADLINE " + tp.getDeadline().getValueInPreferredTimeUnitRepresentation() + " " + Time.getTimeUnitString(tp.getDeadline().getPreferredTimeUnit()) + ",";
				StringTokenizer st = new StringTokenizer(t.getTaskPseudoCode(), "\n");
				taskString += "EXECUTION PATTERN ";
				while(st.hasMoreTokens()){
					taskString += st.nextToken() + " ";
				}
				taskString += "\n";
				fw.write(taskString);
			}
			
			fw.write("END TASKSET\n");
			fw.flush();
			fw.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void appendToHeader(String s){
		header.append(s);
	}	
	
	public void write(TaskSet ts){
		
		StringBuffer buff = new StringBuffer();
		
		buff.append("#N_TASK "+ts.size() + "\n");
//		buff.append("#UTIL "+ utilizationFormat.format(ts.getUtilization()) + "\n");
		buff.append("BEGIN "+ts.getTasksetName() + "\n");
		Task task;
		TaskParameters taskParam;
		for(int i = 0; i < ts.size(); i++){
			
			task = ts.getTask(i);
			taskParam = task.getTaskParameters();
			
			buff.append(task.getName());
			buff.append(",");
			buff.append(taskParam.getPeriod().getTimeValue(TimeUnit.MILLISECONDS));
			buff.append(",");
			buff.append(taskParam.getDeadline().getTimeValue(TimeUnit.MILLISECONDS));
			buff.append(",");
			double computationInMillisec = taskParam.getComputation().getTimeValue(TimeUnit.MICROSECONDS)/1000F; 
//			buff.append(computationFormat.format(computationInMillisec));
			buff.append("\n");
		}
		buff.append("END\n\n");
		
		pw.append(buff.toString());		
	} 	
	
	public void close(){
		pw.close();
	}

	
}

