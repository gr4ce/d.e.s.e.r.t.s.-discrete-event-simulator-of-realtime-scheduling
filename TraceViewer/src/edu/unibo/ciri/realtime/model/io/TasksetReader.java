package edu.unibo.ciri.realtime.model.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskParameters;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.model.time.Time;

public class TasksetReader {
	
	
	public static TaskSet read(File fileToOpen){
		
		boolean inTaskset = false;
		
		TaskSet ts = new TaskSet();
		Task t;
		TaskParameters tp;
		
		FileInputStream fis;
		InputStreamReader isr;
		String bufferString;
		
		/*
		 * Preparing the outpu file 
		 */
		BufferedReader br;
		FileWriter output; 		
		
		try {
			fis = new FileInputStream(fileToOpen);
			isr = new InputStreamReader(fis);
			br = new BufferedReader(isr);
					
			while((bufferString = br.readLine()) != null){												
				
				if(!inTaskset){
					if(bufferString.startsWith("BEGIN")){
						inTaskset = true;
						StringTokenizer tasksetName = new StringTokenizer(bufferString);
						tasksetName.nextToken(); //BEGIN
						tasksetName.nextToken(); //TASKSET
						ts.setTasksetName(tasksetName.nextToken());
					}
				}else{
					if(bufferString.startsWith("END")){
						inTaskset = false;		
						break;
					}else{
						StringTokenizer taskLine = new StringTokenizer(bufferString, ",");
						t = new Task(taskLine.nextToken());
						tp = new TaskParameters();
						t.setTaskParameters(tp);
						
						while(taskLine.hasMoreTokens()){
							String token = taskLine.nextToken();
							StringTokenizer element;
							
							if(token.startsWith("OFFSET")){
								System.out.println(token);
								element = new StringTokenizer(token);
								element.nextToken();
								tp.setOffset(new Time(Long.parseLong(element.nextToken()), element.nextToken()));
							}else if(token.startsWith("PERIOD")){
								System.out.println(token);
								element = new StringTokenizer(token);
								element.nextToken();
								tp.setPeriod(new Time(Long.parseLong(element.nextToken()), element.nextToken()));
							}else if(token.startsWith("DEADLINE")){
								System.out.println(token);
								element = new StringTokenizer(token);
								element.nextToken();
								tp.setDeadline(new Time(Long.parseLong(element.nextToken()), element.nextToken()));
							}else if(token.startsWith("MODEL")){
								
							}else if(token.startsWith("EXECUTION PATTERN")){
								element = new StringTokenizer(token);
								element.nextToken();//EXECUTION
								element.nextToken();//PATTERN
								StringBuffer taskPseudoCode = new StringBuffer();
								while(element.hasMoreTokens()){									
									String first = element.nextToken();
									if(first.equalsIgnoreCase("execute")){
										taskPseudoCode.append(first + " ");
										taskPseudoCode.append(element.nextToken() + " ");
										taskPseudoCode.append(element.nextToken() + "\n");
									}else if(first.equalsIgnoreCase("take")){
										taskPseudoCode.append(first + " ");
										taskPseudoCode.append(element.nextToken() + "\n");
									}else if(first.equalsIgnoreCase("give")){
										taskPseudoCode.append(first + " ");
										taskPseudoCode.append(element.nextToken() + "\n");
									}
								}
								t.setTaskPseudoCode(taskPseudoCode.toString());
								
							}else if(token.startsWith("EXECUTION TIME")){
								
							}
							
						}
						
						ts.addTask(t);
						
					}
				}
			}
			
			br.close();
			isr.close();
			fis.close();			
			
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
		catch (IOException e) {		
			e.printStackTrace();
		}
		
		return ts;
	} 
	
	private Task readTask(String buffer){
		
		Task t = null;
		
		return t;
		
	}

}
