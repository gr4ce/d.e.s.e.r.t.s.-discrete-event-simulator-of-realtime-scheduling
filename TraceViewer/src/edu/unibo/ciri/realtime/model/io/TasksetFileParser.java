package edu.unibo.ciri.realtime.model.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.StringTokenizer;

import edu.unibo.ciri.realtime.model.TaskSet;


public class TasksetFileParser implements Iterator<TaskSet>{
	
	private File sourceDatasetFile = null;
	private String sourceDatasetFileName = null;
	private BufferedReader br;
	private FileInputStream fis;
	private InputStreamReader isr;
	private String bufferString;
	
	private TaskSet currentReaded = null;
	
	public TasksetFileParser(String fileName){
		setFileName(fileName);
	}
	
	public void setFileName(String fileName){
		sourceDatasetFileName = fileName;
		sourceDatasetFile = null;
	}		
		
	public void openTasketFile() {
		
		if(sourceDatasetFile == null){
			sourceDatasetFile = new File(sourceDatasetFileName);
			
			try {
				fis = new FileInputStream(sourceDatasetFile);
				isr = new InputStreamReader(fis);
				br = new BufferedReader(isr);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		
	}
	

	@Override
	public boolean hasNext() {
		
		try {
			while((bufferString = br.readLine()) != null){
				
								
				if(bufferString.startsWith("BEGIN")){
					
					StringTokenizer st = new StringTokenizer(bufferString);
					
					if(!st.hasMoreTokens())
						assert(false);
					assert(st.nextToken().equals("BEGIN"));
					
					if(!st.hasMoreTokens())
						assert(false);
					
					currentReaded = new TaskSet();
					currentReaded.setTasksetName(st.nextToken());
					TaskParser tp = new TaskParser();
					while((bufferString = br.readLine()) != null){
						if(bufferString.startsWith("END"))
							break;
						tp.setSourceLine(bufferString);
						currentReaded.addTask(tp.getTask());
					}
					return true;
				}
				
			}	
			return false;
			
		} catch (IOException e) {			
		}
		return false;
	}

	@Override
	public TaskSet next() {
			
		return currentReaded;
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	}
}