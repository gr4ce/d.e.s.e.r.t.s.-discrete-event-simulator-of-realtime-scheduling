package edu.unibo.ciri.realtime.model.io;

import java.util.ArrayList;
import java.util.Comparator;


import edu.unibo.ciri.realtime.model.DataSet;
import edu.unibo.ciri.realtime.model.TaskSet;



public class SortedDataSet extends DataSet {
	
	
	private Comparator<TaskSet> comparator;
	
	public SortedDataSet(Comparator<TaskSet> comparator){
		this.comparator = comparator;
		elements = new ArrayList<TaskSet>();
	}
	
	@Override
	public void addTaskSet(TaskSet t){
		
		TaskSet ts;
		
		int i;
		
		for(i = 0; i < elements.size(); i++){
			ts = elements.get(i);
			if(comparator.compare(t, ts) >= 0){
				elements.add(i, t);
				return;
			}				
		}
		
		elements.add(i, t);
		
	}
	
	

}
