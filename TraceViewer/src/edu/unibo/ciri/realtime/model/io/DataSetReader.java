package edu.unibo.ciri.realtime.model.io;

import edu.unibo.ciri.realtime.model.DataSet;

public interface DataSetReader {
		
	public void readDataSet();
	
	public void writeDataSet();
	
	public void setDataSet(DataSet ds);
	
	public DataSet getDataSet();

}
