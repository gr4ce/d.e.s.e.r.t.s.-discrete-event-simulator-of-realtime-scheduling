package edu.unibo.ciri.realtime.model.io;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import edu.unibo.ciri.collection.PriorityLinkedList;
import edu.unibo.ciri.desert.application.TraceViewer;
import edu.unibo.ciri.desert.event.EventType;
import edu.unibo.ciri.desert.event.RTAIEvent;
import edu.unibo.ciri.desert.resource.SharedResource;
import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskParameters;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.model.time.Time;


public class TraceFileParser{
	
	private File sourceDatasetFile = null;
	private String sourceDatasetFileName = null;
	private BufferedReader br;
	private FileInputStream fis;
	private InputStreamReader isr;
	private String bufferString;
	private boolean releaseFound = false;
	
	private ArrayList<String> taskNames = new ArrayList<String>();
	private Hashtable<String, Task> tasks = new Hashtable<String, Task>();
	private TaskSet ts = new TaskSet(); 
	
	private ArrayList<String> resourceNames = new ArrayList<String>();
	private Hashtable<String, SharedResource> resources = new Hashtable<String, SharedResource>();
	
	

	private Hashtable<Task, PriorityLinkedList<RTAIEvent>> events = new Hashtable<Task, PriorityLinkedList<RTAIEvent>>();
	
	public TraceFileParser(String fileName){
		setFileName(fileName);
		openTasketFile();
	}
	
	
	
	public Hashtable<String, SharedResource> getResources() {
		return resources;
	}



	public void setFileName(String fileName){
		sourceDatasetFileName = fileName;
		sourceDatasetFile = null;
	}		
		
	public void openTasketFile() {		
		if(sourceDatasetFile == null){
			sourceDatasetFile = new File(sourceDatasetFileName);			
			try {
				fis = new FileInputStream(sourceDatasetFile);
				isr = new InputStreamReader(fis);
				br  = new BufferedReader(isr);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}		
	}
	
	public boolean checkTaskExists(String name){		
		Enumeration<Task> taskNames = events.keys();		
		while(taskNames.hasMoreElements()){			
			String taskName = taskNames.nextElement().getName();
			if(name.equals(taskName))
				return true;			
		}		
		return false;		
	}
	
	public boolean checkSharedResourceExists(String name){		
		Iterator<String> it = resourceNames.iterator();
		int idCounter = 0;
		while(it.hasNext()){			
			String resName = it.next();
			if(name.equals(resName))
				return true;
			idCounter++;
		}		
		resourceNames.add(name);
		JOptionPane.showConfirmDialog(TraceViewer.frame, "Found a new resource in trace file : "+name + "Please select a color", "New resource found", JOptionPane.OK_OPTION);
		Color c = JColorChooser.showDialog(TraceViewer.frame, "Select a color for " + name, Color.magenta);
		resources.put(name, new SharedResource(idCounter, name, c));
		System.out.println("[TraceFileParser] - New resource found: " + name + "(" + idCounter + ")");
		
		return false;		
	}
	
	public EventType identifyEventType(String event){
		
		EventType eventType = EventType.NONE;
		
		if( event.equalsIgnoreCase("job_started") ){
			eventType = EventType.JOB_START;
		} else if( event.equalsIgnoreCase("job_completed") ){
			eventType = EventType.JOB_COMPLETION;
		} else if( event.equalsIgnoreCase("job_release") ){
			eventType = EventType.JOB_RELEASE;
		} else if( event.equalsIgnoreCase("sem_access_requested") ){
			eventType = EventType.SHARED_RESOURCE_REQUEST;
		} else if( event.equalsIgnoreCase("sem_access_granted") ){
			eventType = EventType.SHARED_RESOURCE_ACCESS_GRANTED;
		} else if( event.equalsIgnoreCase("sem_access_denied") ){
			eventType = EventType.SHARED_RESOURCE_ACCESS_DENIED;
		} else if( event.equalsIgnoreCase("sem_released") ){
			eventType = EventType.SHARED_RESOURCE_RELEASE;
		} else if( event.equalsIgnoreCase("application_start") ){
			eventType = EventType.APPLICATION_START;
		} else if( event.equalsIgnoreCase("job_preempted") ){
			eventType = EventType.JOB_PREEMPTION;
		} else if( event.equalsIgnoreCase("application_stop") ){
			eventType = EventType.APPLICATION_STOP;
		} else if( event.equalsIgnoreCase("job_resumed") ){
			eventType = EventType.JOB_RESUME;
		} else if( event.equalsIgnoreCase("APERIODIC_REQUEST_FIRED") ){
			eventType = EventType.JOB_RELEASE;
		}
		
		return eventType;
	}
	
	public Hashtable<String,Task> getTasks(){
		return tasks;
	}

	public ArrayList<String> getTaskNames(){
		return taskNames;
	}
	
	public ArrayList<String> getResourceNames(){
		return resourceNames;
	}
	
	
	
	private void parseTaskSection(){
		try {
			while(!(bufferString = br.readLine()).equals("END_SECTION")){
				
				StringTokenizer st = new StringTokenizer(bufferString);
				String taskName;
				Time period,deadline,offset;
				
				/*
				 * Reading task name 
				 */
				taskName = st.nextToken();				
				
				/*
				 * Reading period
				 */
				String tmp = st.nextToken();
				period = new Time(Long.parseLong(tmp), TimeUnit.NANOSECONDS);
				
				/*
				 * Reading dealine
				 */				
				tmp = st.nextToken();
				deadline = new Time(Long.parseLong(tmp), TimeUnit.NANOSECONDS);
				
				/*
				 * Reading offset
				 */
				tmp = st.nextToken();
				offset = new Time(Long.parseLong(tmp), TimeUnit.NANOSECONDS); 
				
				/*
				 * Creating task
				 */
				Task t = new Task(taskName);
				TaskParameters tp = new TaskParameters();
				tp.setPeriod(period);
				tp.setDeadline(deadline);
				tp.setOffset(offset);
				t.setTaskParameters(tp);
				
				taskNames.add(taskName);
				tasks.put(taskName, t);
				ts.addTask(t);
				events.put(t, new PriorityLinkedList<RTAIEvent>());
			}
			
		}catch(IOException e){
			System.out.println("ERROR: in parsing task section");
		}
	}
	
	public TaskSet getTaskset(){
		return ts;
	}
	
	private void parseEventSection(){
		
		try {
			
			Task all = new Task("ALL");
			tasks.put("ALL", all);
			events.put(all, new PriorityLinkedList<RTAIEvent>());
			RTAIEvent toBeInserted;	
			while(!(bufferString = br.readLine()).equals("END_SECTION")){
				
				toBeInserted = new RTAIEvent();		
				StringTokenizer st = new StringTokenizer(bufferString);
				
				/*
				 * Parsing event type 
				 */
				String eventTypeName = st.nextToken();
				EventType eventType  = identifyEventType(eventTypeName);
				
				/*
				 * Reading task name 
				 */
				String taskName = st.nextToken();				
				
				if(taskName.equals("MAIN") && eventType != EventType.APPLICATION_STOP)
					continue;
				
				/*
				 * Parsing even time 
				 */
				String timeString = st.nextToken();
				long value = Long.parseLong(timeString);		
				Time eventTime = new Time(value);
				
				
				toBeInserted.setName(taskName);
				toBeInserted.setTask(tasks.get(taskName));
				toBeInserted.setTime(eventTime);
				toBeInserted.setType(eventType);				
				
				String resName;
				
				switch(eventType){
					case JOB_START:
						eventTime = new Time(value + 5000);
						toBeInserted.setTime(eventTime);
						break;
					case JOB_WAIT:
						releaseFound = true;
						break;
					case SHARED_RESOURCE_REQUEST:
						resName = st.nextToken();
						checkSharedResourceExists(resName);
						toBeInserted.setSharedResource(resources.get(resName));
						break;
					case SHARED_RESOURCE_ACCESS_GRANTED:
						resName = st.nextToken();
						toBeInserted.setSharedResource(resources.get(resName));
						break;
					case SHARED_RESOURCE_RELEASE:
						resName = st.nextToken();
						toBeInserted.setSharedResource(resources.get(resName));
						break;
					case SHARED_RESOURCE_ACCESS_DENIED:
						resName = st.nextToken();
						toBeInserted.setSharedResource(resources.get(resName));
						break;
					default:
						//doNothing						
				}
				
				
				System.out.println(toBeInserted);
				if(!taskName.equals("MAIN"))
					events.get(tasks.get(taskName)).orderedInsert(toBeInserted);
				events.get(all).orderedInsert(toBeInserted.clone());
				
			}
			
		}catch(IOException e){
			System.out.println("ERROR: in parsing task section");
			System.exit(-1);
		}
	}
	
	public Hashtable<Task, PriorityLinkedList<RTAIEvent>> parse() {				
		try {
					
			while((bufferString = br.readLine()) != null){							
				
				StringTokenizer st = new StringTokenizer(bufferString);
				
				/* At this level you have to find a SECTION definition with a name */
				String sectionString = st.nextToken();
				System.out.println(sectionString);
				if(!sectionString.equals("SECTION"))
					throw new IOException("Section string not present!");
				
				sectionString = st.nextToken();
				if(sectionString.equals("TASK_INFO")){
					parseTaskSection();
				}else if(sectionString.equals("TRACED_EVENTS")){
					parseEventSection();
				}else{
					throw new IOException("section not recognized");
				}				
			}				
		}catch(IOException e){
			System.out.print("ERROR: in parsing sections, ");
			System.out.println(e.getMessage());
		}	
		
		return events;
		
	}

	public boolean hasFoundRelease() {
		return releaseFound;
	}





}