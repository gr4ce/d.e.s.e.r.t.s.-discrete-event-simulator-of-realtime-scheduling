package edu.unibo.ciri.realtime.model.io;

import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskParameters;

public class TaskParser {
	
	StringTokenizer st; 
	Task task;
	TaskParameters parameters;
	
	public TaskParser(){}
	
	public TaskParser(String sourceLine){
		setSourceLine(sourceLine);
	}
	
	public void setSourceLine(String sourceLine){
		st = new StringTokenizer(sourceLine,",");
		
	}
	
	public Task getTask(){
		String token = "<not set>";
		
		//name
		if(st.hasMoreTokens()){
			token = st.nextToken();			
		}
		task = new Task(token);
		parameters = new TaskParameters();				
		
		//period
		if(st.hasMoreTokens()){
			token = st.nextToken();
//			double period = Double.parseDouble(token);
//			long   longp = (long)(period*1000);
//			parameters.setPeriod(longp,TimeUnit.MICROSECONDS);
			parameters.setPeriod(Long.parseLong(token), TimeUnit.MILLISECONDS);
		}
		
		//deadline
		if(st.hasMoreTokens()){
			token = st.nextToken();
//			double deadline = Double.parseDouble(token);
//			long   longd = (long)(deadline*1000);
//			parameters.setDeadline(longd,TimeUnit.MICROSECONDS);
			parameters.setDeadline(Long.parseLong(token), TimeUnit.MILLISECONDS);
		}
		
		//execution
		if(st.hasMoreTokens()){
			token = st.nextToken();
			double exec = Double.parseDouble(token);
//			System.out.println(exec);
			long   longexec = (long)(exec*1000);
//			System.out.println(longexec);
			parameters.setComputation(longexec,TimeUnit.MICROSECONDS);
		}
		
		//utlization
		if(st.hasMoreTokens()){
			//useless
			token = st.nextToken();
		}
		
		task.setTaskParameters(parameters);
		return task;
		
	}

}
