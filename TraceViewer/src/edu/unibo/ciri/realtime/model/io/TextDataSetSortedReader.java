package edu.unibo.ciri.realtime.model.io;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.StringTokenizer;

import edu.unibo.ciri.realtime.model.DataSet;
import edu.unibo.ciri.realtime.model.TaskSet;

public class TextDataSetSortedReader implements DataSetReader {

	private SortedDataSet ds = null;
	private String  fileName = null;
	
	private Comparator<TaskSet> c = new Comparator<TaskSet>(){

		@Override
		public int compare(TaskSet arg0, TaskSet arg1) {
			
			long hyp0 = arg0.getHyperperiod().getTimeValue();
			long hyp1 = arg1.getHyperperiod().getTimeValue();
			
			if(hyp0 > hyp1)
				return -1;
			
			if(hyp0 < hyp1)
				return 1;
			
			return 0;
		}
		
	};
	
	public void setFileName(String fileName){
		this.fileName = fileName;
	}
	
	public DataSet getDataSet(){
		return ds;
	}
	
	public void setDataSet(DataSet dataset){
		if(!(dataset instanceof SortedDataSet))
			throw new ClassCastException();
		this.ds = (SortedDataSet)dataset;
	}
	
	@Override
	public void writeDataSet() {
		
	}

	@Override
	public void readDataSet() {
						
		FileInputStream	  fis;
		InputStreamReader isr;
		BufferedReader 	  br;
		
		String bufferString;
		String paramString;
		
		StringTokenizer st;
				
			
		try {
			
			fis = new FileInputStream(fileName);
			isr = new InputStreamReader(fis);
			br  = new BufferedReader(isr);	
			
			
//			while((bufferString = br.readLine()) != null){
//				
//				if(bufferString.startsWith("@")){
//					
//					paramString = bufferString.substring(1, bufferString.length());
//					
//					if(paramString.startsWith("SIZE")){
//						
//						st = new StringTokenizer(paramString);
//						
//						//Verifichiamo la presenza del token SIZE
//						if(!st.hasMoreTokens())
//							assert(false);						
//						assert(st.nextToken().equals("SIZE"));
//						
//						//Verifichiamo la presenza del token relativo al valore del paramtero SIZE
//						if(!st.hasMoreTokens())
//							assert(false);
//						
//						size = Integer.parseInt(st.nextToken());
//						ds = new DataSet(size); 
//						break;						
//					}			
//					
//					
//					continue;
//				}
//				break;
//			}
			
					
			if(ds == null)
				ds = new SortedDataSet(c);
			
			TaskSet currentReaded = new TaskSet();
			
			while((bufferString = br.readLine()) != null){
				if(bufferString.startsWith("@")){
					
					paramString = bufferString.substring(1, bufferString.length());
					
					if(paramString.startsWith("UTIL")){
						
						st = new StringTokenizer(paramString);
						
						//Verifichiamo la presenza del token UTIL
						if(!st.hasMoreTokens())
							assert(false);						
						if(!st.nextToken().equals("UTIL"))
							assert(false);
						
						//Verifichiamo la presenza del token relativo al valore del paramtero UTIL
						if(!st.hasMoreTokens())
							assert(false);
						
						String token = st.nextToken();
						System.out.println(token);
						currentReaded.setUtilizationString(token);
						
						continue;					
					}
					
				}
				if(bufferString.startsWith("BEGIN")){
					
					st = new StringTokenizer(bufferString);
					
					if(!st.hasMoreTokens())
						assert(false);
					
					assert(st.nextToken().equals("BEGIN"));
						
					
					if(!st.hasMoreTokens())
						assert(false);					
					
					
					currentReaded.setTasksetName(st.nextToken());
					
					TaskParser tp = new TaskParser();
					
					while((bufferString = br.readLine()) != null){
						if(bufferString.startsWith("END")){
							ds.addTaskSet(currentReaded);
							currentReaded = new TaskSet();
							break;
						}
						tp.setSourceLine(bufferString);
						currentReaded.addTask(tp.getTask());
					}
					
					
				}
				
			}
			
			ds.remove();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		
		
	}
}
