package edu.unibo.ciri.realtime.model.time;


import java.util.concurrent.TimeUnit;


public class Time {	
	
	private long timeValue;
	private final static TimeUnit defaultResolution = TimeUnit.MICROSECONDS;
	private static TimeUnit resolution = defaultResolution;	
	private TimeUnit preferredTimeUnitRepresentation = TimeUnit.MILLISECONDS; 
	
	
	public static void setResolution(TimeUnit res){
		resolution = res;
	}
	
	public static TimeUnit getResolution(){
		return resolution;
	}
	
	public Time(long value){
		this.timeValue = value;
	}
	
	public Time(long value, TimeUnit unit){
		
		preferredTimeUnitRepresentation = unit;
		if(unit != resolution){
			this.timeValue = resolution.convert(value, unit);
		}			
		else
			this.timeValue = value;
	}
	
	public Time(long value, String timeUnitString){
		
		preferredTimeUnitRepresentation = getTimeUnitFromString(timeUnitString);
		if(preferredTimeUnitRepresentation != resolution){
			this.timeValue = resolution.convert(value, preferredTimeUnitRepresentation);
		}			
		else
			this.timeValue = value;
	}
	
	
	
	public void setTimeValue(long value, TimeUnit unit){
					
		if(unit != null && unit != resolution){
			this.timeValue = resolution.convert(value, unit);
		}			
		else
			this.timeValue = value;
	}
	
	public void setTimeValue(long value){
		this.timeValue = value;
	}
	
	public long getTimeValue(TimeUnit t){
		return t.convert(this.timeValue, resolution);
	}
	
	public long getTimeValue(){
		return timeValue;
	}
	
	public TimeUnit preferredTimeUnitRepresentation(){
		return preferredTimeUnitRepresentation;
	}
	
	public void setPreferredTimeUnitRepresentation(TimeUnit preferredRepresentation){
		preferredTimeUnitRepresentation = preferredRepresentation;
	}
	
	public String getPreferredTimeUnitRepresentationString(){
		return getTimeUnitString(preferredTimeUnitRepresentation);
	}
	
	public long getValueInPreferredTimeUnitRepresentation(){
		return getTimeValue(preferredTimeUnitRepresentation);
	}
	
	public static TimeUnit getTimeUnitFromString(String s){
		
		if(s.equals("ms")){
			return TimeUnit.MILLISECONDS;
		}else if(s.equals("ns")){
			return TimeUnit.NANOSECONDS;
		}else if(s.equals("us")){
			return TimeUnit.MICROSECONDS;
		}else if(s.equals("s")){
			return TimeUnit.SECONDS;
		}else 
			return null;
	}
	
	public static String getTimeUnitString(TimeUnit t){
		String time;
		switch (t) {
			case MILLISECONDS:
				time = "ms";				
				break;
			case MICROSECONDS:
				time = "us";				
				break;
			case NANOSECONDS:
				time = "ns";				
				break;
			case SECONDS:
				time = "sec";				
				break;
			default:
				time = "not valid";
				break;
		}
		return time;
	}
	
	public String toString(){
		return getValueInPreferredTimeUnitRepresentation() + " " + getPreferredTimeUnitRepresentationString();
	}

	public TimeUnit getPreferredTimeUnit() {
		
		return preferredTimeUnitRepresentation;
	}	
	
}
