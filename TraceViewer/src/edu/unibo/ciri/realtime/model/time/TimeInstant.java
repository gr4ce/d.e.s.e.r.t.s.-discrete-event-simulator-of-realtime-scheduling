package edu.unibo.ciri.realtime.model.time;

import java.util.concurrent.TimeUnit;

public class TimeInstant extends Time implements Comparable<TimeInstant> {

	public TimeInstant(){
		super(0);	
	}
	
	public TimeInstant(long value) {
		super(value);		
	}
	
	public TimeInstant(long value, TimeUnit unit){
		super(value, unit);		
	}
	
	public void add(TimeInstant t){
		this.setTimeValue( this.getTimeValue() + t.getTimeValue() );
	}
	
	@Override
	/* 
	 * Returns 
	 * 1 when "this" is greater then arg
	 * -1 when "this" is less then arg
	 */
	public int compareTo(TimeInstant arg) {
		
		if(this.getTimeValue() < arg.getTimeValue())
			return -1;
		
		if(this.getTimeValue() > arg.getTimeValue())
			return 1;
		
		return 0;
	}
	
		
	@Override
	public boolean equals(Object ti){
		
		TimeInstant i;
		
		if(ti instanceof TimeInstant){
			i = (TimeInstant)ti;
			if(i.getTimeValue() == this.getTimeValue())
				return true;
			return false;
		}

		throw new ClassCastException();
		
	}

}
