package edu.unibo.ciri.realtime.model.time;

import java.util.concurrent.TimeUnit;

public class TimeInterval {
	
	TimeInstant leftEnd;
	TimeInstant rightEnd;
	Time duration;
	
	
	public TimeInterval(TimeInstant leftEnd, TimeInstant rightEnd){
		//TODO insert exception instead of an assert
		assert(rightEnd.getTimeValue() >= leftEnd.getTimeValue());
		
		this.leftEnd = leftEnd; 
		this.rightEnd = rightEnd;
		this.duration = new Time(this.rightEnd.getTimeValue() - this.leftEnd.getTimeValue());
	}
	
	public TimeInterval(TimeInstant leftHand, Time duration){		
		this.leftEnd = leftHand;
		this.duration = duration;
		this.rightEnd = new TimeInstant( leftHand.getTimeValue() + duration.getTimeValue());		
	}
	
	private void updateDuration(){
		this.duration.setTimeValue(rightEnd.getTimeValue() - leftEnd.getTimeValue(), Time.getResolution());
	}
	
	public void setEndPoints(long leftEnd, long rightEnd, TimeUnit unit){
		this.leftEnd.setTimeValue(leftEnd, unit);
		this.rightEnd.setTimeValue(rightEnd, unit);
		updateDuration();
	}
	
	public void shiftLeft(Time q){
		this.leftEnd.setTimeValue(leftEnd.getTimeValue() + q.getTimeValue(), Time.getResolution());
		this.rightEnd.setTimeValue(rightEnd.getTimeValue() + q.getTimeValue(), Time.getResolution());
		assert(this.duration.getTimeValue() == (rightEnd.getTimeValue() - leftEnd.getTimeValue()));
	}
	
	public boolean isDisjoint(TimeInterval arg){
		
		long a,b,c,d;
		
		a = leftEnd.getTimeValue();
		b = rightEnd.getTimeValue();
		c = arg.getLeftEnd().getTimeValue();
		d = arg.getRightEnd().getTimeValue();
		
		if( c > b || a > d )
			return true;
		return false;
	}
	
//	public boolean isAttached(TimeInterval arg){
//		
//	}
//	
//	public boolean isPartiallyOverlapped(){
//		
//	}
	
//	public void shiftRight(TimeDuration q){
//		this.leftEnd.setValue(leftEnd.getValue() + q.getValue(), Time.getResolution());
//		this.rightEnd.setValue(rightEnd.getValue() + q.getValue(), Time.getResolution());
//		assert(this.duration.getValue() == (rightEnd.getValue() - leftEnd.getValue()));
//	}
	
//	public void setLeftEnd(long value, TimeUnit unit){		
//		this.leftEnd.setValue(value, unit);
//		assert(rightEnd.compareTo(rightEnd) >= 0);
//	}
//	
//		
//	public void setRightEnd(long value, TimeUnit unit){
//		this.rightEnd.setValue(value, unit);
//		assert(rightEnd.compareTo(rightEnd) >= 0);
//	}
	
	public TimeInstant getLeftEnd(){
		return leftEnd;
	}
	
	public TimeInstant getRightEnd(){
		return rightEnd;
	}
	
	public Time getDuration(){
		return duration;
	}
	
	
	public String toString(){
		return "[" + leftEnd.getTimeValue() + "," + rightEnd.getTimeValue() + "]"; 
	}
	

}
