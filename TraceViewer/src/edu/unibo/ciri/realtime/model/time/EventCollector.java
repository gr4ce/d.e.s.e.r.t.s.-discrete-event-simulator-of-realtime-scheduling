package edu.unibo.ciri.realtime.model.time;

import java.util.Vector;
import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.event.management.EventListener;
import edu.unibo.ciri.desert.graphics.diagram.schedule.ScheduleDiagramContainer;



public class EventCollector implements EventListener{
	
	
	ScheduleDiagramContainer container;
	
	public void setScheduleDiagramContainer(ScheduleDiagramContainer cont){
		container = cont;
	}
	
		
	private void handleJobCompletion(Event event) {
		
		CollectedEvent ce = new CollectedEvent();		
		ce.collect(event);
		container.addElement(ce);
	}

	
	private void handleJobRelease(Event event) {
		
		CollectedEvent ce = new CollectedEvent();		
		ce.collect(event);
		container.addElement(ce);
	}

	
	private void handleJobDeadline(Event event){
		
		
									
		CollectedEvent ce = new CollectedEvent();		
		ce.collect(event);
		container.addElement(ce);	
	}
	

	private void handleJobStart(Event event) {
		
		CollectedEvent ce = new CollectedEvent();		
		ce.collect(event);
		container.addElement(ce);
	}

	
	private void handleJobResume(Event event) {
		
//		if(!Configuration.isCollectPreemptionAndResumeTime())
//			return;
		
		CollectedEvent ce = new CollectedEvent();		
		ce.collect(event);
		container.addElement(ce);
	}
	
	private void handleJobWait(Event event) {		

		CollectedEvent ce = new CollectedEvent();		
		ce.collect(event);
		container.addElement(ce);
	}
	
	
	private void handleJobPreemption(Event event) {
		
//		if(!Configuration.isCollectPreemptionAndResumeTime())
//			return;
		
		CollectedEvent ce = new CollectedEvent();		
		ce.collect(event);
		container.addElement(ce);
	}

	
	private void handleJobPriorityChange(Event event) {

		CollectedEvent ce = new CollectedEvent();		
		ce.collect(event);
		container.addElement(ce);
		
	}
	
	private void handleSimulationSoftStop(Event event) {
				
		CollectedEvent ce = new CollectedEvent();		
		ce.collect(event);
		container.addElement(ce);		
	}
	
	private void handleJobOverrun(Event event) {
		
//		if(!Configuration.isCollectOverrunEvents())
//			return;
		
		CollectedEvent ce = new CollectedEvent();		
		ce.collect(event);
		container.addElement(ce);
		
	}



	private void handleSimulationHalt(Event event) {

		CollectedEvent ce = new CollectedEvent();		
		ce.collect(event);
		container.addElement(ce);
	}

	@Override
	public void handle(Event event) {
		
		
		
		switch(event.getEventType()){
		
			case JOB_RELEASE:
				handleJobRelease(event);
				break;
			case JOB_WAIT:
				handleJobWait(event);
				break;
			case JOB_START:
				handleJobStart(event);
				break;
			case JOB_RESUME:
				handleJobResume(event);
				break;
			case JOB_OVERRUN:
				handleJobOverrun(event);
				break;
			case JOB_COMPLETION:
				handleJobCompletion(event);
				break;		
			case JOB_PREEMPTION:
				handleJobPreemption(event);
				break;		
			case JOB_DEADLINE:
				handleJobDeadline(event);
				break;
			case JOB_PRIORITY_CHANGE:
				handleJobPriorityChange(event);
				break;		
			case SIMULATION_HALT:
				handleSimulationHalt(event);
				break;
			case SIMULATION_SOFT_STOP:
				handleSimulationSoftStop(event);
				break;
			default:			
		}		
	}

	
	@Override
	public String getName() {
		return "DataCollector";
	}





}
