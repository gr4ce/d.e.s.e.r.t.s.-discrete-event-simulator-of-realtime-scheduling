package edu.unibo.ciri.realtime.model.time;

import java.text.NumberFormat;


public class LinuxTime {	
	
	private static NumberFormat nf = NumberFormat.getInstance();
	static{
		nf.setMinimumIntegerDigits(9);
		nf.setGroupingUsed(false);
	}
	
	private long sec;
	private long nsec;
	
	
		
	public LinuxTime(long seconds, long nanoseconds){
		
		assert(nanoseconds < 100000000);
		
		this.sec = seconds;
		this.nsec = nanoseconds;
	}
	
	public LinuxTime(){
		
		this.sec = 0;
		this.nsec = 0;
		
	}
	
	
	
	public void setSecondsValue(long seconds){		
		this.sec = seconds;
	}
	
	public void setNanoSecondsValue(long nanoseconds){		
		assert(nanoseconds < 100000000);
		this.nsec = nanoseconds;
	}
	
	public long getSecondValue(){
		return sec;
	}
	
	public long getNanoSecondValue(){
		return nsec;
	}
	
	
	
	public String toString(){
		return sec + "." + nf.format(nsec);
	}	
	
}
