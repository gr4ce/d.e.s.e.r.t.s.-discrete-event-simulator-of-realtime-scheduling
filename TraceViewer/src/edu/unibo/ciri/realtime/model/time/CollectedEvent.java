package edu.unibo.ciri.realtime.model.time;

import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.event.EventType;

public class CollectedEvent {
	
	private int 	  taskId = -1;
	private long 	  jobId  = -1;
	private long 	  time   = -1;
	private int		  cpu	 = -1;
	private EventType type   = EventType.NONE;
	
	
	
	public CollectedEvent(){
		
	}
	
	
	
	public CollectedEvent(int taskId, int jobId, long time, EventType type, int cpuId){
		this.taskId = taskId;
		this.jobId  = jobId;
		this.time   = time;
		this.type	= type;
		this.setCpu(cpuId);
		
	}
	
	public void collect(Event e){
		this.taskId = e.getTask().getTaskId();
		this.jobId  = e.getJob().getUniqueId();
		this.time   = e.getEventTimeValue();
		this.type   = e.getEventType();
		this.setCpu(e.getCpu());
	}
	
	public int getTaskId() {
		return taskId;
	}
	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}
	public long getJobId() {
		return jobId;
	}
	public void setJobId(long jobId) {
		this.jobId = jobId;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	public EventType getType() {
		return type;
	}
	public void setType(EventType type) {
		this.type = type;
	}

	public void setCpu(int cpu) {
		this.cpu = cpu;
	}
	public int getCpu() {
		return cpu;
	}

	
}
