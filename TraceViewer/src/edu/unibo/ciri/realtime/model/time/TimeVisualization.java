package edu.unibo.ciri.realtime.model.time;

import java.util.concurrent.TimeUnit;

public class TimeVisualization {
	
	public static String getString(Time t){
		
		StringBuffer timeRepresentation = new StringBuffer();
		long value;
		
		if(t.getTimeValue(TimeUnit.DAYS) != 0)
			timeRepresentation.append(t.getTimeValue(TimeUnit.DAYS)+ " " + getUnitString(TimeUnit.DAYS) + " ");
		
		
		if(t.getTimeValue(TimeUnit.HOURS) != 0)
			timeRepresentation.append(t.getTimeValue(TimeUnit.HOURS) % 24 + " " + getUnitString(TimeUnit.HOURS) + " ");	
		
		
		if(t.getTimeValue(TimeUnit.MINUTES) != 0)
			timeRepresentation.append(t.getTimeValue(TimeUnit.MINUTES) % 60 + " " + getUnitString(TimeUnit.MINUTES) + " ");
		
		
		if(t.getTimeValue(TimeUnit.SECONDS) != 0)
			timeRepresentation.append(t.getTimeValue(TimeUnit.SECONDS) % 60 + " " + getUnitString(TimeUnit.SECONDS) + " ");
		
		
		value = t.getTimeValue(TimeUnit.MILLISECONDS);
		if(value != 0 && (value % 1000) != 0)
			timeRepresentation.append(value % 1000 + " " + getUnitString(TimeUnit.MILLISECONDS) + " ");
		if(Time.getResolution() == TimeUnit.MILLISECONDS)
			return timeRepresentation.toString();
		
		
		value = t.getTimeValue(TimeUnit.MICROSECONDS);
		if(value != 0 && (value % 1000) != 0)
			timeRepresentation.append(value % 1000 + " " + getUnitString(TimeUnit.MICROSECONDS) + " ");
		if(Time.getResolution() == TimeUnit.MICROSECONDS)
			return timeRepresentation.toString();
		
		
		value = t.getTimeValue(TimeUnit.NANOSECONDS);
		if(value != 0 && (value % 1000) != 0)
			timeRepresentation.append(value % 1000 + " " + getUnitString(TimeUnit.NANOSECONDS) + " ");
		if(Time.getResolution() == TimeUnit.NANOSECONDS)
			return timeRepresentation.toString();
		
		return timeRepresentation.toString();
	}
	
	private static String getUnitString(TimeUnit t){
		switch(t){
			case NANOSECONDS:
				return "ns";
			case MICROSECONDS:
				return "us";
			case MILLISECONDS:
				return "ms";
			case SECONDS:
				return "sec";
			case MINUTES:
				return "min";
			case HOURS:
				return "hr";
			case DAYS:
				return "days";	
				default:
			return "<INVALID>";				
		}
	} 

}
