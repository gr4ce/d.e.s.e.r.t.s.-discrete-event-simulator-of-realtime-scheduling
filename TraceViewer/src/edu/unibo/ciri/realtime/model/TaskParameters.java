package edu.unibo.ciri.realtime.model;

import java.util.concurrent.TimeUnit;

import edu.unibo.ciri.realtime.model.time.Time;
import edu.unibo.ciri.realtime.policies.overrun.OverrunPolicy;
import edu.unibo.ciri.realtime.policies.overrun.OverrunPolicy.OverrunPolicyEnum;
import edu.unibo.ciri.rtmanager.application.taskeditor.TaskModelComboBox;


import util.Perturbator;

@SuppressWarnings("unused")
public class TaskParameters{
	
	public enum TaskModelEnum{Periodic, Sporadic, Aperiodic};
	private Time period;
	private Time deadline;
	private Time computation;
	private Time offset;	
	private int  staticPriority;
	private int  threshold;
	private TaskModelEnum taskModel;
	private OverrunPolicyEnum overrunPolicy = null;
	private static OverrunPolicyEnum defaultPolicy = OverrunPolicyEnum.NAIVE;
	private boolean perturbation = false;
	private Time computationPerturbed;
	private int workingSetSize;
	
	public TaskParameters(){
		
		period 	 	= new Time(0L,TimeUnit.MICROSECONDS);
		deadline 	= new Time(0L,TimeUnit.MICROSECONDS);
		computation = new Time(0L,TimeUnit.MICROSECONDS);
		offset 		= new Time(0L,TimeUnit.MICROSECONDS);
		
		workingSetSize=2;
	}
	
	public Time getPeriod() {
		return period;
	}
	
	public void setPeriod(Time p) {
		period = p;
	}
	
	public void setPeriod(long value, TimeUnit timeUnit) {
		period.setPreferredTimeUnitRepresentation(timeUnit);
		period.setTimeValue(value,timeUnit);
	}
	
	public void setWorkingSetSize(int setSize){
		this.workingSetSize = setSize;
	}
	
	public int getWorkingSetSize(){
		return this.workingSetSize;
	}
	
	public Time getDeadline() {
		return deadline;
	}
	
	public void setDeadline(Time dl) {
		deadline = dl;
	}
	
	public void setDeadline(long value, TimeUnit timeUnit) {
		deadline.setPreferredTimeUnitRepresentation(timeUnit);
		deadline.setTimeValue(value,timeUnit);
	}	
	
	public Time getComputation() {
		if(perturbation){
			computationPerturbed.setTimeValue(computation.getTimeValue(TimeUnit.MICROSECONDS), TimeUnit.MICROSECONDS);
			Perturbator.perturbTime(computationPerturbed);
			return computationPerturbed;
		}
		return computation;
	}
	
	public void setComputation(Time computation) {
		this.computation = computation;
	}
	
	public void setComputation(long value, TimeUnit timeUnit) {
		computation.setPreferredTimeUnitRepresentation(timeUnit);
		this.computation.setTimeValue(value, timeUnit);
	}	
	
	public int getStaticPriority() {
		return staticPriority;
	}
	
	public void setStaticPriority(int staticPriority) {
		this.staticPriority = staticPriority;
	}
	
	public int getThreshold() {
		return threshold;
	}
	
	public void setThreshold(int threshold) {
		this.threshold = threshold;
	}	
	
	public Time getOffset() {
		return offset;
	}
	
	public void setOffset(Time os) {
		offset = os;
	}
	
	public void setOffset(long value, TimeUnit unit){
		offset.setPreferredTimeUnitRepresentation(unit);
		offset.setTimeValue(value, unit);
	}
	
	public void enableComputationJitter(){
		perturbation = true;
		computationPerturbed = new Time(computation.getTimeValue(TimeUnit.MICROSECONDS), TimeUnit.MICROSECONDS);
	}

	public void setOverrunPolicy(OverrunPolicyEnum overrunPolicy) {
		this.overrunPolicy = overrunPolicy;
	}

	public OverrunPolicyEnum getOverrunPolicy() {
		if(overrunPolicy == null)
			return defaultPolicy;
		return overrunPolicy;
	}
	
	public TaskModelEnum getTaskModel() {
		return taskModel;
	}

	public void setTaskModel(TaskModelEnum taskModel) {
		this.taskModel = taskModel;
	}
	
	public void setTaskModel(String value) {
		this.taskModel = TaskModelEnum.valueOf(value);
	}
	
	public String toString(){
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("O = " + offset);
		sb.append(" ");
		sb.append("P = " + period);
		sb.append(" ");
		sb.append("D = " + deadline);
		sb.append(" ");		
		if(computation != null && computation.getTimeValue() != 0){			
			sb.append(computation);
			sb.append(" ");
		}
		sb.append("\n");
		
		
		return sb.toString();
	}

}
