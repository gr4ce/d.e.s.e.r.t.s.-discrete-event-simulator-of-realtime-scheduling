package edu.unibo.ciri.realtime.model;

import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import edu.unibo.ciri.realtime.model.time.Time;

public class ExecutionPattern {
	
	private ArrayList<Time> chunks = new ArrayList<Time>();
	private ArrayList<String> res = new ArrayList<String>();
	
	public ExecutionPattern(){
		
	}
	
	public void importTaskCode(String taskCode){
		
		Vector<String> instructions = new Vector<String>();
		StringTokenizer st = new StringTokenizer(taskCode, "\n");
		StringTokenizer tokens;
		while(st.hasMoreTokens()){
			String inst = st.nextToken();
			if(inst.startsWith("execute")){
				tokens = new StringTokenizer(inst);
				tokens.nextToken();
				Long value = Long.parseLong(tokens.nextToken());
				Time t = new Time(value, tokens.nextToken());
				chunks.add(t);
			}else if(inst.startsWith("take")){
				tokens = new StringTokenizer(inst);
				tokens.nextToken();
				String name = tokens.nextToken();
				if(!res.contains(name))
					res.add(name);				
			}else if(inst.startsWith("give")){
				tokens = new StringTokenizer(inst);
				tokens.nextToken();
				String name = tokens.nextToken();
				if(!res.contains(name))
					res.add(name);				
			}
			instructions.add(inst);
		}
		for(String s:instructions){
			System.out.println("Instruction: " + s + ";");
		}
	}
	
	public Time getTotalExecutionRequirement(){
		Time t = new Time(0, TimeUnit.NANOSECONDS);
		for(Time temp: chunks){
			t.setTimeValue( t.getTimeValue() + temp.getTimeValue());
		}
		return t;
	}

	public ArrayList<String> getUsedResources() {		
		return res;
	}

}
