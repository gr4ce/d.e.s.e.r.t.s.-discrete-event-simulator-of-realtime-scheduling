package edu.unibo.ciri.realtime.model;

import java.util.ArrayList;


import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.event.EventType;
import edu.unibo.ciri.model.task.job.JobInfo;
import edu.unibo.ciri.realtime.policies.scheduling.base.PriorityComparator;

public class Job implements Comparable<Job>{
	
	/*
	 * Identification info  
	 */
	private int  instanceNumber  = 0;
	private long uniqueId = 0;
	
	
	/*
	 * This field is necessary to compare two job when they are 
	 * inserted in the data structure of the scheduler 
	 */
	private PriorityComparator comparator;
	
	
	/*
	 * An array that holds all the events related to this job
	 */
	private Event events[] = new Event[EventType.values().length];
	
	
	/*
	 * The task that generated this job
	 */
	private Task task;
	
	
		
	
	@Deprecated
	private ArrayList<Event> handledJobEvents;
	
	
	
	/*
	 * State of the job usefull for its execution 
	 */
	public enum JobState{ UNINITIALIZED, INITIALIZED, RELEASED, RUNNING, IDLE, PREEMPTED, BLOCKED, READY}
	
	private 	JobState 	state = JobState.UNINITIALIZED;
	private		boolean 	hasMissed  = false;
	private		boolean 	hasOverrun = false;
	
	
	/*
	 * Other run-time information 
	 */
	private int currentCpu;
	private int lastCpu;
	private int currentPriority;
	
	/*
	 *  
	 */
	public enum JobContext{ LOCAL,KERNEL_NP }
	private JobContext context = JobContext.LOCAL;
	
	
	/*
	 * Minimal job info needed to track the remaining execution time  
	 */
	private long 	startExecutionTime  	= -1;
	private long 	endExecutionTime		= -1 ;
	private long 	remainingExecutionTime 	= -1;;
	
	
	/*
	 * Extended time information to track every instant an event related 
	 * to this job appened
	 */	
	private JobInfo info;
		
	
		
	public Job(){
		this.info = new JobInfo();		
	}
	
	public Job(int id,Task t){
		this.uniqueId = id;
		this.task = t;
		this.info = new JobInfo();		
		this.info.setRemainingExecution( t.getTaskParameters().getComputation().getTimeValue() );
	}
	
	public boolean hasMissed(){
		return this.hasMissed;
	}
	
	public boolean hasOverrun(){
		return this.hasOverrun;
	}
	
	public void setMissed(){
		this.hasMissed = true;
	}
	
	public void setOverrun(){
		this.hasOverrun = true;
	}
	
	public void setCompareRule(PriorityComparator comp){
		this.comparator = comp;
	}
	
	public PriorityComparator getCompareRule(){
		return this.comparator;
	}
	
	public void setCurrentPriority(int prio){
		this.currentPriority = prio;
	}
	
	public int getCurrentPriority(){
		return this.currentPriority;
	}
	
	public Event getEvent(EventType type) {
		return events[type.ordinal()];
	}

	public void setEvent(Event event) {
		events[event.getEventType().ordinal()] = event;
	}
	
	public void setEvent(EventType et, Event e){
		events[et.ordinal()] = e;
	}

	public ArrayList<Event> getHandledJobEvents() {
		return handledJobEvents;
	}

	public JobState getState() {
		return state;
	}
	
	public JobContext getContext(){
		return context;
	}

	public void setState(JobState state) {
		//TODO con questa soluzione l'ordine degli eventi deve essere il seguente
		//1) ispeziona la coda per vedere dove verr� inserito
		//2) se pos < m setta lo stato del job a running
		//3) questo fa innalzare automaticamente la priorit� corrente
		//   del task alla soglia
		//4) a questo punto si pu� procedere traquillamente ad un inserimento
		//   ordinato nella coda dei processi attivi
		//   Nel caso PT l'inserimento sar� fatto al pi� in una posizione maggiormente
		//   privilegiata rispetto a quanto predetto
		//   NOTA nel caso non preemptive questa operazione non sarebbe necessaria
		//   basterebbe controllare che ci siano almeno m processi nella coda attiva
		//   per inferire che il processo non potr� andare in esecuzione. Comunque
		//   la cosa risulta difficile da gestire senza le soglie
		//   nel caso full preemptive si potrebbe fare direttamente un inserimento
		//   ordinato per tanto la priorit� corrente rester� invariata
		switch(state){
			case RUNNING:				
//				System.out.print("\tpriority switch from " + currentPriority);
				currentPriority = task.getTaskParameters().getThreshold();	
				task.setLastCpu(currentCpu);
				//TODO trigger priority change?
//				System.out.println(" to " + currentPriority);
				break;
			case IDLE:
//				System.out.print("\tpriority switch from " + currentPriority);
				currentPriority = task.getTaskParameters().getStaticPriority();
				//TODO trigger priority change?
//				System.out.println(" to " + currentPriority );
				break;
		}
		
		this.state = state;
	}
	
	public void setContext(JobContext context){
		this.context=context;
	}
	
	
	
	public int getProgInstanceNumber() {
		return instanceNumber;
	}

	public void setProgInstanceNumber(int progressiveInstanceNumber) {
		this.instanceNumber = progressiveInstanceNumber;
	}

	public JobInfo getInfo() {
		return info;
	}

	public void setInfo(JobInfo info) {
		this.info = info;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.info.setRemainingExecution( task.getTaskParameters().getComputation().getTimeValue() );
		this.task = task;		
	}
	
	public String toString(){
		return this.getTask().getName() + "(job " +instanceNumber+ "-"+uniqueId+")["+this.state.toString()+"]";
	}

	
	public int compareTo(Job anotherJob) {
		
//		if(this.context.equals(JobContext.KERNEL_NP)){
//			
//			return 1;
//			
//		}else if(anotherJob.context.equals(JobContext.KERNEL_NP)){
//			
//			return -1;
//			
//		}else{
				assert(comparator != null);
//				if(comparator != null){
					return comparator.compare(this, anotherJob);
//				}
				
//				int thisJobCurrentPriority = this.getCurrentPriority();
//				int anotherJobCurrentPriority = anotherJob.getCurrentPriority();
//				int result = thisJobCurrentPriority - anotherJobCurrentPriority;
//				
//				if(result == 0){
//					if(this.state == JobState.RUNNING)
//						return 1;
//					else if(anotherJob.getState() == JobState.RUNNING)
//						return -1;
//					else
//						return result;
//				}
//					
//				return result;
				
//		}
	}

	public void setCpu(int cpu) {
		
		if(this.currentCpu != cpu){
			this.lastCpu = this.currentCpu;			
		}
		
		this.currentCpu = cpu;
	}

	public int getCpu() {
		return currentCpu;
	}
	
	public int getLastRunningCpu(){
		return lastCpu;
	}
	
	
	public void clear(){
		
		info.clear();		
		info.setRemainingExecution( this.task.getTaskParameters().getComputation().getTimeValue() );
		
		hasMissed  	= false;
		hasOverrun 	= false;
		
		currentCpu 	= -1;
		lastCpu    	= -1;
		
		instanceNumber  = -1;
		
		currentPriority = -1;
		
		startExecutionTime  	= -1;
		endExecutionTime		= -1;
		remainingExecutionTime 	= -1;	
		
		for(int i = 0; i < events.length; i++)
			events[i] = null;
	}

	public void setUniqueId(long uniqueId) {
		this.uniqueId = uniqueId;
	}

	public long getUniqueId() {
		return uniqueId;
	}

	public long getStartExecutionTime() {
		return startExecutionTime;
	}

	public void setStartExecutionTime(long startExecutionTime) {
		this.startExecutionTime = startExecutionTime;
	}

	public long getEndExecutionTime() {
		return endExecutionTime;
	}

	public void setEndExecutionTime(long endExecutionTime) {
		
		assert(endExecutionTime > startExecutionTime);	
		
		remainingExecutionTime -= (endExecutionTime - startExecutionTime);
		
		assert(remainingExecutionTime >= 0);
	}

	public long getRemainingExecutionTime() {
		return remainingExecutionTime;
	}

	public void setRemainingExecutionTime(long remainingExecutionTime) {
		this.remainingExecutionTime = remainingExecutionTime;
	}
	
	
	
}
