package edu.unibo.ciri.realtime.policies.staticsorting;



import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskSet;

import util.SortingAlgorithm;

public interface TaskSorter {
	
	void setSortingAlgorithm(SortingAlgorithm<Task> sortingAlgorthm);
	
	SortingAlgorithm<Task> getSortingAlgorithm();
		
	void sort(Task[] array);
		
	Task[] sort(TaskSet taskset);	
	
}
