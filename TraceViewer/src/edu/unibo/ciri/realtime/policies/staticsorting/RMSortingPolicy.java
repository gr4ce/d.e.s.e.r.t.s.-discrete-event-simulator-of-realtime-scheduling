package edu.unibo.ciri.realtime.policies.staticsorting;

import java.util.Comparator;

import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.policies.scheduling.base.StaticSortingAlgorithm;
import edu.unibo.ciri.util.sort.algorithm.QuickSort;


import util.SortingAlgorithm;
import util.SortingAlgorithm.SortingOrder;


public class RMSortingPolicy extends StaticSortingAlgorithm {
	
	
	private class RMTaskComparator implements Comparator<Task>{
		
		
		public int compare(Task t1, Task t2) {
			
			long period1 = t1.getTaskParameters().getPeriod().getTimeValue();
			long period2 = t2.getTaskParameters().getPeriod().getTimeValue();		
					
			if(period1 < period2)
				return 1;
			if(period1 > period2)
				return -1;
			return 0;
			
		}
		
	}
	
		
	public RMSortingPolicy(SortingAlgorithm<Task> sa) {
		
		super( new TaskSorter() {
			
			private SortingAlgorithm<Task> sortingAlgorithm;
			
			@Override
			public void setSortingAlgorithm(
					SortingAlgorithm<Task> sortingAlgorithm) {
				
				this.sortingAlgorithm = sortingAlgorithm;  
			}

			@Override
			public SortingAlgorithm<Task> getSortingAlgorithm() {
				
				return this.sortingAlgorithm;
			}
			
			@Override
			public void sort(Task[] array) {
				
				sortingAlgorithm.sort(array, SortingOrder.NON_INCREASING_ORDER);		
			}

			@Override
			public Task[] sort(TaskSet taskset) {
				
				Task[] orderedArray = new Task[taskset.size()];
				taskset.getArrayList().toArray(orderedArray);
				sortingAlgorithm.sort(orderedArray, SortingOrder.NON_INCREASING_ORDER);
				
				return orderedArray;
			}		

		});//End of TaskSorter definition and instantiation
		
		sa.setComparator( new RMTaskComparator() );		
		getStaticTaskSorter().setSortingAlgorithm(sa);
		
	}
	
	
	public RMSortingPolicy() {
		
		super( new TaskSorter() {
			
			private SortingAlgorithm<Task> sortingAlgorithm;
			
			@Override
			public void setSortingAlgorithm(
					SortingAlgorithm<Task> sortingAlgorithm) {
				
				this.sortingAlgorithm = sortingAlgorithm;  
			}

			@Override
			public SortingAlgorithm<Task> getSortingAlgorithm() {
				
				return this.sortingAlgorithm;
			}
			
			@Override
			public void sort(Task[] array) {
				
				sortingAlgorithm.sort(array, SortingOrder.NON_INCREASING_ORDER);		
			}

			@Override
			public Task[] sort(TaskSet taskset) {
				
				Task[] orderedArray = new Task[taskset.size()];
				taskset.getArrayList().toArray(orderedArray);
				sortingAlgorithm.sort(orderedArray, SortingOrder.NON_INCREASING_ORDER);
				
				return orderedArray;
			}		

		});//End of TaskSorter definition and instantiation
		
		SortingAlgorithm<Task> sa = new QuickSort<Task>();
		
		sa.setComparator( new RMTaskComparator() );		
		getStaticTaskSorter().setSortingAlgorithm(sa);
		
	}
	


	@Override
	public String getName() {
		
		return "Rate Monotonic";
	}



	



}
