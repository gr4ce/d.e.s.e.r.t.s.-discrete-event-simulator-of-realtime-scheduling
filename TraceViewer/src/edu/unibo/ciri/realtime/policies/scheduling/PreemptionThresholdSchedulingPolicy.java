package edu.unibo.ciri.realtime.policies.scheduling;


//import edu.unibo.ciri.model.task.TaskParameters;
import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.event.management.EventListener;
import edu.unibo.ciri.desert.simulation.management.Simulation;
import edu.unibo.ciri.realtime.model.Job;
import edu.unibo.ciri.realtime.model.Job.JobState;
import edu.unibo.ciri.realtime.policies.scheduling.base.PriorityComparator;
import edu.unibo.ciri.realtime.policies.scheduling.base.StaticSortingAlgorithm;

public class PreemptionThresholdSchedulingPolicy extends FixedPrioritySchedulingPolicy implements EventListener{
	
	private PreemptionThresholdAssignment preemptionThresholdAssigment; 
	private Simulation simulation;
		
	private PriorityComparator priorityComparator = new PriorityComparator(){

		@Override
		public int compare(Job firstJob, Job secondJob) {
			
			int firstJobCurrentPrio;
			int secondJobCurrentPrio;
			
			int firstJobPriority  = getPriorityAssignment().getPriority(firstJob.getTask().getTaskId());
			int secondJobPriority = getPriorityAssignment().getPriority(secondJob.getTask().getTaskId());
			
			int firstJobThreshold  = getPreemptionThresholdAssigment().getThreshold(firstJob.getTask().getTaskId());
			int secondJobThreshold = getPreemptionThresholdAssigment().getThreshold(secondJob.getTask().getTaskId());
			
			boolean firstJobIsRunningOrPreempted  = firstJob.getState() == JobState.RUNNING || firstJob.getState()  == JobState.PREEMPTED;
			boolean secondIsRunningOrPreempted = secondJob.getState() == JobState.RUNNING || secondJob.getState() == JobState.PREEMPTED;
			
			if(firstJobIsRunningOrPreempted)
				firstJobCurrentPrio = firstJobThreshold;
			else
				firstJobCurrentPrio = firstJobPriority;
			
			if(secondIsRunningOrPreempted)
				secondJobCurrentPrio = secondJobThreshold;
			else
				secondJobCurrentPrio = secondJobPriority;
			
			// The less the numeric value, the higher the priority
			if(firstJobCurrentPrio != secondJobCurrentPrio)
				return -(firstJobCurrentPrio - secondJobCurrentPrio);
			
			if(firstJobIsRunningOrPreempted)
				return 1;
			
			if(secondIsRunningOrPreempted)
				return -1;
			
			return 0;
		}
		
	};
	
	/*
	 * Thresholds assignment is made by a specific defined priority assignment
	 * and they works together. This means that no preassigned priority is needed
	 */
	public PreemptionThresholdSchedulingPolicy(){
		//XXX occhio questo pu� essere fonte di problemi. Bisogna fare in modo
		//di creare un assegnamento statico di priorit� in un secondo momento
		//e di settarlo per essere sicuri di non beccarsi dei null pointer
		//exception 
		super(null);
	}
	
	/*
	 * Thresholds assignment is built upon a static sorting policy  
	 */
	public PreemptionThresholdSchedulingPolicy(StaticSortingAlgorithm sa){
		super(sa);
	}
	
	
	@Override
	public String getName() {		
		if(super.getSortingPolicy() != null)
			return "Preemption Threshold with " + super.getSortingPolicy().getName();
		else
			return "Preemption Threshold with internal Threshold assignment"; 
	}

	@Override
	public PriorityComparator getPriorityComparator() {		
		return this.priorityComparator;
	}

	public void setPreemptionThresholdAssigment(
			PreemptionThresholdAssignment preemptionThresholdAssigment) {
		this.preemptionThresholdAssigment = preemptionThresholdAssigment;
	}

	public PreemptionThresholdAssignment getPreemptionThresholdAssigment() {
		return preemptionThresholdAssigment;
	}

	@Override
	public void handle(Event e) {
		
		switch(e.getEventType()){
			case JOB_START:
				handleJobStart(e);
				break;
			case JOB_RESUME:
				handleJobResume(e);
				break;
			case JOB_PREEMPTION:
				handleJobPreemption(e);
				break;
			case JOB_COMPLETION:
				handleJobCompletion(e);
				break;
		}
		
	}
	
	public void handleJobStart(Event e){
		System.out.print(e.getEventType() + " of " + e.getTask().getName() + " priority  " + getPriorityAssignment().getPriority(e.getTask()));
		System.out.println(" threshold " + preemptionThresholdAssigment.getThreshold(e.getTask()));
	}
	
	public void handleJobCompletion(Event e){
		System.out.print(e.getEventType() + " of " + e.getTask().getName() + " priority  " + getPriorityAssignment().getPriority(e.getTask()));
		System.out.println(" threshold " + preemptionThresholdAssigment.getThreshold(e.getTask()));
	}
	
	public void handleJobPreemption(Event e){
		System.out.print(e.getEventType() + " of " + e.getTask().getName() + " priority  " + getPriorityAssignment().getPriority(e.getTask()));
		System.out.println(" threshold " + preemptionThresholdAssigment.getThreshold(e.getTask()));
	}
	
	public void handleJobResume(Event e){
		System.out.print(e.getEventType() + " of " + e.getTask().getName() + " priority  " + getPriorityAssignment().getPriority(e.getTask()));
		System.out.println(" threshold " + preemptionThresholdAssigment.getThreshold(e.getTask()));
	}

	public void setSimulation(Simulation simulation) {
		this.simulation = simulation;
	}

	public Simulation getSimulation() {
		return simulation;
	}

}
