package edu.unibo.ciri.realtime.policies.scheduling.base;

import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.policies.scheduling.FixedPriorityAssignment;
import edu.unibo.ciri.realtime.policies.staticsorting.RMSortingPolicy;
import edu.unibo.ciri.realtime.policies.staticsorting.TaskSorter;

public abstract class StaticSortingAlgorithm {
	
	private TaskSorter 	taskSorter	= null;
	private String 		name 		= "not set";
	
	public StaticSortingAlgorithm(){
		taskSorter = (new RMSortingPolicy()).getStaticTaskSorter();
	}
	
	public StaticSortingAlgorithm(TaskSorter taskSorter){
		this.taskSorter = taskSorter;
	}
	
	public TaskSorter getStaticTaskSorter(){
		return taskSorter;
	}
	
	public FixedPriorityAssignment getStaticPriorityAssignment(TaskSet taskset){
		
		Task[] tasks = new Task[taskset.size()];
		taskset.getArrayList().toArray(tasks);
		taskSorter.sort(tasks);
				
		return new FixedPriorityAssignment(tasks);
		
	}
	
	
	
	/**
	 * @deprecated
	 */
	public void assignPrioritiesToTasks(TaskSet taskset){
		
		Task[] tasks = new Task[taskset.size()];
		taskset.getArrayList().toArray(tasks);
		taskSorter.sort(tasks);
				
		for(int i = 0; i < tasks.length; i++){
			tasks[i].getTaskParameters().setStaticPriority(i);
		}	
		
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
		

}
