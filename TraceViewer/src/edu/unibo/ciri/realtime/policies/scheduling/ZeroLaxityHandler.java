package edu.unibo.ciri.realtime.policies.scheduling;



import edu.unibo.ciri.desert.config.StaticConfig;
import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.event.EventType;
import edu.unibo.ciri.desert.event.management.EventListener;
import edu.unibo.ciri.desert.simulation.management.Simulation;
import edu.unibo.ciri.realtime.model.Job;
import edu.unibo.ciri.realtime.model.Job.JobState;

public class ZeroLaxityHandler implements EventListener{
	
	//come logica arrayLaxity[i]=false di "default", quando scatta la ZeroLaxity L=0 allora diventer� arrayLaxity[i]=true
	//quando faccio partire la simulazione mi accerto che sia tutto a false (anche coi release...)
	private boolean [] arrayLaxity;
	
	Simulation simulation;

	//costruttore con parametro (dimensione tasket e riferimento alla simulazione)
	public ZeroLaxityHandler(Simulation sim){
		simulation = sim;	
		arrayLaxity = new boolean[simulation.getTaskset().size()];		
	}
	
	public void setSize(int size){
		arrayLaxity = new boolean[size];
	}
	
	public void setSimulation(Simulation sim) {
		this.simulation=sim;		
	}

	public boolean hasZeroLaxity(int taskId){
		return arrayLaxity[taskId];
	}
	
	public void setZeroLaxity(int taskId){
		arrayLaxity[taskId]=true;
	}
	
	public void resetZeroLaxity(int taskId){
		arrayLaxity[taskId]=false;
	}
	
	public void clear(){
		for (int i = 0; i < arrayLaxity.length; i++) {
			resetZeroLaxity(i);
		}
	}
	
	
	public void handle(Event event) {
		if(StaticConfig.PRINT_SCHEDULER_HANDLING){	
			if(event.getTask() != null)
				System.out.println("\t[ZeroLaxityHandler][INFO] Handling "+ event.getEventType() + " of "+event.getJob()+ " at time "+ event.getEventTimeValue());
			else
				System.out.println("\t[ZeroLaxityHandler][INFO] Handling "+ event.getEventType() + " "+ event.getEventTimeValue());
		}

		if(StaticConfig.PRINT_ZERO_LAXITY_EVENT) 
				System.out.println("\t\t\tINFOOO task:"+ event.getJob()
					      +" time:"+simulation.getSimulator().getCurrentTime().getTimeValue()+" C_residuo:"+event.getJob().getInfo().getRemainingExecution()
					      +" startExTime:" + event.getJob().getInfo().getStartTime() + " endExTime:"+event.getJob().getInfo().getFinishingTime()  
					      );
		
		switch (event.getEventType()) {
			case JOB_RELEASE:
				ZLHandleJobRelease(event);
				break;
			case JOB_START:
				ZLHandleJobStart(event);
				break;
			case JOB_RESUME:
				ZLHandleJobResume(event); 	
				break;
			case JOB_WAIT:
				ZLHandleJobWait(event);
				break;
			case JOB_PREEMPTION:
				ZLHandleJobPreemption(event); 
				break;
			case JOB_ZERO_LAXITY:
				ZLHandleJobZeroLaxity(event); 
				break;					
			case JOB_COMPLETION: 
					break;					
			case JOB_DEADLINE:
					break;		
			default:
				System.out.println("Founded a " + event.getEventType());
				assert(false);
		}//switch	
		
	}//handle

	
	
	private void ZLHandleJobRelease(Event event) {			
		resetZeroLaxity( event.getTask().getTaskId()  );
//		System.out.println("DEBUG JOBRELEASE TIME"+sim.getSimulator().getCurrentTime().getTimeValue() +" task:"+ event.getTask().getName());	
	}														
	
	

	private void ZLHandleJobResume(Event event) {
//		int taskID 		= event.getTask().getTaskId();
//		String taskName	= event.getTask().getName();
//		long jobID 		= event.getJob().getUniqueId();
//		long currTime 	= event.getEventTimeValue();
//		System.out.println("DEBUG : resume time"+currTime+" "+taskName+" jobID:"+jobID);
		
		//dovr� disinnescare la sua ZL	(se non � la causa che ha scatenato l'aumento di priorit� e ripresa dell'esecuzione, controllo col booleano ZL)
		//qui a differenza della jobStart, se non � ripartito per la ZL, DOVRA' avere un ZL da disinnescare!
		if(!hasZeroLaxity(event.getTask().getTaskId())	&& event.getJob().getEvent(EventType.JOB_ZERO_LAXITY) != null  && !event.getJob().hasMissed() && !event.getJob().hasOverrun() ){

			Event zlEventRemove = event.getJob().getEvent( EventType.JOB_ZERO_LAXITY);
			zlEventRemove.getJob().setEvent(EventType.JOB_ZERO_LAXITY, null);

			if ( !simulation.getEventManager().suppress( zlEventRemove )) assert(false);
			
			if(StaticConfig.PRINT_ZERO_LAXITY_EVENT)System.out.println("\t\tdisinnesco da RESUME per "+zlEventRemove.getTask().getName()+" stato:"+event.getJob().getState()+" ZL t="+zlEventRemove.getEventTimeValue());
		}
//		System.out.println("DEBUG JOBRESUME TIME"+sim.getSimulator().getCurrentTime().getTimeValue()+" task:"+ event.getTask().getName());	
	}

	
	
	private void ZLHandleJobStart(Event event) {	
		
		Job j = event.getJob();
		//controllo se ha o meno il boolean attivo, cio� se � scattata o meno l'evento zero laxity, e il task � entrato in esecuzione per questo aumento di priorit�
		//se cos� fosse non dovr� andare a disinnescare l'evento futuro ZL in quanto gi� verificato...
		if ( !hasZeroLaxity(event.getTask().getTaskId())	&& (j.getEvent(EventType.JOB_ZERO_LAXITY) != null) 	&&	 	(j.getState()!= JobState.RUNNING) && !event.getJob().hasMissed() && !event.getJob().hasOverrun() ){	 
						
			Event zlEventRemove = j.getEvent( EventType.JOB_ZERO_LAXITY);
			j.setEvent(EventType.JOB_ZERO_LAXITY, null);
			if ( !simulation.getEventManager().suppress( zlEventRemove )) assert(false);

			if(StaticConfig.PRINT_ZERO_LAXITY_EVENT)System.out.println("\t\tdisinnesco da START per "+zlEventRemove.getTask().getName()+" stato:"+j.getState()+" ZL t="+zlEventRemove.getEventTimeValue());
		}

//		System.out.println("DEBUG JOBSTART TIME"+sim.getSimulator().getCurrentTime().getTimeValue()+" task:"+ event.getTask().getName());
	}

	
	
	private void ZLHandleJobWait(Event event) {
		
		if ( hasZeroLaxity(event.getTask().getTaskId())  || event.getJob().hasMissed() ) return;

		long 	currTime 				= simulation.getSimulator().getCurrentTime().getTimeValue(),			
		 		remainingExecutionTime 	= event.getJob().getInfo().getRemainingExecution(),
		 		deadlineAssoluta 		= event.getJob().getEvent(EventType.JOB_DEADLINE).getEventTimeValue();
		
		if( ! (remainingExecutionTime >= 0) ) 	assert(false);
		long zlTime = deadlineAssoluta - remainingExecutionTime;
		
		
		if(StaticConfig.PRINT_ZERO_LAXITY_EVENT) 
			System.out.println("\t\tWAIT task:"+ event.getTask().getName()	+" job:"+event.getJob().getUniqueId()	
								+" time:"+currTime 	+" ZLTime:" + zlTime+" C_residuo:"+remainingExecutionTime);
		
		if(zlTime <= currTime){
			System.out.println("Problema istante ZL prima di release... WAIT");
			setZeroLaxity(event.getTask().getTaskId());
		}
	
		Event zlEvent = simulation.getEventFactory().getEventInstance(zlTime, EventType.JOB_ZERO_LAXITY, event.getTask());
		event.getJob().setEvent(zlEvent);
		zlEvent.setJob(event.getJob());
		
		simulation.getEventManager().trigger(zlEvent);
		if(StaticConfig.PRINT_ZERO_LAXITY_EVENT) System.out.println("\t\tinnesco ZL da wait, all'istante"+zlTime+" per task"+event.getJob());
	}
	
	
	
	private void ZLHandleJobPreemption(Event event) {
	

		if ( hasZeroLaxity(event.getTask().getTaskId()) || event.getJob().hasMissed() || event.getJob().hasOverrun()) return;
		
		//innesca un evento ZL
		long 	currTime 				= simulation.getSimulator().getCurrentTime().getTimeValue(),			
 				remainingExecutionTime 	= event.getJob().getInfo().getRemainingExecution(),
				deadlineAssoluta 		= event.getJob().getEvent(EventType.JOB_DEADLINE).getEventTimeValue();

		if(!(remainingExecutionTime >= 0)) assert false;
		long zlTime = deadlineAssoluta - remainingExecutionTime;
		
		if(StaticConfig.PRINT_ZERO_LAXITY_EVENT) 
			System.out.println("\t\tPREEMPT task:"+ event.getTask().getName()+" job:"+event.getJob().getUniqueId()
			+" time:"+currTime 	+" ZLTime:" + zlTime +" D_ass:"+deadlineAssoluta+" C_residuo:"+remainingExecutionTime);
		
		if(zlTime <= currTime){
			System.out.println("Problema istante ZL prima di release... PREEMPTION " + event.getJob());
			setZeroLaxity(event.getTask().getTaskId());
		}
				
		Event zlEvent = simulation.getEventFactory().getEventInstance(zlTime, EventType.JOB_ZERO_LAXITY, event.getTask());
		event.getJob().setEvent(zlEvent);
		zlEvent.setJob(event.getJob());
		
		simulation.getEventManager().trigger(zlEvent);
		if(StaticConfig.PRINT_ZERO_LAXITY_EVENT) System.out.println("\t\tinnesco ZL da PREEMPTION, all'istante"+zlTime+" per task"+event.getJob());
	}
	
	
	
	private void ZLHandleJobZeroLaxity(Event event) {
	
		setZeroLaxity(event.getTask().getTaskId());
		
		//scateno subito un evento priority_change
		long 	currTime = simulation.getSimulator().getCurrentTime().getTimeValue();
		Job j = event.getJob();
		Event priorityChangeEvent = simulation.getEventFactory().getEventInstance(currTime, EventType.JOB_PRIORITY_CHANGE, j.getTask());	
		j.setEvent(priorityChangeEvent);
		priorityChangeEvent.setJob(j);
		simulation.fire(priorityChangeEvent);
	}
	
	
	
	@Override
	public String getName() {
		return "ZeroLaxityHandler";
	}
	
	

}//class
