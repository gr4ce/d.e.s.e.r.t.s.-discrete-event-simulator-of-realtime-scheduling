package edu.unibo.ciri.realtime.policies.scheduling;

import edu.unibo.ciri.realtime.model.Job;

public interface JobComparator {
	
	int compare(Job job1, Job job2);

}
