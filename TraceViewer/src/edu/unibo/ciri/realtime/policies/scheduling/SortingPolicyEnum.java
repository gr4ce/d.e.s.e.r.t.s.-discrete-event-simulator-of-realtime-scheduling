package edu.unibo.ciri.realtime.policies.scheduling;

public enum SortingPolicyEnum {
	
	RATE_MONOTONIC,
	DEADLINE_MONOTONIC,
	DkC,
	USER_DEFINED

}
