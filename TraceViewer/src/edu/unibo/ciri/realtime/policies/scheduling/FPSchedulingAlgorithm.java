package edu.unibo.ciri.realtime.policies.scheduling;

import edu.unibo.ciri.realtime.model.Job;
import edu.unibo.ciri.realtime.policies.scheduling.base.PriorityComparator;
import edu.unibo.ciri.realtime.policies.scheduling.base.SchedulingAlgorithm;


public class FPSchedulingAlgorithm implements SchedulingAlgorithm {
	
	private PriorityComparatorImpl priorityComparator = new PriorityComparatorImpl();

	private class PriorityComparatorImpl implements PriorityComparator{

		private FixedPriorityAssignment priorityAssignment;
		
		public  FixedPriorityAssignment getPriorityAssignment(){
			return priorityAssignment;
		}
		
		public void setPriorityAssignment(FixedPriorityAssignment pa){
			this.priorityAssignment = pa;
		}
		
		@Override
		public int compare(Job job1, Job job2) {
			
			int priority1 = this.priorityAssignment.getPriority(job1.getTask());
			int priority2 = this.priorityAssignment.getPriority(job2.getTask());
			
			/* Assuming that the lower the numeric value the higher the priority */
			return  -(priority1 - priority2);
		}
		
	}
	
	public void setPriorityAssignment(FixedPriorityAssignment pa){
		priorityComparator.setPriorityAssignment(pa);
	}
	
	public FixedPriorityAssignment getPriorityAssignment(){
		return priorityComparator.getPriorityAssignment();
	}
	
	
	@Override
	public String getAlgorithmName() {
		
		return "Fixed Priority";
	}

	@Override
	public PriorityComparator getPriorityComparator() {
		
		return priorityComparator;
	}

}
