package edu.unibo.ciri.realtime.policies.scheduling.base;

import edu.unibo.ciri.realtime.model.Job;

public interface PriorityComparator {
	
	public int compare(Job j1, Job j2);

}
