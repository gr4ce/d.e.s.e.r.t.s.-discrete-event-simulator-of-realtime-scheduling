package edu.unibo.ciri.realtime.policies.scheduling;

import edu.unibo.ciri.realtime.model.Job;
import edu.unibo.ciri.realtime.policies.scheduling.base.PartitioningScheme;
import edu.unibo.ciri.realtime.policies.scheduling.base.PriorityComparator;
import edu.unibo.ciri.realtime.policies.scheduling.base.SchedulingAlgorithm;
import edu.unibo.ciri.realtime.policies.scheduling.base.SchedulingPolicy;

public class EDZLSchedulingPolicy extends SchedulingPolicy {
	
	public static final String policyName = "EDZL";
	
	private ZeroLaxityHandler 		handler;
	private PriorityComparatorImpl 	priorityComparator 	= new PriorityComparatorImpl();	
	private PriorityComparator 		supercomparator;
	private SchedulingAlgorithm     baseSchedulingAlgorithm;
			
	private class PriorityComparatorImpl implements PriorityComparator{
	
				
		@Override
		public int compare(Job job1, Job job2) {
			
			boolean lax1 = handler.hasZeroLaxity(job1.getTask().getTaskId()) , 
					lax2 = handler.hasZeroLaxity(job2.getTask().getTaskId()) ;
			
			if( lax1 ){				
				if (  lax2 ) {
					//se entrambi hanno stessa laxity=0 (0,0), far� riferimento all'algo. originale
					return supercomparator.compare(job1, job2);
				}
				else{ 
					// ha laxity=0 solo il job1 ed � prioritario (1,0)
					return 1;
				}
			}
			else{				
				if ( lax2 ) {
					// ha laxity=0 solo il job2 ed � prioritario (0,1)
					return -1;
				}
				else{ 
					// se entrambi non hanno laxity=0 (1,1), far� riferimento all'algo. originale
					return supercomparator.compare(job1, job2);
				}					
			}
			
		}//compare
		
	}//PriorityComparator
		
	
	
	

	
	public EDZLSchedulingPolicy( ZeroLaxityHandler handler ) {
		assert(handler != null);
		this.handler = handler;
		
		super.policyName              = policyName;		
		super.partitioningScheme      = PartitioningScheme.GLOBAL;
		super.priorityAssignementMode = PriorityAssignmentMode.DYNAMIC;
		baseSchedulingAlgorithm       = new EDFSchedulingAlgorithm();
		supercomparator               = baseSchedulingAlgorithm.getPriorityComparator();
		super.schedulingAlgorithm     = new ZLSchedulingAlgorithm();
	}

	
	public void setZLHandler(ZeroLaxityHandler zlhandler){
		handler = zlhandler;
	}

	public String getName() {		
		return policyName;
	}

	@Override
	public PriorityComparator getPriorityComparator() {	
		return priorityComparator;
	}
	
	@Override
	public String getSchedulingAlgorithmName(){
		return baseSchedulingAlgorithm.getAlgorithmName() + " with " + super.schedulingAlgorithm.getAlgorithmName();
	}


	@Override
	public boolean isMultiprocessor() {
		return true;
	}
	
}
