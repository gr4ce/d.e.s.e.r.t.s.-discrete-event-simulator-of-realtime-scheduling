package edu.unibo.ciri.realtime.policies.scheduling.base;

import edu.unibo.ciri.realtime.policies.scheduling.PriorityAssignmentMode;

public abstract class SchedulingPolicy {
	
	
	protected String policyName;
	
	protected SchedulingAlgorithm    schedulingAlgorithm     = null;
	protected StaticSortingAlgorithm sortingAlgorithm        = null;
	protected PartitioningAlgorithm  partiotioningAlgorithm  = null;
	protected PartitioningScheme	 partitioningScheme		 = null;
	protected PriorityAssignmentMode priorityAssignementMode = null;
	
	
	public String getPolicyName(){
		return policyName;
	}
	
	public abstract boolean isMultiprocessor();
		
	public SchedulingAlgorithm getSchedulingAlgorithm(){
		return schedulingAlgorithm;
	}
		
	public StaticSortingAlgorithm getStaticSortingPolicy(){
		return sortingAlgorithm;
	}
	
	public PartitioningAlgorithm  getPartitioningAlgorithm(){
		return partiotioningAlgorithm;
	}
	
	public abstract PriorityComparator getPriorityComparator();
		
	@Override
	public String toString() {
		
		StringBuffer desc = new StringBuffer();
		
		desc.append("Scheduling policy name    : " + policyName + "\n");
		desc.append("Priority assignment mode  : " + priorityAssignementMode + "\n");
		desc.append("Scheduling algorithm      : " + getSchedulingAlgorithmName() + "\n");
						
		String sortingPolicyName;
		if(sortingAlgorithm != null){
			sortingPolicyName = sortingAlgorithm.getName();
			desc.append("Priority sorting algorithm: " + sortingPolicyName + "\n");
		}
				
		if(!isMultiprocessor()){			
			desc.append("Platform supported        : Single-processor\n");
			return desc.toString();
		}
				
		desc.append("Platform supported        : Multi-processor\n");
		desc.append("Partitioning scheme       : " + partitioningScheme + "\n");
		
		String partiotioningAlgorithmName;
		if(partiotioningAlgorithm != null){			
			partiotioningAlgorithmName = partiotioningAlgorithm.getAlgorithmName();
			desc.append("Priority sorting algorithm: " + partiotioningAlgorithmName + "\n");
		}
				
		return desc.toString();
	}
	
	
	public String getSchedulingAlgorithmName(){
		return schedulingAlgorithm.getAlgorithmName();
	}

}
