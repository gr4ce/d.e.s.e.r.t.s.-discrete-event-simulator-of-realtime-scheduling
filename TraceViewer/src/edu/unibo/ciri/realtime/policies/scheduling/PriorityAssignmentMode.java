package edu.unibo.ciri.realtime.policies.scheduling;

public enum PriorityAssignmentMode {
	
	
	
	FIXED_TASK_PRIORITY ("fixed-task"), 
	FIXED_JOB_PRIORITY ("fixed-job"), 
	DYNAMIC ("dynamic");
	
	private final String name;
	
	private PriorityAssignmentMode(String name) {
		this.name = name;
	}

	@Override
	public String toString(){
		return this.name;
	}
}
