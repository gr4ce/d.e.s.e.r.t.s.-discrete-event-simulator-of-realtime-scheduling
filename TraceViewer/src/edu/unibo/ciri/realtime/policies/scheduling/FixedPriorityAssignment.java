package edu.unibo.ciri.realtime.policies.scheduling;

import edu.unibo.ciri.realtime.model.Task;


public class FixedPriorityAssignment {
	
	private int[] priorities;
	private int[] taskIds;
	private Task[] tasks;
	
	public FixedPriorityAssignment(Task[] taskArray){
		
		int size = taskArray.length;
		
		tasks = taskArray;
		
		taskIds = new int[size];
		priorities = new int[size];
		
		for(int i = 0; i < size; i++){
			priorities[taskArray[i].getTaskId()] = i;
			taskIds[i] = taskArray[i].getTaskId();
		}
		
	}	
	
	public Task[] getSortedTaskArray(){		
		return tasks;
	}
	
	public int getPriority(Task t){
		return priorities[t.getTaskId()];
	}
	
	public int getPriority(int taskId){
		return priorities[taskId];
	}
	
	public Task getTaskByPriority(int priority){
		return tasks[priority];
	}
	
	public int getTaskId(int priority){
		return taskIds[priority];
	}

}
