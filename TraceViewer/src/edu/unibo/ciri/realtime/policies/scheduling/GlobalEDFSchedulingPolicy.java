package edu.unibo.ciri.realtime.policies.scheduling;

import edu.unibo.ciri.realtime.policies.scheduling.base.PartitioningScheme;
import edu.unibo.ciri.realtime.policies.scheduling.base.PriorityComparator;
import edu.unibo.ciri.realtime.policies.scheduling.base.SchedulingPolicy;


public class GlobalEDFSchedulingPolicy extends SchedulingPolicy {
	
	public static final String policyName = "GlobalEDF";
	
	public GlobalEDFSchedulingPolicy(){
		
		super.policyName              = policyName;
		super.partitioningScheme      = PartitioningScheme.GLOBAL;
		super.priorityAssignementMode = PriorityAssignmentMode.FIXED_JOB_PRIORITY;
		super.schedulingAlgorithm     = new EDFSchedulingAlgorithm();
		
	}
		
	

	@Override
	public PriorityComparator getPriorityComparator() {
		
		return schedulingAlgorithm.getPriorityComparator();
	}



	@Override
	public String getPolicyName() {		
		return "Global-EDF";
	}



	



	@Override
	public boolean isMultiprocessor() {		
		return true;
	}


}
