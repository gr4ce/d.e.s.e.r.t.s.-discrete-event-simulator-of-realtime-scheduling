package edu.unibo.ciri.realtime.policies.scheduling.base;


public interface SchedulingAlgorithm {
	
	public String getAlgorithmName();
	
	public PriorityComparator getPriorityComparator();

}
