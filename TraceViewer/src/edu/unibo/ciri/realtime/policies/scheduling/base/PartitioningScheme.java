package edu.unibo.ciri.realtime.policies.scheduling.base;

public enum PartitioningScheme {
	
	GLOBAL 			 ("global"), 
	SEMI_PARTITIONED ("semi-partitioned"), 
	PARTITIONED      ("partiotioned"), 
	CLUSTERED		 ("clustered");
	
	private String name;
	
	private PartitioningScheme(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		
		return name;
	}
	
	
};