package edu.unibo.ciri.realtime.policies.scheduling;

import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.policies.scheduling.base.PartitioningScheme;
import edu.unibo.ciri.realtime.policies.scheduling.base.PriorityComparator;
import edu.unibo.ciri.realtime.policies.scheduling.base.SchedulingPolicy;
import edu.unibo.ciri.realtime.policies.staticsorting.RMSortingPolicy;


public class GlobalRMSchedulingPolicy extends SchedulingPolicy {
	
	public static final String policyName = "GlobalRM";
	
	public GlobalRMSchedulingPolicy(TaskSet t){
		
		super.policyName              = policyName;
		super.partitioningScheme      = PartitioningScheme.GLOBAL;
		super.priorityAssignementMode = PriorityAssignmentMode.FIXED_TASK_PRIORITY;
		super.sortingAlgorithm		  = new RMSortingPolicy();
		
		FPSchedulingAlgorithm fpsa    = new FPSchedulingAlgorithm();
		FixedPriorityAssignment fpa   = super.sortingAlgorithm.getStaticPriorityAssignment(t);
		fpsa.setPriorityAssignment(fpa);
		
		super.schedulingAlgorithm     = fpsa;
		
	}
			
	

	@Override
	public PriorityComparator getPriorityComparator() {		
		return schedulingAlgorithm.getPriorityComparator();
	}



	@Override
	public boolean isMultiprocessor() {		
		return true;
	}


}
