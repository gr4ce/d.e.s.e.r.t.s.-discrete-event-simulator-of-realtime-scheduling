package edu.unibo.ciri.realtime.policies.scheduling;

import edu.unibo.ciri.realtime.model.Job;
import edu.unibo.ciri.realtime.policies.scheduling.base.PriorityComparator;
import edu.unibo.ciri.realtime.policies.scheduling.base.StaticSortingAlgorithm;

public class FixedPriorityZeroLaxitySchedulingPolicy extends FixedPrioritySchedulingPolicy {
	
	private StaticSortingAlgorithm sortingPolicy;
	
	
	private class PriorityComparatorImpl implements PriorityComparator{
	
		private FixedPriorityAssignment priorityAssignment;
		
		public  FixedPriorityAssignment getPriorityAssignment(){
			return priorityAssignment;
		}
		
		public void setPriorityAssignment(FixedPriorityAssignment pa){
			this.priorityAssignment = pa;		
		}
		
		@Override
		public int compare(Job job1, Job job2) {
			
			boolean lax1=handler.hasZeroLaxity(job1.getTask().getTaskId()) , 
					lax2=handler.hasZeroLaxity(job2.getTask().getTaskId()) ;
			
			if( lax1 ){
				if (  lax2 ) {
					//se entrambi hanno stessa laxity=0 (0,0)
					return supercomparator.compare(job1, job2);
				}
				else{ 
					// ha laxity=0 il job1 ed � prioritario (1,0)
					return 1;
				}
			}
			else{
				if ( lax2 ) {
					// ha laxity=0 il job2 ed � prioritario (0,1)
					return -1;
				}
				else{ 
					// se entrambi non hanno laxity=0 (1,1)
					return supercomparator.compare(job1, job2);
				}					
			}
			
		}//compare
		
	}//PriorityComparator
	
		
	private ZeroLaxityHandler 		handler;
	private PriorityComparatorImpl 	priorityComparator 	= new PriorityComparatorImpl();	
	private PriorityComparator 		supercomparator 	;
	
	
	//costruttore
	public FixedPriorityZeroLaxitySchedulingPolicy(StaticSortingAlgorithm ssp, ZeroLaxityHandler handler) {
		super(ssp);
//		assert(handler != null); 	//commentato perch� nella sim.withSuite, pu� servirmi di crearne uno senza sapere l'handler
		this.handler = handler;		//quando ho SIMULATOR_REUSE_RESOURCES=true e assegnarlo subito dopo, e ogni volta che lo riuso... 
	}
	
	public void setZLHandler(ZeroLaxityHandler handler){
		this.handler=handler;
	}
	
	public void setPriorityAssignment(FixedPriorityAssignment pa){
		priorityComparator.setPriorityAssignment(pa);
		super.setPriorityAssignment(pa);				
		supercomparator = super.getPriorityComparator();
	}
	
	public FixedPriorityAssignment getPriorityAssignment(){
		return priorityComparator.getPriorityAssignment();
	}

	public String getName() {		
		return sortingPolicy.getName();
	}

	public PriorityComparator getPriorityComparator() {	
		assert(priorityComparator.getPriorityAssignment() != null);
		return priorityComparator;
	}
	
	public void setSortingPolicy(StaticSortingAlgorithm sortingPolicy) {
		this.sortingPolicy = sortingPolicy;
	}

	public StaticSortingAlgorithm getSortingPolicy() {
		return sortingPolicy;
	}
	

	
}
