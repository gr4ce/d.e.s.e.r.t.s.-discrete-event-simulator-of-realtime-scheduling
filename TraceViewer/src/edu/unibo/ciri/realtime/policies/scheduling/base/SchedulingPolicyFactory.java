package edu.unibo.ciri.realtime.policies.scheduling.base;

import java.security.InvalidAlgorithmParameterException;


import edu.unibo.ciri.desert.simulation.management.Simulation;
import edu.unibo.ciri.realtime.policies.scheduling.EDZLSchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.GlobalDMSchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.GlobalEDFSchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.GlobalRMSchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.GlobalRMZLSchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.ZeroLaxityHandler;

public class SchedulingPolicyFactory {
	
	private Simulation simulation;
	
	public SchedulingPolicyFactory(Simulation sim){
		simulation = sim;
	}
	
	
	
	public SchedulingPolicy newInstanceByName(String name) throws InvalidAlgorithmParameterException{
		
		if(name.equals(GlobalEDFSchedulingPolicy.policyName))
			return new GlobalEDFSchedulingPolicy();
		
		if(name.equals(GlobalRMSchedulingPolicy.policyName))
			return new GlobalRMSchedulingPolicy(simulation.getTaskset());
		
		if(name.equals(GlobalDMSchedulingPolicy.policyName))
			return new GlobalDMSchedulingPolicy(simulation.getTaskset());
		
		if(name.equals(EDZLSchedulingPolicy.policyName)){
			ZeroLaxityHandler zlHandler = new ZeroLaxityHandler(simulation);
			return new EDZLSchedulingPolicy(zlHandler);
		}
		
		if(name.equals(GlobalRMZLSchedulingPolicy.policyName)){
			ZeroLaxityHandler zlHandler = new ZeroLaxityHandler(simulation);
			return new GlobalRMZLSchedulingPolicy(zlHandler, simulation.getTaskset());
		}
		
		throw new InvalidAlgorithmParameterException();
	
		
	}
		
	
	
	

}
