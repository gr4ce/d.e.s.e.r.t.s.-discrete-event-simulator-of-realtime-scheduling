package edu.unibo.ciri.realtime.policies.scheduling;

import java.util.Enumeration;
import java.util.Hashtable;

import edu.unibo.ciri.realtime.model.Task;
import edu.unibo.ciri.realtime.model.TaskSet;


public class PreemptionThresholdAssignment {
	
	private int[]  thresholds;
	private int[]  taskIds;
	
	private Hashtable<Task,Integer> assignment = new Hashtable<Task,Integer>();
	private boolean isFeasible = false;
	
//	public PreemptionThresholdAssignment(){}
	
	public PreemptionThresholdAssignment(int size){
		
		thresholds 	= new int[size];
		taskIds		= new int[size];
	}
	
	public void addTaskThresholdPair(Task task, int threshold){
		assignment.put(task, new Integer(threshold));
		thresholds[task.getTaskId()] = threshold;
		taskIds[threshold] = task.getTaskId();
	}
	
	public void setTaskThresholdPair(Task task, int threshold){
		assignment.remove(task);
		assignment.put(task, new Integer(threshold));
	}
	
	public void setFeasible(){
		isFeasible = true;
	}
	
	public boolean isFeasible(){
		return isFeasible;
	}
	
//	public int getThresholdByTaskName(String name){
//		return assignment.get(name).intValue();
//	}
	
	public int getThreshold(Task task){
		return thresholds[task.getTaskId()];
	}
	
	public int getThreshold(int taskIds){
		return thresholds[taskIds];
	}
	
	
	
	public String toString(){
		String s = "{";
		Enumeration<Task> keys = assignment.keys();
		while(keys.hasMoreElements()){
			Task task = keys.nextElement();
			s += "(" + task.getName() + ",";
			s += assignment.get(task) + "),";
		}
		
		s = s.substring(0, s.length()-1);
		s += "}";
		
		return s;
	}
	
	public void assignThreshold(TaskSet ts){
		Enumeration<Task> keys = assignment.keys();
		while(keys.hasMoreElements()){
			Task task = keys.nextElement();
			ts.getTask(task.getName()).getTaskParameters().setThreshold(assignment.get(task));			
		}
	}
	
	public PreemptionThresholdAssignment clone(){
		
		PreemptionThresholdAssignment copy = new PreemptionThresholdAssignment(thresholds.length);
		Enumeration<Task> keys = assignment.keys();
		while(keys.hasMoreElements()){
			Task t = keys.nextElement();
			copy.addTaskThresholdPair( t, assignment.get(t));
		}
		return copy;
	}

}
	
	