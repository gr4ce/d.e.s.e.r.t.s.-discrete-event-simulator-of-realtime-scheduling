package edu.unibo.ciri.realtime.policies.scheduling;

import edu.unibo.ciri.realtime.model.Job;
import edu.unibo.ciri.realtime.policies.scheduling.base.PriorityComparator;
import edu.unibo.ciri.realtime.policies.scheduling.base.SchedulingPolicy;
import edu.unibo.ciri.realtime.policies.scheduling.base.StaticSortingAlgorithm;

public class FixedPrioritySchedulingPolicy extends SchedulingPolicy{
	
	private StaticSortingAlgorithm sortingPolicy;
	
	private class PriorityComparatorImpl implements PriorityComparator{

		private FixedPriorityAssignment priorityAssignment;
		
		public  FixedPriorityAssignment getPriorityAssignment(){
			return priorityAssignment;
		}
		
		public void setPriorityAssignment(FixedPriorityAssignment pa){
			this.priorityAssignment = pa;
		}
		
		@Override
		public int compare(Job job1, Job job2) {
			
			int priority1 = this.priorityAssignment.getPriority(job1.getTask());
			int priority2 = this.priorityAssignment.getPriority(job2.getTask());
			
			/* Assuming that the lower the numeric value the higher the priority */
			return  -(priority1 - priority2);
		}
		
	}
	
	private PriorityComparatorImpl priorityComparator = new PriorityComparatorImpl();
	
	public FixedPrioritySchedulingPolicy(StaticSortingAlgorithm ssp){
		setSortingPolicy(ssp);
	}	
	
	public void setPriorityAssignment(FixedPriorityAssignment pa){
		priorityComparator.setPriorityAssignment(pa);
	}
	
	public FixedPriorityAssignment getPriorityAssignment(){
		return priorityComparator.getPriorityAssignment();
	}

	
	@Override
	public PriorityComparator getPriorityComparator() {	
		assert(priorityComparator.getPriorityAssignment() != null);
		return priorityComparator;
	}
	
	public void setSortingPolicy(StaticSortingAlgorithm sortingPolicy) {
		this.sortingPolicy = sortingPolicy;
	}

	public StaticSortingAlgorithm getSortingPolicy() {
		return sortingPolicy;
	}

	@Override
	public boolean isMultiprocessor() {
		// TODO Auto-generated method stub
		return false;
	}

}
