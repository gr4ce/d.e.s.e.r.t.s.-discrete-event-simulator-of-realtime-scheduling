package edu.unibo.ciri.realtime.policies.scheduling;

import edu.unibo.ciri.realtime.model.Job;
import edu.unibo.ciri.realtime.model.TaskSet;
import edu.unibo.ciri.realtime.policies.scheduling.base.PartitioningScheme;
import edu.unibo.ciri.realtime.policies.scheduling.base.PriorityComparator;
import edu.unibo.ciri.realtime.policies.scheduling.base.SchedulingPolicy;
import edu.unibo.ciri.realtime.policies.staticsorting.RMSortingPolicy;

public class GlobalRMZLSchedulingPolicy extends SchedulingPolicy{
	
	public static final String policyName = "GlobalRMZL";
	
	private ZeroLaxityHandler 				handler;
	private PriorityComparatorImpl 			priorityComparator; 	
	private PriorityComparator 				rmComparator;
	private FPSchedulingAlgorithm 			fixedPrioritySchedulingAlgorithm;
	
	
	public GlobalRMZLSchedulingPolicy(ZeroLaxityHandler handler, TaskSet t){
		
		assert(handler != null);
		this.handler = handler;
		
		super.policyName              = policyName;		
		super.partitioningScheme      = PartitioningScheme.GLOBAL;
		super.priorityAssignementMode = PriorityAssignmentMode.DYNAMIC;
		super.schedulingAlgorithm     = new ZLSchedulingAlgorithm();		
		
		priorityComparator			  = new PriorityComparatorImpl();	
		
		
		fixedPrioritySchedulingAlgorithm   	= new FPSchedulingAlgorithm();
		super.sortingAlgorithm         		= new RMSortingPolicy();
		FixedPriorityAssignment fpa   		= super.sortingAlgorithm.getStaticPriorityAssignment(t);
		
		fixedPrioritySchedulingAlgorithm.setPriorityAssignment(fpa);		
		
		priorityComparator.setPriorityAssignment(fpa);
		rmComparator  = fixedPrioritySchedulingAlgorithm.getPriorityComparator();
		
	}
	
	
	private class PriorityComparatorImpl implements PriorityComparator{
		
		private FixedPriorityAssignment priorityAssignment;
		
		public  FixedPriorityAssignment getPriorityAssignment(){
			return priorityAssignment;
		}
		
		public void setPriorityAssignment(FixedPriorityAssignment pa){
			this.priorityAssignment = pa;		
		}
		
		@Override
		public int compare(Job job1, Job job2) {
			
			boolean job1HasZeroLaxity = handler.hasZeroLaxity(job1.getTask().getTaskId()) , 
					job2HasZeroLaxity = handler.hasZeroLaxity(job2.getTask().getTaskId()) ;
			
			if( job1HasZeroLaxity ){
				if (  job2HasZeroLaxity ) {
					//se entrambi hanno stessa laxity=0 (0,0)
					return rmComparator.compare(job1, job2);
				}
				else{ 
					// ha laxity=0 il job1 ed � prioritario (1,0)
					return 1;
				}
			}
			else{
				if ( job2HasZeroLaxity ) {
					// ha laxity=0 il job2 ed � prioritario (0,1)
					return -1;
				}
				else{ 
					// se entrambi non hanno laxity=0 (1,1)
					return rmComparator.compare(job1, job2);
				}					
			}
			
		}//compare
		
	}//PriorityComparator

	public void setPriorityAssignment(FixedPriorityAssignment pa){
		priorityComparator.setPriorityAssignment(pa);
		
		FPSchedulingAlgorithm fpsa    = new FPSchedulingAlgorithm();	 
		fpsa.setPriorityAssignment(pa);
		
		rmComparator = fpsa.getPriorityComparator();
	}
	
	public FixedPriorityAssignment getPriorityAssignment(){
		return priorityComparator.getPriorityAssignment();
	}
	
	@Override
	public boolean isMultiprocessor() {
		return true;
	}
	
	@Override
	public String getSchedulingAlgorithmName(){
		return fixedPrioritySchedulingAlgorithm.getAlgorithmName() + " with " + super.getSchedulingAlgorithmName();
	}

	
	@Override
	public PriorityComparator getPriorityComparator() {
		return priorityComparator;
	}

}
