package edu.unibo.ciri.realtime.policies.scheduling;

import edu.unibo.ciri.desert.event.EventType;
import edu.unibo.ciri.realtime.model.Job;
import edu.unibo.ciri.realtime.model.Job.JobState;
import edu.unibo.ciri.realtime.policies.scheduling.base.PriorityComparator;
import edu.unibo.ciri.realtime.policies.scheduling.base.SchedulingAlgorithm;

public class ZLSchedulingAlgorithm implements SchedulingAlgorithm {

	PriorityComparator priorityComparator = new PriorityComparator(){

		@Override
		public int compare(Job job1, Job job2) {
			
			long absoluteDeadline1 = job1.getEvent(EventType.JOB_DEADLINE).getEventTime().getTimeValue();
			long absoluteDeadline2 = job2.getEvent(EventType.JOB_DEADLINE).getEventTime().getTimeValue();
					
			if(absoluteDeadline1 > absoluteDeadline2)
				return -1;
			
			if(absoluteDeadline1 < absoluteDeadline2)
				return 1;
			
			
			/* If the two jobs have the same deadline prefer the one that was previusly running.
			 * If both are running prefer job1 */
			if(job1.getState() == JobState.RUNNING)
				return 1;
			if(job2.getState() == JobState.RUNNING)
			 	return -1;
			return 0;
			
		}
		
	};
	
	@Override
	public String getAlgorithmName() {
		
		return "Zero Laxity Priority Promotion";
	}

	@Override
	public PriorityComparator getPriorityComparator() {
		
		return priorityComparator;
	}

}
