package edu.unibo.ciri.collection;

public class SingleLinkedList<E extends Comparable<E>> {
	
	protected int size;
	protected SingleLinkElementWrap<E> head;
	protected SingleLinkElementWrap<E> tail;
	
	
	public SingleLinkedList(){
		initList();
	}
	
	
	
	private void initList(){
		head = new SingleLinkElementWrap<E>(null);
		tail = new SingleLinkElementWrap<E>(null);
		head.setNext(tail);
		size = 0;
	}
	
		
	public int size(){		
		return size;
	}
	
	protected void insertBetween(SingleLinkElementWrap<E> target, SingleLinkElementWrap<E> prev, SingleLinkElementWrap<E> next){
		
		target.setNext(next);
		prev.setNext(target);
		size++;		
	}
	
	protected void removeBetween(SingleLinkElementWrap<E> target, SingleLinkElementWrap<E> prev, SingleLinkElementWrap<E> next){		
		prev.setNext(next);
		target.unlink();
		size--;
	}
	
	public int positionOf(E elem){
		
		if(elem == null || size == 0)
			return -1;
		
		SingleLinkElementWrap<E> linkedElem = head;
		
		int count = 0;
		while((linkedElem = linkedElem.getNext()) != tail 
				&& !linkedElem.getContent().equals(elem) )
			count++;	
		
		return count;
	}
	
	
	
	public void clear(){
		
		//TODO really need that?????? You can't simply unlink head and tail
		SingleLinkElementWrap<E> curr = head.getNext();
		SingleLinkElementWrap<E> prev = curr;
		while((curr = curr.getNext()) != tail){
			prev.setNext(null);
			prev = curr;
		}
		
		initList();
	}
	
	
	
	public boolean removeElementAt(int position){		
		
		if( position >= size || position < 0)
			return false;
		
		int count = 0;
		SingleLinkElementWrap<E> curr = head.getNext();
		SingleLinkElementWrap<E> prev = null;
		while( count != position ){
			prev = curr;
			curr = curr.getNext();
			count++;
		}
		assert(prev != null);
		removeBetween(curr, prev, curr.getNext());
		return true;
	
	}
	
	
	public boolean removeElement(E elem){
		
		if(elem == null)
			return false;
		
		if(!this.contains(elem))
			return false;
				
		SingleLinkElementWrap<E> curr = head;
		SingleLinkElementWrap<E> prev = curr;
		while( !(curr = curr.getNext()).getContent().equals(elem) ){
			prev = curr;
		}
		removeBetween(curr, prev, curr.getNext());
		return true;				
	}
	
	public E popHead(){
		
		//or test size == 0
		if(head.getNext() == tail)
			return null;
		
		SingleLinkElementWrap<E> first = head.getNext();
		E elem = first.getContent();
		assert(elem != null);		 
		
		removeBetween(first, head, first.getNext());
		
		assert(size >= 0);		
		return elem;
	}
	
	public E getHead(){
						
		return head.getNext().getContent();
	}
	
	public E getNext(E elem){
		
		SingleLinkElementWrap<E> curr = head;
		while( (curr = curr.getNext()) != tail){
			if(elem.equals(curr.getContent()) && curr.getNext() != tail){
				return curr.getNext().getContent();
			}
		}
		return null;
	}
	
	
	
	public void enqueue(E elem){    	
		SingleLinkElementWrap<E> newTail = new SingleLinkElementWrap<E>(null);
		tail.setContent(elem);  
		tail.setNext(newTail);	
		tail = newTail;
	}
    
    
	public boolean contains(E elem){
		
		SingleLinkElementWrap<E> current = head;
		while((current = current.getNext()) != tail){
			if(current.getContent().equals(elem))
				return true;
		}
		return false;		
	}
	
	public boolean hasMoreElements(){
		
		if(head.getNext() == tail)
			return false;
		return true;
		
	}	
	
	public String toString(){
		
		if(head.getNext() == tail)
			return "empty";
		
		StringBuffer sb = new StringBuffer();
		SingleLinkElementWrap<E> current = head;
		sb.append("HEAD");
		
		while((current = current.getNext())!= tail){
			sb.append( " -> " + current.getContent().toString() );			
		}
		
		if(current == tail)
			sb.append(" -> TAIL");
		
		return sb.toString();
	}

}
