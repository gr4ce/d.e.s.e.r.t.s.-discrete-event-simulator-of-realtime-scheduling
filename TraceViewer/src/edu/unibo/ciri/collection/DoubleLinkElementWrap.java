package edu.unibo.ciri.collection;



public class DoubleLinkElementWrap<E extends Comparable<E>> {
	
	private DoubleLinkElementWrap<E> prev = null;
	private DoubleLinkElementWrap<E> next = null;
	
	private E content = null;
	
	public DoubleLinkElementWrap(){
		//empty
	}
	
	public  DoubleLinkElementWrap(E element){
		content = element;
	}
	
	
	public DoubleLinkElementWrap<E> getPrev() {		
		return prev;
	}
	
	public void setPrev(DoubleLinkElementWrap<E> prev) {
		this.prev = prev;
	}
	
	public DoubleLinkElementWrap<E> getNext() {
		return next;
	}
	
	public void setNext(DoubleLinkElementWrap<E> next) {
		this.next = next;
	}
	public E getContent() {
		return content;
	}
	public void setContent(E content) {
		this.content = content;
	} 
	
	public void unlink(){
		this.next = null;
		this.prev = null;
	}
	
	public void clear(){
		unlink();
		content = null;
	}
	
	public int compareTo(DoubleLinkElementWrap<E> o) {
		return this.getContent().compareTo(o.getContent());
		
	}

	

}
