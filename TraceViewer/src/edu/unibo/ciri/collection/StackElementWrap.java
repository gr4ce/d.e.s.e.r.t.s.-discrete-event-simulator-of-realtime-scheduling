package edu.unibo.ciri.collection;

public class StackElementWrap<E>{
	
	private StackElementWrap<E> prev = null;
	private StackElementWrap<E> next = null;
	
	private E content = null;
	
	public StackElementWrap(){
		
	}
	
	public StackElementWrap(E element){
		content = element;
	}
	
	
	public StackElementWrap<E> getPrev() {		
		return prev;
	}
	
	public void setPrev(StackElementWrap<E> prev) {
		this.prev = prev;
	}
	
	public StackElementWrap<E> getNext() {
		return next;
	}
	
	public void setNext(StackElementWrap<E> next) {
		this.next = next;
	}
	public E getContent() {
		return content;
	}
	public void setContent(E content) {
		this.content = content;
	} 
	
	public void unlink(){
		this.next = null;
		this.prev = null;
	}
	
	public void clear(){
		unlink();
		content = null;
	}
	
}
