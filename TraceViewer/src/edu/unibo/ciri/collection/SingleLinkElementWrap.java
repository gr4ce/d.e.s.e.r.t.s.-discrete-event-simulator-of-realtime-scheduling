package edu.unibo.ciri.collection;



public class SingleLinkElementWrap<E> {
	
	
	private SingleLinkElementWrap<E> next = null;
	private E content;
	private int id = -1;
	private static int globalId = 0;
	
	
	public SingleLinkElementWrap(){
		id = globalId++;
		content = null;
	}
	
	public int getId(){
		return id;
	}
	
	public SingleLinkElementWrap(E element){
		id = globalId++;
		content = element;
	}
	
		
	public SingleLinkElementWrap<E> getNext() {
		return next;
	}
	
	public void setNext(SingleLinkElementWrap<E> next) {
		this.next = next;
	}
	public E getContent() {
		return content;
	}
	public void setContent(E content) {
		this.content = content;
	} 
	
	public void unlink(){
		this.next = null;
	}
	
	public void clear(){
		this.next = null;
		this.content = null;
	}

	
	

	

}
