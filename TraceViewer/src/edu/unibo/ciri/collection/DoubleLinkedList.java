package edu.unibo.ciri.collection;


public class DoubleLinkedList<E extends Comparable<E>>{

	protected int size;
	protected DoubleLinkElementWrap<E> head;
	protected DoubleLinkElementWrap<E> tail;
	
	
	public DoubleLinkedList(){
		initList();
	}
	
	
	
	private void initList(){
		head = new DoubleLinkElementWrap<E>(null);
		tail = new DoubleLinkElementWrap<E>(null);
		head.setNext(tail);
		tail.setPrev(head);
		size = 0;

	}
	

	
		
	public int size(){		
		return size;
	}
	
	private void insertBetween(DoubleLinkElementWrap<E> target, DoubleLinkElementWrap<E> prev, DoubleLinkElementWrap<E> next){
		
		target.setNext(next);
		target.setPrev(prev);
		prev.setNext(target);
		next.setPrev(target);
		size++;
		
	}
	
	public int positionOf(E elem){
		
		if(elem == null || size == 0)
			return -1;
		
		DoubleLinkElementWrap<E> linkedElem = head;
		
		int count = 0;
		while((linkedElem = linkedElem.getNext()) != null 
				&& !linkedElem.getContent().equals(elem) )
			count++;	
		
		return count;
	}
	
	private void removeBetween(DoubleLinkElementWrap<E> target, DoubleLinkElementWrap<E> prev, DoubleLinkElementWrap<E> next){		
		prev.setNext(next);
		next.setPrev(prev);
		target.unlink();
		size--;
	}
	
	public void clear(){
		//TODO really need that?????? You can't simply unlink head and tail
		DoubleLinkElementWrap<E> curr = head.getNext();
		DoubleLinkElementWrap<E> prev;
		while((curr = curr.getNext()) != tail){
			prev = curr.getPrev();
			prev.setPrev(null);
			prev.setNext(null);
		}
		
		initList();
	}
	
	
	
	public boolean removeElementAt(int position){		
		
		if( position >= size || position < 0)
			return false;
		
		int count = 0;
		DoubleLinkElementWrap<E> curr = head.getNext();
		while( count != position ){
			curr = curr.getNext();
			count++;
		}
		removeBetween(curr, curr.getPrev(), curr.getNext());
		return true;
	
	}
	
	
	public boolean removeElement(E elem){
		
		if(elem == null)
			return false;
		
		if(!this.contains(elem))
			return false;
				
		DoubleLinkElementWrap<E> curr = head;
		while( !(curr = curr.getNext()).getContent().equals(elem) ){
		}
		removeBetween(curr, curr.getPrev(), curr.getNext());
		return true;				
	}
	
	public E popHead(){
		
		//or test size == 0
		if(head.getNext() == tail)
			return null;
		
		DoubleLinkElementWrap<E> first = head.getNext();
		E elem = first.getContent();
		assert(elem != null);		 
		
		removeBetween(first, head, first.getNext());
		
		assert(size >= 0);		
		return elem;
	}
	
	public E getHead(){
		
		if(head.getNext() == tail)
			return null;
		
		return head.getNext().getContent();
	}
	
	public E getNext(E elem){
		
		DoubleLinkElementWrap<E> curr = head;
		while( (curr = curr.getNext()) != tail){
			if(elem.equals(curr.getContent()) && curr.getNext() != tail){
				return curr.getNext().getContent();
			}
		}
		return null;
	}
	
	
	
	public void enqueue(E elem){    	
		DoubleLinkElementWrap<E> linkedElem = new DoubleLinkElementWrap<E>(elem);    	    	
    	insertBetween(linkedElem, tail.getPrev(), tail);		
	}
    
    
	public boolean contains(E elem){
		
		DoubleLinkElementWrap<E> current = head;
		while((current = current.getNext()) != tail){
			if(current.getContent().equals(elem))
				return true;
		}
		return false;		
	}
	
	public boolean hasMoreElements(){
		
		if(head.getNext() == tail)
			return false;
		return true;
		
	}	
	
	public String toString(){
		
		if(head.getNext() == tail)
			return "empty";
		
		StringBuffer sb = new StringBuffer();
		DoubleLinkElementWrap<E> current = head;
		sb.append("HEAD");
		
		while((current = current.getNext())!= tail){
			sb.append( " -> " + current.getContent().toString() );			
		}
		if(current == tail)
			sb.append(" -> TAIL");
		
		return sb.toString();
	}
	
}
