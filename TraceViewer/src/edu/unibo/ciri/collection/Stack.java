package edu.unibo.ciri.collection;

import java.util.Vector;

public class Stack<E> {

	private int size = 0;
	
	private Vector<E> elements = new Vector<E>();
			
	public int size(){		
		return size;
	}	
		
	public E pop(){				
		return elements.remove(--size);
	}
		
	public void push(E elem){		
		elements.add(elem);		
    	size++;    	
	}	
	
	public E readTop(){
		return elements.get(size - 1);
	}
	
	public String toString(){
		return elements.toString();
	}

	public void clear(){
		elements.clear();		
	}
			
}
