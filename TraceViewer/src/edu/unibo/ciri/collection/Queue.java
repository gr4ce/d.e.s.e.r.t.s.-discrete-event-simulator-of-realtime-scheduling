package edu.unibo.ciri.collection;

import java.util.Iterator;
import java.util.Vector;

import util.QueueIterator;


public class Queue<E> implements Iterable<E>{

	private int size = 0;
	private SingleLinkElementWrap<E> head = null;
	private SingleLinkElementWrap<E> tail = null;
	
	private Vector<SingleLinkElementWrap<E>> dynFreeWrappers;
	private SingleLinkElementWrap<E>[]       statfreeWrappers;
	private int freeWrapperStartCapacity = 0;
	private static final boolean DYNAMIC_QUEUE = true;
	
	public Queue(){			
		
		if(DYNAMIC_QUEUE){
			
			dynFreeWrappers  = new Vector<SingleLinkElementWrap<E>>(freeWrapperStartCapacity);
			for(int i = 0; i < freeWrapperStartCapacity; i++)
				dynFreeWrappers.add(new SingleLinkElementWrap<E>());
		}
		else{
		
			statfreeWrappers = new SingleLinkElementWrap[freeWrapperStartCapacity];
			for(int i = 0; i < freeWrapperStartCapacity; i++)
				statfreeWrappers[i] = new SingleLinkElementWrap<E>();
		}
		
		
		
	}
	
			
	public int size(){		
		return size;
	}
	
		
	public E poll(){
				
		if(head == null)
			return null;
		
		SingleLinkElementWrap<E> second = head.getNext();
		E elem = head.getContent();
		assert(elem != null);		 
		
		
		
		if(DYNAMIC_QUEUE){
			
			dynFreeWrappers.add(head);
		}
		else{
			
			store(head);
		}
		
		head.clear();
		head = second;
		size--;		
		assert(size >= 0);
		if(size == 0)
			tail = null;		
		
		return elem;
	}
	
	public E getFirst(){
		
		if(head == null)
			return null;
		
		return head.getContent();
	}
	
	public E getNext(E elem){
		
		if(head == null)
			return null;
		
		SingleLinkElementWrap<E> curr = head;
		
		do{
			if(elem.equals(curr.getContent())){
				if(curr.getNext() != null)
					return curr.getNext().getContent();			
				return null;
			}				
		}while( (curr = curr.getNext()) != null);
		
		return null;
	}
		
	
	public void offer(E elem){    	
		
		SingleLinkElementWrap<E> linkedElem;
		
		if(DYNAMIC_QUEUE){
			int currentFreeWrappersSize = dynFreeWrappers.size(); 
			
			if(currentFreeWrappersSize > 0){

				linkedElem = dynFreeWrappers.remove(currentFreeWrappersSize - 1);
				linkedElem.setContent(elem);
			}
			else{
				linkedElem = new SingleLinkElementWrap<E>(elem);
	
			}    	    	
		}else{
			linkedElem = pollFirstFree();
			linkedElem.setContent(elem);
		}
		
		assert(linkedElem != null);
    	
    	if(size == 0){    		
    		assert(tail == null);
    		assert(head == null);
    		
    		head = tail = linkedElem;    		
    	}	
    	else{
    		assert(tail != null);
    		assert(head != null);    		
    		
    		tail.setNext(linkedElem);
    		tail = linkedElem;
    	}
    	size++;
    	
	}
    
    
	public boolean contains(E elem){
		
		if(head == null)
			return false;
		
		SingleLinkElementWrap<E> current = head;
		do{
			if(current.getContent().equals(elem))
				return true;
		}while((current = current.getNext()) != null);
		
		return false;		
	}
	
	public boolean remove(E elem){
		
		if(elem == null || head == null)
			return false;
		
		SingleLinkElementWrap<E> curr = head.getNext();
		SingleLinkElementWrap<E> prev = head;
		
		if(prev.getContent().equals(elem)){
			poll();
			return true;
		}
		
		do{
			if(curr.getContent().equals(elem)){
				prev.setNext(curr.getNext());
				curr.clear();
				
				if(DYNAMIC_QUEUE)					
					dynFreeWrappers.add(curr);				
				else					
					store(curr);
				
				
				size--;
				assert(size >= 0);
				return true;
			}
			prev = curr;
				
		}while((curr = curr.getNext()) != null);
		
		assert(size >= 0);
		
		return false;
		
	}
	
	
	
	public String toString(){
		
		if(head == null)
			return "empty";
		
		StringBuffer sb = new StringBuffer();
		SingleLinkElementWrap<E> current = head;
		sb.append("HEAD");
		
		do{
			sb.append( " -> " + current.getContent().toString() );			
		}while((current = current.getNext())!= null);
		if(current == tail)
			sb.append(" -> TAIL");
		
		return sb.toString();
	}


	@Override
	public Iterator<E> iterator() {
		return  new QueueIterator<E>(this);
	}
	
	public void clear(){
		
		SingleLinkElementWrap<E> elem = head;
		SingleLinkElementWrap<E> curr;
		
		while(size > 0){
			curr = elem;
			elem = elem.getNext();			
			curr.setContent(null);			
			if(DYNAMIC_QUEUE)					
				dynFreeWrappers.add(head);				
			else					
				store(head);
			size--;
		}
		
		head = null;
		tail = null;
		assert(size == 0);
	}
	
	
	private SingleLinkElementWrap<E> pollFirstFree(){
		
		SingleLinkElementWrap<E> e;
		
		for(int i = 0; i < statfreeWrappers.length; i++){
			if(statfreeWrappers[i] != null){
				e = statfreeWrappers[i];
				statfreeWrappers[i] = null;
				return e;
			}
		}
		
		assert(false);
		return null;
		
	}
	
	private void store(SingleLinkElementWrap<E> e){
		
		for(int i = 0; i < statfreeWrappers.length; i++){
			if(statfreeWrappers[i] == null){
				statfreeWrappers[i] = e;
				e.clear();
				return;
			}
		}
		
		assert(false);
	}
	
}
