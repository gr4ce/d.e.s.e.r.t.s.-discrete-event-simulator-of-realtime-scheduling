package overhead;

import edu.unibo.ciri.realtime.model.time.Time;

public class Overhead {	
		
	private OverheadType type = null;
	private String name 	  = null;
	private Time start 		  = null;
	private Time end   		  = null;	
	
	public Overhead(){};
	
	public Overhead(String name, OverheadType type, Time start, Time end){
		this.name  = name;
		this.type  = type;
		this.start = start;
		this.end   = end;
	}

	public OverheadType getType() {
		return type;
	}

	public void setType(OverheadType type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Time getStartTime() {
		return start;
	}

	public void setStartTime(Time start) {
		this.start = start;
	}

	public Time getEndTime() {
		return end;
	}

	public void setEndTime(Time end) {
		this.end = end;
	}
	
	public long getDelay(){
		return end.getTimeValue() - start.getTimeValue();
	}
	
	
	
	
}


