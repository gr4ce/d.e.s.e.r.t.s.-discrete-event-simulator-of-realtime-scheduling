package overhead;

public enum OverheadType {

	CONTEXT_SWITCH,
	INTERRUPT_HANDLING,
	MIGRATION
	
}
