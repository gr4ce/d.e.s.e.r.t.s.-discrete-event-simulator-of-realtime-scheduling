package overhead;

import java.awt.Color;

import view.EventGraphicsDisplay;
import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.event.EventType;
import edu.unibo.ciri.desert.event.management.EventListener;
import edu.unibo.ciri.desert.event.management.EventManager;
import edu.unibo.ciri.desert.graphics.diagram.schedule.elements.DrawableRange;
import edu.unibo.ciri.realtime.model.Job;
import edu.unibo.ciri.realtime.model.Job.JobContext;

public class OverHeadHandler implements EventListener {
	
	protected 	EventGraphicsDisplay 	graphicDisplay;
	private 	OverheadCalculator 		calc;
	private 	EventManager 			eventManager;
	
	public void handle(Event event) {
		
		switch (event.getEventType()) {
		case OVERHEAD_CONTEXT_SWITCH_START:
			handleOverheadContextSwitchStart(event);
			break;
		case OVERHEAD_CONTEXT_SWITCH_END:
			handleOverheadContextSwitchEnd(event);
			break;
		case OVERHEAD_MIGRATION_START:
			handleOverheadMigrationStart(event);
			break;
		case OVERHEAD_MIGRATION_END:
			handleOverheadMigrationEnd(event);
			break;
		default:
			
		}
	}
	
	public  OverHeadHandler(OverheadCalculator calc,EventManager eventManager){
		this.calc=calc;
		this.eventManager=eventManager;
	}
	
	public void setGraphicDisplay(EventGraphicsDisplay el){
		this.graphicDisplay = el;
	}
	
	private void handleOverheadMigrationStart(Event event) {		
		
	}
	
	private void handleOverheadMigrationEnd(Event event) {		
		
	}
	
	private void handleOverheadContextSwitchStart(Event event) {
		//System.out.println("handleOverheadContextSwitchStart"+event);
		//System.out.println("OVERHEAD CONTEXT SWITCH "+ event.getTask().getName()+" t: "+event.getEventTimeValue());
		
		Job j=event.getJob();
		//Cambio il contesto del job in kernel_np
		j.setContext(JobContext.KERNEL_NP);
		
		
		
		long overheadLenght			= calc.getContextSwitchOverhead(event);
		long remainingExecution 	= j.getInfo().getRemainingExecution();
		long newRemainingExecution	= remainingExecution+overheadLenght;
		j.getInfo().setRemainingExecution(newRemainingExecution);
		
		//Inserisco un evento di cambio prioritˆ visto che ho cambiato la prioritˆ di un job
		Event priorityChange = eventManager.getEventFactory().getEventInstance(event.getEventTimeValue(), EventType.JOB_PRIORITY_CHANGE, j.getTask());
		priorityChange.setCpu(j.getCpu());
		priorityChange.setJob(j);
		eventManager.trigger(priorityChange);
		
		//Inserisco l'evento di fine overhead nella coda degli eventi
		Event overheadEnd = eventManager.getEventFactory().getEventInstance(event.getEventTimeValue()+overheadLenght, EventType.OVERHEAD_CONTEXT_SWITCH_END, j.getTask());
		overheadEnd.setCpu(j.getCpu());
		overheadEnd.setJob(j);
		eventManager.trigger(overheadEnd);
		
		//Inserisco un evento di cambio prioritˆ visto che ho cambiato la prioritˆ di un job
		Event priorityChangeEnd = eventManager.getEventFactory().getEventInstance(event.getEventTimeValue()+overheadLenght, EventType.JOB_PRIORITY_CHANGE, j.getTask());
		priorityChangeEnd.setCpu(j.getCpu());
		priorityChangeEnd.setJob(j);
		eventManager.trigger(priorityChangeEnd);
		
		
		if(!(graphicDisplay == null)){
			//graphicDisplay.getTimeDiagram().addDrawableOverhead(new DrawableOverhead(event.getEventTimeValue(),500,event.getJob()));
			graphicDisplay.getScheduleDiagramContainer().addDrawableRange(new DrawableRange(event.getEventTimeValue(),overheadLenght,event.getJob(),Color.yellow));
			//System.out.println("Time Diagram"+graphicDisplay.getTimeDiagram());
		}
		
		//Event jobCompleteEvent=event.getJob().getEvent(EventType.JOB_COMPLETION);
		//System.out.println("TEST: "+jobCompleteEvent);
		//eventManager.retrigger(jobCompleteEvent, jobCompleteEvent.getEventTime().getTimeValue()+1);//1 � il ritardo, da cambiare...
		//System.out.println("OVERHEAD: "+jobCompleteEvent.getEventTime().getTimeValue()+jobCompleteEvent.getTask().getName());
		
	}
	
	private void handleOverheadContextSwitchEnd(Event event) {
		//System.out.println("handleOverheadContextSwitchEnd");
		Job j = event.getJob();
		//Cambio il contesto del job in kernel_np
		
		if(!event.getTask().getName().equals("SCHEDULER")){
			j.setContext(JobContext.LOCAL);
		}
		
				
	}

	

}
