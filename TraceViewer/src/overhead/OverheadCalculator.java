package overhead;

import edu.unibo.ciri.desert.event.Event;
import edu.unibo.ciri.desert.resource.Processor;
import edu.unibo.ciri.desert.resource.VirtualPlatform;
import edu.unibo.ciri.realtime.model.Task;

public class OverheadCalculator {
	private VirtualPlatform plat;
	private long contextSwitchConstant=200;
	private long migrationConstant=0;
	
	public OverheadCalculator(VirtualPlatform plat){
		this.plat=plat;		
	}
	
	public long getContextSwitchOverhead(Event event){
		long overhead=contextSwitchConstant;
		/*
		int cpuNumber=event.getCpu();
		Processor proc=plat.getProcessor(cpuNumber);
		if(!(proc.lastRunning()==null)){
				Task lastRunningTask=plat.getProcessor(event.getCpu()).lastRunning().getTask();
				if(!(lastRunningTask.equals(event.getTask()))){//controllo se l'ultimo task ad aver eseguito sul processore � diverso da quello che deve entrare in esecuzione
					System.out.println(event.getEventTimeValue()+" Migration of "+event.getTask().getName());
					
				}
		}*/

		int cpuNumber=event.getCpu();
		//int oldCpuNumber=event.getTask().getExecutedJobs().getLast().getCpu();
		int oldCpuNumber=event.getTask().getLastCpu();
		//System.out.println(oldCpuNumber);
		if(cpuNumber!=oldCpuNumber){
			//System.out.println(event.getEventTimeValue()+" Migration of "+event.getTask().getName()+" from "+oldCpuNumber+" to "+cpuNumber+" Job Number+ "+event.getJob().getId());
			int workingSet=event.getTask().getTaskParameters().getWorkingSetSize();
			overhead=overhead+(migrationConstant*workingSet);
		}

		return overhead;
	}

}
