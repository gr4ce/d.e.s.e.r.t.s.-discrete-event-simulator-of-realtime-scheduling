package overhead;

import edu.unibo.ciri.collection.Queue;
import edu.unibo.ciri.desert.config.Configuration;
import edu.unibo.ciri.realtime.model.time.Time;

public class OverheadFactory {
	
	private Queue<Overhead> freeOverhead = new Queue<Overhead>();
	private enum  OverheadCreationPolicy { BY_NEED, IN_BLOCK };
	private final OverheadCreationPolicy defaultPolicy = OverheadCreationPolicy.IN_BLOCK;
	private       OverheadCreationPolicy policy = null;
	private final int BLOCK_SIZE = 100;
	
	
	public OverheadFactory(){
		String policyName = Configuration.getOverheadCreationPolicy();
		if(policyName.equals("BY NEED")){
			policy = OverheadCreationPolicy.BY_NEED;
		}
		else if(policyName.equals("IN BLOCK")){
			policy = OverheadCreationPolicy.IN_BLOCK;
		}
		else
			policy = defaultPolicy;
	}
	
	public Overhead getOverheadInstance(String name, OverheadType type, Time start, Time end){
		
		Overhead ovh;
		
		if(freeOverhead.size() == 0){
			createInstance();
		}
			
		ovh = freeOverhead.poll();
		ovh.setName(name);
		ovh.setType(type);
		ovh.setStartTime(start);
		ovh.setEndTime(end);
		
		return ovh;
	}
	
	public void freeOverheadInstance(Overhead ovh){
		freeOverhead.offer(ovh);
	}
	
	public void setOverheadCreationPolicy(){
		
	}
	
	private void createInstance(){
		
		switch(policy){
			case BY_NEED:
				Overhead ovh = new Overhead();
				freeOverhead.offer(ovh);
				break;
			case IN_BLOCK:				
				for(int i = 0; i < BLOCK_SIZE; i++){
					ovh = new Overhead();
					freeOverhead.offer(ovh);
				}					
				break;
			default:
				System.out.println("OverheadCreationPolicy not present!");
				assert(false);
				
		}
	}

}
