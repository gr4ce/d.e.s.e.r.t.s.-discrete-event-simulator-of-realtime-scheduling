/**
 *     Project: SharedResources
 *
 *        File: resources.c
 *
 *  Created on: 10/apr/2013
 *
 *      Author: Giuseppe Salvatore
 *
 * Description: Shared resources handling
 *
 */

/*
 * Standard module inclusions
 */
#include <linux/module.h>
#include <linux/version.h>
#include <linux/param.h>
#include <linux/sched.h>
#include <linux/errno.h>

/*
 * RTAI inclusions
 */
#include <rtai_sem.h>

/*
 * Machine dependant inclusions
 */
#include <asm/io.h>
#include <asm/rtai.h>


/*
 * Project inclusions
 */
#include "resources.h"
#include "realtime_task.h"
#include "unibo_rtai_tracer.h"



static shared_resource resources[NUMBER_OF_RESOURCES];


void initialize_resources(void){

	int i,j;
	realtime_task *task;

	for(i = 0; i < NUMBER_OF_RESOURCES; i++){
		rt_typed_sem_init(&resources[i],1, RES_TYPE);
	}


//SECTION: Register resources


	for(j = 0; j < NUMBER_OF_TASKS; j++){
		task = get_task_by_index(j);
		for(i = 0; i < NUMBER_OF_RESOURCES; i++){
			task->resource[i] = &resources[i];
		}
	}
}

void destroy_resources (void){

	int i = 0;

	for(; i < NUMBER_OF_RESOURCES; i++)
		rt_sem_delete(&resources[i]);
}
