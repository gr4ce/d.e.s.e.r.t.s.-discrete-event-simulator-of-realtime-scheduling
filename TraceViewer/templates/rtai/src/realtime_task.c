/**
 *     Project: SharedResources
 *
 *        File: periodic_task.c
 *
 *  Created on: 10/apr/2013
 *
 *      Author: Giuseppe Salvatore
 *
 * Description: Periodic task execution
 *
 */



/**
 * Standard module inclusions
 */
#include <linux/module.h>
#include <linux/version.h>
#include <linux/types.h>
#include <linux/param.h>
#include <linux/types.h>
#include <linux/sched.h>
#include <linux/errno.h>


/**
 * Machine dependant inclusions
 */
#include <asm/io.h>
#include <asm/rtai.h>

//#include <rtai.h>
//#include <rtai_hal.h>
#include <rtai_sched.h>
#include <rtai_nam2num.h>
#include <rtai_registry.h>
#include <rtai_types.h>
#include <unibo_rtai_tracer.h>

#include "resources.h"
#include "global_defs.h"
#include "calibration.h"
#include "realtime_task.h"
#include "aperiodic_model.h"
#include "scheduling_policies.h"



#define NOT_DEFINED -1

static realtime_task tasks[NUMBER_OF_TASKS];

RTIME 		start_time_ticks   = 0;

static char *model_name[] = {
	"PERIODIC",
	"SPORADIC",
	"APERIODIC"
};


char* get_task_model_name(realtime_task_model model){
	return model_name[model];
}


void initialize_tasks(void){

	realtime_task *task;
	RTIME now_ticks;

	now_ticks = rt_get_time();
	start_time_ticks = now_ticks + nano2count( ms_to_ns( 10 ));
	printk("[%s] - Start application time = %llu\n",MODULE_NAME, count2nano(start_time_ticks));

	printk("UNIBO_TRACER(traced_event)@SECTION TASK_INFO\n");
	
//SECTION: Periodic task initalization

	//create_periodic_task(0,"TASK_1", 1, ms_to_ns(0), ms_to_ns(12), ms_to_ns(12), TASK_1_execution_pattern);
	//create_periodic_task(1,"TASK_2", 2, ms_to_ns(0), ms_to_ns(14), ms_to_ns(14), TASK_2_execution_pattern);
	//create_periodic_task(2,"TASK_3", 4, ms_to_ns(0), ms_to_ns(16), ms_to_ns(16), TASK_3_execution_pattern);
	//create_periodic_task(3,"TASK_4", 5, ms_to_ns(0), ms_to_ns(20), ms_to_ns(20), TASK_4_execution_pattern);

//SECTION: Aperiodic task initalization

	//task = create_aperiodic_task(4,"APER", 3);
	//bind_aperiodic_request_to_realtime_task(task);


	printk("UNIBO_TRACER(traced_event)@END_SECTION\n");
	printk("UNIBO_TRACER(traced_event)@SECTION TRACED_EVENTS\n");
}


void destroy_tasks(void){

	int i = 0;

	for(; i < NUMBER_OF_TASKS; i++){
		rt_drg_on_name(i);
		rt_task_delete(&tasks[i].rtai_task);
	}

	rt_drg_on_name(MAIN_TASK);
}

realtime_task* get_task_by_index(int index){
	return &tasks[index];
}

RTIME get_start_time_ns(){
	return count2nano(start_time_ticks);
}


void rt_task_shell_edf(long int task_index){

	realtime_task *task = &tasks[task_index];


	RTIME deadline_ticks = nano2count(task->deadline_ns);
	RTIME period_ticks   = nano2count(task->param.period_ns);


	int instances_count = 0;


	while(task->is_active){

		rt_task_set_resume_end_times(-period_ticks, -deadline_ticks);

		unibo_trace_event(JOB_STARTED, &task->rtai_task, rt_get_time_ns(), NULL);

		/*
		 * Real-Time Task code
		 */
		task->code((void*) task);


		/*
		 * End execution checkings
		 */
		instances_count++;
		if(instances_count == task->number_of_instances)
			task->is_active = false;

		unibo_trace_event(JOB_COMPLETED, &task->rtai_task, rt_get_time_ns(), NULL);

	}
}


void rt_task_shell_fixed_prio(long int task_index){

	realtime_task *task 	  = &tasks[task_index];

	int instances_count = 0;
	int result = 0;



	while(task->is_active){

		result = rt_task_wait_period();

		printk("UNIBO_TRACER(traced_event)@JOB_STARTED %s %lld\n", (char*)&task->name, CURRENT_TIME_NS);
		if(result == RTE_TMROVRN)
			printk("JOB_OVERRUN_DETECTED for %s\n",(char*)&task->name);
		/*
		 * Realtime Task code
		 */
		task->code((void*)task);

		/*
		 * End execution checkings
		 */
		instances_count++;
		if(instances_count == task->number_of_instances)
			task->is_active = false;

		printk("UNIBO_TRACER(traced_event)@JOB_COMPLETED %s %lld\n", (char*)&task->name, CURRENT_TIME_NS);

	}

}



realtime_task* create_periodic_task
(
		uint16_t index,
		const char name[],
		int prio,
		RTIME offset_ns,
		RTIME deadline_ns,
		RTIME period_ns,
		realtime_task_code task_code
)
{

	RTIME period_ticks, now_ticks, offset_ticks;
	realtime_task *task = &tasks[index];



	strcpy(task->name, name);

	task->task_index   = index;
	task->is_active	   = true;
	task->code		   = task_code;

	task->offset_ns    = offset_ns;
	task->deadline_ns  = deadline_ns;

	if(prio != NOT_DEFINED)
		task->priority	= prio;

	/*
	 * Periodic task specific parameter
	 */
	task->param.period_ns = period_ns;
	task->number_of_instances = 2;

	period_ticks = nano2count(task->param.period_ns);
	offset_ticks = nano2count(task->offset_ns);

	printk("UNIBO_TRACER(task_info)@%s %llu %llu %llu\n"
				,task->name
				,task->param.period_ns
				,task->deadline_ns
				,task->offset_ns);

	switch(get_scheduling_policy()){

		case RATE_MONOTONIC:
			rt_task_init_cpuid(&task->rtai_task,rt_task_shell_fixed_prio,(long int)index,DEFAULT_STACK_SIZE,task->priority,0,0,CPU_0);
			break;

		case EARLIEST_DEADLINE_FIRST:
			rt_task_init_cpuid(&task->rtai_task,rt_task_shell_edf,(long int)index,DEFAULT_STACK_SIZE,task->priority,0,0,CPU_0);
			break;

		case PREEMPTION_THRESHOLD:
			//TODO write the handler for this case
			break;


	}

#ifdef  REGISTER_TASK_IN_CREATE_TASK
	unibo_tracer_register_task(&task->rtai_task, task->name);
#endif

	rt_register(index,(void*) &task->rtai_task,0,NULL);
	rt_task_make_periodic(&task->rtai_task, start_time_ticks - period_ticks + offset_ticks, period_ticks);

	return task;
}

realtime_task* create_sporadic_task
(
		uint16_t index,
		const char name[],
		int prio,
		RTIME offset_ns,
		RTIME deadline_ns,
		RTIME minimum_interarrival_time_ns,
		RTIME maximum_interarrival_time_ns,
		realtime_task_code task_code
)
{
	RTIME now_ticks;
	realtime_task *task = &tasks[index];


	strcpy(task->name, name);

	task->task_index   = index;
	task->is_active	   = true;
	task->code		   = task_code;

	task->offset_ns    = offset_ns;
	task->deadline_ns  = deadline_ns;

	if(prio != NOT_DEFINED)
		task->priority	= index;

	/*
	 * Sporadic task specific parameter
	 */
	task->param.maximum_interarrival_time_ns = maximum_interarrival_time_ns;
	task->param.minimum_interarrival_time_ns = minimum_interarrival_time_ns;

	return task;
}


//SECTION: Task definition



/*
void TASK_1_execution_pattern(void *param){

	realtime_task *task = (realtime_task*) param;

	busy_wait_ms(1);

}

void TASK_2_execution_pattern(void *param){

	realtime_task *task = (realtime_task*) param;

	busy_wait_ms(1);
	rt_sem_wait(task->resource[RESOURCE_1]);
	busy_wait_ms(1);
	rt_sem_signal(task->resource[RESOURCE_1]);
	busy_wait_ms(1);
}

void TASK_3_execution_pattern(void *param){

	realtime_task *task = (realtime_task*) param;

	busy_wait_ms(1);
	rt_sem_wait(task->resource[RESOURCE_1]);
	busy_wait_ms(2);
	rt_sem_signal(task->resource[RESOURCE_1]);
	busy_wait_ms(1);
}

void TASK_4_execution_pattern(void *param){

	realtime_task *task = (realtime_task*) param;

	busy_wait_ms(1);
	rt_sem_wait(task->resource[RESOURCE_1]);
	busy_wait_ms(2);
	rt_sem_signal(task->resource[RESOURCE_1]);
	busy_wait_ms(1);
}
*/

