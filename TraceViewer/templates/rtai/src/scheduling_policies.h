/**
 *     Project: SharedResources
 *
 *        File: scheduling_policies.h
 *
 *  Created on: 28/may/2013
 *
 *      Author: Giuseppe Salvatore
 *
 * Description: Scheduling policies management
 *
 */

#ifndef SCHEDULING_POLICIES_H_
#define SCHEDULING_POLICIES_H_

#include "realtime_task.h"

typedef enum{
	RATE_MONOTONIC,
	DEADLINE_MONOTONIC,
	EARLIEST_DEADLINE_FIRST,
	PREEMPTION_THRESHOLD
} scheduling_policy;


void 				set_scheduling_policy	(scheduling_policy policy);
scheduling_policy 	get_scheduling_policy	(void);

void assign_rate_monotonic_priorities(realtime_task* t);

void assign_deadline_monotonic_priorities(realtime_task* t);

int  get_task_priority(realtime_task* t);


#endif /* SCHEDULING_POLICIES_H_ */
