/**
 *     Project: SharedResources
 *
 *        File: resources.h
 *
 *  Created on: 10/apr/2013
 *
 *      Author: Giuseppe Salvatore
 *
 * Description: Shared resources handling
 *
 */

#ifndef RESOURCES_H_
#define RESOURCES_H_

#include <rtai.h>
#include <rtai_sem.h>

#define shared_resource SEM



//SECTION: Resources declaration

//The previous section must looks like this
//#define RESOURCE_1 0
//#define RESOURCE_2 1
//#define NUMBER_OF_RESOURCES 5
//#define RES_TYPE RES_SEM|PRIO_Q




void    initialize_resources	(void);
void    destroy_resources	(void);


#endif /* RESOURCES_H_ */
