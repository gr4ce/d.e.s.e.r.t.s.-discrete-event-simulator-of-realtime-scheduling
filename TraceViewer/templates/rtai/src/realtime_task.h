/**
 *     Project: SharedResources
 *
 *        File: periodic_task.h
 *
 *  Created on: 10/apr/2013
 *
 *      Author: Giuseppe Salvatore
 *
 * Description: Realtime tasks
 *
 */

#ifndef REALTIME_TASK_H_
#define REALTIME_TASK_H_


/**
 * Standard module inclusions
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/param.h>
#include <linux/types.h>


/**
 * Machine dependant inclusions
 */
#include <asm/io.h>
#include <asm/types.h>


/**
 * RTAI inclusions
 */
#include <rtai.h>
#include <rtai_sem.h>
#include <rtai_sched.h>
#include <rtai_types.h>


#include "global_defs.h"
#include "resources.h"


//SECTION: Number of tasks
//#define NUMBER_OF_TASKS 5


#define MAIN_TASK NUMBER_OF_TASKS


typedef enum{
	PERIODIC,
	SPORADIC,
	APERIODIC,
} realtime_task_model;

char* get_task_model_name(realtime_task_model model);


typedef struct _realtime_task realtime_task;
typedef struct _aperiodic_request rt_aperiodic_request;



typedef struct{

	/*
	 * The three known realtime task models
	 */
	realtime_task_model model;

	/*
	 * Periodic Task Model
	 */
	RTIME period_ns;

	/*
	 * Sporadic Task Model
	 */
	RTIME minimum_interarrival_time_ns;
	RTIME maximum_interarrival_time_ns;

	/*
	 * Aperiodic Task Model
	 */
	rt_aperiodic_request *first_request;
	int total_requests;

} realtime_task_param;


#define REALTIME_TASK_PARAM_CTOR {-1, -1, -1, -1, NULL, -1}


#define PRIORITY_NOT_DEFINED -1

#define START_TIME      get_start_time_ns()//(count2nano(start_time_ticks))
#define CURRENT_TIME_NS (rt_get_time_ns() - START_TIME)


typedef void(*realtime_task_code)(void *param);

#ifndef RT_TASK
#define RT_TASK struct rt_task_struct
#endif

struct _realtime_task{

	unsigned short		task_index;
	RT_TASK				rtai_task;

	bool				is_active;
	unsigned int 		number_of_instances;

	char  				name[20];

	RTIME 				offset_ns;
	RTIME 				deadline_ns;

	unsigned short 		priority;

	realtime_task_code  code;
	realtime_task_param	param;

	shared_resource		*resource[NUMBER_OF_RESOURCES];

};

struct _aperiodic_request {

	RTIME 				request_arrival_time;
	realtime_task_code 	request_code;
	realtime_task 		*task;

};

#undef RT_TASK
void initialize_tasks(void);
void destroy_tasks(void);


RTIME get_start_time_ns(void);


void  periodic_task_shell(long int task_parameters);

realtime_task* create_periodic_task(uint16_t, const char name[], int prio, RTIME, RTIME, RTIME, realtime_task_code);


void  sporadic_task_shell(long int task_parameters);

realtime_task* create_sporadic_task(uint16_t, const char name[], int prio, RTIME, RTIME, RTIME ,RTIME, realtime_task_code);




realtime_task* get_task_by_index(int index);



//SECTION: Task code declaration
//void TASK_1_execution_pattern(void *param);
//void TASK_2_execution_pattern(void *param);
//void TASK_3_execution_pattern(void *param);
//void TASK_4_execution_pattern(void *param);


#endif /* REALTIME_TASK_H_ */
