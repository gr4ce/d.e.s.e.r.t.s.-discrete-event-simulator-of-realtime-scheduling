/**
 *     Project: KernelSchedulingModule
 *
 *        File: base_module.c
 *
 *  Created on: 5/dec/2012
 *
 *      Author: Giuseppe Salvatore
 *
 * Description: A kernel module to view scheduling policies
 *              provided by RTAI
 *
 */

#define LICENSE_STRING		"GPL"
#define AUTHOR_STRING		"Giuseppe Salvatore"
#define DESCRIPTION_STRING  "Kernel Scheduling Module"



/**
 * Standard module inclusions
 */
#include <linux/param.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/version.h>



/**
 * Machine dependant inclusions
 */
#include <asm/io.h>
#include <rtai.h>
#include <rtai_sched.h>
#include <rtai_registry.h>
#include <rtai_tasklets.h>
#include <unibo_rtai_tracer.h>

#if defined(__i386__) && defined(__x86_64__)


#endif

#if defined(__i386__) && !defined(__x86_64__)

#include <asm-i386/rtai_hal.h>

#ifndef __DIVDI3__
#define __DIVDI3__
extern long long __divdi3(long long a, long long b){
        return rtai_ulldiv(a,b,NULL);
}
#endif

#endif



/**
 * All the local path inclusions
 */
#include "resources.h"
#include "global_defs.h"
#include "calibration.h"
#include "realtime_task.h"
#include "scheduling_policies.h"






//****************************************************************************************************/
// Begin Kernel Module code
//****************************************************************************************************/
int init_module(void) {

	printk("[%s] - Starting to load module\n",MODULE_NAME);
	printk("[%s] - Default scheduler decision taken in periodic mode\n",MODULE_NAME);



//SECTION: Timer initialization
	
//	rt_set_oneshot_mode();
//	start_rt_timer(us_to_ns(500));

//SECTION: Calibration

//	calibrate_loop();

//SECTION: Scheduling policy
	
//	set_scheduling_policy(RATE_MONOTONIC);

	initialize_tasks();

//SECTION: Register main task
	
	initialize_resources();

	return 0;
}


void cleanup_module(void) {

	stop_rt_timer();

	destroy_resources();
	
	destroy_tasks();

	rt_drg_on_name(MAIN_TASK);

	printk("[%s] - Module successfully removed\n",MODULE_NAME);


}
//****************************************************************************************************
// End Kernel Module code
//****************************************************************************************************

MODULE_LICENSE     (LICENSE_STRING);
MODULE_AUTHOR      (AUTHOR_STRING);
MODULE_DESCRIPTION (DESCRIPTION_STRING);





