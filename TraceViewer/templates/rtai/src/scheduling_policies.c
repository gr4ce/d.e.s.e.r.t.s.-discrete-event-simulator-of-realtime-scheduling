/**
 *     Project: SharedResources
 *
 *        File: scheduling_policies.c
 *
 *  Created on: 28/may/2013
 *
 *      Author: Giuseppe Salvatore
 *
 * Description: Scheduling policies management
 *
 */

#include "global_defs.h"
#include "realtime_task.h"
#include "scheduling_policies.h"


static scheduling_policy sched_policy;

static int task_priority_map[NUMBER_OF_TASKS];


void set_scheduling_policy(scheduling_policy policy){
	sched_policy = policy;
}


scheduling_policy 	get_scheduling_policy(void){
	return sched_policy;
}


void assign_rate_monotonic_priorities(realtime_task* first_task){
	return;
}


void assign_deadline_monotonic_priorities(realtime_task* first_task){
	return;
}


int  get_task_priority(realtime_task* t){
	return task_priority_map[t->task_index];
}
