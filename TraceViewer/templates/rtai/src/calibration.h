/**
 *     Project: SharedResources
 *
 *        File: calibration.h
 *
 *  Created on: 10/apr/2013
 *
 *      Author: Giuseppe Salvatore
 *
 * Description: Calibration capabilities
 *
 */


#ifndef  TIME_UTILS_H_
#define  TIME_UTILS_H_

#include <rtai.h>

#define EXECUTION_LOOP_SIZE_MS  6695
#define EXECUTION_LOOP_SIZE_US  6695
#define EXECUTION_LOOP_SIZE_NS  6695

#define SAMPLE_TIME  rt_get_time_ns
//#define SAMPLE_TIME  get_time_tsc
//#define SAMPLE_TIME  rt_get_cpu_time_ns

void    calibrate_loop	(void);

void 	busy_wait_ms	(RTIME ms);
void 	busy_wait_us	(RTIME us);
void 	busy_wait_ns	(RTIME ns);


#endif   /* TIME_UTILS_H_ */
