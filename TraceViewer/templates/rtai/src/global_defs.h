/**
 *     Project: SharedResources
 *
 *        File: global_defs.c
 *
 *  Created on: 10/apr/2013
 *
 *      Author: Giuseppe Salvatore
 *
 * Description: All the public definitions
 *
 */

#ifndef  GLOBAL_DEFS_H_
#define  GLOBAL_DEFS_H_

#if defined(__i386__) && defined(__x86_32__)
#include <asm-i386/rtai.h>
#endif

#if defined(__i386__) && defined(__x86_64__)
#include <asm/rtai.h>
#endif



//SECTION: Application name


#define FIXED_NUMBER_OF_INSTANCES 0
#define FIXED_USER_DEFINED_TIME   1
#define HYPERPERIOD		  3

//SECTION: Stop mode


//SECTION: Stop time



#define CPU_0 0
#define CPU_1 1


#define ms_to_ns(x) (x * 1000000)
#define us_to_ns(x) (x * 1000)


#define DEFAULT_STACK_SIZE 50000


#define PRINT  rt_printk


#define CPU_FREQ_HZ   1733106000
#define CPU_FREQ_KHZ  1733106
#define CPU_FREQ_MHZ  1733.106
#define CPU_FREQ_GHZ  1.733106


#define MICROSEC * 1000
#define MILLISEC * 1000000
#define SEC      * 1000000000

#define REGISTER_TASK_IN_CREATE_TASK



#endif   /* GLOBAL_DEFS_H_ */

