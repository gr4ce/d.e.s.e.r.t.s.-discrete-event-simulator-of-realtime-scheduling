/*
 * aperiodic_model.c
 *
 *  Created on: May 31, 2013
 *      Author: gr4ce
 */

#include <rtai_registry.h>
#include <rtai_tasklets.h>
#include <unibo_rtai_tracer.h>

#include "calibration.h"
#include "realtime_task.h"
#include "aperiodic_model.h"


#define NON_PERIODIC_TIMER 0
#define TASKLET_PRIORITY   0

#define TASK_SUSPEND_CONDITION rt_task_suspend(&task->rtai_task)


static struct rt_tasklet_struct aperiodic_request_orchestrator;


void one_shot_handler(unsigned long data);


//SECTION: Aperiodic requests



void bind_aperiodic_request_to_realtime_task(realtime_task *task){

	int i;
	for(i = 0; i < MAX_REQUESTS; i++)
		aperiodic_request_list[i].task = task;
	
//SECTION: Bind aperiodic request code

}

void rt_aperiodic_task_shell_fixed_prio(long int task_index){

	realtime_task *task = get_task_by_index(task_index);

	int current_request = 0;
	int result = 0;



//	unibo_trace_event(JOB_RELEASE, &task->rtai_task, count2nano(next_period()), NULL);

	while(task->is_active){

		TASK_SUSPEND_CONDITION;

		/*
		 * Overrun condition check
		 */
		printk("UNIBO_TRACER(traced_event)@JOB_STARTED %s %lld\n", (char*)&task->name, CURRENT_TIME_NS);
		if(result == RTE_TMROVRN)
			printk("JOB_OVERRUN_DETECTED for %s\n",(char*)&task->name);

		/*
		 * Realtime Task code
		 */
		 aperiodic_request_list[current_request].request_code((void*) task);

		/*
		 * End execution check
		 */
		current_request++;
		if(current_request == MAX_REQUESTS)
			task->is_active = false;


//		unibo_trace_event(JOB_COMPLETED, &task->rtai_task, rt_get_time_ns(), NULL);
		printk("UNIBO_TRACER(traced_event)@JOB_COMPLETED %s %lld\n", (char*)&task->name, CURRENT_TIME_NS);

	}
}


rt_aperiodic_request* get_next_aperiodic_request(void){

	static int current_request_index = 0;

	if(current_request_index < MAX_REQUESTS)
		return &aperiodic_request_list[current_request_index++];

	return NULL;
}

realtime_task* create_aperiodic_task
(
		uint16_t index,
		const char name[],
		int prio
)
{

	realtime_task *task = get_task_by_index(index);

	strcpy(task->name, name);

	task->task_index   = index;
	task->is_active	   = true;


	if(prio != PRIORITY_NOT_DEFINED)
		task->priority	= prio;


	printk("UNIBO_TRACER(task_info)@%s %llu %llu %llu\n"
					,task->name
					,(unsigned long long)ms_to_ns(60)
					,task->deadline_ns
					,task->offset_ns);


#ifdef  REGISTER_TASK_IN_CREATE_TASK
	unibo_tracer_register_task(&task->rtai_task, task->name);
#endif

	rt_register(index,(void*) &task->rtai_task,0,NULL);

	rt_task_init_cpuid(&task->rtai_task,rt_aperiodic_task_shell_fixed_prio,(long int)index,DEFAULT_STACK_SIZE,task->priority,0,0,CPU_0);
	rt_task_resume(&task->rtai_task);
	rt_insert_timer(&aperiodic_request_orchestrator, TASKLET_PRIORITY, nano2count(aperiodic_request_list[0].request_arrival_time + get_start_time_ns() ), NON_PERIODIC_TIMER, one_shot_handler, (unsigned long) get_next_aperiodic_request(), 0);

	return task;
}


void one_shot_handler(unsigned long data){


	rt_aperiodic_request *aperiodic_rq_ptr = (rt_aperiodic_request*) data;
	rt_aperiodic_request *next_request_ptr = get_next_aperiodic_request();
	RTIME trigger_time;

	realtime_task *aperiodic_task = aperiodic_rq_ptr->task;

	if(next_request_ptr != NULL){
		trigger_time = next_request_ptr->request_arrival_time + get_start_time_ns();
		rt_insert_timer(&aperiodic_request_orchestrator, TASKLET_PRIORITY, nano2count(trigger_time), NON_PERIODIC_TIMER, one_shot_handler, (unsigned long) next_request_ptr, 0);
	}

	rt_task_resume(&aperiodic_task->rtai_task);
	unibo_trace_event(APERIODIC_REQUEST_FIRED, &aperiodic_task->rtai_task, rt_get_time_ns(), NULL);
}


