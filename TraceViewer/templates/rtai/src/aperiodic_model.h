/*
 * aperiodic_model.h
 *
 *  Created on: May 31, 2013
 *      Author: gr4ce
 */

#ifndef APERIODIC_MODEL_H_
#define APERIODIC_MODEL_H_

#include "realtime_task.h"

rt_aperiodic_request* get_next_aperiodic_request(void);

void  aperiodic_task_shell(long int task_parameters);

void bind_aperiodic_request_to_realtime_task(realtime_task *task);

realtime_task* create_aperiodic_task(uint16_t, const char name[], int);

#endif /* APERIODIC_MODEL_H_ */
