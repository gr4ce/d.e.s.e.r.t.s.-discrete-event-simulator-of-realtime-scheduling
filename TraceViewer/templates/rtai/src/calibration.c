/**
 *     Project: SharedResources
 *
 *        File: calibration.c
 *
 *  Created on: 10/apr/2013
 *
 *      Author: Giuseppe Salvatore
 *
 * Description: Calibration capabilities
 *
 */


/**
 * Standard module inclusions
 */
#include <linux/module.h>
#include <linux/version.h>
#include <linux/param.h>
#include <linux/sched.h>
#include <linux/errno.h>


/**
 * Machine dependant inclusions
 */
#include <asm/io.h>
#include <asm/rtai.h>


/**
 * Project dependant inclusions
 */
#include <rtai.h>
#include <rtai_sched.h>
#include <rtai_shm.h>
#include <rtai_sem.h>
#include <rtai_msg.h>



/**
 * All the local path inclusions
 */
#include "calibration.h"
#include "global_defs.h"


#define STEP_SIZE	     1

#define MAX_ERROR_NS         100
#define ERROR_THRESHOLD      MAX_ERROR_NS


#define MAX_CALIB_ITERATION  500
#define CONSECUTIVE_SUCCESS  10

#define HARD_DISABLE_INTERRUPT

#ifdef  HARD_DISABLE_INTERRUPT

#define DISABLE_INTERRUPTS   	__asm__ __volatile__("cli;");
#define ENABLE_INTERRUPTS	__asm__ __volatile__("sti;");

#else

#define DISABLE_INTERRUPTS  local_bh_disable()
#define ENABLE_INTERRUPTS	local_bh_enable()

#endif


#define ABS(x)  ((x > 0) ? x : -x)

#define CPUID_NO_INPUT_IA64 	__asm__ __volatile__(   "cpuid;"			\
						:/*no output*/			\
						:/*no input*/			\
						:"%rax","%rbx","%rcx","%rdx" 	\
						)

#define CPUID_NO_INPUT_IA32 	__asm__ __volatile__(   "cpuid;"			\
						:/*no output*/			\
						:/*no input*/			\
						:"%eax","%ebx","%ecx","%edx" 	\
						)

#define NOOP __asm__ __volatile__("nop;")


//#define LOOP_INSTRUCTION     rdtsc_barrier()
//#define LOOP_INSTRUCTION     NOOP
#define LOOP_INSTRUCTION       CPUID_NO_INPUT_IA32



static unsigned long loop_size_ms = EXECUTION_LOOP_SIZE_MS;
static unsigned long loop_size_us = EXECUTION_LOOP_SIZE_US;
static unsigned long loop_size_ns = EXECUTION_LOOP_SIZE_NS;


static void calibrate_busy_wait_ms(RTIME target_ms);
static void calibrate_busy_wait_us(RTIME target_us);
static void calibrate_busy_wait_ns(RTIME target_ns);


inline void busy_wait_ms(RTIME millisec){

	volatile unsigned long n_cycle = loop_size_ms * millisec;

	for(; n_cycle > 0; n_cycle--)
	   LOOP_INSTRUCTION;

}

inline void busy_wait_us(RTIME microsec){

	volatile unsigned long n_cycle = loop_size_us * microsec/1000.0F;

	for(; n_cycle > 0; n_cycle--)
	   LOOP_INSTRUCTION;

}

inline void busy_wait_ns(RTIME nanosec){


	volatile unsigned long n_cycle = loop_size_ns * nanosec/1000000.0F;

	for(; n_cycle > 0; n_cycle--)
	   LOOP_INSTRUCTION;

}




void calibrate_loop(void){

	PRINT("[%s] ---------- Startig calibration routine ----------\n",MODULE_NAME);

	calibrate_busy_wait_ms(1);
	calibrate_busy_wait_us(400);
	calibrate_busy_wait_ns(900000);

}


static void calibrate_busy_wait_ms(RTIME target_ms){

	unsigned long long start, stop;
	long long int drift,duration;
	int iter = 0, consecutive_success = 0;

	start = SAMPLE_TIME();
	busy_wait_ms(target_ms);
	stop  = SAMPLE_TIME();
	duration = stop - start;

	do{

	  DISABLE_INTERRUPTS;

	  start = SAMPLE_TIME();
	  busy_wait_ms(target_ms);
	  stop  = SAMPLE_TIME();

	  ENABLE_INTERRUPTS;

	  duration = stop - start;
      drift = duration - target_ms * 1000000;

	  busy_wait_ms(10);

	  //prints output if the value is near the desired one

	  if( ABS(drift) < (ERROR_THRESHOLD * 10) ){
		  PRINT("[%s] - Requested %3lu ms... got %7llu ns",MODULE_NAME,target_ms,duration);
		  PRINT(" --> drift = %5lld ns  --> loop_size_ms = %lu ",drift,loop_size_ms);
	  }

	  //scaling the loop_size
	  if(ABS(drift) < ERROR_THRESHOLD){
		  consecutive_success++;
		  PRINT(" SUCCESS\n");
	  }
	  else{
		  if(drift > 0)
			  loop_size_ms-=STEP_SIZE;
		  else
			  loop_size_ms+=STEP_SIZE;

		  consecutive_success = 0;
		  if( ABS(drift) < (ERROR_THRESHOLD * 10) )
			  PRINT("\n");
	  }

	  iter++;

	}while( consecutive_success < CONSECUTIVE_SUCCESS && iter <= MAX_CALIB_ITERATION);


	if(consecutive_success == CONSECUTIVE_SUCCESS){
	  PRINT("[%s] - Calibration routine succeded \n", MODULE_NAME);
	  PRINT("[%s] - Use %lu for #define EXECUTION_LOOP_SIZE_MS in calibration.h \n",MODULE_NAME,loop_size_ms);
	}
	else{
	  PRINT("[%s] - Can't find suitable value for loop_size_ms ",MODULE_NAME);
	  PRINT("...try to expand error threshold \n");
	}


}

static void calibrate_busy_wait_us(RTIME target_us){

	unsigned long long start, stop;
	long long int drift,duration;
	int iter = 0, consecutive_success = 0;

	start = SAMPLE_TIME();
	busy_wait_us(target_us);
	stop  = SAMPLE_TIME();
	duration = stop - start;

	do{

	  DISABLE_INTERRUPTS;

	  start = SAMPLE_TIME();
	  busy_wait_us(target_us);
	  stop  = SAMPLE_TIME();

	  ENABLE_INTERRUPTS;

	  duration = stop - start;
      drift = duration - target_us * 1000;

	  busy_wait_ms(10);

	  //prints output if the value is near the desired one

	  if( ABS(drift) < (ERROR_THRESHOLD * 100) ){
		PRINT("[%s] - Requested %6lu us... got %7llu ns",MODULE_NAME,target_us,duration);
		PRINT(" --> drift = %5lld ns  --> loop_size_us = %lu ",drift,loop_size_us);
	  }


	  //scaling the loop_size
	  if(ABS(drift) < ERROR_THRESHOLD){
		consecutive_success++;
		PRINT(" SUCCESS\n");
	  }
	  else{
		if(drift > 0)
		  loop_size_us-=STEP_SIZE;
		else
		  loop_size_us+=STEP_SIZE;

		consecutive_success = 0;
		if( ABS(drift) < (ERROR_THRESHOLD * 100) )
		  PRINT("\n");
	  }

	  iter++;

	}while( consecutive_success < CONSECUTIVE_SUCCESS && iter <= MAX_CALIB_ITERATION);


	if(consecutive_success == CONSECUTIVE_SUCCESS){
	  PRINT("[%s] - Calibration routine succeded \n", MODULE_NAME);
	  PRINT("[%s] - Use %lu for #define EXECUTION_LOOP_SIZE_US in calibration.h \n",MODULE_NAME,loop_size_us);
	}
	else{
	  PRINT("[%s] - Can't find suitable value for loop_size_us ",MODULE_NAME);
	  PRINT("...try to expand error threshold \n");
	}
	
}

static void calibrate_busy_wait_ns(RTIME target_ns){

	unsigned long long start, stop,duration;
	long long int drift;
	int iter = 0, consecutive_success = 0;

	start = SAMPLE_TIME();
	busy_wait_ns(target_ns);
	stop  = SAMPLE_TIME();
	duration = stop - start;

	do{

	  DISABLE_INTERRUPTS;

	  start = SAMPLE_TIME();
	  busy_wait_ns(target_ns);
	  stop  = SAMPLE_TIME();

	  ENABLE_INTERRUPTS;

	  duration = stop - start;
      drift = duration - target_ns;

	  busy_wait_ms(10);

	  //prints output if the value is near the desired one

	  if( ABS(drift) < (ERROR_THRESHOLD * 100) ){
		PRINT("[%s] - Requested %10lu ns... got %7llu ns",MODULE_NAME,target_ns,duration);
		PRINT(" --> drift = %5lld ns  --> loop_size_us = %lu ",drift,loop_size_ns);
	  }


	  //scaling the loop_size
	  if(ABS(drift) < ERROR_THRESHOLD){
		consecutive_success++;
		PRINT(" SUCCESS\n");
	  }
	  else{
		if(drift > 0)
		  loop_size_ns-=STEP_SIZE;
		else
		  loop_size_ns+=STEP_SIZE;

		consecutive_success = 0;
		if( ABS(drift) < (ERROR_THRESHOLD * 100) )
		  PRINT("\n");
	  }

	  iter++;

	}while( consecutive_success < CONSECUTIVE_SUCCESS && iter <= MAX_CALIB_ITERATION);


	if(consecutive_success == CONSECUTIVE_SUCCESS){
	  PRINT("[%s] - Calibration routine succeded \n", MODULE_NAME);
	  PRINT("[%s] - Use %lu for #define EXECUTION_LOOP_SIZE_NS in calibration.h \n",MODULE_NAME,loop_size_ns);
	}
	else{
	  PRINT("[%s] - Can't find suitable value for loop_size_ns ",MODULE_NAME);
	  PRINT("...try to expand error threshold \n");
	}
}







